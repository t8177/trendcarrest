from django.contrib import admin

from core.models import AdministrativeDeposit, AdministrativeWithdraw, Balance, Currency

admin.site.register([AdministrativeWithdraw,AdministrativeDeposit])

@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('ratio','date',)

@admin.register(Balance)
class BalanceAdmin(admin.ModelAdmin):
    list_display = ('type','currency','amount',)