from django.db import models
from django.utils import timezone

from account.models import CustomMixin


class Balance(models.Model):

    CURRENCY_CHOICES = (
        ('usd', 'USD'),
        ('uah', 'UAH'),
    )

    TYPE_CHOICES = (
        ('cash','Cash'),
        ('bank','Bank'),
    )

    amount = models.FloatField(default=0)
    currency = models.CharField(max_length=3,choices=CURRENCY_CHOICES,default='uah')
    type = models.CharField(max_length=4,choices=TYPE_CHOICES,default='cash')

    class Meta:
        unique_together = ('currency', 'type')

    def __str__(self):
        return f'{self.currency.upper()} {self.type} Balance'

class AdministrativeWithdraw(CustomMixin):
    
        CURRENCY_CHOICES = (
            ('usd', 'USD'),
            ('uah', 'UAH'),
        )
    
        amount = models.FloatField(default=0)
        currency = models.CharField(max_length=3,choices=CURRENCY_CHOICES,default='uah')
        note = models.CharField(max_length=255,blank=True,null=True)
        date = models.DateField(default=timezone.now)


        def __str__(self):
            return f'{self.amount} {self.currency.upper()} Administrative Withdraw'


class AdministrativeDeposit(CustomMixin):
    
        CURRENCY_CHOICES = (
            ('usd', 'USD'),
            ('uah', 'UAH'),
        )
    
        amount = models.FloatField(default=0)
        currency = models.CharField(max_length=3,choices=CURRENCY_CHOICES,default='uah')
        note = models.CharField(max_length=255,blank=True,null=True)
        date = models.DateField(default=timezone.now)
    
        def __str__(self):
            return f' {self.amount} {self.currency.upper()} Administrative Deposit'


class Currency(models.Model):
    ratio = models.FloatField(default=1)
    date = models.DateField(default=timezone.now)

    class Meta:
        verbose_name = 'Currency'
        verbose_name_plural = 'Currencies'
        ordering = ['date','id']
    
    def __str__(self):
        return str(self.ratio)