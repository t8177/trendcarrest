from django.http.response import JsonResponse
from rest_framework import viewsets
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView

from core.models import AdministrativeDeposit, AdministrativeWithdraw, Currency
from core.api.serializers import (
    CurrencySerializer
)



class CreateCurrencyView(CreateAPIView):
    serializer_class = CurrencySerializer


class GetCurrencyView(APIView):

    def get(self, request, *args, **kwargs):
        query_date = request.GET.get('date')
        if query_date:
            c = Currency.objects.filter(date__lte=query_date).last()
        else:
            c = Currency.objects.last()
        
        serializer = CurrencySerializer(c)
        return JsonResponse(serializer.data)