from django.urls import path
from core.api import views as api_views

app_name = 'core'
urlpatterns = [
    path('currency/',api_views.CreateCurrencyView.as_view(),name='currency-create'),
    path('get-currency/',api_views.GetCurrencyView.as_view(),name='currency-get'),
]