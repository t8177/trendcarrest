from django.contrib import admin
from car.models import (
    Brand,
    Car,
    CarClass,
    CarImageTask,
    CarPaper,
    Case,
    Color,
    Fuel,
    CarImage,
    Office,
    Model,
)


admin.site.register([CarImage,CarClass,Office,Case,Fuel,Color,Model,CarImageTask,Brand])

#### INLINES ###

class ImageInline(admin.TabularInline):
    model = CarImage
    extra = 1

class PaperInline(admin.TabularInline):
    model = CarPaper
    extra = 1

#### INLINES ###


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    inlines = [ImageInline,PaperInline]
    list_per_page=10