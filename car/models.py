from django.db import models
from django.contrib.auth import get_user_model
from account.models import CustomMixin

User = get_user_model()

class Brand(models.Model):
    name = models.CharField(max_length=50)
    logo = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.name

class Model(models.Model):
    name = models.CharField(max_length=50)
    brand = models.ForeignKey(Brand, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.name

class Fuel(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
        
class Case(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
        
class CarClass(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Color(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
        
class Office(models.Model):
    name = models.CharField(max_length=50)
    address = models.TextField()

    def __str__(self):
        return self.name


class Car(CustomMixin):
    HEALTH_STATUS = (
        ('G', 'Good'),
        ('D', "DTP"),
        ('O', "Oil"),
        ('B', "Brake"),
        ('T', "Tyre"),
    )
    STATUS = (
        ('F', 'Free'),
        ('C', "Credit"),
        ('R', "Rent"),
        ('RTP', "Rent Taxi Percent"),
        ('RTD', "Rent Taxi Daily"),
        ('RTW', "Rent Taxi Week"),
    )
    TRANSMISSIONS = (
        ('A', 'Atuo'),
        ('M', "Manual"),
    )

    TYRES = (
        ('S', 'Summer'),
        ('W', "Winter"),
    )

    model = models.ForeignKey(Model, on_delete=models.CASCADE, related_name='cars')
    office = models.ForeignKey(Office, on_delete=models.CASCADE, default='1', related_name='cars')
    number = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin_code = models.IntegerField(null=True, blank=True)
    is_available = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    case = models.ForeignKey(Case, on_delete=models.SET_NULL, null=True, blank=True)
    health_status = models.CharField(max_length=1, choices=HEALTH_STATUS, default='G')
    status = models.CharField(max_length=3, choices=STATUS, default='F')
    driver = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name="cars")
    car_class = models.ForeignKey(CarClass, on_delete=models.SET_NULL, null=True, blank=True)
    color = models.ForeignKey(Color, on_delete=models.SET_NULL, null=True, blank=True)
    transmission = models.CharField(max_length=1, choices=TRANSMISSIONS, default='A')
    fuel = models.ForeignKey(Fuel, on_delete=models.SET_NULL, null=True, blank=True)
    fuel_tank = models.PositiveSmallIntegerField(null=True, blank=True)
    doors = models.PositiveSmallIntegerField(null=True, blank=True)
    seats = models.PositiveSmallIntegerField(null=True, blank=True)
    wheel_size = models.PositiveSmallIntegerField(null=True, blank=True)
    current_tyre = models.CharField(max_length=1, choices=TYRES,default='S')
    bought_date = models.DateField(null=True, blank=True)
    bought_price = models.FloatField(null=True, blank=True)
    current_km = models.FloatField(null=True, blank=True)
    oil_km_limit = models.PositiveSmallIntegerField(null=True, blank=True)
    climate = models.BooleanField()
    climate_control = models.BooleanField()
    is_verified = models.BooleanField()
    reserve_key = models.BooleanField(default=False)
    advertise = models.ForeignKey('taxi.Company', on_delete=models.SET_NULL, null=True, blank=True, related_name='cars')

    def __str__(self):
        return f"{self.model.name} - {self.year} - {self.number} - {self.status} "
        
class CarImage(CustomMixin):
    car = models.ForeignKey(Car, on_delete=models.CASCADE, related_name='images')
    path = models.ImageField(upload_to="cars/")
    is_main = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.car.model.name} - {self.car.year} - {self.car.number} "



class CarImageTask(CustomMixin):
    car = models.ForeignKey(Car, on_delete=models.SET_NULL, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    images = models.ManyToManyField(CarImage, blank=True)


class CarPaper(CustomMixin):
    car = models.ForeignKey(Car, on_delete=models.CASCADE, related_name='papers')
    path = models.ImageField(upload_to="cars/papers/")

    def __str__(self):
        return f"{self.car.model.name} - {self.car.year} - {self.car.number} "
