# Generated by Django 4.0.2 on 2022-02-18 18:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('car', '0001_initial'),
        ('taxi', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='advertise',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='cars', to='taxi.company'),
        ),
        migrations.AddField(
            model_name='car',
            name='car_class',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='car.carclass'),
        ),
        migrations.AddField(
            model_name='car',
            name='case',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='car.case'),
        ),
        migrations.AddField(
            model_name='car',
            name='color',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='car.color'),
        ),
        migrations.AddField(
            model_name='car',
            name='fuel',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='car.fuel'),
        ),
        migrations.AddField(
            model_name='car',
            name='model',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='car.model'),
        ),
        migrations.AddField(
            model_name='car',
            name='office',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='cars', to='car.office'),
        ),
    ]
