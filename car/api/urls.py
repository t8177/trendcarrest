from django.urls import path
from django.urls.conf import include
from car.api import views as api_views
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers

router = DefaultRouter()
router.register('cars',api_views.CarViewSet,basename='car')
# router.register('images',api_views.CarImageViewSet,basename='image')
router.register('models',api_views.CarModelViewSet,basename='model')
router.register('fuels',api_views.FuelViewSet,basename='fuel')
router.register('cases',api_views.CaseViewSet,basename='case')
router.register('class',api_views.CarClassViewSet,basename='class')
router.register('colors',api_views.ColorViewSet,basename='color')

image_router = routers.NestedSimpleRouter(router, 'cars', lookup='car')
image_router.register('images',api_views.CarImageViewSet,basename='image')
image_router.register('papers',api_views.CarPaperViewSet,basename='paper')

 
app_name = 'cars'

urlpatterns = [
    # path('', api_views.CarListCreateAPIView.as_view(), name='car-list'),
    # path('<int:pk>/', api_views.CarAPIView.as_view(), name='car-detail'),
    path('',include(router.urls)),
    path('',include(image_router.urls)),
    path('list/basic/',api_views.CarBasicListAPIView.as_view(),name='car-basic-list'),
    path('list/',api_views.CarListAPIView.as_view(),name='car-list'),
    path('<int:pk>/',api_views.CarDetailAPIView.as_view(),name='car-detail'),
    path('<int:pk>/delete/',api_views.CarDeleteAPIView.as_view(),name='car-delete'),
    path('<int:pk>/update/',api_views.CarUpdateAPIView.as_view(),name='car-update'),
    path('create/',api_views.CarCreateAPIView.as_view(),name='car-create'),
]
