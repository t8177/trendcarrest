from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from rest_framework import status, viewsets
from rest_framework.views import APIView
from car.api.serializers import BasicCarListSerializer, CarListSerializer, CarDetailSerializer,CarUpdateSerializer, CarClassSerializer, CarFrontSerializer, CarImageSerializer, CarImageTaskSerializer,\
      CarCreateSerializer, CaseSerializer, ColorSerializer, FuelSerializer, ModelSerializer, OfficeSerializer, CarPaperSerializer, \
         CarCompactListSerializer
from account.api.permissions import CustomDjangoModelPermissions
from car.models import Car, CarClass, CarImage, CarImageTask, CarPaper, Case, Color, Fuel, Model, Office
from car.filters import CarFilter

 

# class CarListCreateAPIView(ListCreateAPIView):
#     queryset = Car.objects.all().order_by('-id')
#     filter_backends = [DjangoFilterBackend]
#     filterset_class = CarFilter
#     permission_classes = [CustomDjangoModelPermissions]

#     def get_serializer_class(self):
#         if self.request.method in SAFE_METHODS:
#             return CarListSerializer
#         else:
#             return CarSerializer



# class CarAPIView(RetrieveUpdateDestroyAPIView):
#     queryset = Car.objects.all()
#     serializer_class = CarSerializer
#     permission_classes = [CustomDjangoModelPermissions]

#     def get_serializer_class(self):
#         if self.request.method in SAFE_METHODS:
#             return CarListSerializer
#         else:
#             return CarSerializer

class CarViewSet(viewsets.ModelViewSet):
    queryset = Car.objects.all().order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filterset_class = CarFilter
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.request.method in SAFE_METHODS:
            if self.action == 'list':
                return CarCompactListSerializer
            return CarListSerializer
        else:
            return CarCreateSerializer


    

class CarImageViewSet(viewsets.ModelViewSet):
    queryset = CarImage.objects.all()
    serializer_class = CarImageSerializer

    def get_queryset(self):
        return CarImage.objects.filter(car=self.kwargs['car_pk'])
    
    def perform_create(self, serializer):
        serializer.save(car_id=self.kwargs['car_pk'])


class CarPaperViewSet(viewsets.ModelViewSet):
    queryset = CarPaper.objects.all()
    serializer_class = CarPaperSerializer

    def get_queryset(self):
        return CarPaper.objects.filter(car=self.kwargs['car_pk'])
    
    def perform_create(self, serializer):
        serializer.save(car_id=self.kwargs['car_pk'])



class CarModelViewSet(viewsets.ModelViewSet):
    queryset = Model.objects.all()
    serializer_class = ModelSerializer


class FuelViewSet(viewsets.ModelViewSet):
    queryset = Fuel.objects.all()
    serializer_class = FuelSerializer

class CaseViewSet(viewsets.ModelViewSet):
    queryset = Case.objects.all()
    serializer_class = CaseSerializer

class CarClassViewSet(viewsets.ModelViewSet):
    queryset = CarClass.objects.all()
    serializer_class = CarClassSerializer

class ColorViewSet(viewsets.ModelViewSet):
    queryset = Color.objects.all()
    serializer_class = ColorSerializer

class OfficeViewSet(viewsets.ModelViewSet):
    queryset = Office.objects.all()
    serializer_class = OfficeSerializer


 
 
    


# New API Views 

class CarBasicListAPIView(ListAPIView):
    queryset = Car.objects.all()
    serializer_class = BasicCarListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


class CarListAPIView(ListAPIView):
    queryset = Car.objects.all().order_by('-id')
    serializer_class = CarListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class CarDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Car.objects.all()
    serializer_class = CarDetailSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class CarUpdateAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Car.objects.all()
    serializer_class = CarUpdateSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class CarDeleteAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Car.objects.all()
    serializer_class = CarDetailSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class CarCreateAPIView(ListCreateAPIView):
    queryset = Car.objects.all()
    serializer_class = CarCreateSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
