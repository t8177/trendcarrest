import datetime
from car.models import CarImageTask, CarPaper, Model, Fuel, Case, CarClass, Color, Office, Car, CarImage, Brand
from rest_framework import serializers
from account.api.serializers import UserFullDetailSerializer, CustomerBasicSerializer
from credit.models import Credit, CreditPayment


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['id','name']

class ModelSerializer(serializers.ModelSerializer):
    brand = BrandSerializer()
    class Meta:
        model = Model
        fields = ['id','name','brand']


class FuelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fuel
        fields = ['id','name']


class CaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Case
        fields = ['id','name']


class CarClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarClass
        fields = ['id','name']


class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = ['id','name']


class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ['id','name']

class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ['id','name']

class CarImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarImage
        fields = ['id','path']
        read_only_fields = ['car']

class CarPaperSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarPaper
        fields = ['id','path']
        read_only_fields = ['car']




 

class CarForContractSerializer(serializers.ModelSerializer):
    images = CarImageSerializer(many=True)
    papers = CarPaperSerializer(many=True)
    model = ModelSerializer()
    case = CaseSerializer()
    color = ColorSerializer()
    advertise = serializers.SerializerMethodField()
    brand = serializers.SerializerMethodField()



    def get_advertise(self,car,*args,**kwargs):
        advertisement =  car.advertise
        if advertisement:
            data = {
                'id':advertisement.id,
                'name':advertisement.name,
            }
            return data
        else:
            return None
    

    def get_brand(self,car,*args,**kwargs):
        if car.model.brand:
            return car.model.brand.name
        return None

    class Meta:
        model = Car
        fields = ('__all__')

class CarCompactListSerializer(serializers.ModelSerializer):
    images = CarImageSerializer(many=True)
    papers = CarPaperSerializer(many=True)
    model = ModelSerializer()
    office = OfficeSerializer()
    case = CaseSerializer()
    car_class = CarClassSerializer()
    color = ColorSerializer()
    fuel = FuelSerializer()
    advertise = serializers.SerializerMethodField()
    brand = serializers.SerializerMethodField()
    driver = CustomerBasicSerializer()



    def get_advertise(self,car,*args,**kwargs):
        advertisement =  car.advertise
        if advertisement:
            data = {
                'id':advertisement.id,
                'name':advertisement.name,
            }
            return data
        else:
            return None
    
    

    def get_brand(self,car,*args,**kwargs):
        if car.model.brand:
            return car.model.brand.name
        return None

    class Meta:
        model = Car
        fields = ('__all__')





class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = '__all__'

class CarFrontSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    main_image = serializers.SerializerMethodField()
    # model = serializers.CharField(source='model.name')
    # office = serializers.CharField(source='office.name')

    def get_user(self,car,*args,**kwargs):
        contract = car.contracts.filter(is_active=True).first()
        if contract:
            context = self.parent.context if self.parent else self.context
            # if self.parent:
            # u = UserFullDetailSerializer(contract.customer,context=context)
            # else:
            #     u = UserFullDetailSerializer(contract.customer)
            return contract.customer.get_full_name()
        else:
            return None

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.model:
            representation['model'] = instance.model.name
        if instance.office:
            representation['office'] = instance.office.name

        return representation

    def get_main_image(self,car,*args,**kwargs):
        main_image = car.images.filter(is_main=True).first()
        if main_image:
            return main_image.id
    class Meta:
        model = Car
        fields = ('__all__')
        

class CarImageTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = CarImageTask
        fields = ('__all__')

 


class CarCompactSerializer(serializers.ModelSerializer):
    model = serializers.CharField(source='model.name')
    office = serializers.SerializerMethodField()
    images = CarImageSerializer(many=True)

    def get_office(self,car,*args,**kwargs):
        if car.office:
            return car.office.name
        else:
            return None

    class Meta:
        model = Car
        fields = ('id','model','number','is_active','office','images')


# New Serializers

class BasicCarListSerializer(serializers.ModelSerializer):
    model = serializers.SerializerMethodField()

    def get_model(self,car,*args,**kwargs):
        if car.model:
            return car.model.name
        else:
            return None
    class Meta:
        model = Car
        fields = ('id','model','number')


class BasicCarSerializer(serializers.ModelSerializer):
    model = serializers.SerializerMethodField()
    images = CarImageSerializer(many=True)
    def get_model(self,car,*args,**kwargs):
        if car.model:
            return car.model.name
        else:
            return None
    class Meta:
        model = Car
        fields = ('id','model','number','year', 'images')

class CarListSerializer(serializers.ModelSerializer):
    images = CarImageSerializer(many=True)
    model = ModelSerializer()
    osago_end_date = serializers.SerializerMethodField()
    casco_end_date = serializers.SerializerMethodField()
    casco_next_payment = serializers.SerializerMethodField()
    casco_step = serializers.SerializerMethodField()
    office = OfficeSerializer()
    advertise = serializers.SerializerMethodField()
    driver = CustomerBasicSerializer()

     
    def get_osago_end_date(self,car,*args,**kwargs):
        osago = car.osagos.last()
        if osago and osago.is_active:
            return osago.end_date
        else:
            return None

    def get_casco_end_date(self,car,*args,**kwargs):
        casco = car.cascos.last()
        if casco and casco.is_active:
            return casco.end_date
        else:
            return None

    def get_casco_next_payment(self,car,*args,**kwargs):
        casco = car.cascos.last()
        if casco and casco.is_active:
            return casco.next_payment
        else:
            return None

    def get_casco_step(self,car,*args,**kwargs):
        casco = car.cascos.last()
        if casco and casco.is_active:
            return casco.step
        else:
            return None

    def get_advertise(self,car,*args,**kwargs):
        advertisement =  car.advertise
        if advertisement:
            data = {
                'id':advertisement.id,
                'name':advertisement.name,
            }
            return data
        else:
            return None
    
    class Meta:
        model = Car
        fields = ['id','images','model','osago_end_date','casco_end_date','casco_next_payment','casco_step','office','advertise','driver','number','year','is_available','is_active','status','health_status','current_tyre','is_verified','reserve_key',]


class CarDetailSerializer(serializers.ModelSerializer):
    images = CarImageSerializer(many=True)
    papers = CarPaperSerializer(many=True)
    model = ModelSerializer()
    office = OfficeSerializer()
    case = CaseSerializer()
    car_class = CarClassSerializer()
    color = ColorSerializer()
    fuel = FuelSerializer()
    advertise = serializers.SerializerMethodField()
    driver = CustomerBasicSerializer()

     
        
    def get_advertise(self,car,*args,**kwargs):
        advertisement =  car.advertise
        if advertisement:
            data = {
                'id':advertisement.id,
                'name':advertisement.name,
            }
            return data
        else:
            return None
    
    class Meta:
        model = Car
        fields = ('__all__')

class CarCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = '__all__'

class CarUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = '__all__'