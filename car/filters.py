import django_filters
from car.models import Car
from django_filters import rest_framework as filters

class CarFilter(django_filters.FilterSet):
    model = filters.CharFilter(field_name='model__name',lookup_expr='iexact')
    office = filters.CharFilter(field_name='office__name',lookup_expr='iexact')
    case = filters.CharFilter(field_name='case__name',lookup_expr='iexact')
    color = filters.CharFilter(field_name='color__name',lookup_expr='iexact')
    fuel = filters.CharFilter(field_name='fuel__name',lookup_expr='iexact')
    min_year = filters.NumberFilter(field_name="year", lookup_expr='gte')
    max_year = filters.NumberFilter(field_name="year", lookup_expr='lte')    # is_active = filters.CharFilter(field_name='number',lookup_expr='iexact')
    brand = filters.CharFilter(field_name='model__brand__name',lookup_expr='iexact')


    class Meta:
        model = Car
        fields = ['year','is_active','is_available','health_status','transmission',\
            'fuel_tank','doors','seats','wheel_size','current_tyre','bought_date','bought_price','climate','climate_control',\
                'is_verified']