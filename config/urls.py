from django.contrib import admin
from django.urls import path, include
from django.urls.conf import re_path
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Trendcar API",
      default_version='v1',
      description="Accounting tool for trendcar",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    path('api/car/', include('car.api.urls', namespace='car-api')),
    path('api/insurance/', include('insurance.api.urls', namespace='insurance-api')),
    path('api/dtp/', include('dtp.api.urls', namespace='dtp-api')),
    path('api/credit/', include('credit.api.urls', namespace='credit-api')),
    path('api/account/',include('account.api.urls',namespace='account-api')),
    path('api/core/',include('core.api.urls',namespace='core-api')),
    path('api/taxi/',include('taxi.api.urls',namespace='taxi-api')),
    path('api/rent/',include('rent.api.urls',namespace='rent-api')),
    path('api/renttaxi/',include('rent_taxi.api.urls',namespace='rent-taxi-api')),
    path('api/renttaxidaily/',include('rent_taxi_daily.api.urls',namespace='rent-taxi-daily-api')),
    path('api/shtraf/',include('shtraf.api.urls',namespace='shtraf-api')),
    path('api/debt/',include('debt.api.urls',namespace='debt-api')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
