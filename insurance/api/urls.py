from django.urls import path
from insurance.api import views as api_views


app_name = 'insurance'

urlpatterns = [
    path('casco/list/',api_views.CascoListAPIView.as_view(),name='casco-list'),
    path('osago/list/',api_views.OsagoListAPIView.as_view(),name='osago-list'),
]
