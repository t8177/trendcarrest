from rest_framework import serializers
from car.api.serializers import CarImageSerializer, ModelSerializer
from account.api.serializers import CustomerBasicSerializer
from insurance.models import Casco, CascoImage, Company
from car.models import Car

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class CascoListSerializer(serializers.ModelSerializer):
    images = CarImageSerializer(many=True)
    model = ModelSerializer()
    driver = CustomerBasicSerializer()
    casco_details = serializers.SerializerMethodField()

    def get_casco_details(self,car,*args,**kwargs):
        casco = car.cascos.last()
        data = {}
        if casco and casco.is_active:
            data['start_date'] = casco.start_date
            data['end_date'] = casco.end_date
            data['next_payment'] = casco.next_payment
            data['id'] = casco.id
            data['step'] = casco.step
            data['company'] = casco.company.name
            data['company_id'] = casco.company.id
            data['amount_year'] = casco.amount_year
            data['compensation'] = casco.compensation
            if casco and casco.is_active and casco.images.count() > 0:
                data['contract'] = casco.images.last().file.url
            else:
                data['contract'] = None
            return data
        else:
            return None

    
    class Meta:
        model = Car
        fields = ['id','images','model','casco_details','driver','number','year','is_available','is_active','status','is_verified',]



class OsagoListSerializer(serializers.ModelSerializer):
    images = CarImageSerializer(many=True)
    model = ModelSerializer()
    driver = CustomerBasicSerializer()
    osago_details = serializers.SerializerMethodField()

    def get_osago_details(self,car,*args,**kwargs):
        osago = car.osagos.last()
        data = {}
        if osago and osago.is_active:
            data['start_date'] = osago.start_date
            data['end_date'] = osago.end_date
            data['next_payment'] = osago.next_payment
            data['id'] = osago.id
            data['company'] = osago.company.name
            data['company_id'] = osago.company.id
            data['amount_year'] = osago.amount_year
            data['compensation'] = osago.compensation
            if osago and osago.is_active and osago.images.count() > 0:
                data['contract'] = osago.images.last().file.url
            else:
                data['contract'] = None
            return data
        else:
            return None

    
    class Meta:
        model = Car
        fields = ['id','images','model','osago_details','driver','number','year','is_available','is_active','status','is_verified',]