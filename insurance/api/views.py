import datetime
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from rest_framework import status, viewsets
from rest_framework.views import APIView
from insurance.api.serializers import CascoListSerializer, OsagoListSerializer
from insurance.models import Casco
from car.models import Car


class CascoListAPIView(ListAPIView):
    queryset = Car.objects.all().order_by('-id')
    serializer_class = CascoListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


class OsagoListAPIView(ListAPIView):
    queryset = Car.objects.all().order_by('-id')
    serializer_class = OsagoListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]