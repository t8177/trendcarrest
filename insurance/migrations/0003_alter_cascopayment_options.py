from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('insurance', '0002_cascopayment_step'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cascopayment',
            options={'ordering': ('date',)},
        ),
    ]
