from django.db.models.signals import  post_delete
from django.dispatch import receiver
from .models import *


# For Company
@receiver(post_delete, sender=CascoPayment)
def calculate_step_after_one_deleted(sender, instance, *args,**kwargs):
    try:
        next_obj = instance.casco.payments.filter(date__gt=instance.date).first()
        next_obj.save()
    except:
        instance.casco.step = instance.step - 1
        instance.casco.save()
