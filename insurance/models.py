from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from car.models import Car
from account.mixins import CustomMixin
from datetime import timedelta


# Insurance companies
class Company(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


# Casco
class Casco(models.Model):
    car = models.ForeignKey(Car,on_delete=models.CASCADE,related_name='cascos')
    company = models.ForeignKey(Company,on_delete=models.CASCADE,related_name='cascos')
    amount_year = models.FloatField(default=0)
    compensation = models.FloatField(default=0)
    step = models.PositiveSmallIntegerField(default=0,validators=[MinValueValidator(0), MaxValueValidator(4)])
    start_date = models.DateField()
    end_date = models.DateField()

    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    @property
    def amount_kv(self):
        if self.type == 'O':
            return None
        return round(self.amount_year/4,2)

    @property
    def next_payment(self):
        quarter_days ={
                '0':0,
                '1':92,
                '2':184,
                '3':276,
                '4':365
            }
        return self.start_date + timedelta(days=quarter_days[str(self.step)])

    def __str__(self):
        return f'{self.car.model.name} - {self.car.number}'



class CascoPayment(CustomMixin):
    casco = models.ForeignKey(Casco, on_delete=models.CASCADE, related_name='payments')
    date = models.DateField()
    amount = models.FloatField(default=0)
    step = models.PositiveIntegerField(default=1)
    is_verified = models.BooleanField(default=False)

    class Meta:
        ordering = ('date',)


    def __str__(self):
        return f'{self.casco.car.model.name} - {self.casco.car.year} - {self.date} - {self.step}'

    def save(self,*args,**kwargs):
        # total_debt and payment_left getting from previous record
        try:
            prev_obj = self.casco.payments.filter(date__lt=self.date).last()
            self.step = prev_obj.step + 1
        except:
            self.step = 1

        self.casco.step = self.step
        self.casco.save()

        super(CascoPayment,self).save(*args,**kwargs)
        # To trigger all next records
        try:
            next_obj = self.casco.payments.filter(date__gt=self.date).first()
            next_obj.save()
        except:
            pass
        


class CascoImage(models.Model):
    casco = models.ForeignKey(Casco, on_delete=models.CASCADE, related_name='images')
    file = models.FileField(upload_to='casco/')

    def __str__(self):
        return f'{self.casco.car.model.name} - {self.casco.car.number}'



# Osago
class Osago(models.Model):
    car = models.ForeignKey(Car,on_delete=models.CASCADE,related_name='osagos')
    company = models.ForeignKey(Company,on_delete=models.CASCADE,related_name='osagos')
    amount_year = models.FloatField(default=0)
    compensation = models.FloatField(default=0)
    start_date = models.DateField()
    end_date = models.DateField()

    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.car.model.name} - {self.car.number}'

    @property
    def next_payment(self):
        return self.end_date



class OsagoPayment(CustomMixin):
    osago = models.ForeignKey(Osago, on_delete=models.CASCADE, related_name='payments')
    date = models.DateField()
    amount = models.FloatField(default=0)
    is_verified = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.osago.car.model.name} - {self.osago.car.year} - {self.date}'


class OsagoImage(models.Model):
    osago = models.ForeignKey(Osago, on_delete=models.CASCADE, related_name='images')
    file = models.FileField(upload_to='osago/')

    def __str__(self):
        return f'{self.osago.car.model.name} - {self.osago.car.year}'
