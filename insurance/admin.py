from django.contrib import admin
from .models import *

admin.site.register(Company)
admin.site.register(CascoPayment)

class CascoImageInline(admin.TabularInline):
    model = CascoImage
    extra = 1

class CascoPaymentInline(admin.TabularInline):
    model = CascoPayment
    extra = 1

@admin.register(Casco)
class CascoAdmin(admin.ModelAdmin):
    inlines = [CascoImageInline, CascoPaymentInline]


class OsagoImageInline(admin.TabularInline): 
    model = OsagoImage
    extra = 1

class OsagoPaymentInline(admin.TabularInline):
    model = OsagoPayment
    extra = 1

@admin.register(Osago)
class OsagoAdmin(admin.ModelAdmin):
    inlines = [OsagoImageInline, OsagoPaymentInline]