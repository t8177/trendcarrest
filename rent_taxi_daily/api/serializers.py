from django.db import models
from django.db.models import fields
from rent_taxi_daily.models import *
from rest_framework import serializers
from account.api.serializers import ContractSerializer
from car.api.serializers import CarSerializer, BasicCarSerializer
from datetime import date, timedelta
from django.utils import timezone
from django.db.models import Q
from django.db.models import Count, Sum, F
  
class RentTaxiDailyListSerializer(serializers.ModelSerializer):
    car = BasicCarSerializer()
    last_driver = serializers.SerializerMethodField()
    lastweek_count = serializers.SerializerMethodField()
    lastweek_total = serializers.SerializerMethodField()
    thisweek_count = serializers.SerializerMethodField()
    thisweek_total = serializers.SerializerMethodField()
    debt = serializers.SerializerMethodField()
    class Meta:
        model = RentTaxiDaily
        fields = ['id','is_active','car','last_driver','lastweek_count','lastweek_total','thisweek_count','thisweek_total','debt']
    
    def get_last_driver(self, obj):
        last_revenue = obj.revenues.last()
        if last_revenue:
            contract = last_revenue.contract
            data = {
                "id":contract.customer.id,
                "first_name":contract.customer.first_name,
                "last_name":contract.customer.last_name
            }
            if contract.customer.images.last():
                data[ "image"] = contract.customer.images.last().path.url
            if contract.customer.phones.last():
                data[ "phone"] = contract.customer.phones.last().number
             
            return data
        else:
            return None

    def get_lastweek_count(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_last_week,date__lt=monday_of_this_week, rent_taxi_daily=obj)
        total = 0
        for i in revenues:
            items = RevenueItem.objects.filter(revenue=i)
            for x in items:
                total +=x.order_count
        return total

    def get_lastweek_total(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_last_week,date__lt=monday_of_this_week, rent_taxi_daily=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                total +=x.total_cash
                total +=x.total_card
        return total

    def get_thisweek_count(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_this_week, rent_taxi_daily=obj)
        total = 0
        for i in revenues:
            items = RevenueItem.objects.filter(revenue=i)
            for x in items:
                total +=x.order_count
        return total

    def get_thisweek_total(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_this_week, rent_taxi_daily=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                total +=x.total_cash
                total +=x.total_card
        return total


    def get_debt(self, obj):
        last_revenue = obj.revenues.last()
        total = 0
        items = last_revenue.items.all()
        for x in items:
            total +=x.debt_all   
        return total 
    
    
class RentTaxiDailyRevenueItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = RevenueItem
        fields = '__all__'

class RentTaxiDailyRevenueSerializer(serializers.ModelSerializer):
    rentitems = RentTaxiDailyRevenueItemSerializer(many=True)
    class Meta:
        model = Revenue
        fields = ['id','date','contract','driver','rent_taxi_daily','rentitems']



class RentTaxiDailyRetrieveSerializer(serializers.ModelSerializer):
    car = CarSerializer()
    rentrevenues = RentTaxiDailyRevenueSerializer(many=True)
    class Meta:
        model = RentTaxiDaily
        fields = ['id','is_active','car','rentrevenues']