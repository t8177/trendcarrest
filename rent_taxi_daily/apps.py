from django.apps import AppConfig


class RentTaxiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rent_taxi_daily'

    def ready(self):
        import rent_taxi_daily.signals