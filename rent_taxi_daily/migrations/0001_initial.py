# Generated by Django 4.0.3 on 2022-03-13 22:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('car', '0005_car_driver'),
        ('account', '0004_contract_deposit'),
        ('taxi', '0013_alter_revenueitem_options'),
    ]

    operations = [
        migrations.CreateModel(
            name='RentTaxi',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('car', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='renttaxisdaily', to='car.car')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Revenue',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('date', models.DateField()),
                ('contract', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rentdailyrevenues', to='account.contract')),
                ('driver', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rentdailyrevenues', to=settings.AUTH_USER_MODEL)),
                ('rent_taxi', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rentrevenues', to='rent_taxi_daily.renttaxi')),
            ],
            options={
                'ordering': ('date',),
            },
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('commission_from_driver', models.FloatField(default=0)),
                ('daily_rent', models.FloatField(default=4500)),
            ],
        ),
        migrations.CreateModel(
            name='RevenueItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_count', models.PositiveSmallIntegerField(default=0)),
                ('company_will_pay_brand', models.FloatField(default=0)),
                ('amount_cash', models.FloatField(default=0)),
                ('amount_card', models.FloatField(default=0)),
                ('company_commission', models.FloatField(default=0)),
                ('rent_amount', models.FloatField(default=0)),
                ('commission_from_driver', models.FloatField(default=0)),
                ('debt_to_bolt', models.FloatField(default=0)),
                ('company_will_pay_total', models.FloatField(default=0)),
                ('driver_must_pay', models.FloatField(default=0)),
                ('we_must_pay', models.FloatField(default=0)),
                ('driver_paid', models.FloatField(default=0)),
                ('we_paid', models.FloatField(default=0)),
                ('debt_today', models.FloatField(default=0)),
                ('debt_all', models.FloatField(default=0)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rentdailyrevenues', to='taxi.company')),
                ('rent_taxi', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='renttaxirevenues', to='rent_taxi_daily.renttaxi')),
                ('revenue', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rentitems', to='rent_taxi_daily.revenue')),
            ],
            options={
                'ordering': ('revenue__date',),
            },
        ),
    ]
