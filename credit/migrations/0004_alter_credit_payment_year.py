# Generated by Django 4.0.3 on 2022-03-03 21:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('credit', '0003_rename_total_credit_car_value_total_credit_expenses_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='credit',
            name='payment_year',
            field=models.FloatField(),
        ),
    ]
