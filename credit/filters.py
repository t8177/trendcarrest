import django_filters
from credit.models import Credit, CreditPayment
from django_filters import rest_framework as filters

class CreditFilter(django_filters.FilterSet):
    type = filters.CharFilter(field_name='type',lookup_expr='iexact')
    customer = filters.NumberFilter(field_name='contract__customer__id')
    # min_year = filters.NumberFilter(field_name="year", lookup_expr='gte')
    # max_year = filters.NumberFilter(field_name="year", lookup_expr='lte')   



    class Meta:
        model = Credit
        fields = ['is_permitted','is_completed','is_verified','type','payment_date']


class CreditPaymentFilter(django_filters.FilterSet):
    min_date = filters.CharFilter(field_name="date", lookup_expr='gte')
    max_date = filters.CharFilter(field_name="date", lookup_expr='lte')   
    min_amount = filters.NumberFilter(field_name="amount", lookup_expr='gte')
    max_amount = filters.NumberFilter(field_name="amount", lookup_expr='lte')   

    class Meta:
        model = CreditPayment
        fields = ['is_verified']