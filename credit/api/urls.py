from django.urls import path
from credit.api import views as api_views
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers


app_name = 'credits'

urlpatterns = [
    path('', api_views.CreditCreatListAPIView.as_view(), name='credit-list'),
    path('<int:pk>/', api_views.CreditAPIView.as_view(), name='credit-detail'),
    path('payment/', api_views.CreditPaymentCreatListAPIView.as_view(), name='payment-list'),
    path('payment/<int:pk>', api_views.CreditPaymentAPIView.as_view(), name='payment-detail'),
    path('image/', api_views.CreditImageCreatListAPIView.as_view(), name='image-list'),
    path('image/<int:pk>', api_views.CreditImageAPIView.as_view(), name='image-detail'),

]
