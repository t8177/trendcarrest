from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from account.api.permissions import CustomDjangoModelPermissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from credit.api.serializers import CreditFormSerializer, CreditImageSerializer, CreditSerializer, CreditPaymentSerializer
from credit.models import Credit, CreditImage, CreditPayment
from credit.filters import CreditFilter, CreditPaymentFilter
from rest_framework.viewsets import ModelViewSet


class CreditCreatListAPIView(generics.ListCreateAPIView):
    queryset = Credit.objects.all().order_by('-id')
    serializer_class = CreditSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = CreditFilter
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return CreditSerializer
        return CreditFormSerializer


class CreditAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Credit.objects.all()
    serializer_class = CreditSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


    def get_serializer_class(self):
        if self.request.method == 'GET':
            return CreditSerializer
        return CreditFormSerializer


# class CreditViewSet(ModelViewSet):
#     queryset = Credit.objects.all().order_by('-id')
#     serializer_class = CreditSerializer
#     filter_backends = [DjangoFilterBackend]
#     filterset_class = CreditFilter
#     permission_classes = [CustomDjangoModelPermissions]

class CreditPaymentCreatListAPIView(generics.ListCreateAPIView):
    queryset = CreditPayment.objects.all().order_by('-id')
    serializer_class = CreditPaymentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = CreditPaymentFilter
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


class CreditPaymentAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = CreditPayment.objects.all()
    serializer_class = CreditPaymentSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


class CreditImageCreatListAPIView(generics.ListCreateAPIView):
    queryset = CreditImage.objects.all()
    serializer_class = CreditImageSerializer


class CreditImageAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = CreditImage.objects.all()
    serializer_class = CreditImageSerializer