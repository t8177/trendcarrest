from django.db import models
from django.db.models import fields
from credit.models import Credit, CreditImage, CreditPayment
from rest_framework import serializers
from account.api.serializers import CustomerSerializer
from car.api.serializers import CarForContractSerializer
from account.models import Contract

class CreditPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CreditPayment
        fields = '__all__'

class CreditImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = CreditImage
        fields = ('__all__')

class ContractForCreditsSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer()
    car = CarForContractSerializer()

    class Meta:
        model = Contract
        fields = '__all__'
    
class CreditSerializer(serializers.ModelSerializer):
    contract = ContractForCreditsSerializer()
    payments = CreditPaymentSerializer(many=True)
    images = CreditImageSerializer(many=True)
    payment = serializers.SerializerMethodField()
    class Meta:
        model = Credit
        fields = ['id','contract','percent_total','percent_expenses','expenses','debt','payment_year','notes','images','payment_count','payment_date','car_value_total','total_credit','first_payment','type','balance',\
                    'is_completed','is_permitted','is_verified','payment','amounts','created_at','payments']

    def get_payment(self,credit,*args,**kwargs):
        payment = 0
        payment = round(((credit.total_credit)/credit.payment_count),2)
        return payment

    

class CreditFormSerializer(serializers.ModelSerializer):

    class Meta:
        model = Credit
        fields = '__all__'