from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Sum, Value as V
from django.db.models.functions import Coalesce
from django.db.models.query_utils import Q
from account.models import CustomMixin
from core.models import Currency


class Credit(CustomMixin):

    CREDIT_TYPES =(
        ('M','Monthly'),
        ('W','Weekly')
    )

    contract = models.ForeignKey('account.Contract', on_delete=models.SET_NULL, null=True, blank=True, related_name='credits')
    payment_count = models.PositiveSmallIntegerField()
    payment_year = models.FloatField()
    payment_date = models.PositiveSmallIntegerField(validators =[MinValueValidator(1), MaxValueValidator(28)], default=1)
    car_value_total = models.FloatField()
    total_credit = models.FloatField(default=0)
    first_payment = models.FloatField(default=0)
    type = models.CharField(max_length=1, choices=CREDIT_TYPES)
    balance = models.FloatField(default=0)
    percent_total = models.IntegerField(default=0)
    percent_expenses = models.IntegerField(default=0)
    expenses = models.FloatField(default=0) 
    is_completed = models.BooleanField(default=False)
    is_permitted = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    payment = models.FloatField(default=0)
    debt = models.FloatField(default=0)
    notes = models.TextField(blank=True, null=True)


    @property
    def amounts(self):
        paid = round(self.payments.aggregate(paid=Coalesce(Sum('total_amount'),V(0),output_field=models.FloatField()))['paid'],2)
        unpaid = round(self.total_credit - paid,2)
        return {'paid':paid,'unpaid':unpaid}
    @property
    def debt(self):
        debt = round(self.payments.aggregate(debt=Coalesce(Sum('debt_week'),V(0),output_field=models.FloatField()))['debt'],2)
        return debt

    def __str__(self):
        return f"{self.contract.car} - {self.type}"
    
    def save(self,*args,**kwargs):
        self.expenses = self.car_value_total*self.percent_expenses/100
        self.total_credit = round(((self.car_value_total-self.first_payment+self.expenses)*self.percent_total/100) + (self.car_value_total + self.expenses-self.first_payment),0 )
        super(Credit,self).save(*args,**kwargs)

class CreditPayment(CustomMixin):
    credit = models.ForeignKey(Credit, on_delete=models.SET_NULL, null=True, blank=True, related_name='payments')
    date = models.DateField()
    payment_left = models.FloatField(default=0)
    amount = models.FloatField()
    amount_uah = models.FloatField(default=0)
    total_amount = models.FloatField(default=0, editable=False)

    debt_week = models.FloatField(default=0)
    debt_total = models.FloatField(default=0) #

    is_verified = models.BooleanField(default=False)
    
    class Meta:
        ordering = ('date',)

    def save(self,*args,**kwargs):
        # Trigger all records' save method
        print('Credit Payment record save() triggered -- '+str(self.credit.total_credit)+'-'+str(self.date))
        

        c = Currency.objects.filter(date__lte=self.date).last()
        self.total_amount = round(self.amount + (self.amount_uah / c.ratio),2)
        self.debt_week = round(self.total_amount -((self.credit.total_credit)/self.credit.payment_count),2)
        
            
        # total_debt and payment_left getting from previous record
        try:
            prev_obj = self.credit.payments.filter(date__lt=self.date).last()
            self.debt_total = round(float(prev_obj.debt_total) + self.debt_week,2)
            self.payment_left = prev_obj.payment_left - self.total_amount
        except:
            self.debt_total = round(self.debt_week,2)
            self.payment_left = self.credit.total_credit - self.credit.first_payment - self.total_amount

        
        super(CreditPayment,self).save(*args,**kwargs)
        # To trigger all next records
        try:
            next_obj = self.credit.payments.filter(date__gt=self.date).first()
            next_obj.save()
        except:
            pass

    def __str__(self):
        return str(self.date)
    

class CreditImage(CustomMixin):
    credit = models.ForeignKey(Credit, on_delete=models.SET_NULL,null=True, blank=True, related_name='images')
    path = models.FileField(upload_to='credit_images')