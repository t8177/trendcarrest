from django.db.models.signals import  post_delete, post_save
from django.dispatch import receiver
from .models import *


@receiver(post_delete, sender=CreditPayment)
def calculate_payments_after_one_deleted(sender, instance, *args,**kwargs):
    try:
        next_obj = instance.credit.payments.filter(date__gt=instance.date).first()
        next_obj.save()
    except:
        pass

@receiver(post_save, sender=Credit)
def change_car_driver_when_create(sender, instance,created,**kwargs):
    if created:
        print('Hey')
        car = instance.contract.car
        car.driver = instance.contract.customer
        car.status = instance.contract.type
        car.save()


@receiver(post_delete, sender=Credit)
def change_car_driver_when_deleted(sender, instance, *args,**kwargs):
    car = instance.contract.car
    instance.contract.is_active = False
    instance.contract.save()
    print(car.contracts.filter(is_active=True).last())
    print('HHey')

    try:
        car.driver = car.contracts.filter(is_active=True).last().customer
        car.status = car.contracts.filter(is_active=True).last().type
        car.save()
    except:
        car.driver = None
        car.status = 'F'
        car.save()
    