from django.contrib import admin
from credit.models import Credit, CreditImage,CreditPayment

admin.site.register([CreditPayment])


#### INLINES ####
class PaymentInline(admin.TabularInline):
    model = CreditPayment
    extra = 1
    ordering = ('-date',)

class ImageInline(admin.TabularInline):
    model = CreditImage
    extra = 1

#### INLINES ####

@admin.register(Credit)
class CreditAdmin(admin.ModelAdmin):
    inlines = [ImageInline, PaymentInline]