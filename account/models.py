from django.db import models
from account.managers import CustomUserManager
from account.mixins import CustomMixin
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator

class CustomUser(AbstractUser):
    USER_TYPES = (
        ('F','Free'),
        ('C','Credit'),
        ('S','Stuff'),
        ('R','Rent'),
        ('RTP','Rent Taxi Percent'),
        ('RTD','Rent Taxi Daily'),
        ('RTW','Rent Taxi Weekly'),
    )
    username = models.CharField(max_length=128, unique=True)
    father_name = models.CharField(max_length=50, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    passport = models.CharField(max_length=50, null=True, blank=True)
    license_number = models.CharField(max_length=50, null=True, blank=True)
    type = models.CharField(max_length=3, choices=USER_TYPES, default='F')
    is_black_listed = models.BooleanField(default=False)    
    is_active = models.BooleanField(default=True)
    note = models.TextField(null=True, blank=True)
    rating = models.PositiveIntegerField(default=10, validators=[MaxValueValidator(10),MinValueValidator(1)])

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.get_full_name()
    
    def get_full_name(self):
        if self.father_name:
            return f'{self.last_name} {self.first_name} {self.father_name}'
        return f'{self.last_name} {self.first_name}'





class UserImage(CustomMixin):
    user = models.ForeignKey(CustomUser, related_name='images', on_delete=models.CASCADE)
    path = models.ImageField("image", upload_to='accounts/user_images')
    is_main = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email

class PassportImage(CustomMixin):
    user = models.ForeignKey(CustomUser, related_name='passports', on_delete=models.CASCADE)
    path = models.ImageField("image", upload_to='accounts/passport_images')
    is_main = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email


class LicenseImage(CustomMixin):
    user = models.ForeignKey(CustomUser, related_name='licenses', on_delete=models.CASCADE)
    path = models.ImageField("image", upload_to='accounts/license_images')
    is_main = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email


class UserPhone(CustomMixin):
    user = models.ForeignKey(CustomUser, related_name='phones', on_delete=models.CASCADE)
    number = models.CharField(max_length=30)
    note = models.CharField(max_length=150, null=True, blank=True)
    is_main = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email


class Contract(CustomMixin):
    CONTRACT_TYPES = (
        ('F', 'Free'),
        ('C', "Credit"),
        ('R', "Rent"),
        ('RTP', "Rent Taxi Percent"),
        ('RTD', "Rent Taxi Daily"),
        ('RTW', "Rent Taxi Week"),
    )
  
    customer = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='contracts')
    car = models.ForeignKey('car.Car', on_delete=models.CASCADE, related_name='contracts')
    type = models.CharField(max_length=3, choices=CONTRACT_TYPES)
    signature = models.ImageField(upload_to='accounts/signatures', blank=True,null=True)
    start_date = models.DateField()
    end_date = models.DateField()
    deposit_amount = models.FloatField(default=0)
    is_active = models.BooleanField(default=True)

    # class Meta:
    #     unique_together = ('customer','car','is_active')

    def __str__(self):
        return f'{self.customer} - {self.car} - {self.get_type_display()}'



class Score(models.Model):
    customer = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='scores')
    score = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(10),MinValueValidator(1)])
    note = models.TextField(null=True, blank=True)

    def __str__(self):
        return f'{self.customer.first_name} {self.customer.last_name} - {self.score}'