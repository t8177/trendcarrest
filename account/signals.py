from django.db.models.signals import  post_delete, post_save
from django.db.models import Avg
from django.dispatch import receiver

from .models import *


@receiver(post_save, sender=Contract)
def change_car_driver_when_create(sender, instance,created,**kwargs):
    if created:
        print('Hey')
        if instance.is_active == True:
            user = instance.customer
            user.type = instance.type
            user.save()


@receiver(post_delete, sender=Contract)
def change_car_driver_when_deleted(sender, instance, *args,**kwargs):
    user = instance.customer
    print('HHey')
    try:
        user.type = user.filter(is_active=True).last().type
        user.save()
    except:
        user.type = 'F'
        user.save()


@receiver(post_save, sender=CustomUser)
def default_score_when_user_created(sender, instance,created,**kwargs):
    if created:
        print('Hey')
        Score.objects.create(customer=instance, score=10)
    

@receiver(post_delete, sender=Score)
def change_car_driver_when_deleted(sender, instance, *args,**kwargs):
    customer = instance.customer
    rating = customer.scores.filter().aggregate(Avg('score'))['score__avg']
    customer.rating = rating
    customer.save()

@receiver(post_save, sender=Score)
def default_score_when_user_created(sender, instance,created,**kwargs):
    print('Hey')
    customer = instance.customer
    rating = customer.scores.filter().aggregate(Avg('score'))['score__avg']
    customer.rating = rating
    customer.save()