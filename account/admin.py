from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _
from account.models import (
    CustomUser,
    Contract,
    LicenseImage,
    PassportImage,
    UserImage,
    UserPhone,
    Score
)

admin.site.register(Contract)
admin.site.register(Score)

############################# INLINES ###############################

class UserImageInline(admin.TabularInline):
    model = UserImage
    extra = 1

class PassportImageInline(admin.TabularInline):
    model = PassportImage
    extra = 1 

class LicenseImageInline(admin.TabularInline):
    model = LicenseImage
    extra = 1

class UserPhoneInline(admin.TabularInline):
    model = UserPhone
    extra = 1  

############################# INLINES #################################

@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    model = CustomUser
    list_display = ('username','get_full_name', 'is_staff', 'is_active')
    list_filter = ('username', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('username', 'password','type')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'father_name','birth_date','passport', 'license_number','note','rating')}),
        (_('Permissions'), {
            'classes': ('collapse',),
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('username',)
    ordering = ('username',)
    inlines = (UserPhoneInline,UserImageInline,PassportImageInline,LicenseImageInline)