from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers

from .views import CustomAuthToken, CustomerListAPIView, ImageViewSet, LogoutView, MyProfileView, TestView, UserDetailAPIView,CustomerBlackListAPIView,\
     ContractListCreateAPIView,ContractAPIView, CustomerViewSet, NumberViewSet

app_name = 'account-api'

router = DefaultRouter()

router.register('customers', CustomerViewSet, basename='customer')

customer_router = routers.NestedSimpleRouter(router, 'customers', lookup='user')
customer_router.register('phones', NumberViewSet, basename='phones')
customer_router.register('images', ImageViewSet, basename='images')


urlpatterns =[
    path('login/', CustomAuthToken.as_view()),
    # path('', MyProfileView.as_view()),
    # path('users/',UserListAPIView.as_view()),
    # path('<int:pk>', UserDetailAPIView.as_view()),
    path('test/',TestView.as_view()),
    path('logout/',LogoutView.as_view()),
    path('contracts/',ContractListCreateAPIView.as_view()),
    path('contracts/<int:pk>',ContractAPIView.as_view()),
    
    path('',include(router.urls)),
    path('',include(customer_router.urls)),
    # -- New Urls -- #
    path('list/', CustomerListAPIView.as_view()),
    path('list/black/', CustomerBlackListAPIView.as_view()),

]

