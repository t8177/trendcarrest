from django.contrib.auth import get_user_model
from django.db.models import fields, Avg, Sum
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from account.models import Contract, LicenseImage, PassportImage, UserImage, UserPhone, Score
from debt.models import Debt

User = get_user_model()

class UserImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserImage
        fields = '__all__'

class PassportImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = PassportImage
        fields = '__all__'

class LicenseImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = LicenseImage
        fields = '__all__'

class UserPhoneSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserPhone
        fields = '__all__'
        read_only_fields = ('user',)


class UserDetailSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'token',
        )

    def get_token(self, user):
        token, created = Token.objects.get_or_create(user=user)
        return token.key

        
class CustomerSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'father_name',
            'birth_date',
            'get_full_name',
        )


class CustomerSerializer(serializers.ModelSerializer):
    images = UserImageSerializer(many=True)
    passports = PassportImageSerializer(many=True)
    licenses = LicenseImageSerializer(many=True)
    phones = UserPhoneSerializer(many=True)

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'father_name',
            'birth_date',
            'get_full_name',
            'images',
            'passports',
            'licenses',
            'phones',
            'passport',
            'license_number'
        )







class ContractSerializer(serializers.ModelSerializer):
    car = serializers.SerializerMethodField()
    customer = CustomerSerializer()


    def get_car(self, obj):
        data = {
            'id': obj.car.id,
            'model':obj.car.model.name,
            'number':obj.car.number,
            'year':obj.car.year,
        }
        return data
    class Meta:
        model = Contract
        fields = ['id','signature','type','customer','car','is_active','start_date','end_date','created_at']

class ContractFlatSerializer(serializers.ModelSerializer):
    car = serializers.SerializerMethodField()


    def get_car(self, obj):
        data = {
            'id': obj.car.id,
            'model':obj.car.model.name,
            'number':obj.car.number,
            'year':obj.car.year,
        }
        return data
    class Meta:
        model = Contract
        fields = ['signature','type','car','is_active','created_at']


class ContractFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contract
        fields = ('__all__')

class UserFullDetailSerializer(serializers.ModelSerializer):
    images = UserImageSerializer(many=True)
    passports = PassportImageSerializer(many=True)
    licenses = LicenseImageSerializer(many=True)
    phones = UserPhoneSerializer(many=True)
    active_contract = ContractFlatSerializer(source='contract')
    contracts = ContractFlatSerializer(many=True,source='contracts.all',read_only=True)

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'father_name',
            'birth_date',
            'get_full_name',
            'active_contract',
            'contracts',
            'images',
            'passports',
            'licenses',
            'phones'
        )


class CustomerRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'type'
        )
    
    def save(self, **kwargs):
        password = User.objects.make_random_password()
        super().save(password=password,**kwargs)
        # print(self.validated_data)
        # u = User.objects.create_user(password=password,**self.validated_data)
        # print(u.id)
        # return u


# -- New Serializers --- #
class CustomerListSerializer(serializers.ModelSerializer):
    images = UserImageSerializer(many=True)
    passports = PassportImageSerializer(many=True)
    phones = UserPhoneSerializer(many=True)
    cars = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    debt = serializers.SerializerMethodField()

    def get_debt(self, obj):
        debt = Debt.objects.filter(customer=obj,status='N').aggregate(Sum('amount'))
        return debt['amount__sum']

    def get_rating(self, obj):
        rating = Score.objects.filter(customer=obj).aggregate(Avg('score'))
        return int(round(rating['score__avg']))

    def get_cars(self, obj):
        cars = obj.cars.filter(is_active=True)
        data = []
        for car in cars:
            data.append({
                'id': car.id,
                'model':car.model.name,
                'number':car.number,
                'year':car.year,
                'status': car.status
            })
        return data


    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'father_name',
            'birth_date',
            'get_full_name',
            'images',
            'passports',
            'passport',
            'license_number',
            'phones',
            'cars',
            'rating',
            'debt',
            'note'
        )

class CustomerBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'father_name',
            'birth_date',
            'get_full_name',
        )