from decimal import Context
from django.contrib.auth import get_user_model
from rest_framework import permissions, serializers,status, viewsets, generics
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView, ListAPIView, ListCreateAPIView, RetrieveAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from account.api.permissions import CustomDjangoModelPermissions
from account.api.serializers import ContractFormSerializer, ContractSerializer, CustomerRegisterSerializer, UserDetailSerializer, UserFullDetailSerializer, UserImageSerializer, UserPhoneSerializer, CustomerListSerializer
from django_filters import rest_framework as filters
from account.models import Contract, UserImage, UserPhone
from django.db.models import Q

User = get_user_model()



class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        user_serializer = UserDetailSerializer(user)
        return Response(user_serializer.data, status=200)

class MyProfileView(GenericAPIView):
    serializer_class = UserFullDetailSerializer
    permission_classes = [IsAuthenticated]
    
    def get(self,request):
        # user = self.serializer_class(data=request.user.id,context={'request':request})
        # user.is_valid()
        ser = UserFullDetailSerializer(request.user)
        return Response(ser.data,status=200)


class UserDetailAPIView(RetrieveAPIView):
    model = User
    serializer_class = UserFullDetailSerializer
    permission_classes = [CustomDjangoModelPermissions]
    queryset = User.objects.all()


class UserListAPIView(ListAPIView):
    model = User
    serializer_class = UserFullDetailSerializer
    permission_classes = [CustomDjangoModelPermissions]
    queryset = User.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('type',)


class TestView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self,request):
        return Response(request.user.email)

class LogoutView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)


class ContractListCreateAPIView(ListCreateAPIView):
    queryset = Contract.objects.all()
    permission_classes = [CustomDjangoModelPermissions]

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ContractSerializer
        return ContractFormSerializer

class ContractAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    permission_classes = [CustomDjangoModelPermissions]


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication,]

    def get_serializer_class(self):
        if self.action == 'create':
            return CustomerRegisterSerializer
        return UserFullDetailSerializer


class NumberViewSet(viewsets.ModelViewSet):
    queryset = UserPhone.objects.all()
    permission_classes = [CustomDjangoModelPermissions]
    serializer_class = UserPhoneSerializer

    def get_queryset(self):
        return UserPhone.objects.filter(user=self.kwargs['user_pk'])


    def perform_create(self, serializer):
        serializer.save(user_id=self.kwargs['user_pk'])


class ImageViewSet(viewsets.ModelViewSet):
    queryset = UserImage.objects.all()
    permission_classes = [CustomDjangoModelPermissions]
    serializer_class = UserImageSerializer

    def get_queryset(self):
        return UserImage.objects.filter(user=self.kwargs['user_pk'])


    def perform_create(self, serializer):
        serializer.save(user_id=self.kwargs['user_pk'])



# -- New Views --- #
class CustomerListAPIView(generics.ListAPIView):
    queryset = User.objects.all().order_by('-id')
    serializer_class = CustomerListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        drivers = User.objects.filter(is_active=True)
        return drivers

class CustomerBlackListAPIView(generics.ListAPIView):
    queryset = User.objects.all().order_by('-id')
    serializer_class = CustomerListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        drivers = User.objects.filter(rating__lte=4)
        return drivers