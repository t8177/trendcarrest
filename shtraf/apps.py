from django.apps import AppConfig


class ShtrafConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shtraf'

    def ready(self):
        import shtraf.signals