from django.contrib import admin
from .models import *


class ShtrafItemInline(admin.TabularInline):
    model = ShtrafImage
    extra = 1

@admin.register(Shtraf)
class ShtrafAdmin(admin.ModelAdmin):
    inlines = [ShtrafItemInline,]

