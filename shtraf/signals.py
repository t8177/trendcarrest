from django.db.models.signals import  post_delete, post_save
from django.dispatch import receiver
from .models import *
  

@receiver(post_save, sender=Shtraf)
def change_debt_when_create(sender, instance,created,**kwargs):
    
    if created:
        if instance.debt_status == 'P':
            Debt.objects.create(
                car=instance.car,
                customer=instance.driver,
                created_date=instance.created_date,
                paid_date=instance.created_date,
                amount=instance.amount,
                status="P",
                )
        elif instance.debt_status == 'N':
            Debt.objects.create(
                car=instance.car,
                customer=instance.driver,
                created_date=instance.created_date,
                amount=instance.amount,
                status="N",
                )

