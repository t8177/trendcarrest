from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from account.api.permissions import CustomDjangoModelPermissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from shtraf.api.serializers import *
from shtraf.models import *
from car.models import Car
from credit.filters import CreditFilter, CreditPaymentFilter
from rest_framework.viewsets import ModelViewSet
from datetime import date, timedelta
from django.utils import timezone
from django.db.models import Q


class ShtrafListAPIView(generics.ListAPIView):
    queryset = Shtraf.objects.all().order_by('-id')
    serializer_class = ShtrafListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class CarListWithShtrafAPIView(generics.ListAPIView):
    serializer_class = CarListWithShtrafSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        cars = Car.objects.filter(Q(shtrafs__status='N')|Q(shtrafs__paid_date__isnull=False,shtrafs__paid_date__gte=monday_of_this_week)).distinct()
        return cars

class ShtrafCreateAPIView(generics.CreateAPIView):
    queryset = Shtraf.objects.all().order_by('-id')
    serializer_class = ShtrafCreateSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]