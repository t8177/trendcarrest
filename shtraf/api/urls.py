from django.urls import path
from shtraf.api import views as api_views
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers


app_name = 'shtraf'

urlpatterns = [
    path('list/', api_views.ShtrafListAPIView.as_view(), name='shtraf-list'),
    path('list/car/', api_views.CarListWithShtrafAPIView.as_view(), name='shtraf-list-car'),
    path('create/', api_views.ShtrafCreateAPIView.as_view(), name='shtraf-list'),

]
