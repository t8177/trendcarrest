from django.db import models
from django.db.models import fields, Q
from shtraf.models import *
from rest_framework import serializers
from account.api.serializers import CustomerSerializer, CustomerBasicSerializer
from car.api.serializers import CarSerializer, CarImageSerializer, ModelSerializer, OfficeSerializer
from datetime import date, timedelta
from django.utils import timezone

class ShtrafImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShtrafImage
        fields = '__all__'
 
class ShtrafListSerializer(serializers.ModelSerializer):
    files = ShtrafImageSerializer(many=True, read_only=True)
    driver = CustomerSerializer()
    car = CarSerializer()
    class Meta:
        model = Shtraf
        fields = ['id','amount','is_active','status','car','driver','files']


class ShtrafCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shtraf
        fields = '__all__'


class CarListWithShtrafSerializer(serializers.ModelSerializer):
    images = CarImageSerializer(many=True)
    model = ModelSerializer()
    office = OfficeSerializer()
    advertise = serializers.SerializerMethodField()
    driver = CustomerBasicSerializer()
    shtrafs = serializers.SerializerMethodField()

    def get_shtrafs(self,obj,*args,**kwargs):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        data = obj.shtrafs.filter(Q(status='N')|Q(paid_date__isnull=False,paid_date__gte=monday_of_this_week)).distinct()
        map = []
        for d in data:
            j = {}
            j['id'] = d.id
            j['status'] = d.status
            j['debt_status'] = d.debt_status
            j['amount'] = d.amount
            j['created_date'] = d.created_date
            if d.paid_date:
                j['paid_date'] = d.paid_date
            if d.car:
                j['driverid'] = d.driver.id
                j['drivername'] = d.driver.first_name
                j['driversurname'] = d.driver.last_name
            j['note'] = d.note
            map.append(j)
        return map
    def get_advertise(self,car,*args,**kwargs):
        advertisement =  car.advertise
        if advertisement:
            data = {
                'id':advertisement.id,
                'name':advertisement.name,
            }
            return data
        else:
            return None
    
    class Meta:
        model = Car
        fields = ['id','images','model','office','advertise','driver','number','year','is_available','is_active','status','health_status','current_tyre','is_verified','reserve_key','shtrafs']
