import imp
from django.db import models
from account.models import CustomMixin
from car.models import Car
from debt.models import Debt
from django.contrib.auth import get_user_model

User = get_user_model()

class Shtraf(CustomMixin):
    SHTRAF_STATUS = (
        ('P','Paid'),
        ('R','Refunded'),
        ('N','Not paid'),
    )
    DEBT_STATUS = (
        ('P','Driver paid'),
        ('N','Driver not paid yet'),
        ('W','Not related to any driver'),
    )
    no = models.CharField(max_length=255, null=True, blank=True)
    datetime = models.DateTimeField()
    car = models.ForeignKey(Car, on_delete=models.CASCADE, related_name="shtrafs")
    driver = models.ForeignKey(User, on_delete=models.CASCADE, related_name="shtrafs")
    
    created_date = models.DateField()
    paid_date = models.DateField(blank=True, null=True)
    
    amount = models.FloatField()
    note = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=1, choices=SHTRAF_STATUS, default='N')
    debt_status = models.CharField(max_length=1, choices=DEBT_STATUS, default='W')
    is_active = models.BooleanField(default=True)
    
    def __str__(self):
        return self.car.model.name


class ShtrafImage(models.Model):
    shtraf = models.ForeignKey(Shtraf, on_delete=models.CASCADE, related_name="images")
    file = models.FileField()

    def __str__(self):
        return self.shtraf.car.model.name