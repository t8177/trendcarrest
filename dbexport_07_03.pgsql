--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: account_contract; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_contract (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    type character varying(2) NOT NULL,
    signature character varying(100),
    start_date date NOT NULL,
    end_date date NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint NOT NULL,
    customer_id bigint
);


ALTER TABLE public.account_contract OWNER TO alvinseyidov;

--
-- Name: account_contract_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_contract_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_contract_id_seq OWNER TO alvinseyidov;

--
-- Name: account_contract_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_contract_id_seq OWNED BY public.account_contract.id;


--
-- Name: account_customuser; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_customuser (
    id bigint NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    username character varying(128) NOT NULL,
    father_name character varying(50),
    birth_date date,
    passport character varying(50),
    license_number character varying(50),
    type character varying(3) NOT NULL,
    is_black_listed boolean NOT NULL
);


ALTER TABLE public.account_customuser OWNER TO alvinseyidov;

--
-- Name: account_customuser_groups; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_customuser_groups (
    id bigint NOT NULL,
    customuser_id bigint NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.account_customuser_groups OWNER TO alvinseyidov;

--
-- Name: account_customuser_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_customuser_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_customuser_groups_id_seq OWNER TO alvinseyidov;

--
-- Name: account_customuser_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_customuser_groups_id_seq OWNED BY public.account_customuser_groups.id;


--
-- Name: account_customuser_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_customuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_customuser_id_seq OWNER TO alvinseyidov;

--
-- Name: account_customuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_customuser_id_seq OWNED BY public.account_customuser.id;


--
-- Name: account_customuser_user_permissions; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_customuser_user_permissions (
    id bigint NOT NULL,
    customuser_id bigint NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.account_customuser_user_permissions OWNER TO alvinseyidov;

--
-- Name: account_customuser_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_customuser_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_customuser_user_permissions_id_seq OWNER TO alvinseyidov;

--
-- Name: account_customuser_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_customuser_user_permissions_id_seq OWNED BY public.account_customuser_user_permissions.id;


--
-- Name: account_licenseimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_licenseimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    is_main boolean NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.account_licenseimage OWNER TO alvinseyidov;

--
-- Name: account_licenseimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_licenseimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_licenseimage_id_seq OWNER TO alvinseyidov;

--
-- Name: account_licenseimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_licenseimage_id_seq OWNED BY public.account_licenseimage.id;


--
-- Name: account_passportimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_passportimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    is_main boolean NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.account_passportimage OWNER TO alvinseyidov;

--
-- Name: account_passportimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_passportimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_passportimage_id_seq OWNER TO alvinseyidov;

--
-- Name: account_passportimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_passportimage_id_seq OWNED BY public.account_passportimage.id;


--
-- Name: account_userimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_userimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    is_main boolean NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.account_userimage OWNER TO alvinseyidov;

--
-- Name: account_userimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_userimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_userimage_id_seq OWNER TO alvinseyidov;

--
-- Name: account_userimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_userimage_id_seq OWNED BY public.account_userimage.id;


--
-- Name: account_userphone; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_userphone (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    number character varying(30) NOT NULL,
    note character varying(150),
    is_main boolean NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.account_userphone OWNER TO alvinseyidov;

--
-- Name: account_userphone_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_userphone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_userphone_id_seq OWNER TO alvinseyidov;

--
-- Name: account_userphone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_userphone_id_seq OWNED BY public.account_userphone.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO alvinseyidov;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO alvinseyidov;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO alvinseyidov;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO alvinseyidov;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO alvinseyidov;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO alvinseyidov;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO alvinseyidov;

--
-- Name: car_brand; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_brand (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    logo character varying(100)
);


ALTER TABLE public.car_brand OWNER TO alvinseyidov;

--
-- Name: car_brand_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_brand_id_seq OWNER TO alvinseyidov;

--
-- Name: car_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_brand_id_seq OWNED BY public.car_brand.id;


--
-- Name: car_car; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_car (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    number character varying(50) NOT NULL,
    year smallint NOT NULL,
    vin_code integer,
    is_available boolean NOT NULL,
    is_active boolean NOT NULL,
    transmission character varying(1) NOT NULL,
    fuel_tank smallint,
    doors smallint,
    seats smallint,
    wheel_size smallint,
    current_tyre character varying(1) NOT NULL,
    bought_date date,
    bought_price double precision,
    current_km double precision,
    oil_km_limit smallint,
    climate boolean NOT NULL,
    climate_control boolean NOT NULL,
    is_verified boolean NOT NULL,
    reserve_key boolean NOT NULL,
    advertise_id bigint,
    car_class_id bigint,
    case_id bigint,
    color_id bigint,
    fuel_id bigint,
    model_id bigint,
    office_id bigint,
    health_status character varying(1) NOT NULL,
    status character varying(3) NOT NULL,
    driver_id bigint,
    CONSTRAINT car_car_doors_check CHECK ((doors >= 0)),
    CONSTRAINT car_car_fuel_tank_check CHECK ((fuel_tank >= 0)),
    CONSTRAINT car_car_oil_km_limit_check CHECK ((oil_km_limit >= 0)),
    CONSTRAINT car_car_seats_check CHECK ((seats >= 0)),
    CONSTRAINT car_car_wheel_size_check CHECK ((wheel_size >= 0)),
    CONSTRAINT car_car_year_check CHECK ((year >= 0))
);


ALTER TABLE public.car_car OWNER TO alvinseyidov;

--
-- Name: car_car_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_car_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_car_id_seq OWNER TO alvinseyidov;

--
-- Name: car_car_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_car_id_seq OWNED BY public.car_car.id;


--
-- Name: car_carclass; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carclass (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.car_carclass OWNER TO alvinseyidov;

--
-- Name: car_carclass_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carclass_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carclass_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carclass_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carclass_id_seq OWNED BY public.car_carclass.id;


--
-- Name: car_carimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    is_main boolean NOT NULL,
    car_id bigint NOT NULL
);


ALTER TABLE public.car_carimage OWNER TO alvinseyidov;

--
-- Name: car_carimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carimage_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carimage_id_seq OWNED BY public.car_carimage.id;


--
-- Name: car_carimagetask; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carimagetask (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    car_id bigint,
    user_id bigint
);


ALTER TABLE public.car_carimagetask OWNER TO alvinseyidov;

--
-- Name: car_carimagetask_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carimagetask_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carimagetask_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carimagetask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carimagetask_id_seq OWNED BY public.car_carimagetask.id;


--
-- Name: car_carimagetask_images; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carimagetask_images (
    id bigint NOT NULL,
    carimagetask_id bigint NOT NULL,
    carimage_id bigint NOT NULL
);


ALTER TABLE public.car_carimagetask_images OWNER TO alvinseyidov;

--
-- Name: car_carimagetask_images_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carimagetask_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carimagetask_images_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carimagetask_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carimagetask_images_id_seq OWNED BY public.car_carimagetask_images.id;


--
-- Name: car_carpaper; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carpaper (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    car_id bigint NOT NULL
);


ALTER TABLE public.car_carpaper OWNER TO alvinseyidov;

--
-- Name: car_carpaper_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carpaper_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carpaper_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carpaper_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carpaper_id_seq OWNED BY public.car_carpaper.id;


--
-- Name: car_case; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_case (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.car_case OWNER TO alvinseyidov;

--
-- Name: car_case_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_case_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_case_id_seq OWNER TO alvinseyidov;

--
-- Name: car_case_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_case_id_seq OWNED BY public.car_case.id;


--
-- Name: car_color; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_color (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.car_color OWNER TO alvinseyidov;

--
-- Name: car_color_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_color_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_color_id_seq OWNER TO alvinseyidov;

--
-- Name: car_color_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_color_id_seq OWNED BY public.car_color.id;


--
-- Name: car_fuel; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_fuel (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.car_fuel OWNER TO alvinseyidov;

--
-- Name: car_fuel_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_fuel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_fuel_id_seq OWNER TO alvinseyidov;

--
-- Name: car_fuel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_fuel_id_seq OWNED BY public.car_fuel.id;


--
-- Name: car_model; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_model (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    brand_id bigint
);


ALTER TABLE public.car_model OWNER TO alvinseyidov;

--
-- Name: car_model_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_model_id_seq OWNER TO alvinseyidov;

--
-- Name: car_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_model_id_seq OWNED BY public.car_model.id;


--
-- Name: car_office; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_office (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    address text NOT NULL
);


ALTER TABLE public.car_office OWNER TO alvinseyidov;

--
-- Name: car_office_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_office_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_office_id_seq OWNER TO alvinseyidov;

--
-- Name: car_office_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_office_id_seq OWNED BY public.car_office.id;


--
-- Name: core_administrativedeposit; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.core_administrativedeposit (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    amount double precision NOT NULL,
    currency character varying(3) NOT NULL,
    note character varying(255),
    date date NOT NULL
);


ALTER TABLE public.core_administrativedeposit OWNER TO alvinseyidov;

--
-- Name: core_administrativedeposit_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.core_administrativedeposit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_administrativedeposit_id_seq OWNER TO alvinseyidov;

--
-- Name: core_administrativedeposit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.core_administrativedeposit_id_seq OWNED BY public.core_administrativedeposit.id;


--
-- Name: core_administrativewithdraw; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.core_administrativewithdraw (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    amount double precision NOT NULL,
    currency character varying(3) NOT NULL,
    note character varying(255),
    date date NOT NULL
);


ALTER TABLE public.core_administrativewithdraw OWNER TO alvinseyidov;

--
-- Name: core_administrativewithdraw_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.core_administrativewithdraw_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_administrativewithdraw_id_seq OWNER TO alvinseyidov;

--
-- Name: core_administrativewithdraw_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.core_administrativewithdraw_id_seq OWNED BY public.core_administrativewithdraw.id;


--
-- Name: core_balance; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.core_balance (
    id bigint NOT NULL,
    amount double precision NOT NULL,
    currency character varying(3) NOT NULL,
    type character varying(4) NOT NULL
);


ALTER TABLE public.core_balance OWNER TO alvinseyidov;

--
-- Name: core_balance_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.core_balance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_balance_id_seq OWNER TO alvinseyidov;

--
-- Name: core_balance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.core_balance_id_seq OWNED BY public.core_balance.id;


--
-- Name: core_currency; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.core_currency (
    id bigint NOT NULL,
    ratio double precision NOT NULL,
    date date NOT NULL
);


ALTER TABLE public.core_currency OWNER TO alvinseyidov;

--
-- Name: core_currency_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.core_currency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_currency_id_seq OWNER TO alvinseyidov;

--
-- Name: core_currency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.core_currency_id_seq OWNED BY public.core_currency.id;


--
-- Name: credit_credit; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.credit_credit (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    payment_count smallint NOT NULL,
    payment_year double precision NOT NULL,
    payment_date smallint NOT NULL,
    car_value_total double precision NOT NULL,
    first_payment double precision NOT NULL,
    type character varying(1) NOT NULL,
    balance double precision NOT NULL,
    percent_total integer NOT NULL,
    percent_expenses integer NOT NULL,
    is_completed boolean NOT NULL,
    is_permitted boolean NOT NULL,
    is_verified boolean NOT NULL,
    is_active boolean NOT NULL,
    payment double precision NOT NULL,
    debt double precision NOT NULL,
    notes text,
    contract_id bigint,
    expenses double precision NOT NULL,
    total_credit double precision NOT NULL,
    CONSTRAINT credit_credit_payment_count_check CHECK ((payment_count >= 0)),
    CONSTRAINT credit_credit_payment_date_check CHECK ((payment_date >= 0))
);


ALTER TABLE public.credit_credit OWNER TO alvinseyidov;

--
-- Name: credit_credit_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.credit_credit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credit_credit_id_seq OWNER TO alvinseyidov;

--
-- Name: credit_credit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.credit_credit_id_seq OWNED BY public.credit_credit.id;


--
-- Name: credit_creditimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.credit_creditimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    credit_id bigint
);


ALTER TABLE public.credit_creditimage OWNER TO alvinseyidov;

--
-- Name: credit_creditimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.credit_creditimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credit_creditimage_id_seq OWNER TO alvinseyidov;

--
-- Name: credit_creditimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.credit_creditimage_id_seq OWNED BY public.credit_creditimage.id;


--
-- Name: credit_creditpayment; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.credit_creditpayment (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    debt_week double precision NOT NULL,
    debt_total double precision NOT NULL,
    payment_left double precision NOT NULL,
    amount double precision NOT NULL,
    amount_uah double precision NOT NULL,
    total_amount double precision NOT NULL,
    is_verified boolean NOT NULL,
    credit_id bigint
);


ALTER TABLE public.credit_creditpayment OWNER TO alvinseyidov;

--
-- Name: credit_creditpayment_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.credit_creditpayment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credit_creditpayment_id_seq OWNER TO alvinseyidov;

--
-- Name: credit_creditpayment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.credit_creditpayment_id_seq OWNED BY public.credit_creditpayment.id;


--
-- Name: debt_debt; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.debt_debt (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    amount double precision NOT NULL,
    paid boolean NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint,
    driver_id bigint NOT NULL
);


ALTER TABLE public.debt_debt OWNER TO alvinseyidov;

--
-- Name: debt_debt_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.debt_debt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.debt_debt_id_seq OWNER TO alvinseyidov;

--
-- Name: debt_debt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.debt_debt_id_seq OWNED BY public.debt_debt.id;


--
-- Name: debt_debtimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.debt_debtimage (
    id bigint NOT NULL,
    file character varying(100) NOT NULL,
    debt_id bigint NOT NULL
);


ALTER TABLE public.debt_debtimage OWNER TO alvinseyidov;

--
-- Name: debt_debtimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.debt_debtimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.debt_debtimage_id_seq OWNER TO alvinseyidov;

--
-- Name: debt_debtimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.debt_debtimage_id_seq OWNED BY public.debt_debtimage.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id bigint NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO alvinseyidov;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO alvinseyidov;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO alvinseyidov;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO alvinseyidov;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO alvinseyidov;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO alvinseyidov;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO alvinseyidov;

--
-- Name: rent_taxi_renttaxi; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_renttaxi (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    car_id bigint,
    is_active boolean NOT NULL
);


ALTER TABLE public.rent_taxi_renttaxi OWNER TO alvinseyidov;

--
-- Name: rent_taxi_renttaxi_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_renttaxi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_renttaxi_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_renttaxi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_renttaxi_id_seq OWNED BY public.rent_taxi_renttaxi.id;


--
-- Name: rent_taxi_revenue; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_revenue (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    contract_id bigint NOT NULL,
    driver_id bigint NOT NULL,
    renttaxi_id bigint NOT NULL
);


ALTER TABLE public.rent_taxi_revenue OWNER TO alvinseyidov;

--
-- Name: rent_taxi_revenue_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_revenue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_revenue_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_revenue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_revenue_id_seq OWNED BY public.rent_taxi_revenue.id;


--
-- Name: rent_taxi_revenueitem; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_revenueitem (
    id bigint NOT NULL,
    amount_cash double precision NOT NULL,
    amount_card double precision NOT NULL,
    order_count smallint NOT NULL,
    paid double precision NOT NULL,
    company_id bigint NOT NULL,
    revenue_id bigint NOT NULL,
    bolt_will_pay double precision NOT NULL,
    rent_amount double precision NOT NULL,
    bolt_will_pay_total double precision NOT NULL,
    commission_from_driver double precision NOT NULL,
    debt_all double precision NOT NULL,
    debt_to_bolt double precision NOT NULL,
    debt_today double precision NOT NULL,
    driver_must_pay double precision NOT NULL,
    rent_taxi_id bigint,
    CONSTRAINT rent_taxi_revenueitem_order_count_check CHECK ((order_count >= 0))
);


ALTER TABLE public.rent_taxi_revenueitem OWNER TO alvinseyidov;

--
-- Name: rent_taxi_revenueitem_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_revenueitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_revenueitem_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_revenueitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_revenueitem_id_seq OWNED BY public.rent_taxi_revenueitem.id;


--
-- Name: shtraf_shtraf; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.shtraf_shtraf (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    datetime timestamp with time zone NOT NULL,
    amount double precision NOT NULL,
    paid boolean NOT NULL,
    car_id bigint NOT NULL,
    driver_id bigint NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.shtraf_shtraf OWNER TO alvinseyidov;

--
-- Name: shtraf_shtraf_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.shtraf_shtraf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shtraf_shtraf_id_seq OWNER TO alvinseyidov;

--
-- Name: shtraf_shtraf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.shtraf_shtraf_id_seq OWNED BY public.shtraf_shtraf.id;


--
-- Name: shtraf_shtrafimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.shtraf_shtrafimage (
    id bigint NOT NULL,
    file character varying(100) NOT NULL,
    shtraf_id bigint NOT NULL
);


ALTER TABLE public.shtraf_shtrafimage OWNER TO alvinseyidov;

--
-- Name: shtraf_shtrafimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.shtraf_shtrafimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shtraf_shtrafimage_id_seq OWNER TO alvinseyidov;

--
-- Name: shtraf_shtrafimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.shtraf_shtrafimage_id_seq OWNED BY public.shtraf_shtrafimage.id;


--
-- Name: taxi_company; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.taxi_company (
    id bigint NOT NULL,
    name character varying(100) NOT NULL,
    commission double precision NOT NULL,
    tarif1_count integer NOT NULL,
    tarif1_pay double precision NOT NULL,
    tarif2_count integer NOT NULL,
    tarif2_pay double precision NOT NULL,
    tarif3_count integer NOT NULL,
    tarif3_pay double precision NOT NULL,
    CONSTRAINT taxi_company_tarif1_count_check CHECK ((tarif1_count >= 0)),
    CONSTRAINT taxi_company_tarif2_count_check CHECK ((tarif2_count >= 0)),
    CONSTRAINT taxi_company_tarif3_count_check CHECK ((tarif3_count >= 0))
);


ALTER TABLE public.taxi_company OWNER TO alvinseyidov;

--
-- Name: taxi_company_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.taxi_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxi_company_id_seq OWNER TO alvinseyidov;

--
-- Name: taxi_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.taxi_company_id_seq OWNED BY public.taxi_company.id;


--
-- Name: taxi_revenue; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.taxi_revenue (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    contract_id bigint NOT NULL,
    driver_id bigint NOT NULL,
    taxi_id bigint NOT NULL
);


ALTER TABLE public.taxi_revenue OWNER TO alvinseyidov;

--
-- Name: taxi_revenue_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.taxi_revenue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxi_revenue_id_seq OWNER TO alvinseyidov;

--
-- Name: taxi_revenue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.taxi_revenue_id_seq OWNED BY public.taxi_revenue.id;


--
-- Name: taxi_revenueitem; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.taxi_revenueitem (
    id bigint NOT NULL,
    amount_cash double precision NOT NULL,
    amount_card double precision NOT NULL,
    order_count smallint NOT NULL,
    paid double precision NOT NULL,
    company_id bigint NOT NULL,
    revenue_id bigint NOT NULL,
    debt_all double precision NOT NULL,
    debt_today double precision NOT NULL,
    taxi_id bigint,
    CONSTRAINT taxi_revenueitem_order_count_check CHECK ((order_count >= 0))
);


ALTER TABLE public.taxi_revenueitem OWNER TO alvinseyidov;

--
-- Name: taxi_revenueitem_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.taxi_revenueitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxi_revenueitem_id_seq OWNER TO alvinseyidov;

--
-- Name: taxi_revenueitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.taxi_revenueitem_id_seq OWNED BY public.taxi_revenueitem.id;


--
-- Name: taxi_taxi; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.taxi_taxi (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint
);


ALTER TABLE public.taxi_taxi OWNER TO alvinseyidov;

--
-- Name: taxi_taxi_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.taxi_taxi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxi_taxi_id_seq OWNER TO alvinseyidov;

--
-- Name: taxi_taxi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.taxi_taxi_id_seq OWNED BY public.taxi_taxi.id;


--
-- Name: account_contract id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_contract ALTER COLUMN id SET DEFAULT nextval('public.account_contract_id_seq'::regclass);


--
-- Name: account_customuser id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser ALTER COLUMN id SET DEFAULT nextval('public.account_customuser_id_seq'::regclass);


--
-- Name: account_customuser_groups id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups ALTER COLUMN id SET DEFAULT nextval('public.account_customuser_groups_id_seq'::regclass);


--
-- Name: account_customuser_user_permissions id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.account_customuser_user_permissions_id_seq'::regclass);


--
-- Name: account_licenseimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_licenseimage ALTER COLUMN id SET DEFAULT nextval('public.account_licenseimage_id_seq'::regclass);


--
-- Name: account_passportimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_passportimage ALTER COLUMN id SET DEFAULT nextval('public.account_passportimage_id_seq'::regclass);


--
-- Name: account_userimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userimage ALTER COLUMN id SET DEFAULT nextval('public.account_userimage_id_seq'::regclass);


--
-- Name: account_userphone id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userphone ALTER COLUMN id SET DEFAULT nextval('public.account_userphone_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: car_brand id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_brand ALTER COLUMN id SET DEFAULT nextval('public.car_brand_id_seq'::regclass);


--
-- Name: car_car id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car ALTER COLUMN id SET DEFAULT nextval('public.car_car_id_seq'::regclass);


--
-- Name: car_carclass id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carclass ALTER COLUMN id SET DEFAULT nextval('public.car_carclass_id_seq'::regclass);


--
-- Name: car_carimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimage ALTER COLUMN id SET DEFAULT nextval('public.car_carimage_id_seq'::regclass);


--
-- Name: car_carimagetask id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask ALTER COLUMN id SET DEFAULT nextval('public.car_carimagetask_id_seq'::regclass);


--
-- Name: car_carimagetask_images id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images ALTER COLUMN id SET DEFAULT nextval('public.car_carimagetask_images_id_seq'::regclass);


--
-- Name: car_carpaper id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carpaper ALTER COLUMN id SET DEFAULT nextval('public.car_carpaper_id_seq'::regclass);


--
-- Name: car_case id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_case ALTER COLUMN id SET DEFAULT nextval('public.car_case_id_seq'::regclass);


--
-- Name: car_color id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_color ALTER COLUMN id SET DEFAULT nextval('public.car_color_id_seq'::regclass);


--
-- Name: car_fuel id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_fuel ALTER COLUMN id SET DEFAULT nextval('public.car_fuel_id_seq'::regclass);


--
-- Name: car_model id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_model ALTER COLUMN id SET DEFAULT nextval('public.car_model_id_seq'::regclass);


--
-- Name: car_office id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_office ALTER COLUMN id SET DEFAULT nextval('public.car_office_id_seq'::regclass);


--
-- Name: core_administrativedeposit id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_administrativedeposit ALTER COLUMN id SET DEFAULT nextval('public.core_administrativedeposit_id_seq'::regclass);


--
-- Name: core_administrativewithdraw id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_administrativewithdraw ALTER COLUMN id SET DEFAULT nextval('public.core_administrativewithdraw_id_seq'::regclass);


--
-- Name: core_balance id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_balance ALTER COLUMN id SET DEFAULT nextval('public.core_balance_id_seq'::regclass);


--
-- Name: core_currency id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_currency ALTER COLUMN id SET DEFAULT nextval('public.core_currency_id_seq'::regclass);


--
-- Name: credit_credit id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_credit ALTER COLUMN id SET DEFAULT nextval('public.credit_credit_id_seq'::regclass);


--
-- Name: credit_creditimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditimage ALTER COLUMN id SET DEFAULT nextval('public.credit_creditimage_id_seq'::regclass);


--
-- Name: credit_creditpayment id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditpayment ALTER COLUMN id SET DEFAULT nextval('public.credit_creditpayment_id_seq'::regclass);


--
-- Name: debt_debt id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debt ALTER COLUMN id SET DEFAULT nextval('public.debt_debt_id_seq'::regclass);


--
-- Name: debt_debtimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debtimage ALTER COLUMN id SET DEFAULT nextval('public.debt_debtimage_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: rent_taxi_renttaxi id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_renttaxi ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_renttaxi_id_seq'::regclass);


--
-- Name: rent_taxi_revenue id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_revenue_id_seq'::regclass);


--
-- Name: rent_taxi_revenueitem id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_revenueitem_id_seq'::regclass);


--
-- Name: shtraf_shtraf id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtraf ALTER COLUMN id SET DEFAULT nextval('public.shtraf_shtraf_id_seq'::regclass);


--
-- Name: shtraf_shtrafimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtrafimage ALTER COLUMN id SET DEFAULT nextval('public.shtraf_shtrafimage_id_seq'::regclass);


--
-- Name: taxi_company id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_company ALTER COLUMN id SET DEFAULT nextval('public.taxi_company_id_seq'::regclass);


--
-- Name: taxi_revenue id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue ALTER COLUMN id SET DEFAULT nextval('public.taxi_revenue_id_seq'::regclass);


--
-- Name: taxi_revenueitem id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem ALTER COLUMN id SET DEFAULT nextval('public.taxi_revenueitem_id_seq'::regclass);


--
-- Name: taxi_taxi id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_taxi ALTER COLUMN id SET DEFAULT nextval('public.taxi_taxi_id_seq'::regclass);


--
-- Data for Name: account_contract; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_contract (id, created_at, updated_at, type, signature, start_date, end_date, is_active, car_id, customer_id) FROM stdin;
13	2022-03-03 16:57:58.280435+00	2022-03-03 16:57:58.280455+00	CR		2021-09-21	2022-09-21	t	78	3
14	2022-03-03 16:59:03.182095+00	2022-03-03 16:59:03.182135+00	CR		2021-05-10	2023-05-10	t	83	4
15	2022-03-03 17:00:06.904862+00	2022-03-03 17:00:06.904884+00	CR		2020-10-26	2022-10-26	t	53	5
16	2022-03-03 17:01:24.505755+00	2022-03-03 17:01:24.505778+00	CR		2020-07-17	2022-07-19	t	85	6
17	2022-03-03 17:03:12.216391+00	2022-03-03 17:03:12.21643+00	CR		2020-09-04	2022-09-04	t	126	7
18	2022-03-03 17:04:06.761409+00	2022-03-03 17:04:06.76143+00	CR		2021-01-03	2023-01-03	t	88	8
19	2022-03-03 17:07:48.523149+00	2022-03-03 17:07:48.523173+00	CR		2021-04-19	2022-04-17	t	5	9
20	2022-03-03 17:08:35.355072+00	2022-03-03 17:08:35.355095+00	CR		2021-08-03	2022-08-03	t	13	10
21	2022-03-03 17:09:22.431095+00	2022-03-03 17:09:22.43113+00	CR		2021-12-07	2023-12-07	t	14	11
22	2022-03-03 17:10:17.848362+00	2022-03-03 17:10:17.848385+00	CR		2021-07-26	2023-07-27	t	23	12
23	2022-03-03 17:11:16.85102+00	2022-03-03 17:11:16.851043+00	CR		2021-09-13	2022-09-13	t	43	13
24	2022-03-03 17:12:02.250645+00	2022-03-03 17:12:02.25067+00	CR		2021-10-25	2023-10-25	t	65	14
25	2022-03-03 17:16:05.030885+00	2022-03-03 17:16:05.030908+00	CR		2021-11-23	2022-11-23	t	54	15
26	2022-03-03 17:19:55.500845+00	2022-03-03 17:19:55.500872+00	CR		2021-12-03	2023-12-07	t	91	16
27	2022-03-03 17:20:34.9768+00	2022-03-03 17:20:34.976821+00	CR		2022-01-01	2023-01-01	t	80	17
28	2022-03-03 17:21:43.413707+00	2022-03-03 17:21:43.413729+00	CR		2021-02-15	2022-01-10	t	58	18
29	2022-03-03 17:23:53.504474+00	2022-03-03 17:23:53.504499+00	CR		2021-03-10	2022-09-06	t	3	19
30	2022-03-03 17:24:44.616141+00	2022-03-03 17:24:44.616162+00	CR		2020-12-29	2022-12-29	t	45	20
31	2022-03-03 17:25:42.259243+00	2022-03-03 17:25:42.259273+00	CR		2022-01-15	2023-01-15	t	55	21
32	2022-03-03 17:26:20.433388+00	2022-03-03 17:26:20.433414+00	CR		2020-06-30	2022-06-30	t	56	22
33	2022-03-03 17:27:04.330652+00	2022-03-03 17:27:04.330676+00	CR		2020-12-29	2022-12-29	t	59	23
34	2022-03-03 17:28:12.030155+00	2022-03-03 17:28:12.030178+00	CR		2021-11-09	2023-11-09	t	95	24
35	2022-03-03 17:28:45.446464+00	2022-03-03 17:28:45.446487+00	CR		2021-06-02	2022-06-02	t	61	25
36	2022-03-03 17:29:25.609285+00	2022-03-03 17:29:25.609308+00	CR		2021-09-01	2023-09-01	t	81	26
37	2022-03-03 17:30:16.108586+00	2022-03-03 17:30:16.108625+00	CR		2022-01-13	2024-01-13	t	64	26
38	2022-03-03 17:31:44.376693+00	2022-03-03 17:31:44.376717+00	CR		2020-09-16	2022-03-16	t	52	27
39	2022-03-03 17:32:16.591558+00	2022-03-03 17:32:16.591584+00	CR		2021-09-09	2022-09-09	t	87	28
40	2022-03-03 17:33:24.37982+00	2022-03-03 17:33:24.379849+00	CR		2021-09-02	2022-09-02	t	6	29
41	2022-03-03 17:34:07.08881+00	2022-03-03 17:34:07.088832+00	CR		2021-10-28	2022-10-28	t	17	30
42	2022-03-03 17:34:49.837542+00	2022-03-03 17:34:49.837567+00	CR		2020-10-23	2022-10-23	t	60	31
43	2022-03-03 17:35:30.72855+00	2022-03-03 17:35:30.728579+00	CR		2021-11-11	2022-11-11	t	67	32
44	2022-03-03 17:36:16.189977+00	2022-03-03 17:36:16.19+00	CR		2021-12-10	2022-12-10	t	82	33
45	2022-03-03 17:37:09.620298+00	2022-03-03 17:37:09.620318+00	CR		2021-09-24	2022-09-24	t	84	34
46	2022-03-03 17:38:05.224916+00	2022-03-03 17:38:05.224942+00	CR		2021-04-15	2022-10-13	t	86	35
47	2022-03-03 17:39:14.163555+00	2022-03-03 17:39:14.163582+00	CR		2021-05-20	2023-05-20	t	131	36
48	2022-03-03 17:40:14.437916+00	2022-03-03 17:40:14.437956+00	CR		2020-10-08	2022-10-08	t	93	37
49	2022-03-03 17:41:53.583605+00	2022-03-03 17:41:53.583626+00	CR		2020-05-29	2022-05-29	t	130	38
50	2022-03-03 17:43:00.112132+00	2022-03-03 17:43:00.112156+00	CR		2021-10-02	2023-10-02	t	90	39
51	2022-03-03 17:44:02.01858+00	2022-03-03 17:44:02.018604+00	CR		2021-01-29	2023-01-29	t	76	40
52	2022-03-03 17:44:43.158026+00	2022-03-03 17:44:43.158054+00	CR		2021-12-17	2022-12-17	t	68	41
53	2022-03-03 17:45:48.867113+00	2022-03-03 17:45:48.867144+00	CR		2021-12-16	2023-12-16	t	77	42
54	2022-03-03 17:46:41.421173+00	2022-03-03 17:46:41.421195+00	CR		2022-02-17	2024-02-17	t	18	17
57	2022-03-06 12:37:17.875357+00	2022-03-06 12:37:17.875392+00	TX		2022-03-01	2022-03-15	t	10	1
\.


--
-- Data for Name: account_customuser; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_customuser (id, password, last_login, is_superuser, first_name, last_name, email, is_staff, is_active, date_joined, username, father_name, birth_date, passport, license_number, type, is_black_listed) FROM stdin;
20	pbkdf2_sha256$260000$Z0S461XcGZz2Zvj7IQrjBc$znTQadOAjg3BPjZ0I+V2VOV9RA6UlVGEWVJebkQDZGo=	\N	f	Рахман	Абдыресулов		f	t	2022-03-03 12:06:49+00	user18	\N	\N	\N	\N	C	f
21	pbkdf2_sha256$260000$LRPYLmD0W0fXO1FrCfoLbK$iT3fyCkg0CjPKEbj13M7uuJbHuAdqP91flJEgTdT8CM=	\N	f	Довлетгельди	Абдиев		f	t	2022-03-03 12:07:29+00	user19	\N	\N	\N	\N	C	f
1	pbkdf2_sha256$260000$hDChMxXLeX4DjlrpXqFmHZ$izEYcGe43bv0gMB9+1hxBB4uqNV8ELt59QKkhNhqur4=	2022-02-24 19:29:10+00	t	Elvin	Seyidov		t	t	2022-02-23 13:24:25+00	alvinseyidov	\N	\N	\N	\N	RTD	f
2	pbkdf2_sha256$260000$PmrY4xGAJ2IqDXzt9gKa7D$rKOEfmxDlor2G4lQN1De9Vb/yfWcWix5lDBuTAgq73I=	\N	f	Farid	Mammadxanli		f	t	2022-02-23 14:28:15+00	farid	\N	\N	\N	\N	S	f
12	pbkdf2_sha256$260000$CG5qiQqtqLVsgt2B9rDnPw$Ph3rD6V3qk2WwBeH+Ky/nxvQupBILAms1idcqcuomzU=	\N	f	Сергей	Бережной		f	t	2022-03-03 09:40:12+00	user10	\N	\N	\N	\N	C	f
22	pbkdf2_sha256$260000$8VplipG7baPUPBCOvshkwB$VGcRmzlbmtF5oH4evrKpWFECvpgNMVeu5ERT6c2uaTI=	\N	f	Юрий	Москалик		f	t	2022-03-03 12:09:08+00	user20	\N	\N	\N	\N	C	f
32	pbkdf2_sha256$260000$Wpq7HBdi4Wzp3ILB1nyDjl$ZDkTg2MWzCoAr48YoD2sgsZaJtt+50d4rUE3Ho65mpk=	\N	f	Сердар	Халилов		f	t	2022-03-03 12:13:00+00	user30	\N	\N	\N	\N	C	f
7	pbkdf2_sha256$260000$srzBEAkB1VoImQKl77a7Ox$skry7bPVOxHbEvnapRBk28giwi5rDCx1NrUqrbq9cww=	\N	f	Бабагелди	Авдиев		f	t	2022-03-03 09:26:52+00	user5	\N	\N	\N	\N	C	f
8	pbkdf2_sha256$260000$8ltOCbNNJL34ZYNpH87cbZ$gnQQRNUeSKGIKkCwAIhOQr4o+94PGikCepctxPM1imQ=	\N	f	Артём	Айрапетов		f	t	2022-03-03 09:34:17+00	user6	\N	\N	\N	\N	C	f
9	pbkdf2_sha256$260000$xrNckhkItK4DnOAcXfQgCp$qqqwhRkVQySGldI5IGMLYaFTlYE2ctl2m6KUcFQpst0=	\N	f	Гурбан	Батыров		f	t	2022-03-03 09:34:51+00	user7	\N	\N	\N	\N	C	f
10	pbkdf2_sha256$260000$us1hHDykUSBbrtduUtqE3q$j79iDmEEKkY7OA81jrvP4n5wlSHqZaai+qhvvYnb0vE=	\N	f	ВЫКУП	(без договора)		f	t	2022-03-03 09:38:59+00	user8	\N	\N	\N	\N	C	f
3	pbkdf2_sha256$260000$Z3y7F1MQMFAouFnTgsLEcJ$zvaMaf8BG/ouK5UzDUKvWtuGhuqWUFaauG9jbBfmeCc=	\N	f	Каюн	Дмитрий		f	t	2022-03-03 09:22:41+00	user1	\N	\N	\N	\N	C	f
13	pbkdf2_sha256$260000$fAbdVu4E1mHHpoB8dfzGp1$GRi3GZ5lw61SJRMPDstFDYu/78sdBLpSHD8Kt/cZ7vQ=	\N	f	Ойвен	Исмайлов		f	t	2022-03-03 09:40:38+00	user11	\N	\N	\N	\N	C	f
14	pbkdf2_sha256$260000$MutI2CiXvVwwAIkWiAoOrp$DBWPOfBK7vniZD37TW9mLeY7gmPANzb70f3wxMSEmLI=	\N	f	Заур	выкуп		f	t	2022-03-03 09:41:42+00	user12	\N	\N	\N	\N	F	f
15	pbkdf2_sha256$260000$qaT6DFj5YYzqevzW8rXhaW$CIYSxFwPQnaCJ5bZxX0zKX2CY2G0DiTbbFuvOTDOc98=	\N	f	Олег	Лаптев		f	t	2022-03-03 09:43:12+00	user13	\N	\N	\N	\N	C	f
16	pbkdf2_sha256$260000$36eiypWJMLTwN0Co1mXqkm$xUfhoEEGWBLWxinGMHUzvqZ30ol8nzmvNVTC5RGF/0M=	\N	f	Эрхан	Кара		f	t	2022-03-03 09:43:42+00	user14	\N	\N	\N	\N	C	f
17	pbkdf2_sha256$260000$4l0vr7HrAgxeAu2sYfxfyq$mNQ05/3U0OL+q9KCEt1NkAgUeJC2J7Yx4RlLEoU49TM=	\N	f	Азат	Кулиев		f	t	2022-03-03 09:44:24+00	user15	\N	\N	\N	\N	C	f
18	pbkdf2_sha256$260000$1Ax4VV20ic9wb41ROWH8R6$a5Q5LPtxH2QZgzpihLre9n+u/t+YZEy0bQ5y2HRcrYM=	\N	f	Али	выкуп		f	t	2022-03-03 12:05:57+00	user16	\N	\N	\N	\N	C	f
19	pbkdf2_sha256$260000$pVqOXS6JmOP9690ZuEGizX$rvcmQSqwcuwW2ao6Ekhq9qsLhpS1XKqE03jDE0GT4vM=	\N	f	Рустем	Чолиев		f	t	2022-03-03 12:06:16+00	user17	\N	\N	\N	\N	F	f
4	pbkdf2_sha256$260000$YmP4EAOeK85Nbt43rmoX6O$kkEja2hmhvOx8akMdViQzDXVFL4q89dcxep7GGda/h8=	\N	f	Шохрат	Язгулиев		f	t	2022-03-03 09:24:35+00	user2	\N	\N	\N	\N	C	f
23	pbkdf2_sha256$260000$IYQM3rEVutjxEdYPSEYmox$W0YEZK2c0fhR65KIm5aAHogv58E6gqTco82+zcti6Mc=	\N	f	Сергей	Гончаров		f	t	2022-03-03 12:09:37+00	user21	\N	\N	\N	\N	C	f
24	pbkdf2_sha256$260000$l3H9nQwL02dymu45qbpfEA$BykiqUCtVtv0PAwtifBZ0t2UzyaCxfZw0gW5f98nrto=	\N	f	Фарух	Фархатов		f	t	2022-03-03 12:09:58+00	user22	\N	\N	\N	\N	C	f
25	pbkdf2_sha256$260000$bKfo2a1a4J2wBbankuIpYh$2lOqhXdJMOeNot8nfCihzEX1ozdhQPGUnya9hyUthps=	\N	f	Бегенч	Атаджанов		f	t	2022-03-03 12:10:18+00	user23	\N	\N	\N	\N	C	f
26	pbkdf2_sha256$260000$OPIotuxTe65AKApto117XC$iP/RgiBL4O2WTcbx4CmTaMuXSlSOe61isIJEh/23YQ4=	\N	f	Эльвин	Валиев		f	t	2022-03-03 12:10:41+00	user24	\N	\N	\N	\N	C	f
27	pbkdf2_sha256$260000$zLuJVnxssNIcxtnhJ5INyt$67HZZvvvgCLt3TrA7bZv4M47/lCkPCUauneZ1qFaysM=	\N	f	Шамиль	Яхяев		f	t	2022-03-03 12:11:04+00	user25	\N	\N	\N	\N	C	f
28	pbkdf2_sha256$260000$ihtg7XzGiwSI1hQRvQU8FH$iD0cDiDeZMkDaKLoIQ5Mtq9hyxUr32dNQye7kxB5Y84=	\N	f	Мухамметсердар	Оммадов		f	t	2022-03-03 12:11:25+00	user26	\N	\N	\N	\N	C	f
29	pbkdf2_sha256$260000$CirGMt1GxEJBAXsCMt23yZ$ELThSE3oP9SShR//mA9STkb52noL0hR37bNgu/GNBFE=	\N	f	Довлет	Аманов		f	t	2022-03-03 12:11:46+00	user27	\N	\N	\N	\N	C	f
30	pbkdf2_sha256$260000$QQ8L6xj0mNTim9jYlPTaRe$vrW/G3OCQ/wRsG7MmwS0PW9pvVohDioECk7eMi60sc8=	\N	f	Умитджан	Бахтияров		f	t	2022-03-03 12:12:06+00	user28	\N	\N	\N	\N	C	f
31	pbkdf2_sha256$260000$FWf7EMtkH2s8XR5huOdnTh$jc3T4uVjc585+mGM/vWlatIv7n2ZcEZQlBNxAmuIpVU=	\N	f	Сердар	Бердиев		f	t	2022-03-03 12:12:32+00	user29	\N	\N	\N	\N	C	f
5	pbkdf2_sha256$260000$PMgl9hglGtZHFLBHYL0e3H$zq92lbVNr4jEk31aRvF5xPvNOWD0YHaR5I5kb6YszWg=	\N	f	Юнус	Чолиев		f	t	2022-03-03 09:25:38+00	user3	\N	\N	\N	\N	C	f
33	pbkdf2_sha256$260000$stLA0QnruUFpykXWAvMkb9$xoK/QSe0lVt3No6WfEHCLyO9z9iyXiT0WYifqJ4KuZs=	\N	f	Нургелди	Бегишов		f	t	2022-03-03 12:13:19+00	user31	\N	\N	\N	\N	C	f
34	pbkdf2_sha256$260000$pLaqNhesGSOXQaIfWW5TF5$yniTVLyUB9FHg/c7xm26W+/ZV6OP4eSxW+JBH768bGc=	\N	f	Байрам	Аманов		f	t	2022-03-03 12:18:26+00	user32	\N	\N	\N	\N	C	f
35	pbkdf2_sha256$260000$yOsTLXpgr86zBmOuTBvyec$JWaQOs51NzFHgjr701H9fvGzdRYRG1B0VtVOfWGAY8M=	\N	f	Богдан	Голяченко		f	t	2022-03-03 12:18:46+00	user33	\N	\N	\N	\N	C	f
36	pbkdf2_sha256$260000$GmcJBj474tSaKyvy3VnhoJ$Ge/ZAIEPuKkePVxryYXwC+IY9yNBosdtSiEKUXowYvU=	\N	f	Василий	Сладкевич		f	t	2022-03-03 12:19:04+00	user34	\N	\N	\N	\N	C	f
37	pbkdf2_sha256$260000$WLii6bhmdS6NFLw0DVuMmI$JIOM6KV9xfLEPBX/4rs9YxvcB8jP7gD3qchs7hN5g3c=	\N	f	Бегенч	Говкиев		f	t	2022-03-03 12:19:24+00	user35	\N	\N	\N	\N	C	f
38	pbkdf2_sha256$260000$5SDoqX9VY5xSGAWe8Y3NVP$moEth9/WdF2C1eJC5fODicQ8hBzHJ0rw3Z1TiPEo6qE=	\N	f	Александр	Волошин		f	t	2022-03-03 12:19:50+00	user36	\N	\N	\N	\N	C	f
39	pbkdf2_sha256$260000$nEQJX208tfsouO9SJnZoMq$I1DUWxtw5OQ0MOE0sCaaxjjAISoKHHK629qg5/qc9/U=	\N	f	Ярослав	Ященко		f	t	2022-03-03 12:20:11+00	user37	\N	\N	\N	\N	C	f
40	pbkdf2_sha256$260000$dpPIKdmgtgM6PgtzdcQSpo$rMM3XCxKqSLhUVBYNlnnlGIbH3FqFV1tNSqBoP+F4uI=	\N	f	МОЙСИК			f	t	2022-03-03 12:20:35+00	user38	\N	\N	\N	\N	C	f
6	pbkdf2_sha256$260000$7qE7sjcxbs32ssC0OirUwm$XWzmTWzlKNXYK+454lmVcJuQFvLYDNwJDruqcHh7OwI=	\N	f	Мукхриддин	Акрамов		f	t	2022-03-03 09:26:06+00	user4	\N	\N	\N	\N	C	f
11	pbkdf2_sha256$260000$zcc5XNbO44w23sDSONnJHB$WqSxcPsQuIhMbwUQ5SErMjSo3YvR4qzgOmHnItyvEEw=	\N	f	Леонид	Романчук		f	t	2022-03-03 09:39:40+00	user9	\N	\N	\N	\N	C	f
41	pbkdf2_sha256$260000$SSSYu7NxUuAh0a7V1GCdWb$SYI9/51RK6uTZ//pWarWkhIQsjX2AH+XZULoYzGb+uE=	\N	f	Шохрат	Пенджиев		f	t	2022-03-03 12:20:59+00	user39	\N	\N	\N	\N	C	f
42	pbkdf2_sha256$260000$hZYSADs7pdTkNsGkXXo7H8$SbLzwOSrbTXDksu+5Gg/OEoLKZSv4xswC32bVmbRFDY=	\N	f	Савченко	Юрий		f	t	2022-03-03 12:21:30+00	user40	\N	\N	\N	\N	C	f
\.


--
-- Data for Name: account_customuser_groups; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_customuser_groups (id, customuser_id, group_id) FROM stdin;
\.


--
-- Data for Name: account_customuser_user_permissions; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_customuser_user_permissions (id, customuser_id, permission_id) FROM stdin;
\.


--
-- Data for Name: account_licenseimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_licenseimage (id, created_at, updated_at, path, is_main, user_id) FROM stdin;
\.


--
-- Data for Name: account_passportimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_passportimage (id, created_at, updated_at, path, is_main, user_id) FROM stdin;
\.


--
-- Data for Name: account_userimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_userimage (id, created_at, updated_at, path, is_main, user_id) FROM stdin;
\.


--
-- Data for Name: account_userphone; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_userphone (id, created_at, updated_at, number, note, is_main, user_id) FROM stdin;
1	2022-02-23 13:33:41.144924+00	2022-02-23 13:33:41.144944+00	0933936397	\N	f	1
2	2022-02-23 14:28:15.164109+00	2022-02-23 14:28:15.16413+00	0509809228	\N	f	2
3	2022-03-03 09:22:41.916224+00	2022-03-03 09:22:41.916251+00	098-757-17-67	\N	f	3
4	2022-03-03 09:24:35.799749+00	2022-03-03 09:24:35.799772+00	063-430-63-73	\N	f	4
5	2022-03-03 09:25:38.750653+00	2022-03-03 09:25:38.750683+00	095-770-37-95	\N	f	5
6	2022-03-03 09:26:06.554101+00	2022-03-03 09:26:06.554125+00	097-520-62-95	\N	f	6
7	2022-03-03 09:26:52.604399+00	2022-03-03 09:26:52.604422+00	066-420-33-81	\N	f	7
8	2022-03-03 09:34:17.669564+00	2022-03-03 09:34:17.669597+00	067-743-27-19	\N	f	8
9	2022-03-03 09:34:51.623832+00	2022-03-03 09:34:51.623854+00	093-211-60-14	\N	f	9
10	2022-03-03 09:40:12.405574+00	2022-03-03 09:40:12.405596+00	063-890-08-79	\N	f	12
11	2022-03-03 09:40:38.663283+00	2022-03-03 09:40:38.663309+00	073-040-40-45	\N	f	13
12	2022-03-03 09:43:42.265544+00	2022-03-03 09:43:42.265567+00	066-876-00-50	\N	f	16
13	2022-03-03 09:44:24.617577+00	2022-03-03 09:44:24.617605+00	063-070-08-25	\N	f	17
14	2022-03-03 12:06:16.890485+00	2022-03-03 12:06:16.890507+00	063-503-76-01	\N	f	19
15	2022-03-03 12:06:49.393308+00	2022-03-03 12:06:49.393331+00	093-035-20-02	\N	f	20
16	2022-03-03 12:07:29.468975+00	2022-03-03 12:07:29.469003+00	066-990-98-98	\N	f	21
17	2022-03-03 12:09:08.817833+00	2022-03-03 12:09:08.817854+00	063-828-39-44	\N	f	22
18	2022-03-03 12:09:37.69763+00	2022-03-03 12:09:37.697652+00	098-201-201-2	\N	f	23
19	2022-03-03 12:09:58.368308+00	2022-03-03 12:09:58.36833+00	063-895-58-62	\N	f	24
20	2022-03-03 12:10:19.095685+00	2022-03-03 12:10:19.095717+00	063-597-01-21	\N	f	25
21	2022-03-03 12:11:04.819665+00	2022-03-03 12:11:04.819689+00	095-383-08-41	\N	f	27
22	2022-03-03 12:11:25.237654+00	2022-03-03 12:11:25.237674+00	095-706-07-97	\N	f	28
23	2022-03-03 12:12:07.086708+00	2022-03-03 12:12:07.086729+00	067-282-05-82	\N	f	30
24	2022-03-03 12:12:32.508585+00	2022-03-03 12:12:32.508606+00	066-919-32-97	\N	f	31
25	2022-03-03 12:12:32.509339+00	2022-03-03 12:12:32.509353+00	063-198-48-65	\N	f	31
26	2022-03-03 12:13:01.083696+00	2022-03-03 12:13:01.083718+00	063-864-15-41	\N	f	32
27	2022-03-03 12:13:19.385774+00	2022-03-03 12:13:19.3858+00	093-381-18-52	\N	f	33
28	2022-03-03 12:18:26.923884+00	2022-03-03 12:18:26.923906+00	093-050-25-02	\N	f	34
29	2022-03-03 12:18:47.107238+00	2022-03-03 12:18:47.107259+00	096-676-71-02	\N	f	35
30	2022-03-03 12:19:04.586595+00	2022-03-03 12:19:04.586618+00	098-464-78-33	\N	f	36
31	2022-03-03 12:19:25.088238+00	2022-03-03 12:19:25.088263+00	093-066-56-60	\N	f	37
32	2022-03-03 12:19:50.955015+00	2022-03-03 12:19:50.955038+00	063-668-93-28	\N	f	38
33	2022-03-03 12:20:11.597065+00	2022-03-03 12:20:11.597105+00	095-070-37-32	\N	f	39
34	2022-03-03 12:20:59.228402+00	2022-03-03 12:20:59.228427+00	063-509-64-70	\N	f	41
35	2022-03-03 12:20:59.229144+00	2022-03-03 12:20:59.229157+00	093-422-54-64	\N	f	41
36	2022-03-03 12:21:30.849227+00	2022-03-03 12:21:30.849258+00	093-515-15-95	\N	f	42
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add Token	6	add_token
22	Can change Token	6	change_token
23	Can delete Token	6	delete_token
24	Can view Token	6	view_token
25	Can add token	7	add_tokenproxy
26	Can change token	7	change_tokenproxy
27	Can delete token	7	delete_tokenproxy
28	Can view token	7	view_tokenproxy
29	Can add user	8	add_customuser
30	Can change user	8	change_customuser
31	Can delete user	8	delete_customuser
32	Can view user	8	view_customuser
33	Can add contract	9	add_contract
34	Can change contract	9	change_contract
35	Can delete contract	9	delete_contract
36	Can view contract	9	view_contract
37	Can add user phone	10	add_userphone
38	Can change user phone	10	change_userphone
39	Can delete user phone	10	delete_userphone
40	Can view user phone	10	view_userphone
41	Can add user image	11	add_userimage
42	Can change user image	11	change_userimage
43	Can delete user image	11	delete_userimage
44	Can view user image	11	view_userimage
45	Can add passport image	12	add_passportimage
46	Can change passport image	12	change_passportimage
47	Can delete passport image	12	delete_passportimage
48	Can view passport image	12	view_passportimage
49	Can add license image	13	add_licenseimage
50	Can change license image	13	change_licenseimage
51	Can delete license image	13	delete_licenseimage
52	Can view license image	13	view_licenseimage
53	Can add brand	14	add_brand
54	Can change brand	14	change_brand
55	Can delete brand	14	delete_brand
56	Can view brand	14	view_brand
57	Can add car	15	add_car
58	Can change car	15	change_car
59	Can delete car	15	delete_car
60	Can view car	15	view_car
61	Can add car class	16	add_carclass
62	Can change car class	16	change_carclass
63	Can delete car class	16	delete_carclass
64	Can view car class	16	view_carclass
65	Can add car image	17	add_carimage
66	Can change car image	17	change_carimage
67	Can delete car image	17	delete_carimage
68	Can view car image	17	view_carimage
69	Can add case	18	add_case
70	Can change case	18	change_case
71	Can delete case	18	delete_case
72	Can view case	18	view_case
73	Can add color	19	add_color
74	Can change color	19	change_color
75	Can delete color	19	delete_color
76	Can view color	19	view_color
77	Can add fuel	20	add_fuel
78	Can change fuel	20	change_fuel
79	Can delete fuel	20	delete_fuel
80	Can view fuel	20	view_fuel
81	Can add office	21	add_office
82	Can change office	21	change_office
83	Can delete office	21	delete_office
84	Can view office	21	view_office
85	Can add model	22	add_model
86	Can change model	22	change_model
87	Can delete model	22	delete_model
88	Can view model	22	view_model
89	Can add car paper	23	add_carpaper
90	Can change car paper	23	change_carpaper
91	Can delete car paper	23	delete_carpaper
92	Can view car paper	23	view_carpaper
93	Can add car image task	24	add_carimagetask
94	Can change car image task	24	change_carimagetask
95	Can delete car image task	24	delete_carimagetask
96	Can view car image task	24	view_carimagetask
97	Can add administrative deposit	25	add_administrativedeposit
98	Can change administrative deposit	25	change_administrativedeposit
99	Can delete administrative deposit	25	delete_administrativedeposit
100	Can view administrative deposit	25	view_administrativedeposit
101	Can add administrative withdraw	26	add_administrativewithdraw
102	Can change administrative withdraw	26	change_administrativewithdraw
103	Can delete administrative withdraw	26	delete_administrativewithdraw
104	Can view administrative withdraw	26	view_administrativewithdraw
105	Can add Currency	27	add_currency
106	Can change Currency	27	change_currency
107	Can delete Currency	27	delete_currency
108	Can view Currency	27	view_currency
109	Can add balance	28	add_balance
110	Can change balance	28	change_balance
111	Can delete balance	28	delete_balance
112	Can view balance	28	view_balance
113	Can add credit	29	add_credit
114	Can change credit	29	change_credit
115	Can delete credit	29	delete_credit
116	Can view credit	29	view_credit
117	Can add credit payment	30	add_creditpayment
118	Can change credit payment	30	change_creditpayment
119	Can delete credit payment	30	delete_creditpayment
120	Can view credit payment	30	view_creditpayment
121	Can add credit image	31	add_creditimage
122	Can change credit image	31	change_creditimage
123	Can delete credit image	31	delete_creditimage
124	Can view credit image	31	view_creditimage
125	Can add company	32	add_company
126	Can change company	32	change_company
127	Can delete company	32	delete_company
128	Can view company	32	view_company
129	Can add revenue	33	add_revenue
130	Can change revenue	33	change_revenue
131	Can delete revenue	33	delete_revenue
132	Can view revenue	33	view_revenue
133	Can add taxi	34	add_taxi
134	Can change taxi	34	change_taxi
135	Can delete taxi	34	delete_taxi
136	Can view taxi	34	view_taxi
137	Can add revenue item	35	add_revenueitem
138	Can change revenue item	35	change_revenueitem
139	Can delete revenue item	35	delete_revenueitem
140	Can view revenue item	35	view_revenueitem
141	Can add rent taxi	36	add_renttaxi
142	Can change rent taxi	36	change_renttaxi
143	Can delete rent taxi	36	delete_renttaxi
144	Can view rent taxi	36	view_renttaxi
145	Can add revenue	37	add_revenue
146	Can change revenue	37	change_revenue
147	Can delete revenue	37	delete_revenue
148	Can view revenue	37	view_revenue
149	Can add revenue item	38	add_revenueitem
150	Can change revenue item	38	change_revenueitem
151	Can delete revenue item	38	delete_revenueitem
152	Can view revenue item	38	view_revenueitem
153	Can add shtraf	39	add_shtraf
154	Can change shtraf	39	change_shtraf
155	Can delete shtraf	39	delete_shtraf
156	Can view shtraf	39	view_shtraf
157	Can add shtraf image	40	add_shtrafimage
158	Can change shtraf image	40	change_shtrafimage
159	Can delete shtraf image	40	delete_shtrafimage
160	Can view shtraf image	40	view_shtrafimage
161	Can add debt	41	add_debt
162	Can change debt	41	change_debt
163	Can delete debt	41	delete_debt
164	Can view debt	41	view_debt
165	Can add debt image	42	add_debtimage
166	Can change debt image	42	change_debtimage
167	Can delete debt image	42	delete_debtimage
168	Can view debt image	42	view_debtimage
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
990c9d166decaad5c05138bd06d12e60267d0a06	2022-02-23 13:40:33.157976+00	1
\.


--
-- Data for Name: car_brand; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_brand (id, name, logo) FROM stdin;
1	Hyundai	\N
2	BMW	\N
3	Toyota	\N
4	Skoda	\N
5	Volkswagen	\N
6	Kia	\N
7	Mazda	\N
8	Opel	\N
9	Subaru	\N
\.


--
-- Data for Name: car_car; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_car (id, created_at, updated_at, number, year, vin_code, is_available, is_active, transmission, fuel_tank, doors, seats, wheel_size, current_tyre, bought_date, bought_price, current_km, oil_km_limit, climate, climate_control, is_verified, reserve_key, advertise_id, car_class_id, case_id, color_id, fuel_id, model_id, office_id, health_status, status, driver_id) FROM stdin;
11	2022-03-01 19:46:19.885957+00	2022-03-01 22:28:41.847048+00	КА 3676 ВО	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	2	1	G	F	\N
10	2022-02-23 13:28:39.610487+00	2022-03-01 22:28:24.841385+00	АІ 8243 OВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	2	1	G	F	\N
23	2022-03-01 20:23:42.931269+00	2022-03-02 21:11:53.071068+00	АІ 5508 МР	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	C	\N
9	2022-02-23 13:28:26.857143+00	2022-03-01 22:28:16.772768+00	АІ 8064 MO	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	2	1	G	F	\N
8	2022-02-23 13:28:06.234911+00	2022-03-01 22:28:05.359071+00	AE 7193 KO	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	1	1	G	F	\N
17	2022-03-01 20:21:19.071816+00	2022-03-02 21:13:00.531122+00	АІ 7462 ОВ	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	4	1	G	C	\N
7	2022-02-23 13:27:53.89819+00	2022-03-01 22:27:54.024694+00	КA 2219 ВВ	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	1	1	G	F	\N
2	2022-02-23 13:26:34.993018+00	2022-03-01 22:29:48.809244+00	АІ 0312 MX	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	1	1	G	F	\N
1	2022-02-23 13:26:18.450592+00	2022-03-01 22:29:58.827331+00	АІ 0315 MX	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	1	1	G	F	\N
45	2022-03-01 20:32:52.721982+00	2022-03-02 21:12:22.264289+00	AІ 7365 МI	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	C	\N
12	2022-03-01 19:46:43.548763+00	2022-03-01 22:28:56.595009+00	KA 9980 AT	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	2	1	G	F	\N
14	2022-03-01 20:19:42.701591+00	2022-03-02 21:11:49.253775+00	AI 3839 ОС	2002	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	3	1	G	C	\N
25	2022-03-01 20:24:27.524002+00	2022-03-01 22:41:50.922124+00	ВМ 0610 СМ	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
4	2022-02-23 13:27:00.29139+00	2022-03-01 22:29:26.506814+00	АI 8250 OB	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	1	1	G	F	\N
26	2022-03-01 20:24:41.292642+00	2022-03-01 22:42:48.054799+00	KA 0514 ВН	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
27	2022-03-01 20:24:58.633026+00	2022-03-01 22:42:54.113973+00	KA 0574 ВМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
29	2022-03-01 20:25:34.365363+00	2022-03-01 22:43:07.626094+00	KA 2853 ВВ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
30	2022-03-01 20:25:51.309237+00	2022-03-01 22:43:16.605722+00	KA 3564 ВI	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
31	2022-03-01 20:26:03.409279+00	2022-03-01 22:43:23.186106+00	KA 5754 ВI	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
32	2022-03-01 20:27:19.317995+00	2022-03-01 22:43:29.224678+00	KA 9134 ВЕ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
33	2022-03-01 20:27:52.029885+00	2022-03-01 22:43:35.934438+00	KA 9236 ВЕ	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
34	2022-03-01 20:28:05.42193+00	2022-03-01 22:43:41.888632+00	KA 9864 AO	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
22	2022-03-01 20:23:30.79391+00	2022-03-01 22:43:56.754282+00	АІ 3605 ОС	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
24	2022-03-01 20:23:59.29255+00	2022-03-01 22:44:02.946507+00	ВМ 0588 СМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
43	2022-03-01 20:32:27.115632+00	2022-03-02 21:11:56.657144+00	АІ 6703 МР	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	C	\N
20	2022-03-01 20:22:36.413883+00	2022-03-01 22:45:36.664573+00	АІ 9423 ІМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	4	1	G	F	\N
21	2022-03-01 20:22:55.244143+00	2022-03-01 22:45:30.73807+00	КА 2026 ВВ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	4	1	G	F	\N
19	2022-03-01 20:22:22.556291+00	2022-03-01 22:45:43.671066+00	АІ 8891 ІК	2007	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	4	1	G	F	\N
60	2022-03-01 21:25:58.933691+00	2022-03-02 21:13:04.353702+00	АІ 6723 ІН	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	8	1	G	C	\N
16	2022-03-01 20:21:01.378+00	2022-03-01 22:46:23.689147+00	АІ 2541 КО	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	4	1	G	F	\N
15	2022-03-01 20:20:43.075461+00	2022-03-01 22:46:39.979182+00	AI 6531 OC	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	5	1	G	F	\N
55	2022-03-01 21:22:38.910492+00	2022-03-02 21:12:25.324257+00	АІ 7166 ІМ	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	7	1	G	C	\N
46	2022-03-01 20:33:07.564279+00	2022-03-01 22:47:39.04383+00	AІ 7453 ОВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
47	2022-03-01 20:36:02.526874+00	2022-03-01 22:47:47.36043+00	AІ 7645 МТ	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
48	2022-03-01 20:36:16.929474+00	2022-03-01 22:47:55.442164+00	AІ 8132 МТ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
49	2022-03-01 20:36:31.563805+00	2022-03-01 22:48:11.048976+00	AІ 8829 ММ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
50	2022-03-01 20:36:58.856062+00	2022-03-01 22:48:18.149887+00	AМ 6023 ЕТ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
51	2022-03-01 20:37:11.395975+00	2022-03-01 22:48:25.674333+00	KA 6328 ВI	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
6	2022-02-23 13:27:42.245522+00	2022-03-02 21:12:57.103022+00	КА 0729 АР	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	1	1	G	C	\N
13	2022-03-01 20:16:18.827728+00	2022-03-02 21:11:45.315181+00	AІ 9149 МН	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	2	1	G	C	\N
58	2022-03-01 21:25:07.644675+00	2022-03-02 21:12:15.587593+00	АА 4934 ВР	2010	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	8	1	G	C	\N
36	2022-03-01 20:28:50.703095+00	2022-03-01 22:49:21.573409+00	АІ 0368 MX	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
37	2022-03-01 20:29:32.747683+00	2022-03-01 22:49:28.774149+00	АІ 3159 MМ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
38	2022-03-01 20:29:52.643421+00	2022-03-01 22:49:44.898545+00	АІ 3167 MМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
39	2022-03-01 20:30:09.81938+00	2022-03-01 22:49:52.888928+00	АІ 3168 MМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
40	2022-03-01 20:30:25.05629+00	2022-03-01 22:49:59.954598+00	АІ 3351 MA	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
41	2022-03-01 20:30:39.529455+00	2022-03-01 22:50:08.796815+00	АІ 3422 MР	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
42	2022-03-01 20:32:12.08395+00	2022-03-01 22:50:20.897803+00	АІ 6304 МР	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
65	2022-03-01 21:36:22.360222+00	2022-03-02 21:12:00.144018+00	AI 0712 OВ	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	11	1	G	C	\N
44	2022-03-01 20:32:39.090013+00	2022-03-01 22:51:01.465909+00	АІ 6896 ОВ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
75	2022-03-01 21:40:42.392485+00	2022-03-01 22:52:25.639949+00	АІ 7652 MТ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	F	\N
69	2022-03-01 21:38:21.949365+00	2022-03-01 22:56:14.547944+00	АІ 0724 MІ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	F	\N
54	2022-03-01 21:22:18.728738+00	2022-03-02 21:12:04.423534+00	АІ 2803 СС	2013	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	7	1	G	C	\N
66	2022-03-01 21:37:30.978341+00	2022-03-01 22:55:34.638097+00	AI 3724 OС	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	12	1	G	F	\N
68	2022-03-01 21:38:10.07538+00	2022-03-02 21:13:40.671847+00	АІ 0589 MІ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	C	\N
18	2022-03-01 20:21:54.717071+00	2022-03-02 21:13:48.391784+00	АІ 6453 ОО	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	4	1	G	C	\N
70	2022-03-01 21:38:34.839039+00	2022-03-01 22:56:49.25237+00	АІ 3620 MP	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	F	\N
71	2022-03-01 21:38:48.293285+00	2022-03-01 22:56:59.631204+00	АІ 5563 MP	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	F	\N
72	2022-03-01 21:39:13.709312+00	2022-03-01 22:57:21.201357+00	АІ 5564 MP	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	F	\N
73	2022-03-01 21:40:07.8131+00	2022-03-01 22:57:31.03064+00	АІ 7045 MP	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	F	\N
74	2022-03-01 21:40:23.256218+00	2022-03-01 22:57:46.028547+00	АІ 7328 MР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	F	\N
56	2022-03-01 21:24:33.336905+00	2022-03-02 21:12:28.546727+00	АА 3967 ТТ	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	8	1	G	C	\N
64	2022-03-01 21:35:59.11692+00	2022-03-02 21:12:46.016615+00	AI 5266 OO	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	C	\N
57	2022-03-01 21:24:53.715485+00	2022-03-01 22:58:27.688954+00	АА 4421 ЕІ	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	8	1	G	F	\N
3	2022-02-23 13:26:48.662705+00	2022-03-02 21:12:19.412761+00	АI 4463 MC	2019	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	1	1	G	C	\N
52	2022-03-01 21:20:03.010299+00	2022-03-02 21:12:49.43949+00	АА 8037 РР	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	7	1	G	C	\N
67	2022-03-01 21:37:57.061228+00	2022-03-02 21:13:07.63657+00	AI 9766 OA	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	12	1	G	C	\N
62	2022-03-01 21:28:03.192083+00	2022-03-01 22:59:55.371148+00	АI 3439 MP	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	9	1	G	F	\N
63	2022-03-01 21:28:17.418829+00	2022-03-01 23:00:04.022641+00	КА 0345 ВМ	2010	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	9	1	G	F	\N
53	2022-03-01 21:20:19.859002+00	2022-03-02 21:11:22.452111+00	АА 8986 СС	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	7	1	G	C	\N
107	2022-03-01 21:56:58.212519+00	2022-03-01 23:06:27.752658+00	АI 5691 MP	2013	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
28	2022-03-01 20:25:14.381425+00	2022-03-01 22:43:00.501613+00	KA 2816 ВВ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
35	2022-03-01 20:28:20.704567+00	2022-03-01 22:49:14.612441+00	KA 9865 AO	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
83	2022-03-01 21:49:00.248677+00	2022-03-02 21:11:15.375338+00	KA 0728 AP	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	18	1	G	C	\N
79	2022-03-01 21:44:35.786749+00	2022-03-01 22:53:51.078283+00	АА 1066 СС	2007	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	15	1	G	F	\N
61	2022-03-01 21:26:47.320921+00	2022-03-02 21:12:39.155489+00	АІ 2258 MМ	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	9	1	G	C	\N
87	2022-03-01 21:51:09.056853+00	2022-03-02 21:12:53.618547+00	КА 3306 ВМ	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	19	1	G	C	\N
84	2022-03-01 21:49:56.468223+00	2022-03-02 21:13:15.82439+00	АЕ 9200 IM	2013	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	18	1	G	C	\N
88	2022-03-01 21:51:24.947691+00	2022-03-02 21:11:36.888423+00	СА 3701 СК	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	19	1	G	C	\N
86	2022-03-01 21:50:48.13225+00	2022-03-02 21:13:19.79774+00	АІ 1893 ІІ	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	19	1	G	C	\N
125	2022-03-01 22:01:40.316635+00	2022-03-01 23:02:38.139179+00	КА 8962 ВМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
94	2022-03-01 21:53:09.020769+00	2022-03-01 23:02:45.339992+00	АI 0253 МР	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
130	2022-03-01 22:02:47.225411+00	2022-03-02 21:13:30.308182+00	АI 3427 MP	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	C	\N
92	2022-03-01 21:52:40.628802+00	2022-03-01 23:03:00.979744+00	АI 6809 ОА	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
80	2022-03-01 21:45:50.769643+00	2022-03-02 21:12:12.175193+00	АІ 8006 МА	2013	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	15	1	G	C	\N
76	2022-03-01 21:40:56.303129+00	2022-03-02 21:13:37.368329+00	АІ 8089 MI	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	C	\N
89	2022-03-01 21:51:51.112186+00	2022-03-01 23:03:29.968619+00	АI 6782 ОА	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
59	2022-03-01 21:25:21.946529+00	2022-03-02 21:12:31.736353+00	АА 6079 СС	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	8	1	G	C	\N
82	2022-03-01 21:48:34.887621+00	2022-03-02 21:13:11.151385+00	AА 1556 ТT	2004	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	17	1	G	C	\N
104	2022-03-01 21:56:11.501856+00	2022-03-01 23:03:59.846843+00	АI 3922 MТ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
103	2022-03-01 21:55:49.268453+00	2022-03-01 23:04:08.330599+00	АI 3702 MP	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
102	2022-03-01 21:55:37.421868+00	2022-03-01 23:04:16.511144+00	АI 2953 ММ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
101	2022-03-01 21:55:24.310133+00	2022-03-01 23:04:25.504033+00	АI 2875 ММ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
100	2022-03-01 21:54:42.795693+00	2022-03-01 23:04:35.062836+00	АI 2846 МР	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
99	2022-03-01 21:54:19.85852+00	2022-03-01 23:04:43.577069+00	АI 2519 МР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
98	2022-03-01 21:54:05.788776+00	2022-03-01 23:04:51.313894+00	АI 2518 МР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
97	2022-03-01 21:53:52.987791+00	2022-03-01 23:04:58.710368+00	АI 1256 ММ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
96	2022-03-01 21:53:38.218569+00	2022-03-01 23:05:07.585712+00	АI 0962 МB	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
81	2022-03-01 21:46:15.596364+00	2022-03-02 21:12:42.830693+00	AI 1495 MO	2006	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	16	1	G	C	\N
114	2022-03-01 21:59:01.980033+00	2022-03-01 23:05:23.173259+00	АI 9681 МР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
113	2022-03-01 21:58:50.045624+00	2022-03-01 23:05:36.566304+00	АI 9364 МР	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
112	2022-03-01 21:58:36.208945+00	2022-03-01 23:05:45.089211+00	АI 9361 МР	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
111	2022-03-01 21:58:22.602034+00	2022-03-01 23:05:52.51975+00	АI 9104 МР	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
110	2022-03-01 21:58:02.065732+00	2022-03-01 23:06:02.049526+00	АI 8861 MТ	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
109	2022-03-01 21:57:20.344383+00	2022-03-01 23:06:10.48921+00	АI 7923 MТ	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
108	2022-03-01 21:57:06.363266+00	2022-03-01 23:06:19.488344+00	АI 6429 MP	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
123	2022-03-01 22:01:15.043877+00	2022-03-01 23:07:40.724541+00	КА 3426 ВМ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
106	2022-03-01 21:56:42.76448+00	2022-03-01 23:06:50.585265+00	АI 5684 MP	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
105	2022-03-01 21:56:25.296676+00	2022-03-01 23:07:01.876874+00	АI 4668 МЕ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
115	2022-03-01 21:59:16.702716+00	2022-03-01 23:07:17.128847+00	АI 9702 МР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
124	2022-03-01 22:01:28.150868+00	2022-03-01 23:07:24.544085+00	КА 6210 ET	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
122	2022-03-01 22:00:57.944905+00	2022-03-01 23:07:59.950846+00	КА 2641 ВВ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
121	2022-03-01 22:00:46.190102+00	2022-03-01 23:08:08.56011+00	КА 2479 ВВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
120	2022-03-01 22:00:28.656404+00	2022-03-01 23:08:21.519111+00	КА 0941 ВМ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
119	2022-03-01 22:00:15.762339+00	2022-03-01 23:08:32.78773+00	КА 0583 ВM	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
118	2022-03-01 22:00:03.059744+00	2022-03-01 23:08:38.996447+00	ВМ 0584 СМ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
117	2022-03-01 21:59:50.274519+00	2022-03-01 23:08:45.824246+00	ВА 7546 ЕМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
116	2022-03-01 21:59:37.066433+00	2022-03-01 23:09:01.044852+00	АI 9814 МВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
134	2022-03-01 22:04:58.074612+00	2022-03-01 23:09:22.638359+00	AI 8500 MT	2007	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	22	1	G	F	\N
133	2022-03-01 22:04:32.031175+00	2022-03-03 00:08:07.410201+00	AI 7103 OB	2011	\N	t	t	A	\N	\N	\N	\N	W	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	21	1	G	RTD	1
132	2022-03-01 22:04:05.760465+00	2022-03-01 23:09:40.433161+00	АI 0886 KP	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
93	2022-03-01 21:52:53.191374+00	2022-03-02 21:13:26.563036+00	АМ 3560 ЕО	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	C	\N
90	2022-03-01 21:52:05.281037+00	2022-03-02 21:13:33.790893+00	АI 6786 ОА	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	C	\N
129	2022-03-01 22:02:35.106282+00	2022-03-01 23:10:00.24345+00	АI 8894 ММ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
128	2022-03-01 22:02:22.3944+00	2022-03-01 23:10:08.215937+00	СА 5408 ІВ	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
127	2022-03-01 22:02:07.985132+00	2022-03-01 23:10:15.204995+00	СА 5407 ІВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
5	2022-02-23 13:27:14.669674+00	2022-03-02 21:11:41.456464+00	АI 9756 IH	2010	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	1	1	G	C	\N
78	2022-03-01 21:42:01.261454+00	2022-03-02 21:11:04.539192+00	KА 6164 EC	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	14	1	G	C	\N
85	2022-03-01 21:50:31.264483+00	2022-03-02 21:11:27.556108+00	АА 6038 РР	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	19	1	G	C	\N
126	2022-03-01 22:01:54.365927+00	2022-03-02 21:11:32.996578+00	КА 9531 ВК	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	C	\N
91	2022-03-01 21:52:27.493327+00	2022-03-02 21:12:08.636347+00	АI 6805 ОА	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	C	\N
95	2022-03-01 21:53:22.705699+00	2022-03-02 21:12:34.975456+00	АI 0954 МB	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	C	\N
131	2022-03-01 22:03:50.10662+00	2022-03-02 21:13:23.275832+00	АА 1058 ЕМ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	C	\N
77	2022-03-01 21:41:37.183756+00	2022-03-02 21:13:44.199847+00	AI 2111 MB	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	13	1	G	C	\N
\.


--
-- Data for Name: car_carclass; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carclass (id, name) FROM stdin;
\.


--
-- Data for Name: car_carimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carimage (id, created_at, updated_at, path, is_main, car_id) FROM stdin;
7	2022-02-23 13:54:58.366175+00	2022-03-01 22:27:32.987129+00	cars/hyundai.jpg	f	5
6	2022-02-23 13:54:51.534767+00	2022-03-01 22:27:43.953897+00	cars/hyundai_f8bw2XT.jpg	f	6
5	2022-02-23 13:54:43.656928+00	2022-03-01 22:27:54.025948+00	cars/hyundai_bn77cc0.jpg	f	7
4	2022-02-23 13:54:33.306099+00	2022-03-01 22:28:05.360417+00	cars/hyundai_k9clxiM.jpg	f	8
2	2022-02-23 13:53:58.256381+00	2022-03-01 22:28:16.774671+00	cars/hyundai_M2r6Kj9.jpg	f	9
1	2022-02-23 13:53:50.051592+00	2022-03-01 22:28:24.842755+00	cars/hyundai_BaHtyzU.jpg	f	10
11	2022-03-01 22:28:41.848392+00	2022-03-01 22:28:41.84841+00	cars/hyundai_GAO4yEE.jpg	f	11
12	2022-03-01 22:28:56.596485+00	2022-03-01 22:28:56.596504+00	cars/hyundai_bKF3N4U.jpg	f	12
13	2022-03-01 22:29:07.215691+00	2022-03-01 22:29:07.21571+00	cars/hyundai_UmmCCxm.jpg	f	13
10	2022-02-23 14:02:42.091759+00	2022-03-01 22:29:26.508395+00	cars/hyundai_Ho9OKsz.jpg	f	4
8	2022-02-23 13:55:14.015669+00	2022-03-01 22:29:35.628842+00	cars/hyundai_LwBv52O.jpg	f	3
9	2022-02-23 13:55:24.895688+00	2022-03-01 22:29:48.810615+00	cars/hyundai_AizHiOH.jpg	f	2
3	2022-02-23 13:54:11.325309+00	2022-03-01 22:29:58.828783+00	cars/hyundai_gxC2C14.jpg	f	1
14	2022-03-01 22:29:16.548086+00	2022-03-01 22:40:59.595545+00	cars/bmw.jpg	f	14
15	2022-03-01 22:41:50.923778+00	2022-03-01 22:41:50.923798+00	cars/hyundai_qBZHcI8.jpg	f	25
16	2022-03-01 22:42:48.056383+00	2022-03-01 22:42:48.056401+00	cars/hyundai_Als1Bt1.jpg	f	26
17	2022-03-01 22:42:54.115739+00	2022-03-01 22:42:54.115761+00	cars/hyundai_DceXF25.jpg	f	27
18	2022-03-01 22:43:00.503434+00	2022-03-01 22:43:00.503454+00	cars/hyundai_S05no23.jpg	f	28
19	2022-03-01 22:43:07.627926+00	2022-03-01 22:43:07.627948+00	cars/hyundai_9sq43qY.jpg	f	29
20	2022-03-01 22:43:16.607529+00	2022-03-01 22:43:16.607548+00	cars/hyundai_ZpAMAW8.jpg	f	30
21	2022-03-01 22:43:23.187595+00	2022-03-01 22:43:23.187615+00	cars/hyundai_VqdIO6c.jpg	f	31
22	2022-03-01 22:43:29.226048+00	2022-03-01 22:43:29.226066+00	cars/hyundai_5XwGShM.jpg	f	32
23	2022-03-01 22:43:35.935803+00	2022-03-01 22:43:35.935821+00	cars/hyundai_6pGpVRJ.jpg	f	33
24	2022-03-01 22:43:41.889888+00	2022-03-01 22:43:41.889905+00	cars/hyundai_TLkFKQz.jpg	f	34
25	2022-03-01 22:43:56.755811+00	2022-03-01 22:43:56.755849+00	cars/hyundai_FRA7Xup.jpg	f	22
26	2022-03-01 22:44:02.94792+00	2022-03-01 22:44:02.947941+00	cars/hyundai_WcjLp3Y.jpg	f	24
27	2022-03-01 22:44:12.349298+00	2022-03-01 22:44:12.349317+00	cars/hyundai_RhMO0Wu.jpg	f	23
28	2022-03-01 22:44:29.073327+00	2022-03-01 22:45:30.739608+00	cars/toyota.jpg	f	21
29	2022-03-01 22:45:36.666053+00	2022-03-01 22:45:36.666071+00	cars/toyota_B1Y6i1K.jpg	f	20
30	2022-03-01 22:45:43.672454+00	2022-03-01 22:45:43.672473+00	cars/toyota_3vYdoOH.jpg	f	19
31	2022-03-01 22:45:53.094809+00	2022-03-01 22:45:53.094828+00	cars/toyota_G56aCd3.jpg	f	18
32	2022-03-01 22:46:17.816121+00	2022-03-01 22:46:17.816142+00	cars/toyota_4TfCQBZ.jpg	f	17
33	2022-03-01 22:46:23.690586+00	2022-03-01 22:46:23.690605+00	cars/toyota_Zbez0L8.jpg	f	16
34	2022-03-01 22:46:39.980494+00	2022-03-01 22:46:39.980512+00	cars/toyota_VRrkisE.jpg	f	15
35	2022-03-01 22:47:32.330637+00	2022-03-01 22:47:32.330653+00	cars/hyundai_QIHj3My.jpg	f	45
36	2022-03-01 22:47:39.045302+00	2022-03-01 22:47:39.04532+00	cars/hyundai_joxHWec.jpg	f	46
37	2022-03-01 22:47:47.361965+00	2022-03-01 22:47:47.361984+00	cars/hyundai_74R59qU.jpg	f	47
38	2022-03-01 22:47:55.443509+00	2022-03-01 22:47:55.443526+00	cars/hyundai_kD0svKQ.jpg	f	48
39	2022-03-01 22:48:11.051008+00	2022-03-01 22:48:11.051028+00	cars/hyundai_41Ksyis.jpg	f	49
40	2022-03-01 22:48:18.151521+00	2022-03-01 22:48:18.15154+00	cars/hyundai_UTsWXFW.jpg	f	50
41	2022-03-01 22:48:25.675828+00	2022-03-01 22:48:25.675846+00	cars/hyundai_KOlkfNo.jpg	f	51
42	2022-03-01 22:48:41.596545+00	2022-03-01 22:48:41.596563+00	cars/skoda-badge.jpg	f	52
43	2022-03-01 22:48:48.561599+00	2022-03-01 22:48:48.561616+00	cars/skoda-badge_23YFQCY.jpg	f	53
44	2022-03-01 22:48:56.198781+00	2022-03-01 22:48:56.198797+00	cars/skoda-badge_KMGvFrl.jpg	f	54
45	2022-03-01 22:49:14.613971+00	2022-03-01 22:49:14.613992+00	cars/hyundai_tqJRXon.jpg	f	35
46	2022-03-01 22:49:21.574872+00	2022-03-01 22:49:21.574891+00	cars/hyundai_8mgzYkG.jpg	f	36
47	2022-03-01 22:49:28.775547+00	2022-03-01 22:49:28.775569+00	cars/hyundai_R2o16RB.jpg	f	37
48	2022-03-01 22:49:44.899859+00	2022-03-01 22:49:44.899876+00	cars/hyundai_rXy36pD.jpg	f	38
49	2022-03-01 22:49:52.890265+00	2022-03-01 22:49:52.890282+00	cars/hyundai_0XOpN9m.jpg	f	39
50	2022-03-01 22:49:59.95607+00	2022-03-01 22:49:59.956087+00	cars/hyundai_ou44D4H.jpg	f	40
51	2022-03-01 22:50:08.798181+00	2022-03-01 22:50:08.798199+00	cars/hyundai_rGqhMWY.jpg	f	41
52	2022-03-01 22:50:20.899406+00	2022-03-01 22:50:20.899436+00	cars/hyundai_tJd9BOu.jpg	f	42
53	2022-03-01 22:50:37.448698+00	2022-03-01 22:50:37.448717+00	cars/hyundai_LTbPW7f.jpg	f	43
54	2022-03-01 22:51:01.467524+00	2022-03-01 22:51:01.467545+00	cars/hyundai_UteWNHn.jpg	f	44
55	2022-03-01 22:52:25.641715+00	2022-03-01 22:52:25.641736+00	cars/KIA-Symbol-Description.jpg	f	75
56	2022-03-01 22:52:35.638337+00	2022-03-01 22:52:35.638359+00	cars/KIA-Symbol-Description_f2MSCFQ.jpg	f	77
57	2022-03-01 22:53:04.02702+00	2022-03-01 22:53:04.027041+00	cars/KIA-Symbol-Description_WvSmBel.jpg	f	78
58	2022-03-01 22:53:51.079827+00	2022-03-01 22:53:51.079845+00	cars/mazda.jfif	f	79
59	2022-03-01 22:53:59.483972+00	2022-03-01 22:53:59.483989+00	cars/mazda_u3c55oB.jfif	f	80
60	2022-03-01 22:54:13.473611+00	2022-03-01 22:54:13.473659+00	cars/opel-emblem-logo-closeup-hood-grill-black-car-53381759.jpg	f	81
61	2022-03-01 22:54:30.247717+00	2022-03-01 22:54:30.247737+00	cars/2019-volkswagen-jetta-review.jpg	f	82
62	2022-03-01 22:54:37.675465+00	2022-03-01 22:54:37.675484+00	cars/2019-volkswagen-jetta-review_qQCyRPz.jpg	f	83
63	2022-03-01 22:54:46.157547+00	2022-03-01 22:54:46.157565+00	cars/2019-volkswagen-jetta-review_Ok30Gra.jpg	f	84
64	2022-03-01 22:55:06.9958+00	2022-03-01 22:55:06.99582+00	cars/KIA-Symbol-Description_eQOSXdl.jpg	f	68
65	2022-03-01 22:55:15.139071+00	2022-03-01 22:55:15.13909+00	cars/KIA-Symbol-Description_sSmBEq7.jpg	f	65
66	2022-03-01 22:55:34.639503+00	2022-03-01 22:55:34.639521+00	cars/KIA-Symbol-Description_KNZwm1s.jpg	f	66
67	2022-03-01 22:55:52.715145+00	2022-03-01 22:55:52.715162+00	cars/KIA-Symbol-Description_AE2guJP.jpg	f	67
68	2022-03-01 22:56:02.652113+00	2022-03-01 22:56:02.652134+00	cars/KIA-Symbol-Description_BVxBwft.jpg	f	68
69	2022-03-01 22:56:14.549301+00	2022-03-01 22:56:14.549319+00	cars/KIA-Symbol-Description_yQIIMUZ.jpg	f	69
70	2022-03-01 22:56:49.253722+00	2022-03-01 22:56:49.25374+00	cars/KIA-Symbol-Description_Rtuu6NN.jpg	f	70
71	2022-03-01 22:56:59.633123+00	2022-03-01 22:56:59.633144+00	cars/KIA-Symbol-Description_djtJT1n.jpg	f	71
72	2022-03-01 22:57:21.202919+00	2022-03-01 22:57:21.202938+00	cars/KIA-Symbol-Description_DW2SVt4.jpg	f	72
73	2022-03-01 22:57:31.032187+00	2022-03-01 22:57:31.032204+00	cars/KIA-Symbol-Description_wRQDnml.jpg	f	73
74	2022-03-01 22:57:46.030418+00	2022-03-01 22:57:46.030452+00	cars/KIA-Symbol-Description_8S5bsM4.jpg	f	74
75	2022-03-01 22:57:55.803505+00	2022-03-01 22:57:55.803524+00	cars/skoda-badge_1Ouoqze.jpg	f	55
76	2022-03-01 22:58:18.618275+00	2022-03-01 22:58:18.618295+00	cars/hyundai_cA9xOh1.jpg	f	56
77	2022-03-01 22:58:27.690567+00	2022-03-01 22:58:27.690587+00	cars/hyundai_eJCzHf2.jpg	f	57
78	2022-03-01 22:58:33.876951+00	2022-03-01 22:58:33.876968+00	cars/hyundai_SHFMJ5F.jpg	f	58
79	2022-03-01 22:58:58.927758+00	2022-03-01 22:58:58.927774+00	cars/hyundai_7WoPrPv.jpg	f	59
80	2022-03-01 22:59:31.880501+00	2022-03-01 22:59:31.880519+00	cars/hyundai_MOQM9ZO.jpg	f	60
81	2022-03-01 22:59:42.241329+00	2022-03-01 22:59:42.241348+00	cars/2019-volkswagen-jetta-review_627Fzfj.jpg	f	61
82	2022-03-01 22:59:55.372595+00	2022-03-01 22:59:55.372611+00	cars/2019-volkswagen-jetta-review_x0TZrh1.jpg	f	62
83	2022-03-01 23:00:04.024135+00	2022-03-01 23:00:04.024154+00	cars/2019-volkswagen-jetta-review_768qq1v.jpg	f	63
84	2022-03-01 23:00:19.382543+00	2022-03-01 23:00:19.382571+00	cars/hyundai_LD5orHt.jpg	f	64
85	2022-03-01 23:02:38.140838+00	2022-03-01 23:02:38.14086+00	cars/hyundai_vMHXG9T.jpg	f	125
86	2022-03-01 23:02:45.342097+00	2022-03-01 23:02:45.342116+00	cars/hyundai_Yduol95.jpg	f	94
87	2022-03-01 23:02:52.594233+00	2022-03-01 23:02:52.594253+00	cars/hyundai_wRTwScH.jpg	f	93
88	2022-03-01 23:03:00.982004+00	2022-03-01 23:03:00.982029+00	cars/hyundai_Eu3Qbs1.jpg	f	92
89	2022-03-01 23:03:12.339005+00	2022-03-01 23:03:12.339023+00	cars/hyundai_TJxs6kW.jpg	f	91
90	2022-03-01 23:03:20.670333+00	2022-03-01 23:03:20.670351+00	cars/hyundai_Eejl563.jpg	f	90
91	2022-03-01 23:03:29.96995+00	2022-03-01 23:03:29.969968+00	cars/hyundai_d9eTccA.jpg	f	89
92	2022-03-01 23:03:39.05918+00	2022-03-01 23:03:39.059197+00	cars/KIA-Symbol-Description_D1ko3Va.jpg	f	88
93	2022-03-01 23:03:48.389861+00	2022-03-01 23:03:48.389878+00	cars/KIA-Symbol-Description_ud35JcA.jpg	f	87
94	2022-03-01 23:03:59.84812+00	2022-03-01 23:03:59.848137+00	cars/hyundai_TYLhXWG.jpg	f	104
95	2022-03-01 23:04:08.332064+00	2022-03-01 23:04:08.332092+00	cars/hyundai_Rg9dPCr.jpg	f	103
96	2022-03-01 23:04:16.51274+00	2022-03-01 23:04:16.512758+00	cars/hyundai_UeOo7cK.jpg	f	102
97	2022-03-01 23:04:25.505405+00	2022-03-01 23:04:25.505425+00	cars/hyundai_JcVm5rk.jpg	f	101
98	2022-03-01 23:04:35.064195+00	2022-03-01 23:04:35.064213+00	cars/hyundai_kD4XEaj.jpg	f	100
99	2022-03-01 23:04:43.578333+00	2022-03-01 23:04:43.57835+00	cars/hyundai_ZjZZraE.jpg	f	99
100	2022-03-01 23:04:51.315159+00	2022-03-01 23:04:51.315175+00	cars/hyundai_gNTvx6m.jpg	f	98
101	2022-03-01 23:04:58.711749+00	2022-03-01 23:04:58.711766+00	cars/hyundai_DXQqUzJ.jpg	f	97
102	2022-03-01 23:05:07.586952+00	2022-03-01 23:05:07.586971+00	cars/hyundai_ADPbWO0.jpg	f	96
103	2022-03-01 23:05:15.728823+00	2022-03-01 23:05:15.728842+00	cars/hyundai_74KPDO8.jpg	f	95
104	2022-03-01 23:05:23.174763+00	2022-03-01 23:05:23.17478+00	cars/hyundai_qUy4een.jpg	f	114
105	2022-03-01 23:05:36.56757+00	2022-03-01 23:05:36.567588+00	cars/hyundai_Sfv5NFt.jpg	f	113
106	2022-03-01 23:05:45.090553+00	2022-03-01 23:05:45.090571+00	cars/hyundai_bHhjspa.jpg	f	112
107	2022-03-01 23:05:52.520977+00	2022-03-01 23:05:52.520994+00	cars/hyundai_mEeeiGZ.jpg	f	111
108	2022-03-01 23:06:02.051111+00	2022-03-01 23:06:02.05113+00	cars/hyundai_2YtJeOQ.jpg	f	110
109	2022-03-01 23:06:10.49066+00	2022-03-01 23:06:10.490678+00	cars/hyundai_9GwFS1I.jpg	f	109
110	2022-03-01 23:06:19.489726+00	2022-03-01 23:06:19.489745+00	cars/hyundai_qBaiP8d.jpg	f	108
111	2022-03-01 23:06:27.754588+00	2022-03-01 23:06:27.754614+00	cars/hyundai_QtvnHSs.jpg	f	107
112	2022-03-01 23:06:42.255772+00	2022-03-01 23:06:42.25579+00	cars/hyundai_GvKL511.jpg	f	106
113	2022-03-01 23:06:50.586561+00	2022-03-01 23:06:50.586579+00	cars/hyundai_wXDGhRT.jpg	f	106
114	2022-03-01 23:07:01.879186+00	2022-03-01 23:07:01.879214+00	cars/hyundai_RS8lSuQ.jpg	f	105
115	2022-03-01 23:07:17.130404+00	2022-03-01 23:07:17.130423+00	cars/hyundai_rSTnmF6.jpg	f	115
116	2022-03-01 23:07:24.545814+00	2022-03-01 23:07:24.545833+00	cars/hyundai_GPMIj0D.jpg	f	124
117	2022-03-01 23:07:40.725888+00	2022-03-01 23:07:40.725904+00	cars/hyundai_dUQOEC0.jpg	f	123
118	2022-03-01 23:07:59.952238+00	2022-03-01 23:07:59.952255+00	cars/hyundai_hmAHbBJ.jpg	f	122
119	2022-03-01 23:08:08.561495+00	2022-03-01 23:08:08.561513+00	cars/hyundai_0AtASAw.jpg	f	121
120	2022-03-01 23:08:21.520489+00	2022-03-01 23:08:21.520507+00	cars/hyundai_99ITHYV.jpg	f	120
121	2022-03-01 23:08:32.789157+00	2022-03-01 23:08:32.789175+00	cars/hyundai_rLZLJyg.jpg	f	119
122	2022-03-01 23:08:38.997895+00	2022-03-01 23:08:38.997914+00	cars/hyundai_bbrZA2p.jpg	f	118
123	2022-03-01 23:08:45.825497+00	2022-03-01 23:08:45.825514+00	cars/hyundai_Yn7Eyew.jpg	f	117
124	2022-03-01 23:09:01.046406+00	2022-03-01 23:09:01.046425+00	cars/hyundai_rYFFXA2.jpg	f	116
125	2022-03-01 23:09:22.640019+00	2022-03-01 23:09:22.64004+00	cars/subaru.png	f	134
126	2022-03-01 23:09:33.813476+00	2022-03-01 23:09:33.813495+00	cars/subaru_XS1CrwA.png	f	133
127	2022-03-01 23:09:40.43446+00	2022-03-01 23:09:40.434477+00	cars/hyundai_02OJQhw.jpg	f	132
128	2022-03-01 23:09:46.880068+00	2022-03-01 23:09:46.880085+00	cars/hyundai_xGt5nhn.jpg	f	131
129	2022-03-01 23:09:53.525232+00	2022-03-01 23:09:53.525249+00	cars/hyundai_DzxGRPr.jpg	f	130
130	2022-03-01 23:10:00.24484+00	2022-03-01 23:10:00.244858+00	cars/hyundai_Co9a82V.jpg	f	129
131	2022-03-01 23:10:08.217661+00	2022-03-01 23:10:08.217682+00	cars/hyundai_JBjYNmt.jpg	f	128
132	2022-03-01 23:10:15.206334+00	2022-03-01 23:10:15.206352+00	cars/hyundai_eIX9USg.jpg	f	127
133	2022-03-01 23:10:22.147064+00	2022-03-01 23:10:22.147085+00	cars/hyundai_R0S7K06.jpg	f	126
\.


--
-- Data for Name: car_carimagetask; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carimagetask (id, created_at, updated_at, car_id, user_id) FROM stdin;
\.


--
-- Data for Name: car_carimagetask_images; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carimagetask_images (id, carimagetask_id, carimage_id) FROM stdin;
\.


--
-- Data for Name: car_carpaper; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carpaper (id, created_at, updated_at, path, car_id) FROM stdin;
1	2022-02-23 13:55:07.860397+00	2022-02-23 13:55:07.860414+00	cars/papers/9.jpg	4
\.


--
-- Data for Name: car_case; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_case (id, name) FROM stdin;
\.


--
-- Data for Name: car_color; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_color (id, name) FROM stdin;
\.


--
-- Data for Name: car_fuel; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_fuel (id, name) FROM stdin;
\.


--
-- Data for Name: car_model; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_model (id, name, brand_id) FROM stdin;
1	Accent	1
2	Avante	1
3	735i	2
4	Corolla	3
5	Camry	3
6	Elantra	1
7	Fabia	4
8	I30	1
9	Jetta	5
10	K5	6
11	Carens	6
12	Forte	6
13	Sorento	6
14	Sportage	6
15	Mazda	7
16	Insignia	8
17	Passat	5
18	Polo	5
19	Rio	6
20	Sonata	1
21	Subaru	9
22	Forester	9
\.


--
-- Data for Name: car_office; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_office (id, name, address) FROM stdin;
1	Главный	Kyiv
\.


--
-- Data for Name: core_administrativedeposit; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.core_administrativedeposit (id, created_at, updated_at, amount, currency, note, date) FROM stdin;
\.


--
-- Data for Name: core_administrativewithdraw; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.core_administrativewithdraw (id, created_at, updated_at, amount, currency, note, date) FROM stdin;
\.


--
-- Data for Name: core_balance; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.core_balance (id, amount, currency, type) FROM stdin;
\.


--
-- Data for Name: core_currency; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.core_currency (id, ratio, date) FROM stdin;
1	28.5	2022-02-23
\.


--
-- Data for Name: credit_credit; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.credit_credit (id, created_at, updated_at, payment_count, payment_year, payment_date, car_value_total, first_payment, type, balance, percent_total, percent_expenses, is_completed, is_permitted, is_verified, is_active, payment, debt, notes, contract_id, expenses, total_credit) FROM stdin;
21	2022-03-05 18:56:26.184742+00	2022-03-05 20:18:04.185257+00	52	1	5	5244.7	1000	W	0	30	10	f	f	f	t	0	0		27	524.47	6200
33	2022-03-05 19:27:41.423366+00	2022-03-05 20:17:02.102879+00	52	1	4	6622.3	500	W	0	30	10	f	f	f	t	0	0		39	662.23	8820
7	2022-03-03 18:54:36.321365+00	2022-03-03 22:37:28.108872+00	12	1	21	14200	1000	M	0	30	10	f	f	f	t	0	0		13	1420	19006
22	2022-03-05 18:58:20.672845+00	2022-03-05 20:17:58.442947+00	47	47	2	5377.6	1300	W	0	30	10	f	f	f	t	0	0		28	537.76	6000
8	2022-03-03 20:34:22.139376+00	2022-03-03 22:39:10.245751+00	104	2	1	7718.75	500	W	0	60	20	f	f	f	t	0	0		14	1543.75	14020
23	2022-03-05 18:59:45.420472+00	2022-03-05 20:17:53.563542+00	78	1.5	3	7343.3	700	W	0	45	15	f	f	f	t	0	0		29	1101.495	11230
9	2022-03-03 20:46:03.296158+00	2022-03-03 22:40:09.709016+00	104	2	1	6364.5	300	W	0	60	20	f	f	f	t	0	0		15	1272.9	11740
10	2022-03-03 20:48:18.94188+00	2022-03-03 22:40:47.145293+00	104	2	1	8125	2000	W	0	60	20	f	f	f	t	0	0		16	1625	12400
11	2022-03-03 20:54:13.815807+00	2022-03-03 22:41:19.657189+00	104	2	1	15537.5	2000	W	0	60	20	f	f	f	t	0	0		17	3107.5	26632
24	2022-03-05 19:01:38.820098+00	2022-03-05 20:17:48.38121+00	104	2	3	12187.5	1000	W	0	60	20	f	f	f	t	0	0		30	2437.5	21800
12	2022-03-03 21:48:39.598806+00	2022-03-05 20:18:53.033366+00	104	2	1	7041.6	400	W	0	60	20	f	f	f	t	0	0		18	1408.32	12880
13	2022-03-03 21:52:26.336844+00	2022-03-05 20:18:47.029717+00	78	1.5	2	7811	700	W	0	45	15	f	f	f	t	0	0		19	1171.65	12010
14	2022-03-05 17:28:39.602288+00	2022-03-05 20:18:40.67909+00	52	1	2	5927	0	W	0	30	10	f	f	f	t	0	0		20	592.7	8476
25	2022-03-05 19:02:58.437093+00	2022-03-05 20:17:43.123996+00	52	1	2	8258.7	500	W	0	30	10	f	f	f	t	0	0		31	825.87	11160
15	2022-03-05 17:37:44.325802+00	2022-03-05 20:18:35.543325+00	104	2	2	5241	0	W	0	60	20	f	f	f	t	0	0		21	1048.2	10063
16	2022-03-05 17:38:57.588665+00	2022-03-05 20:18:29.802149+00	104	2	2	8260.5	500	W	0	60	20	f	f	f	t	0	0		22	1652.1	15060
17	2022-03-05 17:45:38.563719+00	2022-03-05 20:18:24.505757+00	104	2	2	12187.5	1000	W	0	60	20	f	f	f	t	0	0		23	2437.5	21800
18	2022-03-05 17:46:55.51578+00	2022-03-05 20:18:19.124151+00	104	2	2	4279	0	W	0	60	20	f	f	f	t	0	0		24	855.8	8216
19	2022-03-05 18:52:48.302375+00	2022-03-05 20:18:13.784718+00	52	1	2	5454.5	0	W	0	30	10	f	f	f	t	0	0		25	545.45	7800
20	2022-03-05 18:54:12.877393+00	2022-03-05 20:18:09.11781+00	52	1	2	16146.8	4500	W	0	30	10	f	f	f	t	0	0		26	1614.68	17240
26	2022-03-05 19:04:21.806877+00	2022-03-05 20:17:37.525638+00	104	2	3	7447.9	300	W	0	60	20	f	f	f	t	0	0		32	1489.58	13820
27	2022-03-05 19:05:19.774066+00	2022-03-05 20:17:32.814841+00	104	2	3	5822.9	300	W	0	60	20	f	f	f	t	0	0		33	1164.58	10700
28	2022-03-05 19:06:39.462689+00	2022-03-05 20:17:28.308464+00	104	2	3	10833.3	800	W	0	60	20	f	f	f	t	0	0		34	2166.66	19520
29	2022-03-05 19:08:07.911181+00	2022-03-05 20:17:22.764058+00	52	1	4	7499.3	1000	W	0	30	10	f	f	f	t	0	0		35	749.93	9424
30	2022-03-05 19:23:33.38139+00	2022-03-05 20:17:17.574498+00	104	2	4	8612.5	1000	W	0	60	20	f	f	f	t	0	0		36	1722.5	14936
31	2022-03-05 19:24:26.748883+00	2022-03-05 20:17:12.374864+00	104	2	4	10331.25	1000	W	0	60	20	f	f	f	t	0	0		37	2066.25	18236
32	2022-03-05 19:26:10.34021+00	2022-03-05 20:17:07.247482+00	78	1.5	4	6795.2	900	W	0	45	15	f	f	f	t	0	0		38	1019.28	10026
34	2022-03-05 19:41:51.983096+00	2022-03-05 20:16:56.680569+00	52	1	5	6986	500	W	0	30	10	f	f	f	t	0	0		40	698.6	9340
35	2022-03-05 19:43:04.845762+00	2022-03-05 20:16:51.326591+00	52	1	5	6986	500	W	0	30	10	f	f	f	t	0	0		41	698.6	9340
39	2022-03-05 19:48:22.166765+00	2022-03-05 20:16:28.587925+00	52	1	5	6664.3	300	W	0	30	10	f	f	f	t	0	0		45	666.43	9140
36	2022-03-05 19:44:32.257939+00	2022-03-05 20:16:45.854991+00	104	2	5	9354	500	W	0	60	20	f	f	f	t	0	0		42	1870.8	17160
37	2022-03-05 19:45:46.201508+00	2022-03-05 20:16:40.0656+00	52	1	5	9090.9	0	W	0	30	10	f	f	f	t	0	0		43	909.09	13000
38	2022-03-05 19:47:07.625145+00	2022-03-05 20:16:34.113924+00	52	1	5	8849.6	350	W	0	30	10	f	f	f	t	0	0		44	884.96	12200
40	2022-03-05 19:49:38.217551+00	2022-03-05 20:16:23.268798+00	78	1.5	5	7470.4	500	W	0	45	15	f	f	f	t	0	0		46	1120.56	11732
41	2022-03-05 19:51:01.04473+00	2022-03-05 20:16:17.847952+00	104	2	5	12187.5	1000	W	0	60	20	f	f	f	t	0	0		47	2437.5	21800
42	2022-03-05 19:51:48.990162+00	2022-03-05 20:16:12.333382+00	104	2	5	10562.5	1000	W	0	60	20	f	f	f	t	0	0		48	2112.5	18680
43	2022-03-05 19:52:45.271687+00	2022-03-05 20:16:07.346023+00	104	2	5	8125	400	W	0	60	20	f	f	f	t	0	0		49	1625	14960
44	2022-03-05 19:57:51.390975+00	2022-03-05 20:16:01.979426+00	104	2	5	10833.3	800	W	0	60	20	f	f	f	t	0	0		50	2166.66	19520
45	2022-03-05 19:59:02.93484+00	2022-03-05 20:15:56.49549+00	104	2	5	7312.5	0	W	0	60	20	f	f	f	t	0	0		51	1462.5	14040
46	2022-03-05 20:00:53.200524+00	2022-03-05 20:15:51.162797+00	52	1	5	12117.5	1000	W	0	30	10	f	f	f	t	0	0		52	1211.75	16028
47	2022-03-05 20:05:23.506905+00	2022-03-05 20:15:45.148982+00	104	2	2	5416.6	0	W	0	60	20	f	f	f	t	0	0		53	1083.32	10400
48	2022-03-05 20:07:14.654492+00	2022-03-05 20:15:39.642578+00	104	2	4	8802	1500	W	0	60	20	f	f	f	t	0	0		54	1760.4	14500
\.


--
-- Data for Name: credit_creditimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.credit_creditimage (id, created_at, updated_at, path, credit_id) FROM stdin;
2	2022-02-23 22:12:33.14151+00	2022-02-23 22:12:33.141536+00	credit_images/1_1ciMZRs.jpg	\N
3	2022-02-23 22:17:16.397411+00	2022-02-23 22:17:16.39743+00	credit_images/6.jpg	\N
4	2022-02-23 22:17:42.443917+00	2022-02-23 22:17:42.443935+00	credit_images/7.jpg	\N
5	2022-02-23 22:18:07.637458+00	2022-02-23 22:18:07.637475+00	credit_images/5.webp	\N
6	2022-02-23 22:18:39.160458+00	2022-02-23 22:18:39.160474+00	credit_images/8.jpg	\N
7	2022-03-03 20:50:56.683687+00	2022-03-03 20:50:56.683705+00	credit_images/3.png	10
8	2022-03-03 20:51:18.415201+00	2022-03-03 20:51:18.415221+00	credit_images/3_w03m1rM.png	7
9	2022-03-03 20:51:29.439262+00	2022-03-03 20:51:29.439281+00	credit_images/3_GcxR9UB.png	9
10	2022-03-03 20:51:35.711713+00	2022-03-03 20:51:35.711732+00	credit_images/3_q3mxgc5.png	8
11	2022-03-03 20:54:29.931057+00	2022-03-03 20:54:29.931074+00	credit_images/3_bVQxT0W.png	11
12	2022-03-05 20:15:39.644186+00	2022-03-05 20:15:39.644204+00	credit_images/1_ujOc4Lh.jpg	48
13	2022-03-05 20:15:45.150346+00	2022-03-05 20:15:45.150368+00	credit_images/1_6QWP39a.jpg	47
14	2022-03-05 20:15:51.164146+00	2022-03-05 20:15:51.164163+00	credit_images/1_ZhtLwMK.jpg	46
15	2022-03-05 20:15:56.496884+00	2022-03-05 20:15:56.496902+00	credit_images/1_O6tyEGj.jpg	45
16	2022-03-05 20:16:01.980846+00	2022-03-05 20:16:01.980865+00	credit_images/1_Zb8DIGF.jpg	44
17	2022-03-05 20:16:07.347128+00	2022-03-05 20:16:07.347145+00	credit_images/1_V9TXVIH.jpg	43
18	2022-03-05 20:16:12.335866+00	2022-03-05 20:16:12.335886+00	credit_images/1_Tioo4oY.jpg	42
19	2022-03-05 20:16:17.84948+00	2022-03-05 20:16:17.849505+00	credit_images/1_SMfebwY.jpg	41
20	2022-03-05 20:16:23.269986+00	2022-03-05 20:16:23.270004+00	credit_images/1_ih1k3vl.jpg	40
21	2022-03-05 20:16:28.589183+00	2022-03-05 20:16:28.5892+00	credit_images/1_frnev9m.jpg	39
22	2022-03-05 20:16:34.115048+00	2022-03-05 20:16:34.115075+00	credit_images/1_ForRyHy.jpg	38
23	2022-03-05 20:16:40.066745+00	2022-03-05 20:16:40.066764+00	credit_images/1_Ah65W4a.jpg	37
24	2022-03-05 20:16:45.856499+00	2022-03-05 20:16:45.856518+00	credit_images/1_uytCHTi.jpg	36
25	2022-03-05 20:16:51.32776+00	2022-03-05 20:16:51.327776+00	credit_images/1_xrcYROY.jpg	35
26	2022-03-05 20:16:56.681716+00	2022-03-05 20:16:56.681733+00	credit_images/1_Xfww5if.jpg	34
27	2022-03-05 20:17:02.104263+00	2022-03-05 20:17:02.104282+00	credit_images/1_5f47aQW.jpg	33
28	2022-03-05 20:17:07.249037+00	2022-03-05 20:17:07.249063+00	credit_images/1_XqtbwVz.jpg	32
29	2022-03-05 20:17:12.376283+00	2022-03-05 20:17:12.376302+00	credit_images/1_2T9DbQa.jpg	31
30	2022-03-05 20:17:17.575765+00	2022-03-05 20:17:17.575783+00	credit_images/1_8rRx2Ud.jpg	30
31	2022-03-05 20:17:22.766194+00	2022-03-05 20:17:22.766217+00	credit_images/1_LWR6hHM.jpg	29
32	2022-03-05 20:17:28.309703+00	2022-03-05 20:17:28.30972+00	credit_images/1_lvk4vvv.jpg	28
33	2022-03-05 20:17:32.816068+00	2022-03-05 20:17:32.816085+00	credit_images/1_NhllJ0N.jpg	27
34	2022-03-05 20:17:37.526858+00	2022-03-05 20:17:37.526875+00	credit_images/1_HCx7W5o.jpg	26
35	2022-03-05 20:17:43.125381+00	2022-03-05 20:17:43.125399+00	credit_images/1_mPpAhd2.jpg	25
36	2022-03-05 20:17:48.382471+00	2022-03-05 20:17:48.382488+00	credit_images/1_Ua1XZ54.jpg	24
37	2022-03-05 20:17:53.564643+00	2022-03-05 20:17:53.56466+00	credit_images/1_OThRuvU.jpg	23
38	2022-03-05 20:17:58.444176+00	2022-03-05 20:17:58.444193+00	credit_images/1_5viNcJM.jpg	22
39	2022-03-05 20:18:04.186656+00	2022-03-05 20:18:04.186676+00	credit_images/1_0AbHvWF.jpg	21
40	2022-03-05 20:18:09.119403+00	2022-03-05 20:18:09.119423+00	credit_images/1_gu7C7v7.jpg	20
41	2022-03-05 20:18:13.785907+00	2022-03-05 20:18:13.785925+00	credit_images/1_EGrReXS.jpg	19
42	2022-03-05 20:18:19.125524+00	2022-03-05 20:18:19.125542+00	credit_images/1_zp6vDV4.jpg	18
43	2022-03-05 20:18:24.507077+00	2022-03-05 20:18:24.507096+00	credit_images/1_mlwuSbf.jpg	17
44	2022-03-05 20:18:29.803982+00	2022-03-05 20:18:29.804002+00	credit_images/1_bPLlxAx.jpg	16
45	2022-03-05 20:18:35.544475+00	2022-03-05 20:18:35.544492+00	credit_images/1_nt7FTWl.jpg	15
46	2022-03-05 20:18:40.680241+00	2022-03-05 20:18:40.680258+00	credit_images/1_uaOiKqd.jpg	14
47	2022-03-05 20:18:47.030901+00	2022-03-05 20:18:47.030919+00	credit_images/1_92txmvA.jpg	13
48	2022-03-05 20:18:53.034481+00	2022-03-05 20:18:53.034497+00	credit_images/1_eSZEDUW.jpg	12
\.


--
-- Data for Name: credit_creditpayment; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.credit_creditpayment (id, created_at, updated_at, date, debt_week, debt_total, payment_left, amount, amount_uah, total_amount, is_verified, credit_id) FROM stdin;
1	2022-02-23 22:12:33.144479+00	2022-02-23 22:12:33.144495+00	2022-02-23	0	0	0	500	0	500	f	\N
2	2022-02-23 22:17:16.401037+00	2022-02-23 22:17:16.401054+00	2022-02-23	0	0	0	1000	0	1000	f	\N
3	2022-02-23 22:17:42.447028+00	2022-02-23 22:17:42.447045+00	2022-02-23	0	0	0	500	0	500	f	\N
4	2022-02-23 22:18:07.639992+00	2022-02-23 22:18:07.640015+00	2022-02-23	0	0	0	500	0	500	f	\N
6	2022-02-23 22:20:01.651914+00	2022-02-23 22:20:01.65195+00	2022-02-24	-480	-480	600	100	0	100	f	\N
7	2022-02-24 21:46:55.436413+00	2022-02-24 21:46:55.436442+00	2022-02-24	20	-460	700	600	0	600	f	\N
8	2022-02-24 21:50:05.541956+00	2022-02-24 21:50:05.541984+00	2022-02-24	-20	-480	1260	560	0	560	f	\N
9	2022-02-24 21:50:27.730658+00	2022-02-24 21:50:27.7307+00	2022-02-24	20	-460	1860	600	0	600	f	\N
10	2022-02-24 21:50:33.851021+00	2022-02-24 21:50:33.851056+00	2022-02-24	0	-460	2440	580	0	580	f	\N
11	2022-02-24 21:50:47.393425+00	2022-02-24 21:50:47.393456+00	2022-02-24	440	-20	3460	1020	0	1020	f	\N
12	2022-02-24 21:51:46.297487+00	2022-02-24 21:51:46.297509+00	2022-02-24	163	163	1326	826	0	826	f	\N
13	2022-02-24 21:53:46.605408+00	2022-02-24 21:53:46.605446+00	2022-02-24	-163	0	1826	500	0	500	f	\N
14	2022-02-24 21:53:55.702508+00	2022-02-24 21:53:55.702533+00	2022-02-24	0	0	2489	663	0	663	f	\N
15	2022-02-24 21:54:09.186007+00	2022-02-24 21:54:09.186036+00	2022-02-24	0	0	3152	663	0	663	f	\N
16	2022-02-24 21:54:18.414328+00	2022-02-24 21:54:18.414351+00	2022-02-24	-1	-1	3814	662	0	662	f	\N
17	2022-02-24 21:54:38.413138+00	2022-02-24 21:54:38.413158+00	2022-02-24	0	-1	4477	663	0	663	f	\N
18	2022-02-24 21:54:45.654138+00	2022-02-24 21:54:45.65416+00	2022-02-24	1	0	5141	664	0	664	f	\N
19	2022-02-24 21:55:24.693223+00	2022-02-24 21:55:24.693251+00	2022-02-24	28646	28646	34450	29309	0	29309	f	\N
21	2022-03-05 21:52:58.021368+00	2022-03-05 21:52:58.021397+00	2022-03-05	0	0	100	100	0	100	f	48
22	2022-03-05 21:53:09.00917+00	2022-03-05 21:53:09.009194+00	2022-03-05	0	0	200	100	0	100	f	48
23	2022-03-06 12:56:46.127088+00	2022-03-06 12:56:46.127122+00	2022-03-06	0	0	350	150	0	150	f	48
\.


--
-- Data for Name: debt_debt; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.debt_debt (id, created_at, updated_at, date, amount, paid, is_active, car_id, driver_id) FROM stdin;
\.


--
-- Data for Name: debt_debtimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.debt_debtimage (id, file, debt_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2022-02-23 13:25:23.264617+00	1	Hyundai	1	[{"added": {}}]	14	1
2	2022-02-23 13:25:31.674956+00	1	Accent	1	[{"added": {}}]	22	1
3	2022-02-23 13:26:02.851035+00	1	Главный	1	[{"added": {}}]	21	1
4	2022-02-23 13:26:18.453732+00	1	Accent - 2011 - АІ 0315 MX 	1	[{"added": {}}]	15	1
5	2022-02-23 13:26:34.994616+00	2	Accent - 2017 - АІ 0312 MX 	1	[{"added": {}}]	15	1
6	2022-02-23 13:26:48.66439+00	3	Accent - 2019 - АI 4463 MC 	1	[{"added": {}}]	15	1
7	2022-02-23 13:27:00.292952+00	4	Accent - 2008 - АI 8250 OB 	1	[{"added": {}}]	15	1
8	2022-02-23 13:27:14.671143+00	5	Accent - 9756 - АI 9756 IH 	1	[{"added": {}}]	15	1
9	2022-02-23 13:27:35.235462+00	5	Accent - 2010 - АI 9756 IH 	2	[{"changed": {"fields": ["Year"]}}]	15	1
10	2022-02-23 13:27:42.246976+00	6	Accent - 2008 - КА 0729 АР 	1	[{"added": {}}]	15	1
11	2022-02-23 13:27:53.899655+00	7	Accent - 2011 - КA 2219 ВВ 	1	[{"added": {}}]	15	1
12	2022-02-23 13:28:06.236759+00	8	Accent - 2016 - AE 7193 KO 	1	[{"added": {}}]	15	1
13	2022-02-23 13:28:17.774702+00	2	Avente	1	[{"added": {}}]	22	1
14	2022-02-23 13:28:26.859559+00	9	Avente - 2017 - АІ 8064 MO 	1	[{"added": {}}]	15	1
15	2022-02-23 13:28:39.612415+00	10	Avente - 2016 - АІ 8243 OВ 	1	[{"added": {}}]	15	1
16	2022-02-23 13:33:41.14651+00	1	Seyidov Elvin	2	[{"changed": {"fields": ["First name", "Last name"]}}, {"added": {"name": "user phone", "object": ""}}]	8	1
17	2022-02-23 13:34:01.286603+00	1	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	1	[{"added": {}}]	9	1
18	2022-02-23 13:34:08.305951+00	2	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	1	[{"added": {}}]	9	1
19	2022-02-23 13:34:15.369993+00	3	Seyidov Elvin - Accent - 2019 - АI 4463 MC  - Taxi	1	[{"added": {}}]	9	1
20	2022-02-23 13:34:24.270435+00	4	Seyidov Elvin - Accent - 2008 - АI 8250 OB  - Taxi	1	[{"added": {}}]	9	1
21	2022-02-23 13:34:34.061906+00	5	Seyidov Elvin - Accent - 2010 - АI 9756 IH  - Taxi	1	[{"added": {}}]	9	1
22	2022-02-23 13:34:42.911911+00	6	Seyidov Elvin - Accent - 2008 - КА 0729 АР  - Taxi	1	[{"added": {}}]	9	1
23	2022-02-23 13:34:51.785734+00	7	Seyidov Elvin - Accent - 2011 - КA 2219 ВВ  - Taxi	1	[{"added": {}}]	9	1
24	2022-02-23 13:35:00.150711+00	8	Seyidov Elvin - Accent - 2016 - AE 7193 KO  - Taxi	1	[{"added": {}}]	9	1
25	2022-02-23 13:35:07.930723+00	9	Seyidov Elvin - Avente - 2017 - АІ 8064 MO  - Taxi	1	[{"added": {}}]	9	1
26	2022-02-23 13:35:15.631519+00	10	Seyidov Elvin - Avente - 2016 - АІ 8243 OВ  - Taxi	1	[{"added": {}}]	9	1
27	2022-02-23 13:35:32.115294+00	1	Taxi object (1)	1	[{"added": {}}]	34	1
28	2022-02-23 13:35:40.899248+00	1	Taxi object (1)	2	[]	34	1
29	2022-02-23 13:35:46.330166+00	2	Taxi object (2)	1	[{"added": {}}]	34	1
30	2022-02-23 13:35:54.793656+00	3	Taxi object (3)	1	[{"added": {}}]	34	1
31	2022-02-23 13:35:59.74103+00	4	Taxi object (4)	1	[{"added": {}}]	34	1
32	2022-02-23 13:36:05.234824+00	5	Taxi object (5)	1	[{"added": {}}]	34	1
33	2022-02-23 13:36:10.566835+00	6	Taxi object (6)	1	[{"added": {}}]	34	1
34	2022-02-23 13:36:14.440729+00	7	Taxi object (7)	1	[{"added": {}}]	34	1
35	2022-02-23 13:36:19.315024+00	8	Taxi object (8)	1	[{"added": {}}]	34	1
36	2022-02-23 13:36:22.918662+00	9	Taxi object (9)	1	[{"added": {}}]	34	1
37	2022-02-23 13:36:29.117188+00	10	Taxi object (10)	1	[{"added": {}}]	34	1
38	2022-02-23 13:40:33.15992+00	1	990c9d166decaad5c05138bd06d12e60267d0a06	1	[{"added": {}}]	7	1
39	2022-02-23 13:53:02.544742+00	2	Avante	2	[{"changed": {"fields": ["Name"]}}]	22	1
40	2022-02-23 13:53:06.140376+00	10	Avante - 2016 - АІ 8243 OВ 	2	[]	15	1
41	2022-02-23 13:53:50.057967+00	10	Avante - 2016 - АІ 8243 OВ 	2	[{"added": {"name": "car image", "object": "Avante - 2016 - \\u0410\\u0406 8243 O\\u0412 "}}]	15	1
42	2022-02-23 13:53:58.259549+00	9	Avante - 2017 - АІ 8064 MO 	2	[{"added": {"name": "car image", "object": "Avante - 2017 - \\u0410\\u0406 8064 MO "}}]	15	1
43	2022-02-23 13:54:11.327492+00	1	Accent - 2011 - АІ 0315 MX 	2	[{"added": {"name": "car image", "object": "Accent - 2011 - \\u0410\\u0406 0315 MX "}}]	15	1
44	2022-02-23 13:54:33.30774+00	8	Accent - 2016 - AE 7193 KO 	2	[{"added": {"name": "car image", "object": "Accent - 2016 - AE 7193 KO "}}]	15	1
45	2022-02-23 13:54:43.658556+00	7	Accent - 2011 - КA 2219 ВВ 	2	[{"added": {"name": "car image", "object": "Accent - 2011 - \\u041aA 2219 \\u0412\\u0412 "}}]	15	1
46	2022-02-23 13:54:51.536728+00	6	Accent - 2008 - КА 0729 АР 	2	[{"added": {"name": "car image", "object": "Accent - 2008 - \\u041a\\u0410 0729 \\u0410\\u0420 "}}]	15	1
47	2022-02-23 13:54:58.367787+00	5	Accent - 2010 - АI 9756 IH 	2	[{"added": {"name": "car image", "object": "Accent - 2010 - \\u0410I 9756 IH "}}]	15	1
48	2022-02-23 13:55:07.863643+00	4	Accent - 2008 - АI 8250 OB 	2	[{"added": {"name": "car paper", "object": "Accent - 2008 - \\u0410I 8250 OB "}}]	15	1
49	2022-02-23 13:55:14.0175+00	3	Accent - 2019 - АI 4463 MC 	2	[{"added": {"name": "car image", "object": "Accent - 2019 - \\u0410I 4463 MC "}}]	15	1
50	2022-02-23 13:55:24.897475+00	2	Accent - 2017 - АІ 0312 MX 	2	[{"added": {"name": "car image", "object": "Accent - 2017 - \\u0410\\u0406 0312 MX "}}]	15	1
51	2022-02-23 14:02:42.09367+00	4	Accent - 2008 - АI 8250 OB 	2	[{"added": {"name": "car image", "object": "Accent - 2008 - \\u0410I 8250 OB "}}]	15	1
52	2022-02-23 14:28:15.165262+00	2	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
53	2022-02-23 14:28:29.350068+00	2	Mammadxanli Farid	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
54	2022-02-23 14:28:52.685932+00	1	Mammadxanli Farid - Accent - 2011 - АІ 0315 MX  - Taxi	2	[{"changed": {"fields": ["Customer", "Start date", "End date"]}}]	9	1
55	2022-02-23 14:28:58.820402+00	1	Mammadxanli Farid - Accent - 2011 - АІ 0315 MX  - Taxi	2	[]	9	1
56	2022-02-23 14:29:13.399077+00	11	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	1	[{"added": {}}]	9	1
57	2022-02-23 14:29:29.426695+00	11	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	2	[{"changed": {"fields": ["Start date"]}}]	9	1
58	2022-02-23 14:29:42.56526+00	11	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	2	[{"changed": {"fields": ["Start date"]}}]	9	1
59	2022-02-23 14:31:58.531438+00	1	Mammadxanli Farid - Accent - 2011 - АІ 0315 MX  - Taxi	2	[{"changed": {"fields": ["Start date"]}}]	9	1
60	2022-02-23 14:48:20.068805+00	1	Taxi object (1)	2	[{"added": {"name": "revenue", "object": "Revenue object (1)"}}, {"added": {"name": "revenue", "object": "Revenue object (2)"}}, {"added": {"name": "revenue", "object": "Revenue object (3)"}}, {"added": {"name": "revenue", "object": "Revenue object (4)"}}, {"added": {"name": "revenue", "object": "Revenue object (5)"}}]	34	1
61	2022-02-23 14:56:06.29238+00	1	Bolt	1	[{"added": {}}]	32	1
62	2022-02-23 14:56:34.508608+00	2	Uber	1	[{"added": {}}]	32	1
63	2022-02-23 14:56:53.400956+00	3	Uklon	1	[{"added": {}}]	32	1
64	2022-02-23 14:57:55.170546+00	1	Revenue object (1)	2	[{"added": {"name": "revenue item", "object": "RevenueItem object (1)"}}]	33	1
65	2022-02-23 14:58:22.512854+00	1	Revenue object (1)	3		33	1
66	2022-02-23 14:58:29.262659+00	5	Revenue object (5)	3		33	1
67	2022-02-23 14:58:29.264756+00	4	Revenue object (4)	3		33	1
68	2022-02-23 14:58:29.265809+00	3	Revenue object (3)	3		33	1
69	2022-02-23 14:58:29.266702+00	2	Revenue object (2)	3		33	1
70	2022-02-23 14:59:46.145791+00	7	Revenue object (7)	3		33	1
71	2022-02-23 14:59:46.147441+00	6	Revenue object (6)	3		33	1
72	2022-02-23 15:06:56.164633+00	8	Revenue object (8)	2	[{"deleted": {"name": "revenue item", "object": "RevenueItem object (None)"}}]	33	1
73	2022-02-23 15:07:00.196466+00	8	Revenue object (8)	3		33	1
74	2022-02-23 15:13:15.775288+00	12	Revenue object (12)	3		33	1
75	2022-02-23 15:13:15.777106+00	11	Revenue object (11)	3		33	1
76	2022-02-23 15:13:15.777937+00	10	Revenue object (10)	3		33	1
77	2022-02-23 15:13:15.778786+00	9	Revenue object (9)	3		33	1
78	2022-02-23 15:39:39.337202+00	22	Revenue object (22)	3		33	1
79	2022-02-23 15:39:39.338945+00	21	Revenue object (21)	3		33	1
80	2022-02-23 15:39:39.339783+00	20	Revenue object (20)	3		33	1
81	2022-02-23 15:39:39.340568+00	19	Revenue object (19)	3		33	1
82	2022-02-23 15:39:39.341344+00	18	Revenue object (18)	3		33	1
83	2022-02-23 15:39:39.342165+00	17	Revenue object (17)	3		33	1
84	2022-02-23 15:39:39.342953+00	16	Revenue object (16)	3		33	1
85	2022-02-23 15:39:39.343677+00	15	Revenue object (15)	3		33	1
86	2022-02-23 15:39:39.344364+00	14	Revenue object (14)	3		33	1
87	2022-02-23 15:39:39.345098+00	13	Revenue object (13)	3		33	1
88	2022-02-23 15:44:56.337361+00	25	Revenue object (25)	1	[{"added": {}}, {"added": {"name": "revenue item", "object": "RevenueItem object (51)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (52)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (53)"}}]	33	1
89	2022-02-23 15:49:35.417962+00	25	Revenue object (25)	3		33	1
90	2022-02-23 15:49:35.419751+00	24	Revenue object (24)	3		33	1
91	2022-02-23 15:49:35.42067+00	23	Revenue object (23)	3		33	1
92	2022-02-23 15:49:41.785951+00	26	Revenue object (26)	3		33	1
93	2022-02-23 15:50:25.486029+00	28	Revenue object (28)	3		33	1
94	2022-02-23 15:50:25.48774+00	27	Revenue object (27)	3		33	1
95	2022-02-23 15:51:44.625038+00	29	Revenue object (29)	3		33	1
96	2022-02-23 15:52:47.306266+00	30	Revenue object (30)	3		33	1
97	2022-02-23 18:28:04.859043+00	33	Revenue object (33)	3		33	1
98	2022-02-23 18:28:04.861018+00	32	Revenue object (32)	3		33	1
99	2022-02-23 18:28:04.861928+00	31	Revenue object (31)	3		33	1
100	2022-02-23 22:08:27.077776+00	12	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Credit	1	[{"added": {}}]	9	1
101	2022-02-23 22:12:30.70697+00	1	28.5	1	[{"added": {}}]	27	1
102	2022-02-23 22:12:33.150092+00	2	Avante - 2016 - АІ 8243 OВ  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (2)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
103	2022-02-23 22:16:45.878708+00	2	Avante - 2016 - АІ 8243 OВ  - W	2	[]	29	1
104	2022-02-23 22:17:16.404871+00	3	Accent - 2017 - АІ 0312 MX  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (3)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
105	2022-02-23 22:17:42.450483+00	4	Accent - 2010 - АI 9756 IH  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (4)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
106	2022-02-23 22:18:07.643549+00	5	Accent - 2011 - КA 2219 ВВ  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (5)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
107	2022-02-23 22:18:39.16612+00	6	Accent - 2011 - АІ 0315 MX  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (6)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
108	2022-02-23 22:27:24.388815+00	6	Accent - 2011 - АІ 0315 MX  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
109	2022-03-01 19:46:19.888296+00	11	Avante - 2014 - КА 3676 ВО 	1	[{"added": {}}]	15	1
110	2022-03-01 19:46:43.550415+00	12	Avante - 2016 - KA 9980 AT 	1	[{"added": {}}]	15	1
111	2022-03-01 19:47:53.709534+00	12	Avante - 2016 - KA 9980 AT 	2	[{"changed": {"fields": ["Office"]}}]	15	1
112	2022-03-01 20:16:18.829882+00	13	Avante - 2012 - AІ 9149 МН 	1	[{"added": {}}]	15	1
113	2022-03-01 20:19:17.076007+00	2	BMW	1	[{"added": {}}]	14	1
114	2022-03-01 20:19:27.090161+00	3	735i	1	[{"added": {}}]	22	1
115	2022-03-01 20:19:42.7032+00	14	735i - 2002 - AI 3839 ОС 	1	[{"added": {}}]	15	1
116	2022-03-01 20:20:14.392499+00	3	Toyota	1	[{"added": {}}]	14	1
117	2022-03-01 20:20:17.687662+00	4	Corolla	1	[{"added": {}}]	22	1
118	2022-03-01 20:20:38.247813+00	5	Camry	1	[{"added": {}}]	22	1
119	2022-03-01 20:20:43.076891+00	15	Camry - 2015 - AI 6531 OC 	1	[{"added": {}}]	15	1
120	2022-03-01 20:21:01.379677+00	16	Corolla - 2014 - АІ 2541 КО 	1	[{"added": {}}]	15	1
121	2022-03-01 20:21:19.073393+00	17	Corolla - 2018 - АІ 7462 ОВ 	1	[{"added": {}}]	15	1
122	2022-03-01 20:21:54.718697+00	18	Corolla - 2011 - АІ 6453 ОО 	1	[{"added": {}}]	15	1
123	2022-03-01 20:22:22.558068+00	19	Corolla - 2007 - АІ 8891 ІК 	1	[{"added": {}}]	15	1
124	2022-03-01 20:22:36.415763+00	20	Corolla - 2016 - АІ 9423 ІМ 	1	[{"added": {}}]	15	1
125	2022-03-01 20:22:55.246238+00	21	Corolla - 2015 - КА 2026 ВВ 	1	[{"added": {}}]	15	1
126	2022-03-01 20:23:12.362465+00	6	Elantra	1	[{"added": {}}]	22	1
127	2022-03-01 20:23:30.795742+00	22	Elantra - 2016 - АІ 3605 ОС 	1	[{"added": {}}]	15	1
128	2022-03-01 20:23:42.932868+00	23	Elantra - 2017 - АІ 5508 МР 	1	[{"added": {}}]	15	1
129	2022-03-01 20:23:59.294083+00	24	Elantra - 2016 - ВМ 0588 СМ 	1	[{"added": {}}]	15	1
130	2022-03-01 20:24:27.525851+00	25	Elantra - 2018 - ВМ 0610 СМ 	1	[{"added": {}}]	15	1
131	2022-03-01 20:24:41.294319+00	26	Elantra - 2017 - KA 0514 ВН 	1	[{"added": {}}]	15	1
132	2022-03-01 20:24:58.634674+00	27	Elantra - 2016 - KA 0574 ВМ 	1	[{"added": {}}]	15	1
133	2022-03-01 20:25:14.384286+00	28	Elantra - 2017 - KA 2816 ВВ 	1	[{"added": {}}]	15	1
134	2022-03-01 20:25:34.366863+00	29	Elantra - 2017 - KA 2853 ВВ 	1	[{"added": {}}]	15	1
135	2022-03-01 20:25:51.311077+00	30	Elantra - 2017 - KA 3564 ВI 	1	[{"added": {}}]	15	1
136	2022-03-01 20:26:03.410989+00	31	Elantra - 2017 - KA 5754 ВI 	1	[{"added": {}}]	15	1
137	2022-03-01 20:27:19.319968+00	32	Elantra - 2016 - KA 9134 ВЕ 	1	[{"added": {}}]	15	1
138	2022-03-01 20:27:52.031729+00	33	Elantra - 2018 - KA 9236 ВЕ 	1	[{"added": {}}]	15	1
139	2022-03-01 20:28:05.42354+00	34	Elantra - 2015 - KA 9864 AO 	1	[{"added": {}}]	15	1
140	2022-03-01 20:28:20.706389+00	35	Elantra - 2016 - KA 9865 AO 	1	[{"added": {}}]	15	1
141	2022-03-01 20:28:50.704657+00	36	Elantra - 2011 - АІ 0368 MX 	1	[{"added": {}}]	15	1
142	2022-03-01 20:29:32.749336+00	37	Elantra - 2017 - АІ 3159 MМ 	1	[{"added": {}}]	15	1
143	2022-03-01 20:29:52.644958+00	38	Elantra - 2016 - АІ 3167 MМ 	1	[{"added": {}}]	15	1
144	2022-03-01 20:30:09.820951+00	39	Elantra - 2016 - АІ 3168 MМ 	1	[{"added": {}}]	15	1
145	2022-03-01 20:30:25.058128+00	40	Elantra - 2016 - АІ 3351 MA 	1	[{"added": {}}]	15	1
146	2022-03-01 20:30:39.53109+00	41	Elantra - 2018 - АІ 3422 MР 	1	[{"added": {}}]	15	1
147	2022-03-01 20:32:12.085865+00	42	Elantra - 2018 - АІ 6304 МР 	1	[{"added": {}}]	15	1
148	2022-03-01 20:32:27.118516+00	43	Elantra - 2017 - АІ 6703 МР 	1	[{"added": {}}]	15	1
149	2022-03-01 20:32:39.091682+00	44	Elantra - 2017 - АІ 6896 ОВ 	1	[{"added": {}}]	15	1
150	2022-03-01 20:32:52.723702+00	45	Elantra - 2017 - AІ 7365 МI 	1	[{"added": {}}]	15	1
151	2022-03-01 20:33:07.565863+00	46	Elantra - 2016 - AІ 7453 ОВ 	1	[{"added": {}}]	15	1
152	2022-03-01 20:36:02.528643+00	47	Elantra - 2018 - AІ 7645 МТ 	1	[{"added": {}}]	15	1
153	2022-03-01 20:36:16.930958+00	48	Elantra - 2016 - AІ 8132 МТ 	1	[{"added": {}}]	15	1
154	2022-03-01 20:36:31.565346+00	49	Elantra - 2016 - AІ 8829 ММ 	1	[{"added": {}}]	15	1
155	2022-03-01 20:36:58.858097+00	50	Elantra - 2017 - AМ 6023 ЕТ 	1	[{"added": {}}]	15	1
156	2022-03-01 20:37:11.397631+00	51	Elantra - 2017 - KA 6328 ВI 	1	[{"added": {}}]	15	1
157	2022-03-01 21:19:54.757912+00	4	Skoda	1	[{"added": {}}]	14	1
158	2022-03-01 21:19:56.601048+00	7	Fabia	1	[{"added": {}}]	22	1
159	2022-03-01 21:20:03.012765+00	52	Fabia - 2008 - АА 8037 РР 	1	[{"added": {}}]	15	1
160	2022-03-01 21:20:19.860614+00	53	Fabia - 2015 - АА 8986 СС 	1	[{"added": {}}]	15	1
161	2022-03-01 21:22:18.731516+00	54	Fabia - 2013 - АІ 2803 СС 	1	[{"added": {}}]	15	1
162	2022-03-01 21:22:38.912003+00	55	Fabia - 2008 - АІ 7166 ІМ 	1	[{"added": {}}]	15	1
163	2022-03-01 21:23:02.113949+00	8	I30	1	[{"added": {}}]	22	1
164	2022-03-01 21:24:33.33848+00	56	I30 - 2011 - АА 3967 ТТ 	1	[{"added": {}}]	15	1
165	2022-03-01 21:24:53.71705+00	57	I30 - 2011 - АА 4421 ЕІ 	1	[{"added": {}}]	15	1
166	2022-03-01 21:25:07.646196+00	58	I30 - 2010 - АА 4934 ВР 	1	[{"added": {}}]	15	1
167	2022-03-01 21:25:21.948664+00	59	I30 - 2012 - АА 6079 СС 	1	[{"added": {}}]	15	1
168	2022-03-01 21:25:58.935218+00	60	I30 - 2008 - АІ 6723 ІН 	1	[{"added": {}}]	15	1
169	2022-03-01 21:26:23.233284+00	5	Volkswagen	1	[{"added": {}}]	14	1
170	2022-03-01 21:26:29.417988+00	9	Jetta	1	[{"added": {}}]	22	1
171	2022-03-01 21:26:47.322466+00	61	Jetta - 2008 - АІ 2258 MМ 	1	[{"added": {}}]	15	1
172	2022-03-01 21:28:03.193728+00	62	Jetta - 2011 - АI 3439 MP 	1	[{"added": {}}]	15	1
173	2022-03-01 21:28:17.420536+00	63	Jetta - 2010 - КА 0345 ВМ 	1	[{"added": {}}]	15	1
174	2022-03-01 21:28:53.483749+00	6	Kia	1	[{"added": {}}]	14	1
175	2022-03-01 21:28:57.041202+00	10	K5	1	[{"added": {}}]	22	1
176	2022-03-01 21:35:59.118714+00	64	K5 - 2012 - AI 5266 OO 	1	[{"added": {}}]	15	1
177	2022-03-01 21:36:15.887614+00	11	Carens	1	[{"added": {}}]	22	1
178	2022-03-01 21:36:22.361983+00	65	Carens - 2012 - AI 0712 OВ 	1	[{"added": {}}]	15	1
179	2022-03-01 21:36:41.108802+00	12	Forte	1	[{"added": {}}]	22	1
180	2022-03-01 21:37:30.979958+00	66	Forte - 2011 - AI 3724 OС 	1	[{"added": {}}]	15	1
181	2022-03-01 21:37:57.062743+00	67	Forte - 2011 - AI 9766 OA 	1	[{"added": {}}]	15	1
182	2022-03-01 21:38:10.077057+00	68	K5 - 2015 - АІ 0589 MІ 	1	[{"added": {}}]	15	1
183	2022-03-01 21:38:21.951044+00	69	K5 - 2015 - АІ 0724 MІ 	1	[{"added": {}}]	15	1
184	2022-03-01 21:38:34.840631+00	70	K5 - 2016 - АІ 3620 MP 	1	[{"added": {}}]	15	1
185	2022-03-01 21:38:48.294995+00	71	K5 - 2016 - АІ 5563 MP 	1	[{"added": {}}]	15	1
186	2022-03-01 21:39:13.710953+00	72	K5 - 2017 - АІ 5564 MP 	1	[{"added": {}}]	15	1
187	2022-03-01 21:40:07.814817+00	73	K5 - 2017 - АІ 7045 MP 	1	[{"added": {}}]	15	1
188	2022-03-01 21:40:23.257747+00	74	K5 - 2016 - АІ 7328 MР 	1	[{"added": {}}]	15	1
189	2022-03-01 21:40:42.394126+00	75	K5 - 2015 - АІ 7652 MТ 	1	[{"added": {}}]	15	1
190	2022-03-01 21:40:56.305232+00	76	K5 - 2015 - АІ 8089 MI 	1	[{"added": {}}]	15	1
191	2022-03-01 21:41:34.664305+00	13	Sorento	1	[{"added": {}}]	22	1
192	2022-03-01 21:41:37.185285+00	77	Sorento - 2016 - AI 2111 MB 	1	[{"added": {}}]	15	1
193	2022-03-01 21:41:57.78849+00	14	Sportage	1	[{"added": {}}]	22	1
194	2022-03-01 21:42:01.263021+00	78	Sportage - 2011 - KА 6164 EC 	1	[{"added": {}}]	15	1
195	2022-03-01 21:44:20.416045+00	7	Mazda	1	[{"added": {}}]	14	1
196	2022-03-01 21:44:26.37033+00	15	Mazda 6	1	[{"added": {}}]	22	1
197	2022-03-01 21:44:35.788901+00	79	Mazda 6 - 2007 - АА 1066 СС 	1	[{"added": {}}]	15	1
198	2022-03-01 21:45:41.251422+00	15	Mazda	2	[{"changed": {"fields": ["Name"]}}]	22	1
199	2022-03-01 21:45:50.771155+00	80	Mazda - 2013 - АІ 8006 МА 	1	[{"added": {}}]	15	1
200	2022-03-01 21:46:09.789239+00	8	Opel	1	[{"added": {}}]	14	1
201	2022-03-01 21:46:12.956454+00	16	Insignia	1	[{"added": {}}]	22	1
202	2022-03-01 21:46:15.598657+00	81	Insignia - 2006 - AI 1495 MO 	1	[{"added": {}}]	15	1
203	2022-03-01 21:48:24.413675+00	17	Passat	1	[{"added": {}}]	22	1
204	2022-03-01 21:48:34.889247+00	82	Passat - 2004 - AА 1556 ТT 	1	[{"added": {}}]	15	1
205	2022-03-01 21:48:53.493928+00	18	Polo	1	[{"added": {}}]	22	1
206	2022-03-01 21:49:00.250197+00	83	Polo - 2015 - KA 0728 AP 	1	[{"added": {}}]	15	1
207	2022-03-01 21:49:56.469763+00	84	Polo - 2013 - АЕ 9200 IM 	1	[{"added": {}}]	15	1
208	2022-03-01 21:50:13.054107+00	19	Rio	1	[{"added": {}}]	22	1
209	2022-03-01 21:50:31.266+00	85	Rio - 2012 - АА 6038 РР 	1	[{"added": {}}]	15	1
210	2022-03-01 21:50:48.134111+00	86	Rio - 2012 - АІ 1893 ІІ 	1	[{"added": {}}]	15	1
211	2022-03-01 21:51:09.058404+00	87	Rio - 2012 - КА 3306 ВМ 	1	[{"added": {}}]	15	1
212	2022-03-01 21:51:24.949448+00	88	Rio - 2011 - СА 3701 СК 	1	[{"added": {}}]	15	1
213	2022-03-01 21:51:44.72901+00	20	Sonata	1	[{"added": {}}]	22	1
214	2022-03-01 21:51:51.113678+00	89	Sonata - 2012 - АI 6782 ОА 	1	[{"added": {}}]	15	1
215	2022-03-01 21:52:05.282731+00	90	Sonata - 2012 - АI 6786 ОА 	1	[{"added": {}}]	15	1
216	2022-03-01 21:52:27.495258+00	91	Sonata - 2012 - АI 6805 ОА 	1	[{"added": {}}]	15	1
217	2022-03-01 21:52:40.630424+00	92	Sonata - 2016 - АI 6809 ОА 	1	[{"added": {}}]	15	1
218	2022-03-01 21:52:53.193077+00	93	Sonata - 2015 - АМ 3560 ЕО 	1	[{"added": {}}]	15	1
219	2022-03-01 21:53:09.022291+00	94	Sonata - 2015 - АI 0253 МР 	1	[{"added": {}}]	15	1
220	2022-03-01 21:53:22.707225+00	95	Sonata - 2015 - АI 0954 МB 	1	[{"added": {}}]	15	1
221	2022-03-01 21:53:38.220191+00	96	Sonata - 2015 - АI 0962 МB 	1	[{"added": {}}]	15	1
222	2022-03-01 21:53:52.989244+00	97	Sonata - 2016 - АI 1256 ММ 	1	[{"added": {}}]	15	1
223	2022-03-01 21:54:05.7903+00	98	Sonata - 2016 - АI 2518 МР 	1	[{"added": {}}]	15	1
224	2022-03-01 21:54:19.86006+00	99	Sonata - 2016 - АI 2519 МР 	1	[{"added": {}}]	15	1
225	2022-03-01 21:54:42.797082+00	100	Sonata - 2015 - АI 2846 МР 	1	[{"added": {}}]	15	1
226	2022-03-01 21:55:24.311708+00	101	Sonata - 2015 - АI 2875 ММ 	1	[{"added": {}}]	15	1
227	2022-03-01 21:55:37.423432+00	102	Sonata - 2017 - АI 2953 ММ 	1	[{"added": {}}]	15	1
228	2022-03-01 21:55:49.269871+00	103	Sonata - 2016 - АI 3702 MP 	1	[{"added": {}}]	15	1
229	2022-03-01 21:56:11.503314+00	104	Sonata - 2016 - АI 3922 MТ 	1	[{"added": {}}]	15	1
230	2022-03-01 21:56:25.298509+00	105	Sonata - 2016 - АI 4668 МЕ 	1	[{"added": {}}]	15	1
231	2022-03-01 21:56:42.76616+00	106	Sonata - 2014 - АI 5684 MP 	1	[{"added": {}}]	15	1
232	2022-03-01 21:56:58.21395+00	107	Sonata - 2013 - АI 5691 MP 	1	[{"added": {}}]	15	1
233	2022-03-01 21:57:06.366131+00	108	Sonata - 2015 - АI 6429 MP 	1	[{"added": {}}]	15	1
234	2022-03-01 21:57:20.346202+00	109	Sonata - 2015 - АI 6429 MP 	1	[{"added": {}}]	15	1
235	2022-03-01 21:57:46.362453+00	109	Sonata - 2008 - АI 7923 MТ 	2	[{"changed": {"fields": ["Number", "Year"]}}]	15	1
236	2022-03-01 21:58:02.067534+00	110	Sonata - 2012 - АI 8861 MТ 	1	[{"added": {}}]	15	1
237	2022-03-01 21:58:22.603815+00	111	Sonata - 2017 - АI 9104 МР 	1	[{"added": {}}]	15	1
238	2022-03-01 21:58:36.210375+00	112	Sonata - 2017 - АI 9361 МР 	1	[{"added": {}}]	15	1
239	2022-03-01 21:58:50.047518+00	113	Sonata - 2014 - АI 9364 МР 	1	[{"added": {}}]	15	1
240	2022-03-01 21:59:01.981749+00	114	Sonata - 2016 - АI 9681 МР 	1	[{"added": {}}]	15	1
241	2022-03-01 21:59:16.704463+00	115	Sonata - 2016 - АI 9702 МР 	1	[{"added": {}}]	15	1
242	2022-03-01 21:59:37.067863+00	116	Sonata - 2016 - АI 9814 МВ 	1	[{"added": {}}]	15	1
243	2022-03-01 21:59:50.275905+00	117	Sonata - 2016 - ВА 7546 ЕМ 	1	[{"added": {}}]	15	1
244	2022-03-01 22:00:03.062687+00	118	Sonata - 2017 - ВМ 0584 СМ 	1	[{"added": {}}]	15	1
245	2022-03-01 22:00:15.764028+00	119	Sonata - 2016 - КА 0583 ВM 	1	[{"added": {}}]	15	1
246	2022-03-01 22:00:28.657876+00	120	Sonata - 2015 - КА 0941 ВМ 	1	[{"added": {}}]	15	1
247	2022-03-01 22:00:46.19291+00	121	Sonata - 2016 - КА 2479 ВВ 	1	[{"added": {}}]	15	1
248	2022-03-01 22:00:57.946539+00	122	Sonata - 2015 - КА 2641 ВВ 	1	[{"added": {}}]	15	1
249	2022-03-01 22:01:15.046903+00	123	Sonata - 2015 - КА 3426 ВМ 	1	[{"added": {}}]	15	1
250	2022-03-01 22:01:28.152747+00	124	Sonata - 2017 - КА 6210 ET 	1	[{"added": {}}]	15	1
251	2022-03-01 22:01:40.318226+00	125	Sonata - 2016 - КА 8962 ВМ 	1	[{"added": {}}]	15	1
252	2022-03-01 22:01:54.367761+00	126	Sonata - 2016 - КА 9531 ВК 	1	[{"added": {}}]	15	1
253	2022-03-01 22:02:07.98755+00	127	Sonata - 2016 - СА 5407 ІВ 	1	[{"added": {}}]	15	1
254	2022-03-01 22:02:22.396194+00	128	Sonata - 2014 - СА 5408 ІВ 	1	[{"added": {}}]	15	1
255	2022-03-01 22:02:35.108051+00	129	Sonata - 2015 - АI 8894 ММ 	1	[{"added": {}}]	15	1
256	2022-03-01 22:02:47.227091+00	130	Sonata - 2015 - АI 3427 MP 	1	[{"added": {}}]	15	1
257	2022-03-01 22:03:50.108273+00	131	Sonata - 2015 - АА 1058 ЕМ 	1	[{"added": {}}]	15	1
258	2022-03-01 22:04:05.762004+00	132	Sonata - 2016 - АI 0886 KP 	1	[{"added": {}}]	15	1
259	2022-03-01 22:04:20.908305+00	9	Subaru	1	[{"added": {}}]	14	1
260	2022-03-01 22:04:22.552504+00	21	Subaru	1	[{"added": {}}]	22	1
261	2022-03-01 22:04:32.032823+00	133	Subaru - 2011 - AI 7103 OB 	1	[{"added": {}}]	15	1
262	2022-03-01 22:04:52.965921+00	22	Forester	1	[{"added": {}}]	22	1
263	2022-03-01 22:04:58.076358+00	134	Forester - 2007 - AI 8500 MT 	1	[{"added": {}}]	15	1
264	2022-03-01 22:10:57.058846+00	135	Rio - 1 - 1 	1	[{"added": {}}]	15	1
265	2022-03-01 22:11:06.101066+00	136	Polo - 1 - 1 	1	[{"added": {}}]	15	1
266	2022-03-01 22:11:18.553298+00	137	Mazda - 1 - 1 	1	[{"added": {}}]	15	1
267	2022-03-01 22:11:25.4707+00	138	Sportage - 1 - 1 	1	[{"added": {}}]	15	1
268	2022-03-01 22:11:34.54661+00	139	Passat - 1 - 1 	1	[{"added": {}}]	15	1
269	2022-03-01 22:11:46.672526+00	140	Insignia - 1 - 1 	1	[{"added": {}}]	15	1
270	2022-03-01 22:17:18.59739+00	140	Insignia - 1 - 1 	3		15	1
271	2022-03-01 22:17:18.599677+00	139	Passat - 1 - 1 	3		15	1
272	2022-03-01 22:17:18.600575+00	138	Sportage - 1 - 1 	3		15	1
273	2022-03-01 22:17:18.601456+00	137	Mazda - 1 - 1 	3		15	1
274	2022-03-01 22:17:18.602333+00	136	Polo - 1 - 1 	3		15	1
275	2022-03-01 22:17:18.603118+00	135	Rio - 1 - 1 	3		15	1
276	2022-03-01 22:27:32.993971+00	5	Accent - 2010 - АI 9756 IH 	2	[{"changed": {"name": "car image", "object": "Accent - 2010 - \\u0410I 9756 IH ", "fields": ["Path"]}}]	15	1
277	2022-03-01 22:27:43.750836+00	6	Accent - 2008 - КА 0729 АР 	2	[{"changed": {"name": "car image", "object": "Accent - 2008 - \\u041a\\u0410 0729 \\u0410\\u0420 ", "fields": ["Path"]}}]	15	1
278	2022-03-01 22:27:43.956031+00	6	Accent - 2008 - КА 0729 АР 	2	[{"changed": {"name": "car image", "object": "Accent - 2008 - \\u041a\\u0410 0729 \\u0410\\u0420 ", "fields": ["Path"]}}]	15	1
279	2022-03-01 22:27:54.027739+00	7	Accent - 2011 - КA 2219 ВВ 	2	[{"changed": {"name": "car image", "object": "Accent - 2011 - \\u041aA 2219 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
280	2022-03-01 22:28:05.361983+00	8	Accent - 2016 - AE 7193 KO 	2	[{"changed": {"name": "car image", "object": "Accent - 2016 - AE 7193 KO ", "fields": ["Path"]}}]	15	1
281	2022-03-01 22:28:16.77714+00	9	Avante - 2017 - АІ 8064 MO 	2	[{"changed": {"name": "car image", "object": "Avante - 2017 - \\u0410\\u0406 8064 MO ", "fields": ["Path"]}}]	15	1
282	2022-03-01 22:28:24.844791+00	10	Avante - 2016 - АІ 8243 OВ 	2	[{"changed": {"name": "car image", "object": "Avante - 2016 - \\u0410\\u0406 8243 O\\u0412 ", "fields": ["Path"]}}]	15	1
283	2022-03-01 22:28:41.849982+00	11	Avante - 2014 - КА 3676 ВО 	2	[{"added": {"name": "car image", "object": "Avante - 2014 - \\u041a\\u0410 3676 \\u0412\\u041e "}}]	15	1
284	2022-03-01 22:28:56.598252+00	12	Avante - 2016 - KA 9980 AT 	2	[{"added": {"name": "car image", "object": "Avante - 2016 - KA 9980 AT "}}]	15	1
285	2022-03-01 22:29:07.217368+00	13	Avante - 2012 - AІ 9149 МН 	2	[{"added": {"name": "car image", "object": "Avante - 2012 - A\\u0406 9149 \\u041c\\u041d "}}]	15	1
286	2022-03-01 22:29:16.549853+00	14	735i - 2002 - AI 3839 ОС 	2	[{"added": {"name": "car image", "object": "735i - 2002 - AI 3839 \\u041e\\u0421 "}}]	15	1
287	2022-03-01 22:29:26.510781+00	4	Accent - 2008 - АI 8250 OB 	2	[{"changed": {"name": "car image", "object": "Accent - 2008 - \\u0410I 8250 OB ", "fields": ["Path"]}}]	15	1
288	2022-03-01 22:29:35.630639+00	3	Accent - 2019 - АI 4463 MC 	2	[{"changed": {"name": "car image", "object": "Accent - 2019 - \\u0410I 4463 MC ", "fields": ["Path"]}}]	15	1
289	2022-03-01 22:29:48.812334+00	2	Accent - 2017 - АІ 0312 MX 	2	[{"changed": {"name": "car image", "object": "Accent - 2017 - \\u0410\\u0406 0312 MX ", "fields": ["Path"]}}]	15	1
290	2022-03-01 22:29:58.830606+00	1	Accent - 2011 - АІ 0315 MX 	2	[{"changed": {"name": "car image", "object": "Accent - 2011 - \\u0410\\u0406 0315 MX ", "fields": ["Path"]}}]	15	1
291	2022-03-01 22:40:59.597884+00	14	735i - 2002 - AI 3839 ОС 	2	[{"changed": {"name": "car image", "object": "735i - 2002 - AI 3839 \\u041e\\u0421 ", "fields": ["Path"]}}]	15	1
292	2022-03-01 22:41:50.925739+00	25	Elantra - 2018 - ВМ 0610 СМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - \\u0412\\u041c 0610 \\u0421\\u041c "}}]	15	1
293	2022-03-01 22:42:48.058579+00	26	Elantra - 2017 - KA 0514 ВН 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 0514 \\u0412\\u041d "}}]	15	1
294	2022-03-01 22:42:54.117758+00	27	Elantra - 2016 - KA 0574 ВМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - KA 0574 \\u0412\\u041c "}}]	15	1
295	2022-03-01 22:43:00.505592+00	28	Elantra - 2017 - KA 2816 ВВ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 2816 \\u0412\\u0412 "}}]	15	1
296	2022-03-01 22:43:07.630083+00	29	Elantra - 2017 - KA 2853 ВВ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 2853 \\u0412\\u0412 "}}]	15	1
297	2022-03-01 22:43:16.609644+00	30	Elantra - 2017 - KA 3564 ВI 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 3564 \\u0412I "}}]	15	1
298	2022-03-01 22:43:23.18949+00	31	Elantra - 2017 - KA 5754 ВI 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 5754 \\u0412I "}}]	15	1
299	2022-03-01 22:43:29.227717+00	32	Elantra - 2016 - KA 9134 ВЕ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - KA 9134 \\u0412\\u0415 "}}]	15	1
300	2022-03-01 22:43:35.937509+00	33	Elantra - 2018 - KA 9236 ВЕ 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - KA 9236 \\u0412\\u0415 "}}]	15	1
301	2022-03-01 22:43:41.891547+00	34	Elantra - 2015 - KA 9864 AO 	2	[{"added": {"name": "car image", "object": "Elantra - 2015 - KA 9864 AO "}}]	15	1
302	2022-03-01 22:43:56.75758+00	22	Elantra - 2016 - АІ 3605 ОС 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3605 \\u041e\\u0421 "}}]	15	1
303	2022-03-01 22:44:02.949876+00	24	Elantra - 2016 - ВМ 0588 СМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0412\\u041c 0588 \\u0421\\u041c "}}]	15	1
304	2022-03-01 22:44:12.351141+00	23	Elantra - 2017 - АІ 5508 МР 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 5508 \\u041c\\u0420 "}}]	15	1
305	2022-03-01 22:44:29.080334+00	21	Corolla - 2015 - КА 2026 ВВ 	2	[{"added": {"name": "car image", "object": "Corolla - 2015 - \\u041a\\u0410 2026 \\u0412\\u0412 "}}]	15	1
306	2022-03-01 22:45:30.741316+00	21	Corolla - 2015 - КА 2026 ВВ 	2	[{"changed": {"name": "car image", "object": "Corolla - 2015 - \\u041a\\u0410 2026 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
307	2022-03-01 22:45:36.667787+00	20	Corolla - 2016 - АІ 9423 ІМ 	2	[{"added": {"name": "car image", "object": "Corolla - 2016 - \\u0410\\u0406 9423 \\u0406\\u041c "}}]	15	1
308	2022-03-01 22:45:43.674228+00	19	Corolla - 2007 - АІ 8891 ІК 	2	[{"added": {"name": "car image", "object": "Corolla - 2007 - \\u0410\\u0406 8891 \\u0406\\u041a "}}]	15	1
309	2022-03-01 22:45:53.096722+00	18	Corolla - 2011 - АІ 6453 ОО 	2	[{"added": {"name": "car image", "object": "Corolla - 2011 - \\u0410\\u0406 6453 \\u041e\\u041e "}}]	15	1
310	2022-03-01 22:46:17.818112+00	17	Corolla - 2018 - АІ 7462 ОВ 	2	[{"added": {"name": "car image", "object": "Corolla - 2018 - \\u0410\\u0406 7462 \\u041e\\u0412 "}}]	15	1
311	2022-03-01 22:46:23.69235+00	16	Corolla - 2014 - АІ 2541 КО 	2	[{"added": {"name": "car image", "object": "Corolla - 2014 - \\u0410\\u0406 2541 \\u041a\\u041e "}}]	15	1
312	2022-03-01 22:46:39.982289+00	15	Camry - 2015 - AI 6531 OC 	2	[{"added": {"name": "car image", "object": "Camry - 2015 - AI 6531 OC "}}]	15	1
313	2022-03-01 22:47:32.332386+00	45	Elantra - 2017 - AІ 7365 МI 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - A\\u0406 7365 \\u041cI "}}]	15	1
314	2022-03-01 22:47:39.047285+00	46	Elantra - 2016 - AІ 7453 ОВ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - A\\u0406 7453 \\u041e\\u0412 "}}]	15	1
315	2022-03-01 22:47:47.363856+00	47	Elantra - 2018 - AІ 7645 МТ 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - A\\u0406 7645 \\u041c\\u0422 "}}]	15	1
316	2022-03-01 22:47:55.445327+00	48	Elantra - 2016 - AІ 8132 МТ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - A\\u0406 8132 \\u041c\\u0422 "}}]	15	1
317	2022-03-01 22:48:11.052916+00	49	Elantra - 2016 - AІ 8829 ММ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - A\\u0406 8829 \\u041c\\u041c "}}]	15	1
318	2022-03-01 22:48:18.153574+00	50	Elantra - 2017 - AМ 6023 ЕТ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - A\\u041c 6023 \\u0415\\u0422 "}}]	15	1
319	2022-03-01 22:48:25.677577+00	51	Elantra - 2017 - KA 6328 ВI 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 6328 \\u0412I "}}]	15	1
320	2022-03-01 22:48:41.598347+00	52	Fabia - 2008 - АА 8037 РР 	2	[{"added": {"name": "car image", "object": "Fabia - 2008 - \\u0410\\u0410 8037 \\u0420\\u0420 "}}]	15	1
321	2022-03-01 22:48:48.563334+00	53	Fabia - 2015 - АА 8986 СС 	2	[{"added": {"name": "car image", "object": "Fabia - 2015 - \\u0410\\u0410 8986 \\u0421\\u0421 "}}]	15	1
322	2022-03-01 22:48:56.200696+00	54	Fabia - 2013 - АІ 2803 СС 	2	[{"added": {"name": "car image", "object": "Fabia - 2013 - \\u0410\\u0406 2803 \\u0421\\u0421 "}}]	15	1
323	2022-03-01 22:49:14.615827+00	35	Elantra - 2016 - KA 9865 AO 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - KA 9865 AO "}}]	15	1
324	2022-03-01 22:49:21.576706+00	36	Elantra - 2011 - АІ 0368 MX 	2	[{"added": {"name": "car image", "object": "Elantra - 2011 - \\u0410\\u0406 0368 MX "}}]	15	1
325	2022-03-01 22:49:28.777238+00	37	Elantra - 2017 - АІ 3159 MМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 3159 M\\u041c "}}]	15	1
326	2022-03-01 22:49:44.901405+00	38	Elantra - 2016 - АІ 3167 MМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3167 M\\u041c "}}]	15	1
327	2022-03-01 22:49:52.891951+00	39	Elantra - 2016 - АІ 3168 MМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3168 M\\u041c "}}]	15	1
328	2022-03-01 22:49:59.957671+00	40	Elantra - 2016 - АІ 3351 MA 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3351 MA "}}]	15	1
329	2022-03-01 22:50:08.800015+00	41	Elantra - 2018 - АІ 3422 MР 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - \\u0410\\u0406 3422 M\\u0420 "}}]	15	1
330	2022-03-01 22:50:20.901393+00	42	Elantra - 2018 - АІ 6304 МР 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - \\u0410\\u0406 6304 \\u041c\\u0420 "}}]	15	1
331	2022-03-01 22:50:37.450462+00	43	Elantra - 2017 - АІ 6703 МР 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 6703 \\u041c\\u0420 "}}]	15	1
332	2022-03-01 22:51:01.469665+00	44	Elantra - 2017 - АІ 6896 ОВ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 6896 \\u041e\\u0412 "}}]	15	1
333	2022-03-01 22:52:25.644016+00	75	K5 - 2015 - АІ 7652 MТ 	2	[{"added": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 7652 M\\u0422 "}}]	15	1
334	2022-03-01 22:52:35.640524+00	77	Sorento - 2016 - AI 2111 MB 	2	[{"added": {"name": "car image", "object": "Sorento - 2016 - AI 2111 MB "}}]	15	1
335	2022-03-01 22:53:04.029222+00	78	Sportage - 2011 - KА 6164 EC 	2	[{"added": {"name": "car image", "object": "Sportage - 2011 - K\\u0410 6164 EC "}}]	15	1
336	2022-03-01 22:53:51.081641+00	79	Mazda - 2007 - АА 1066 СС 	2	[{"added": {"name": "car image", "object": "Mazda - 2007 - \\u0410\\u0410 1066 \\u0421\\u0421 "}}]	15	1
337	2022-03-01 22:53:59.485635+00	80	Mazda - 2013 - АІ 8006 МА 	2	[{"added": {"name": "car image", "object": "Mazda - 2013 - \\u0410\\u0406 8006 \\u041c\\u0410 "}}]	15	1
338	2022-03-01 22:54:13.47992+00	81	Insignia - 2006 - AI 1495 MO 	2	[{"added": {"name": "car image", "object": "Insignia - 2006 - AI 1495 MO "}}]	15	1
339	2022-03-01 22:54:30.25009+00	82	Passat - 2004 - AА 1556 ТT 	2	[{"added": {"name": "car image", "object": "Passat - 2004 - A\\u0410 1556 \\u0422T "}}]	15	1
340	2022-03-01 22:54:37.677241+00	83	Polo - 2015 - KA 0728 AP 	2	[{"added": {"name": "car image", "object": "Polo - 2015 - KA 0728 AP "}}]	15	1
341	2022-03-01 22:54:46.159216+00	84	Polo - 2013 - АЕ 9200 IM 	2	[{"added": {"name": "car image", "object": "Polo - 2013 - \\u0410\\u0415 9200 IM "}}]	15	1
342	2022-03-01 22:55:06.997944+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"added": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 0589 M\\u0406 "}}]	15	1
343	2022-03-01 22:55:15.141148+00	65	Carens - 2012 - AI 0712 OВ 	2	[{"added": {"name": "car image", "object": "Carens - 2012 - AI 0712 O\\u0412 "}}]	15	1
344	2022-03-01 22:55:34.641329+00	66	Forte - 2011 - AI 3724 OС 	2	[{"added": {"name": "car image", "object": "Forte - 2011 - AI 3724 O\\u0421 "}}]	15	1
345	2022-03-01 22:55:52.716992+00	67	Forte - 2011 - AI 9766 OA 	2	[{"added": {"name": "car image", "object": "Forte - 2011 - AI 9766 OA "}}]	15	1
346	2022-03-01 22:56:02.654722+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"added": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 0589 M\\u0406 "}}]	15	1
347	2022-03-01 22:56:14.551171+00	69	K5 - 2015 - АІ 0724 MІ 	2	[{"added": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 0724 M\\u0406 "}}]	15	1
348	2022-03-01 22:56:49.255672+00	70	K5 - 2016 - АІ 3620 MP 	2	[{"added": {"name": "car image", "object": "K5 - 2016 - \\u0410\\u0406 3620 MP "}}]	15	1
349	2022-03-01 22:56:59.635971+00	71	K5 - 2016 - АІ 5563 MP 	2	[{"added": {"name": "car image", "object": "K5 - 2016 - \\u0410\\u0406 5563 MP "}}]	15	1
350	2022-03-01 22:57:21.204911+00	72	K5 - 2017 - АІ 5564 MP 	2	[{"added": {"name": "car image", "object": "K5 - 2017 - \\u0410\\u0406 5564 MP "}}]	15	1
351	2022-03-01 22:57:31.034796+00	73	K5 - 2017 - АІ 7045 MP 	2	[{"added": {"name": "car image", "object": "K5 - 2017 - \\u0410\\u0406 7045 MP "}}]	15	1
352	2022-03-01 22:57:46.032861+00	74	K5 - 2016 - АІ 7328 MР 	2	[{"added": {"name": "car image", "object": "K5 - 2016 - \\u0410\\u0406 7328 M\\u0420 "}}]	15	1
353	2022-03-01 22:57:55.805618+00	55	Fabia - 2008 - АІ 7166 ІМ 	2	[{"added": {"name": "car image", "object": "Fabia - 2008 - \\u0410\\u0406 7166 \\u0406\\u041c "}}]	15	1
354	2022-03-01 22:58:18.619945+00	56	I30 - 2011 - АА 3967 ТТ 	2	[{"added": {"name": "car image", "object": "I30 - 2011 - \\u0410\\u0410 3967 \\u0422\\u0422 "}}]	15	1
355	2022-03-01 22:58:27.692394+00	57	I30 - 2011 - АА 4421 ЕІ 	2	[{"added": {"name": "car image", "object": "I30 - 2011 - \\u0410\\u0410 4421 \\u0415\\u0406 "}}]	15	1
356	2022-03-01 22:58:33.878677+00	58	I30 - 2010 - АА 4934 ВР 	2	[{"added": {"name": "car image", "object": "I30 - 2010 - \\u0410\\u0410 4934 \\u0412\\u0420 "}}]	15	1
357	2022-03-01 22:58:58.92934+00	59	I30 - 2012 - АА 6079 СС 	2	[{"added": {"name": "car image", "object": "I30 - 2012 - \\u0410\\u0410 6079 \\u0421\\u0421 "}}]	15	1
358	2022-03-01 22:59:31.882229+00	60	I30 - 2008 - АІ 6723 ІН 	2	[{"added": {"name": "car image", "object": "I30 - 2008 - \\u0410\\u0406 6723 \\u0406\\u041d "}}]	15	1
359	2022-03-01 22:59:42.243225+00	61	Jetta - 2008 - АІ 2258 MМ 	2	[{"added": {"name": "car image", "object": "Jetta - 2008 - \\u0410\\u0406 2258 M\\u041c "}}]	15	1
360	2022-03-01 22:59:55.374385+00	62	Jetta - 2011 - АI 3439 MP 	2	[{"added": {"name": "car image", "object": "Jetta - 2011 - \\u0410I 3439 MP "}}]	15	1
361	2022-03-01 23:00:04.025923+00	63	Jetta - 2010 - КА 0345 ВМ 	2	[{"added": {"name": "car image", "object": "Jetta - 2010 - \\u041a\\u0410 0345 \\u0412\\u041c "}}]	15	1
362	2022-03-01 23:00:19.385057+00	64	K5 - 2012 - AI 5266 OO 	2	[{"added": {"name": "car image", "object": "K5 - 2012 - AI 5266 OO "}}]	15	1
363	2022-03-01 23:02:38.143218+00	125	Sonata - 2016 - КА 8962 ВМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 8962 \\u0412\\u041c "}}]	15	1
364	2022-03-01 23:02:45.344265+00	94	Sonata - 2015 - АI 0253 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 0253 \\u041c\\u0420 "}}]	15	1
365	2022-03-01 23:02:52.596183+00	93	Sonata - 2015 - АМ 3560 ЕО 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410\\u041c 3560 \\u0415\\u041e "}}]	15	1
366	2022-03-01 23:03:00.984658+00	92	Sonata - 2016 - АI 6809 ОА 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 6809 \\u041e\\u0410 "}}]	15	1
367	2022-03-01 23:03:12.340848+00	91	Sonata - 2012 - АI 6805 ОА 	2	[{"added": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 6805 \\u041e\\u0410 "}}]	15	1
368	2022-03-01 23:03:20.67202+00	90	Sonata - 2012 - АI 6786 ОА 	2	[{"added": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 6786 \\u041e\\u0410 "}}]	15	1
369	2022-03-01 23:03:29.971644+00	89	Sonata - 2012 - АI 6782 ОА 	2	[{"added": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 6782 \\u041e\\u0410 "}}]	15	1
414	2022-03-02 21:11:22.454488+00	53	Fabia - 2015 - АА 8986 СС 	2	[{"changed": {"fields": ["Status"]}}]	15	1
370	2022-03-01 23:03:39.061141+00	88	Rio - 2011 - СА 3701 СК 	2	[{"added": {"name": "car image", "object": "Rio - 2011 - \\u0421\\u0410 3701 \\u0421\\u041a "}}]	15	1
371	2022-03-01 23:03:48.391841+00	87	Rio - 2012 - КА 3306 ВМ 	2	[{"added": {"name": "car image", "object": "Rio - 2012 - \\u041a\\u0410 3306 \\u0412\\u041c "}}]	15	1
372	2022-03-01 23:03:59.849655+00	104	Sonata - 2016 - АI 3922 MТ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 3922 M\\u0422 "}}]	15	1
373	2022-03-01 23:04:08.333769+00	103	Sonata - 2016 - АI 3702 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 3702 MP "}}]	15	1
374	2022-03-01 23:04:16.514565+00	102	Sonata - 2017 - АI 2953 ММ 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u0410I 2953 \\u041c\\u041c "}}]	15	1
375	2022-03-01 23:04:25.507085+00	101	Sonata - 2015 - АI 2875 ММ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 2875 \\u041c\\u041c "}}]	15	1
376	2022-03-01 23:04:35.065813+00	100	Sonata - 2015 - АI 2846 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 2846 \\u041c\\u0420 "}}]	15	1
377	2022-03-01 23:04:43.580012+00	99	Sonata - 2016 - АI 2519 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 2519 \\u041c\\u0420 "}}]	15	1
378	2022-03-01 23:04:51.316847+00	98	Sonata - 2016 - АI 2518 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 2518 \\u041c\\u0420 "}}]	15	1
379	2022-03-01 23:04:58.713333+00	97	Sonata - 2016 - АI 1256 ММ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 1256 \\u041c\\u041c "}}]	15	1
380	2022-03-01 23:05:07.588549+00	96	Sonata - 2015 - АI 0962 МB 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 0962 \\u041cB "}}]	15	1
381	2022-03-01 23:05:15.730522+00	95	Sonata - 2015 - АI 0954 МB 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 0954 \\u041cB "}}]	15	1
382	2022-03-01 23:05:23.176419+00	114	Sonata - 2016 - АI 9681 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 9681 \\u041c\\u0420 "}}]	15	1
383	2022-03-01 23:05:36.569166+00	113	Sonata - 2014 - АI 9364 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2014 - \\u0410I 9364 \\u041c\\u0420 "}}]	15	1
384	2022-03-01 23:05:45.092277+00	112	Sonata - 2017 - АI 9361 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u0410I 9361 \\u041c\\u0420 "}}]	15	1
385	2022-03-01 23:05:52.522486+00	111	Sonata - 2017 - АI 9104 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u0410I 9104 \\u041c\\u0420 "}}]	15	1
386	2022-03-01 23:06:02.05283+00	110	Sonata - 2012 - АI 8861 MТ 	2	[{"added": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 8861 M\\u0422 "}}]	15	1
387	2022-03-01 23:06:10.492417+00	109	Sonata - 2008 - АI 7923 MТ 	2	[{"added": {"name": "car image", "object": "Sonata - 2008 - \\u0410I 7923 M\\u0422 "}}]	15	1
388	2022-03-01 23:06:19.491483+00	108	Sonata - 2015 - АI 6429 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 6429 MP "}}]	15	1
389	2022-03-01 23:06:27.756977+00	107	Sonata - 2013 - АI 5691 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2013 - \\u0410I 5691 MP "}}]	15	1
390	2022-03-01 23:06:42.257336+00	106	Sonata - 2014 - АI 5684 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2014 - \\u0410I 5684 MP "}}]	15	1
391	2022-03-01 23:06:50.588132+00	106	Sonata - 2014 - АI 5684 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2014 - \\u0410I 5684 MP "}}]	15	1
392	2022-03-01 23:07:01.881766+00	105	Sonata - 2016 - АI 4668 МЕ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 4668 \\u041c\\u0415 "}}]	15	1
393	2022-03-01 23:07:17.132313+00	115	Sonata - 2016 - АI 9702 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 9702 \\u041c\\u0420 "}}]	15	1
394	2022-03-01 23:07:24.547849+00	124	Sonata - 2017 - КА 6210 ET 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u041a\\u0410 6210 ET "}}]	15	1
395	2022-03-01 23:07:40.72789+00	123	Sonata - 2015 - КА 3426 ВМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u041a\\u0410 3426 \\u0412\\u041c "}}]	15	1
396	2022-03-01 23:07:59.954129+00	122	Sonata - 2015 - КА 2641 ВВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u041a\\u0410 2641 \\u0412\\u0412 "}}]	15	1
397	2022-03-01 23:08:08.563233+00	121	Sonata - 2016 - КА 2479 ВВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 2479 \\u0412\\u0412 "}}]	15	1
398	2022-03-01 23:08:21.522226+00	120	Sonata - 2015 - КА 0941 ВМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u041a\\u0410 0941 \\u0412\\u041c "}}]	15	1
399	2022-03-01 23:08:32.791178+00	119	Sonata - 2016 - КА 0583 ВM 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 0583 \\u0412M "}}]	15	1
400	2022-03-01 23:08:38.999641+00	118	Sonata - 2017 - ВМ 0584 СМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u0412\\u041c 0584 \\u0421\\u041c "}}]	15	1
401	2022-03-01 23:08:45.827078+00	117	Sonata - 2016 - ВА 7546 ЕМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0412\\u0410 7546 \\u0415\\u041c "}}]	15	1
402	2022-03-01 23:09:01.048432+00	116	Sonata - 2016 - АI 9814 МВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 9814 \\u041c\\u0412 "}}]	15	1
403	2022-03-01 23:09:22.644664+00	134	Forester - 2007 - AI 8500 MT 	2	[{"added": {"name": "car image", "object": "Forester - 2007 - AI 8500 MT "}}]	15	1
404	2022-03-01 23:09:33.818384+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"added": {"name": "car image", "object": "Subaru - 2011 - AI 7103 OB "}}]	15	1
405	2022-03-01 23:09:40.43609+00	132	Sonata - 2016 - АI 0886 KP 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 0886 KP "}}]	15	1
406	2022-03-01 23:09:46.881786+00	131	Sonata - 2015 - АА 1058 ЕМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410\\u0410 1058 \\u0415\\u041c "}}]	15	1
407	2022-03-01 23:09:53.526777+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 3427 MP "}}]	15	1
408	2022-03-01 23:10:00.246464+00	129	Sonata - 2015 - АI 8894 ММ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 8894 \\u041c\\u041c "}}]	15	1
409	2022-03-01 23:10:08.21981+00	128	Sonata - 2014 - СА 5408 ІВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2014 - \\u0421\\u0410 5408 \\u0406\\u0412 "}}]	15	1
410	2022-03-01 23:10:15.208008+00	127	Sonata - 2016 - СА 5407 ІВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0421\\u0410 5407 \\u0406\\u0412 "}}]	15	1
411	2022-03-01 23:10:22.148896+00	126	Sonata - 2016 - КА 9531 ВК 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 9531 \\u0412\\u041a "}}]	15	1
412	2022-03-02 21:11:04.541337+00	78	Sportage - 2011 - KА 6164 EC 	2	[{"changed": {"fields": ["Status"]}}]	15	1
413	2022-03-02 21:11:15.3773+00	83	Polo - 2015 - KA 0728 AP 	2	[{"changed": {"fields": ["Status"]}}]	15	1
415	2022-03-02 21:11:27.558166+00	85	Rio - 2012 - АА 6038 РР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
416	2022-03-02 21:11:32.998139+00	126	Sonata - 2016 - КА 9531 ВК 	2	[{"changed": {"fields": ["Status"]}}]	15	1
417	2022-03-02 21:11:36.890054+00	88	Rio - 2011 - СА 3701 СК 	2	[{"changed": {"fields": ["Status"]}}]	15	1
418	2022-03-02 21:11:41.458011+00	5	Accent - 2010 - АI 9756 IH 	2	[{"changed": {"fields": ["Status"]}}]	15	1
419	2022-03-02 21:11:45.3175+00	13	Avante - 2012 - AІ 9149 МН 	2	[{"changed": {"fields": ["Status"]}}]	15	1
420	2022-03-02 21:11:49.2553+00	14	735i - 2002 - AI 3839 ОС 	2	[{"changed": {"fields": ["Status"]}}]	15	1
421	2022-03-02 21:11:53.072789+00	23	Elantra - 2017 - АІ 5508 МР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
422	2022-03-02 21:11:56.659472+00	43	Elantra - 2017 - АІ 6703 МР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
423	2022-03-02 21:12:00.145793+00	65	Carens - 2012 - AI 0712 OВ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
424	2022-03-02 21:12:04.42617+00	54	Fabia - 2013 - АІ 2803 СС 	2	[{"changed": {"fields": ["Status"]}}]	15	1
425	2022-03-02 21:12:08.638068+00	91	Sonata - 2012 - АI 6805 ОА 	2	[{"changed": {"fields": ["Status"]}}]	15	1
426	2022-03-02 21:12:12.17699+00	80	Mazda - 2013 - АІ 8006 МА 	2	[{"changed": {"fields": ["Status"]}}]	15	1
427	2022-03-02 21:12:15.672421+00	58	I30 - 2010 - АА 4934 ВР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
428	2022-03-02 21:12:19.414398+00	3	Accent - 2019 - АI 4463 MC 	2	[{"changed": {"fields": ["Status"]}}]	15	1
429	2022-03-02 21:12:22.265861+00	45	Elantra - 2017 - AІ 7365 МI 	2	[{"changed": {"fields": ["Status"]}}]	15	1
430	2022-03-02 21:12:25.326788+00	55	Fabia - 2008 - АІ 7166 ІМ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
431	2022-03-02 21:12:28.548297+00	56	I30 - 2011 - АА 3967 ТТ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
432	2022-03-02 21:12:31.738111+00	59	I30 - 2012 - АА 6079 СС 	2	[{"changed": {"fields": ["Status"]}}]	15	1
433	2022-03-02 21:12:34.977128+00	95	Sonata - 2015 - АI 0954 МB 	2	[{"changed": {"fields": ["Status"]}}]	15	1
434	2022-03-02 21:12:39.157191+00	61	Jetta - 2008 - АІ 2258 MМ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
435	2022-03-02 21:12:42.832462+00	81	Insignia - 2006 - AI 1495 MO 	2	[{"changed": {"fields": ["Status"]}}]	15	1
436	2022-03-02 21:12:46.018313+00	64	K5 - 2012 - AI 5266 OO 	2	[{"changed": {"fields": ["Status"]}}]	15	1
437	2022-03-02 21:12:49.441096+00	52	Fabia - 2008 - АА 8037 РР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
438	2022-03-02 21:12:53.620228+00	87	Rio - 2012 - КА 3306 ВМ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
439	2022-03-02 21:12:57.105092+00	6	Accent - 2008 - КА 0729 АР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
440	2022-03-02 21:13:00.532713+00	17	Corolla - 2018 - АІ 7462 ОВ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
441	2022-03-02 21:13:04.355397+00	60	I30 - 2008 - АІ 6723 ІН 	2	[{"changed": {"fields": ["Status"]}}]	15	1
442	2022-03-02 21:13:07.638468+00	67	Forte - 2011 - AI 9766 OA 	2	[{"changed": {"fields": ["Status"]}}]	15	1
443	2022-03-02 21:13:11.153118+00	82	Passat - 2004 - AА 1556 ТT 	2	[{"changed": {"fields": ["Status"]}}]	15	1
444	2022-03-02 21:13:15.826065+00	84	Polo - 2013 - АЕ 9200 IM 	2	[{"changed": {"fields": ["Status"]}}]	15	1
445	2022-03-02 21:13:19.799509+00	86	Rio - 2012 - АІ 1893 ІІ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
446	2022-03-02 21:13:23.277397+00	131	Sonata - 2015 - АА 1058 ЕМ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
447	2022-03-02 21:13:26.564813+00	93	Sonata - 2015 - АМ 3560 ЕО 	2	[{"changed": {"fields": ["Status"]}}]	15	1
448	2022-03-02 21:13:30.30976+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"changed": {"fields": ["Status"]}}]	15	1
449	2022-03-02 21:13:33.792423+00	90	Sonata - 2012 - АI 6786 ОА 	2	[{"changed": {"fields": ["Status"]}}]	15	1
450	2022-03-02 21:13:37.369868+00	76	K5 - 2015 - АІ 8089 MI 	2	[{"changed": {"fields": ["Status"]}}]	15	1
451	2022-03-02 21:13:40.673451+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
452	2022-03-02 21:13:44.201344+00	77	Sorento - 2016 - AI 2111 MB 	2	[{"changed": {"fields": ["Status"]}}]	15	1
453	2022-03-02 21:13:48.393292+00	18	Corolla - 2011 - АІ 6453 ОО 	2	[{"changed": {"fields": ["Status"]}}]	15	1
454	2022-03-02 23:24:33.691778+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Status"]}}]	15	1
455	2022-03-02 23:24:49.11714+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Status"]}}]	15	1
456	2022-03-02 23:28:08.654852+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
457	2022-03-03 00:08:07.415006+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Current tyre"]}}]	15	1
458	2022-03-03 09:17:41.66814+00	1	Seyidov Elvin	2	[{"changed": {"fields": ["Type"]}}]	8	1
459	2022-03-03 09:17:50.451692+00	2	Mammadxanli Farid	2	[{"changed": {"fields": ["Type"]}}]	8	1
460	2022-03-03 09:22:41.919139+00	3	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
461	2022-03-03 09:23:01.876508+00	3	Дмитрий Каюн	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
462	2022-03-03 09:24:35.800924+00	4	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
463	2022-03-03 09:24:57.826395+00	4	Язгулиев Шохрат	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
464	2022-03-03 09:25:38.751893+00	5	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
465	2022-03-03 09:25:53.175003+00	5	Чолиев Юнус	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
466	2022-03-03 09:26:06.555209+00	6	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
467	2022-03-03 09:26:26.123009+00	6	Акрамов Мукхриддин	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
468	2022-03-03 09:26:39.097161+00	6	Акрамов Мукхриддин	2	[]	8	1
469	2022-03-03 09:26:52.605459+00	7	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
470	2022-03-03 09:27:01.797316+00	7	Авдиев Бабагелди	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
471	2022-03-03 09:34:17.670961+00	8	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
472	2022-03-03 09:34:27.096061+00	8	Айрапетов Артём	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
473	2022-03-03 09:34:51.624974+00	9	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
474	2022-03-03 09:34:59.94593+00	9	Батыров Гурбан	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
475	2022-03-03 09:38:59.48564+00	10	 	1	[{"added": {}}]	8	1
476	2022-03-03 09:39:18.158143+00	10	(без договора) ВЫКУП	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
477	2022-03-03 09:39:40.54883+00	11	 	1	[{"added": {}}]	8	1
478	2022-03-03 09:39:48.88261+00	11	Романчук Леонид	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
479	2022-03-03 09:40:12.406818+00	12	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
480	2022-03-03 09:40:20.402643+00	12	Бережной Сергей	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
481	2022-03-03 09:40:38.664562+00	13	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
482	2022-03-03 09:41:04.281816+00	13	Исмайлов Ойвен	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
483	2022-03-03 09:41:42.889231+00	14	 	1	[{"added": {}}]	8	1
484	2022-03-03 09:41:51.366027+00	14	выкуп Заур	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
485	2022-03-03 09:43:12.163164+00	15	 	1	[{"added": {}}]	8	1
486	2022-03-03 09:43:22.071094+00	15	Лаптев Олег	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
487	2022-03-03 09:43:42.266699+00	16	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
488	2022-03-03 09:44:12.009513+00	16	Кара Эрхан	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
489	2022-03-03 09:44:24.618818+00	17	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
490	2022-03-03 11:42:51.148193+00	17	Кулиев Азат	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
491	2022-03-03 12:05:57.67428+00	18	 	1	[{"added": {}}]	8	1
492	2022-03-03 12:06:04.266447+00	18	выкуп Али	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
493	2022-03-03 12:06:16.891804+00	19	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
494	2022-03-03 12:06:35.811283+00	19	Чолиев Рустем	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
495	2022-03-03 12:06:49.394345+00	20	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
496	2022-03-03 12:07:10.502188+00	20	Абдыресулов Рахман	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
497	2022-03-03 12:07:29.470313+00	21	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
498	2022-03-03 12:08:54.244115+00	21	Абдиев Довлетгельди	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
499	2022-03-03 12:09:08.81928+00	22	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
500	2022-03-03 12:09:17.576706+00	22	Москалик Юрий	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
501	2022-03-03 12:09:37.698784+00	23	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
502	2022-03-03 12:09:45.618147+00	23	Гончаров Сергей	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
503	2022-03-03 12:09:58.369359+00	24	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
504	2022-03-03 12:10:06.260634+00	24	Фархатов Фарух	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
505	2022-03-03 12:10:19.096858+00	25	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
506	2022-03-03 12:10:30.73299+00	25	Атаджанов Бегенч	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
507	2022-03-03 12:10:41.718452+00	26	 	1	[{"added": {}}]	8	1
508	2022-03-03 12:10:46.97698+00	26	Валиев Эльвин	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
509	2022-03-03 12:11:04.820851+00	27	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
510	2022-03-03 12:11:13.343649+00	27	Яхяев Шамиль	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
511	2022-03-03 12:11:25.238771+00	28	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
512	2022-03-03 12:11:33.520157+00	28	Оммадов Мухамметсердар	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
513	2022-03-03 12:11:47.147617+00	29	 	1	[{"added": {}}]	8	1
514	2022-03-03 12:11:53.314882+00	29	Аманов Довлет	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
515	2022-03-03 12:12:07.087858+00	30	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
516	2022-03-03 12:12:14.659945+00	30	Бахтияров Умитджан	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
517	2022-03-03 12:12:32.510211+00	31	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}, {"added": {"name": "user phone", "object": ""}}]	8	1
518	2022-03-03 12:12:39.930561+00	31	Бердиев Сердар	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
519	2022-03-03 12:13:01.0849+00	32	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
520	2022-03-03 12:13:08.207441+00	32	Халилов Сердар	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
521	2022-03-03 12:13:19.386935+00	33	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
522	2022-03-03 12:13:40.773912+00	33	Бегишов Нургелди	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
523	2022-03-03 12:18:26.925191+00	34	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
524	2022-03-03 12:18:35.949391+00	34	Аманов Байрам	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
525	2022-03-03 12:18:47.108438+00	35	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
526	2022-03-03 12:18:54.504467+00	35	Голяченко Богдан	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
527	2022-03-03 12:19:04.587771+00	36	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
528	2022-03-03 12:19:13.871027+00	36	Сладкевич Василий	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
529	2022-03-03 12:19:25.089378+00	37	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
530	2022-03-03 12:19:33.39088+00	37	Говкиев Бегенч	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
531	2022-03-03 12:19:50.95615+00	38	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
532	2022-03-03 12:19:59.566495+00	38	Волошин Александр	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
533	2022-03-03 12:20:11.598346+00	39	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
534	2022-03-03 12:20:20.150003+00	39	Ященко Ярослав	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
535	2022-03-03 12:20:35.928325+00	40	 	1	[{"added": {}}]	8	1
536	2022-03-03 12:20:39.276722+00	40	 МОЙСИК	2	[{"changed": {"fields": ["First name"]}}]	8	1
537	2022-03-03 12:20:59.230052+00	41	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}, {"added": {"name": "user phone", "object": ""}}]	8	1
538	2022-03-03 12:21:18.044198+00	41	Пенджиев Шохрат	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
539	2022-03-03 12:21:30.850494+00	42	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
540	2022-03-03 12:21:43.18562+00	42	Юрий Савченко	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
541	2022-03-03 13:19:44.532663+00	1	Seyidov Elvin	2	[{"changed": {"fields": ["Type"]}}]	8	1
542	2022-03-03 13:20:12.909923+00	1	Seyidov Elvin	2	[{"changed": {"fields": ["Type"]}}]	8	1
543	2022-03-03 13:24:18.334381+00	11	Романчук Леонид	2	[{"changed": {"fields": ["Type"]}}]	8	1
544	2022-03-03 13:24:22.996823+00	3	Дмитрий Каюн	2	[{"changed": {"fields": ["Type"]}}]	8	1
545	2022-03-03 13:24:26.512982+00	12	Бережной Сергей	2	[{"changed": {"fields": ["Type"]}}]	8	1
546	2022-03-03 13:24:33.63749+00	13	Исмайлов Ойвен	2	[{"changed": {"fields": ["Type"]}}]	8	1
547	2022-03-03 13:24:37.566796+00	14	выкуп Заур	2	[]	8	1
548	2022-03-03 13:24:41.250973+00	15	Лаптев Олег	2	[{"changed": {"fields": ["Type"]}}]	8	1
549	2022-03-03 13:24:44.523309+00	16	Кара Эрхан	2	[{"changed": {"fields": ["Type"]}}]	8	1
550	2022-03-03 13:24:47.39463+00	17	Кулиев Азат	2	[{"changed": {"fields": ["Type"]}}]	8	1
551	2022-03-03 13:24:50.836808+00	18	выкуп Али	2	[{"changed": {"fields": ["Type"]}}]	8	1
552	2022-03-03 13:24:54.096223+00	19	Чолиев Рустем	2	[]	8	1
553	2022-03-03 13:24:57.086792+00	20	Абдыресулов Рахман	2	[{"changed": {"fields": ["Type"]}}]	8	1
554	2022-03-03 13:25:00.065987+00	21	Абдиев Довлетгельди	2	[{"changed": {"fields": ["Type"]}}]	8	1
555	2022-03-03 13:25:03.073409+00	4	Язгулиев Шохрат	2	[{"changed": {"fields": ["Type"]}}]	8	1
556	2022-03-03 13:25:05.957928+00	22	Москалик Юрий	2	[{"changed": {"fields": ["Type"]}}]	8	1
557	2022-03-03 13:25:09.181982+00	23	Гончаров Сергей	2	[{"changed": {"fields": ["Type"]}}]	8	1
558	2022-03-03 13:25:12.075462+00	24	Фархатов Фарух	2	[{"changed": {"fields": ["Type"]}}]	8	1
559	2022-03-03 13:25:14.889336+00	25	Атаджанов Бегенч	2	[{"changed": {"fields": ["Type"]}}]	8	1
560	2022-03-03 13:25:17.781559+00	26	Валиев Эльвин	2	[{"changed": {"fields": ["Type"]}}]	8	1
561	2022-03-03 13:25:22.228807+00	27	Яхяев Шамиль	2	[{"changed": {"fields": ["Type"]}}]	8	1
562	2022-03-03 13:25:25.430817+00	28	Оммадов Мухамметсердар	2	[{"changed": {"fields": ["Type"]}}]	8	1
563	2022-03-03 13:25:28.444757+00	29	Аманов Довлет	2	[{"changed": {"fields": ["Type"]}}]	8	1
564	2022-03-03 13:25:31.711983+00	30	Бахтияров Умитджан	2	[{"changed": {"fields": ["Type"]}}]	8	1
565	2022-03-03 13:25:34.973417+00	31	Бердиев Сердар	2	[{"changed": {"fields": ["Type"]}}]	8	1
566	2022-03-03 13:25:39.238814+00	5	Чолиев Юнус	2	[{"changed": {"fields": ["Type"]}}]	8	1
567	2022-03-03 13:25:43.06186+00	32	Халилов Сердар	2	[{"changed": {"fields": ["Type"]}}]	8	1
568	2022-03-03 13:25:47.033502+00	33	Бегишов Нургелди	2	[{"changed": {"fields": ["Type"]}}]	8	1
569	2022-03-03 13:25:50.671052+00	34	Аманов Байрам	2	[{"changed": {"fields": ["Type"]}}]	8	1
570	2022-03-03 13:25:54.14732+00	35	Голяченко Богдан	2	[{"changed": {"fields": ["Type"]}}]	8	1
571	2022-03-03 13:25:57.235219+00	36	Сладкевич Василий	2	[{"changed": {"fields": ["Type"]}}]	8	1
572	2022-03-03 13:26:00.679813+00	37	Говкиев Бегенч	2	[{"changed": {"fields": ["Type"]}}]	8	1
573	2022-03-03 13:26:03.560895+00	38	Волошин Александр	2	[{"changed": {"fields": ["Type"]}}]	8	1
574	2022-03-03 13:26:06.288408+00	39	Ященко Ярослав	2	[{"changed": {"fields": ["Type"]}}]	8	1
575	2022-03-03 13:26:09.809887+00	40	 МОЙСИК	2	[{"changed": {"fields": ["Type"]}}]	8	1
576	2022-03-03 13:26:13.165274+00	41	Пенджиев Шохрат	2	[{"changed": {"fields": ["Type"]}}]	8	1
577	2022-03-03 13:26:16.933958+00	6	Акрамов Мукхриддин	2	[{"changed": {"fields": ["Type"]}}]	8	1
578	2022-03-03 13:26:19.901881+00	42	Юрий Савченко	2	[{"changed": {"fields": ["Type"]}}]	8	1
579	2022-03-03 13:26:23.309196+00	7	Авдиев Бабагелди	2	[{"changed": {"fields": ["Type"]}}]	8	1
580	2022-03-03 13:26:28.00859+00	8	Айрапетов Артём	2	[{"changed": {"fields": ["Type"]}}]	8	1
581	2022-03-03 13:26:30.920328+00	9	Батыров Гурбан	2	[{"changed": {"fields": ["Type"]}}]	8	1
582	2022-03-03 13:26:34.256973+00	10	(без договора) ВЫКУП	2	[{"changed": {"fields": ["Type"]}}]	8	1
583	2022-03-03 16:51:46.046353+00	6	Accent - 2011 - АІ 0315 MX  - W	3		29	1
584	2022-03-03 16:51:46.04872+00	5	Accent - 2011 - КA 2219 ВВ  - W	3		29	1
585	2022-03-03 16:51:46.04966+00	4	Accent - 2010 - АI 9756 IH  - W	3		29	1
586	2022-03-03 16:51:46.050512+00	3	Accent - 2017 - АІ 0312 MX  - W	3		29	1
587	2022-03-03 16:51:46.051382+00	2	Avante - 2016 - АІ 8243 OВ  - W	3		29	1
588	2022-03-03 16:55:45.571464+00	12	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Credit	3		9	1
589	2022-03-03 16:55:45.57379+00	11	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	3		9	1
590	2022-03-03 16:55:45.57471+00	10	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Taxi	3		9	1
591	2022-03-03 16:55:45.575534+00	9	Seyidov Elvin - Avante - 2017 - АІ 8064 MO  - Taxi	3		9	1
592	2022-03-03 16:55:45.576355+00	8	Seyidov Elvin - Accent - 2016 - AE 7193 KO  - Taxi	3		9	1
593	2022-03-03 16:55:45.577155+00	7	Seyidov Elvin - Accent - 2011 - КA 2219 ВВ  - Taxi	3		9	1
594	2022-03-03 16:55:45.577992+00	6	Seyidov Elvin - Accent - 2008 - КА 0729 АР  - Taxi	3		9	1
595	2022-03-03 16:55:45.578776+00	5	Seyidov Elvin - Accent - 2010 - АI 9756 IH  - Taxi	3		9	1
596	2022-03-03 16:55:45.579565+00	4	Seyidov Elvin - Accent - 2008 - АI 8250 OB  - Taxi	3		9	1
597	2022-03-03 16:55:45.580312+00	3	Seyidov Elvin - Accent - 2019 - АI 4463 MC  - Taxi	3		9	1
598	2022-03-03 16:55:45.581067+00	2	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	3		9	1
599	2022-03-03 16:55:45.581807+00	1	Mammadxanli Farid - Accent - 2011 - АІ 0315 MX  - Taxi	3		9	1
600	2022-03-03 16:57:58.283+00	13	Дмитрий Каюн - Sportage - 2011 - KА 6164 EC  - Credit	1	[{"added": {}}]	9	1
601	2022-03-03 16:59:03.186093+00	14	Язгулиев Шохрат - Polo - 2015 - KA 0728 AP  - Credit	1	[{"added": {}}]	9	1
602	2022-03-03 17:00:06.907056+00	15	Чолиев Юнус - Fabia - 2015 - АА 8986 СС  - Credit	1	[{"added": {}}]	9	1
603	2022-03-03 17:01:24.507984+00	16	Акрамов Мукхриддин - Rio - 2012 - АА 6038 РР  - Credit	1	[{"added": {}}]	9	1
604	2022-03-03 17:03:12.219882+00	17	Авдиев Бабагелди - Sonata - 2016 - КА 9531 ВК  - Credit	1	[{"added": {}}]	9	1
605	2022-03-03 17:04:06.763902+00	18	Айрапетов Артём - Rio - 2011 - СА 3701 СК  - Credit	1	[{"added": {}}]	9	1
606	2022-03-03 17:07:48.526173+00	19	Батыров Гурбан - Accent - 2010 - АI 9756 IH  - Credit	1	[{"added": {}}]	9	1
607	2022-03-03 17:08:35.357636+00	20	(без договора) ВЫКУП - Avante - 2012 - AІ 9149 МН  - Credit	1	[{"added": {}}]	9	1
608	2022-03-03 17:09:22.434501+00	21	Романчук Леонид - 735i - 2002 - AI 3839 ОС  - Credit	1	[{"added": {}}]	9	1
609	2022-03-03 17:10:17.850421+00	22	Бережной Сергей - Elantra - 2017 - АІ 5508 МР  - Credit	1	[{"added": {}}]	9	1
610	2022-03-03 17:11:16.853343+00	23	Исмайлов Ойвен - Elantra - 2017 - АІ 6703 МР  - Credit	1	[{"added": {}}]	9	1
611	2022-03-03 17:12:02.252995+00	24	выкуп Заур - Carens - 2012 - AI 0712 OВ  - Credit	1	[{"added": {}}]	9	1
612	2022-03-03 17:16:05.033893+00	25	Лаптев Олег - Fabia - 2013 - АІ 2803 СС  - Credit	1	[{"added": {}}]	9	1
613	2022-03-03 17:19:55.50387+00	26	Кара Эрхан - Sonata - 2012 - АI 6805 ОА  - Credit	1	[{"added": {}}]	9	1
614	2022-03-03 17:20:34.979028+00	27	Кулиев Азат - Mazda - 2013 - АІ 8006 МА  - Credit	1	[{"added": {}}]	9	1
615	2022-03-03 17:21:43.416083+00	28	выкуп Али - I30 - 2010 - АА 4934 ВР  - Credit	1	[{"added": {}}]	9	1
616	2022-03-03 17:23:53.507028+00	29	Чолиев Рустем - Accent - 2019 - АI 4463 MC  - Credit	1	[{"added": {}}]	9	1
617	2022-03-03 17:24:44.618569+00	30	Абдыресулов Рахман - Elantra - 2017 - AІ 7365 МI  - Credit	1	[{"added": {}}]	9	1
618	2022-03-03 17:25:42.261865+00	31	Абдиев Довлетгельди - Fabia - 2008 - АІ 7166 ІМ  - Credit	1	[{"added": {}}]	9	1
619	2022-03-03 17:26:20.435944+00	32	Москалик Юрий - I30 - 2011 - АА 3967 ТТ  - Credit	1	[{"added": {}}]	9	1
620	2022-03-03 17:27:04.333382+00	33	Гончаров Сергей - I30 - 2012 - АА 6079 СС  - Credit	1	[{"added": {}}]	9	1
621	2022-03-03 17:28:12.032671+00	34	Фархатов Фарух - Sonata - 2015 - АI 0954 МB  - Credit	1	[{"added": {}}]	9	1
622	2022-03-03 17:28:45.448772+00	35	Атаджанов Бегенч - Jetta - 2008 - АІ 2258 MМ  - Credit	1	[{"added": {}}]	9	1
623	2022-03-03 17:29:25.611694+00	36	Валиев Эльвин - Insignia - 2006 - AI 1495 MO  - Credit	1	[{"added": {}}]	9	1
624	2022-03-03 17:30:16.111272+00	37	Валиев Эльвин - K5 - 2012 - AI 5266 OO  - Credit	1	[{"added": {}}]	9	1
625	2022-03-03 17:31:44.379444+00	38	Яхяев Шамиль - Fabia - 2008 - АА 8037 РР  - Credit	1	[{"added": {}}]	9	1
626	2022-03-03 17:32:16.593774+00	39	Оммадов Мухамметсердар - Rio - 2012 - КА 3306 ВМ  - Credit	1	[{"added": {}}]	9	1
627	2022-03-03 17:33:24.382527+00	40	Аманов Довлет - Accent - 2008 - КА 0729 АР  - Credit	1	[{"added": {}}]	9	1
628	2022-03-03 17:34:07.09137+00	41	Бахтияров Умитджан - Corolla - 2018 - АІ 7462 ОВ  - Credit	1	[{"added": {}}]	9	1
629	2022-03-03 17:34:49.839805+00	42	Бердиев Сердар - I30 - 2008 - АІ 6723 ІН  - Credit	1	[{"added": {}}]	9	1
630	2022-03-03 17:35:30.731044+00	43	Халилов Сердар - Forte - 2011 - AI 9766 OA  - Credit	1	[{"added": {}}]	9	1
631	2022-03-03 17:36:16.192621+00	44	Бегишов Нургелди - Passat - 2004 - AА 1556 ТT  - Credit	1	[{"added": {}}]	9	1
632	2022-03-03 17:37:09.622473+00	45	Аманов Байрам - Polo - 2013 - АЕ 9200 IM  - Credit	1	[{"added": {}}]	9	1
633	2022-03-03 17:38:05.227537+00	46	Голяченко Богдан - Rio - 2012 - АІ 1893 ІІ  - Credit	1	[{"added": {}}]	9	1
634	2022-03-03 17:39:14.165816+00	47	Сладкевич Василий - Sonata - 2015 - АА 1058 ЕМ  - Credit	1	[{"added": {}}]	9	1
635	2022-03-03 17:40:14.440708+00	48	Говкиев Бегенч - Sonata - 2015 - АМ 3560 ЕО  - Credit	1	[{"added": {}}]	9	1
636	2022-03-03 17:41:53.58605+00	49	Волошин Александр - Sonata - 2015 - АI 3427 MP  - Credit	1	[{"added": {}}]	9	1
637	2022-03-03 17:43:00.114618+00	50	Ященко Ярослав - Sonata - 2012 - АI 6786 ОА  - Credit	1	[{"added": {}}]	9	1
638	2022-03-03 17:44:02.021086+00	51	 МОЙСИК - K5 - 2015 - АІ 8089 MI  - Credit	1	[{"added": {}}]	9	1
639	2022-03-03 17:44:43.160692+00	52	Пенджиев Шохрат - K5 - 2015 - АІ 0589 MІ  - Credit	1	[{"added": {}}]	9	1
640	2022-03-03 17:45:48.869399+00	53	Юрий Савченко - Sorento - 2016 - AI 2111 MB  - Credit	1	[{"added": {}}]	9	1
641	2022-03-03 17:46:41.423364+00	54	Кулиев Азат - Corolla - 2011 - АІ 6453 ОО  - Credit	1	[{"added": {}}]	9	1
642	2022-03-03 18:54:36.327558+00	7	Sportage - 2011 - KА 6164 EC  - M	1	[{"added": {}}]	29	1
643	2022-03-03 18:56:35.895939+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[{"added": {"name": "credit payment", "object": "2022-03-03"}}]	29	1
644	2022-03-03 19:13:26.336389+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[{"changed": {"fields": ["Total"]}}, {"deleted": {"name": "credit payment", "object": "2022-03-03"}}]	29	1
645	2022-03-03 19:17:44.484177+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[{"changed": {"fields": ["Payment count"]}}]	29	1
646	2022-03-03 20:00:02.445949+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[]	29	1
647	2022-03-03 20:34:22.146251+00	8	Polo - 2015 - KA 0728 AP  - W	1	[{"added": {}}]	29	1
648	2022-03-03 20:41:14.687311+00	8	Polo - 2015 - KA 0728 AP  - W	2	[]	29	1
649	2022-03-03 20:42:10.365673+00	8	Polo - 2015 - KA 0728 AP  - W	2	[]	29	1
650	2022-03-03 20:46:03.30131+00	9	Fabia - 2015 - АА 8986 СС  - W	1	[{"added": {}}]	29	1
651	2022-03-03 20:46:21.506109+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
652	2022-03-03 20:47:08.919434+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[]	29	1
653	2022-03-03 20:48:18.946622+00	10	Rio - 2012 - АА 6038 РР  - W	1	[{"added": {}}]	29	1
654	2022-03-03 20:48:30.818228+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
655	2022-03-03 20:48:51.337049+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
656	2022-03-03 20:49:03.133682+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
657	2022-03-03 20:50:56.693195+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (7)"}}]	29	1
658	2022-03-03 20:51:18.420586+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[{"added": {"name": "credit image", "object": "CreditImage object (8)"}}]	29	1
659	2022-03-03 20:51:29.444466+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (9)"}}]	29	1
660	2022-03-03 20:51:35.715945+00	8	Polo - 2015 - KA 0728 AP  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (10)"}}]	29	1
661	2022-03-03 20:54:13.820942+00	11	Sonata - 2016 - КА 9531 ВК  - W	1	[{"added": {}}]	29	1
662	2022-03-03 20:54:29.935405+00	11	Sonata - 2016 - КА 9531 ВК  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (11)"}}]	29	1
663	2022-03-03 21:47:27.848582+00	11	Sonata - 2016 - КА 9531 ВК  - W	2	[]	29	1
664	2022-03-03 21:48:39.603037+00	12	Rio - 2011 - СА 3701 СК  - W	1	[{"added": {}}]	29	1
665	2022-03-03 21:48:58.300311+00	12	Rio - 2011 - СА 3701 СК  - W	2	[]	29	1
666	2022-03-03 21:52:26.341117+00	13	Accent - 2010 - АI 9756 IH  - W	1	[{"added": {}}]	29	1
667	2022-03-03 21:56:05.806246+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"changed": {"fields": ["Payment year"]}}]	29	1
668	2022-03-03 22:03:01.018292+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
669	2022-03-03 22:37:22.360536+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[]	29	1
670	2022-03-03 22:37:28.112949+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[]	29	1
671	2022-03-03 22:38:53.500793+00	8	Polo - 2015 - KA 0728 AP  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
672	2022-03-03 22:39:01.557186+00	8	Polo - 2015 - KA 0728 AP  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
673	2022-03-03 22:39:10.25001+00	8	Polo - 2015 - KA 0728 AP  - W	2	[]	29	1
674	2022-03-03 22:39:50.496199+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
675	2022-03-03 22:40:01.385884+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
676	2022-03-03 22:40:09.71282+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[]	29	1
677	2022-03-03 22:40:47.148975+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
678	2022-03-03 22:41:19.661497+00	11	Sonata - 2016 - КА 9531 ВК  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
679	2022-03-03 22:42:04.144025+00	12	Rio - 2011 - СА 3701 СК  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
680	2022-03-03 22:42:07.670723+00	12	Rio - 2011 - СА 3701 СК  - W	2	[]	29	1
681	2022-03-03 22:43:07.301254+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
682	2022-03-03 22:43:54.30903+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
683	2022-03-05 17:28:39.607512+00	14	Avante - 2012 - AІ 9149 МН  - W	1	[{"added": {}}]	29	1
684	2022-03-05 17:36:44.842926+00	14	Avante - 2012 - AІ 9149 МН  - W	2	[]	29	1
685	2022-03-05 17:37:44.330418+00	15	735i - 2002 - AI 3839 ОС  - W	1	[{"added": {}}]	29	1
686	2022-03-05 17:37:53.870923+00	15	735i - 2002 - AI 3839 ОС  - W	2	[]	29	1
687	2022-03-05 17:38:57.592586+00	16	Elantra - 2017 - АІ 5508 МР  - W	1	[{"added": {}}]	29	1
688	2022-03-05 17:43:05.51233+00	16	Elantra - 2017 - АІ 5508 МР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
689	2022-03-05 17:43:15.713046+00	16	Elantra - 2017 - АІ 5508 МР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
690	2022-03-05 17:44:10.228238+00	16	Elantra - 2017 - АІ 5508 МР  - W	2	[]	29	1
691	2022-03-05 17:45:38.56776+00	17	Elantra - 2017 - АІ 6703 МР  - W	1	[{"added": {}}]	29	1
692	2022-03-05 17:45:47.568407+00	17	Elantra - 2017 - АІ 6703 МР  - W	2	[]	29	1
693	2022-03-05 17:46:55.51965+00	18	Carens - 2012 - AI 0712 OВ  - W	1	[{"added": {}}]	29	1
694	2022-03-05 17:47:02.994106+00	18	Carens - 2012 - AI 0712 OВ  - W	2	[]	29	1
695	2022-03-05 18:52:48.307465+00	19	Fabia - 2013 - АІ 2803 СС  - W	1	[{"added": {}}]	29	1
696	2022-03-05 18:53:02.648658+00	19	Fabia - 2013 - АІ 2803 СС  - W	2	[]	29	1
697	2022-03-05 18:54:12.882532+00	20	Sonata - 2012 - АI 6805 ОА  - W	1	[{"added": {}}]	29	1
698	2022-03-05 18:54:29.503049+00	20	Sonata - 2012 - АI 6805 ОА  - W	2	[]	29	1
699	2022-03-05 18:56:26.189352+00	21	Mazda - 2013 - АІ 8006 МА  - W	1	[{"added": {}}]	29	1
700	2022-03-05 18:56:32.7022+00	21	Mazda - 2013 - АІ 8006 МА  - W	2	[]	29	1
701	2022-03-05 18:58:20.676933+00	22	I30 - 2010 - АА 4934 ВР  - W	1	[{"added": {}}]	29	1
702	2022-03-05 18:58:24.835139+00	22	I30 - 2010 - АА 4934 ВР  - W	2	[]	29	1
703	2022-03-05 18:59:45.425229+00	23	Accent - 2019 - АI 4463 MC  - W	1	[{"added": {}}]	29	1
704	2022-03-05 19:00:01.007641+00	23	Accent - 2019 - АI 4463 MC  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
705	2022-03-05 19:00:04.811918+00	23	Accent - 2019 - АI 4463 MC  - W	2	[]	29	1
706	2022-03-05 19:01:38.824596+00	24	Elantra - 2017 - AІ 7365 МI  - W	1	[{"added": {}}]	29	1
707	2022-03-05 19:01:50.379698+00	24	Elantra - 2017 - AІ 7365 МI  - W	2	[]	29	1
708	2022-03-05 19:02:58.442605+00	25	Fabia - 2008 - АІ 7166 ІМ  - W	1	[{"added": {}}]	29	1
709	2022-03-05 19:03:02.662422+00	25	Fabia - 2008 - АІ 7166 ІМ  - W	2	[]	29	1
710	2022-03-05 19:04:21.811256+00	26	I30 - 2011 - АА 3967 ТТ  - W	1	[{"added": {}}]	29	1
711	2022-03-05 19:04:27.972062+00	26	I30 - 2011 - АА 3967 ТТ  - W	2	[]	29	1
712	2022-03-05 19:05:19.778854+00	27	I30 - 2012 - АА 6079 СС  - W	1	[{"added": {}}]	29	1
713	2022-03-05 19:05:23.897659+00	27	I30 - 2012 - АА 6079 СС  - W	2	[]	29	1
714	2022-03-05 19:06:39.467063+00	28	Sonata - 2015 - АI 0954 МB  - W	1	[{"added": {}}]	29	1
715	2022-03-05 19:06:47.898589+00	28	Sonata - 2015 - АI 0954 МB  - W	2	[]	29	1
716	2022-03-05 19:08:07.915225+00	29	Jetta - 2008 - АІ 2258 MМ  - W	1	[{"added": {}}]	29	1
717	2022-03-05 19:08:13.458036+00	29	Jetta - 2008 - АІ 2258 MМ  - W	2	[]	29	1
718	2022-03-05 19:23:33.386493+00	30	Insignia - 2006 - AI 1495 MO  - W	1	[{"added": {}}]	29	1
719	2022-03-05 19:23:38.480816+00	30	Insignia - 2006 - AI 1495 MO  - W	2	[]	29	1
720	2022-03-05 19:24:26.753363+00	31	K5 - 2012 - AI 5266 OO  - W	1	[{"added": {}}]	29	1
721	2022-03-05 19:24:34.176554+00	31	K5 - 2012 - AI 5266 OO  - W	2	[]	29	1
722	2022-03-05 19:26:10.344706+00	32	Fabia - 2008 - АА 8037 РР  - W	1	[{"added": {}}]	29	1
723	2022-03-05 19:26:14.961984+00	32	Fabia - 2008 - АА 8037 РР  - W	2	[]	29	1
724	2022-03-05 19:27:41.429342+00	33	Rio - 2012 - КА 3306 ВМ  - W	1	[{"added": {}}]	29	1
725	2022-03-05 19:27:48.800539+00	33	Rio - 2012 - КА 3306 ВМ  - W	2	[]	29	1
726	2022-03-05 19:41:51.988105+00	34	Accent - 2008 - КА 0729 АР  - W	1	[{"added": {}}]	29	1
727	2022-03-05 19:41:59.859833+00	34	Accent - 2008 - КА 0729 АР  - W	2	[]	29	1
728	2022-03-05 19:43:04.850505+00	35	Corolla - 2018 - АІ 7462 ОВ  - W	1	[{"added": {}}]	29	1
729	2022-03-05 19:43:15.459934+00	35	Corolla - 2018 - АІ 7462 ОВ  - W	2	[]	29	1
730	2022-03-05 19:44:32.262125+00	36	I30 - 2008 - АІ 6723 ІН  - W	1	[{"added": {}}]	29	1
731	2022-03-05 19:44:44.049666+00	36	I30 - 2008 - АІ 6723 ІН  - W	2	[]	29	1
732	2022-03-05 19:45:46.205286+00	37	Forte - 2011 - AI 9766 OA  - W	1	[{"added": {}}]	29	1
733	2022-03-05 19:45:57.601501+00	37	Forte - 2011 - AI 9766 OA  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
734	2022-03-05 19:46:05.279299+00	37	Forte - 2011 - AI 9766 OA  - W	2	[]	29	1
735	2022-03-05 19:47:07.629066+00	38	Passat - 2004 - AА 1556 ТT  - W	1	[{"added": {}}]	29	1
736	2022-03-05 19:47:14.219733+00	38	Passat - 2004 - AА 1556 ТT  - W	2	[]	29	1
737	2022-03-05 19:48:22.170759+00	39	Polo - 2013 - АЕ 9200 IM  - W	1	[{"added": {}}]	29	1
738	2022-03-05 19:48:29.814009+00	39	Polo - 2013 - АЕ 9200 IM  - W	2	[]	29	1
739	2022-03-05 19:49:38.221405+00	40	Rio - 2012 - АІ 1893 ІІ  - W	1	[{"added": {}}]	29	1
740	2022-03-05 19:49:42.355917+00	40	Rio - 2012 - АІ 1893 ІІ  - W	2	[]	29	1
741	2022-03-05 19:51:01.049293+00	41	Sonata - 2015 - АА 1058 ЕМ  - W	1	[{"added": {}}]	29	1
742	2022-03-05 19:51:06.115833+00	41	Sonata - 2015 - АА 1058 ЕМ  - W	2	[]	29	1
743	2022-03-05 19:51:48.994371+00	42	Sonata - 2015 - АМ 3560 ЕО  - W	1	[{"added": {}}]	29	1
744	2022-03-05 19:51:56.832357+00	42	Sonata - 2015 - АМ 3560 ЕО  - W	2	[]	29	1
745	2022-03-05 19:52:45.276287+00	43	Sonata - 2015 - АI 3427 MP  - W	1	[{"added": {}}]	29	1
746	2022-03-05 19:52:49.170708+00	43	Sonata - 2015 - АI 3427 MP  - W	2	[]	29	1
747	2022-03-05 19:57:51.395442+00	44	Sonata - 2012 - АI 6786 ОА  - W	1	[{"added": {}}]	29	1
748	2022-03-05 19:57:56.867214+00	44	Sonata - 2012 - АI 6786 ОА  - W	2	[]	29	1
749	2022-03-05 19:59:02.941164+00	45	K5 - 2015 - АІ 8089 MI  - W	1	[{"added": {}}]	29	1
750	2022-03-05 19:59:08.27087+00	45	K5 - 2015 - АІ 8089 MI  - W	2	[]	29	1
751	2022-03-05 20:00:53.205009+00	46	K5 - 2015 - АІ 0589 MІ  - W	1	[{"added": {}}]	29	1
752	2022-03-05 20:01:03.368443+00	46	K5 - 2015 - АІ 0589 MІ  - W	2	[]	29	1
753	2022-03-05 20:05:23.511566+00	47	Sorento - 2016 - AI 2111 MB  - W	1	[{"added": {}}]	29	1
754	2022-03-05 20:05:32.838204+00	47	Sorento - 2016 - AI 2111 MB  - W	2	[]	29	1
755	2022-03-05 20:06:01.207649+00	47	Sorento - 2016 - AI 2111 MB  - W	2	[{"changed": {"fields": ["Payment date"]}}]	29	1
756	2022-03-05 20:07:14.658935+00	48	Corolla - 2011 - АІ 6453 ОО  - W	1	[{"added": {}}]	29	1
757	2022-03-05 20:07:31.747399+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[]	29	1
758	2022-03-05 20:15:39.661488+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (12)"}}]	29	1
759	2022-03-05 20:15:45.155179+00	47	Sorento - 2016 - AI 2111 MB  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (13)"}}]	29	1
760	2022-03-05 20:15:51.16831+00	46	K5 - 2015 - АІ 0589 MІ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (14)"}}]	29	1
761	2022-03-05 20:15:56.501604+00	45	K5 - 2015 - АІ 8089 MI  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (15)"}}]	29	1
762	2022-03-05 20:16:01.98531+00	44	Sonata - 2012 - АI 6786 ОА  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (16)"}}]	29	1
763	2022-03-05 20:16:07.351229+00	43	Sonata - 2015 - АI 3427 MP  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (17)"}}]	29	1
764	2022-03-05 20:16:12.341527+00	42	Sonata - 2015 - АМ 3560 ЕО  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (18)"}}]	29	1
765	2022-03-05 20:16:17.854575+00	41	Sonata - 2015 - АА 1058 ЕМ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (19)"}}]	29	1
766	2022-03-05 20:16:23.274201+00	40	Rio - 2012 - АІ 1893 ІІ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (20)"}}]	29	1
767	2022-03-05 20:16:28.593736+00	39	Polo - 2013 - АЕ 9200 IM  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (21)"}}]	29	1
768	2022-03-05 20:16:34.118886+00	38	Passat - 2004 - AА 1556 ТT  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (22)"}}]	29	1
769	2022-03-05 20:16:40.070803+00	37	Forte - 2011 - AI 9766 OA  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (23)"}}]	29	1
770	2022-03-05 20:16:45.861245+00	36	I30 - 2008 - АІ 6723 ІН  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (24)"}}]	29	1
771	2022-03-05 20:16:51.331724+00	35	Corolla - 2018 - АІ 7462 ОВ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (25)"}}]	29	1
772	2022-03-05 20:16:56.685814+00	34	Accent - 2008 - КА 0729 АР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (26)"}}]	29	1
773	2022-03-05 20:17:02.108713+00	33	Rio - 2012 - КА 3306 ВМ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (27)"}}]	29	1
774	2022-03-05 20:17:07.253445+00	32	Fabia - 2008 - АА 8037 РР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (28)"}}]	29	1
775	2022-03-05 20:17:12.380713+00	31	K5 - 2012 - AI 5266 OO  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (29)"}}]	29	1
776	2022-03-05 20:17:17.580226+00	30	Insignia - 2006 - AI 1495 MO  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (30)"}}]	29	1
777	2022-03-05 20:17:22.771222+00	29	Jetta - 2008 - АІ 2258 MМ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (31)"}}]	29	1
778	2022-03-05 20:17:28.314141+00	28	Sonata - 2015 - АI 0954 МB  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (32)"}}]	29	1
779	2022-03-05 20:17:32.820392+00	27	I30 - 2012 - АА 6079 СС  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (33)"}}]	29	1
780	2022-03-05 20:17:37.531125+00	26	I30 - 2011 - АА 3967 ТТ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (34)"}}]	29	1
781	2022-03-05 20:17:43.130225+00	25	Fabia - 2008 - АІ 7166 ІМ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (35)"}}]	29	1
782	2022-03-05 20:17:48.386591+00	24	Elantra - 2017 - AІ 7365 МI  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (36)"}}]	29	1
783	2022-03-05 20:17:53.568453+00	23	Accent - 2019 - АI 4463 MC  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (37)"}}]	29	1
784	2022-03-05 20:17:58.448439+00	22	I30 - 2010 - АА 4934 ВР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (38)"}}]	29	1
785	2022-03-05 20:18:04.191664+00	21	Mazda - 2013 - АІ 8006 МА  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (39)"}}]	29	1
786	2022-03-05 20:18:09.123957+00	20	Sonata - 2012 - АI 6805 ОА  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (40)"}}]	29	1
787	2022-03-05 20:18:13.790121+00	19	Fabia - 2013 - АІ 2803 СС  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (41)"}}]	29	1
788	2022-03-05 20:18:19.130371+00	18	Carens - 2012 - AI 0712 OВ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (42)"}}]	29	1
789	2022-03-05 20:18:24.511609+00	17	Elantra - 2017 - АІ 6703 МР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (43)"}}]	29	1
790	2022-03-05 20:18:29.808944+00	16	Elantra - 2017 - АІ 5508 МР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (44)"}}]	29	1
791	2022-03-05 20:18:35.548329+00	15	735i - 2002 - AI 3839 ОС  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (45)"}}]	29	1
792	2022-03-05 20:18:40.684377+00	14	Avante - 2012 - AІ 9149 МН  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (46)"}}]	29	1
793	2022-03-05 20:18:47.035054+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (47)"}}]	29	1
794	2022-03-05 20:18:53.038349+00	12	Rio - 2011 - СА 3701 СК  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (48)"}}]	29	1
795	2022-03-05 20:33:04.606853+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	1	[{"added": {}}]	9	1
796	2022-03-05 20:33:17.064938+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Credit	2	[{"changed": {"fields": ["Type"]}}]	9	1
797	2022-03-05 20:33:30.292626+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	2	[{"changed": {"fields": ["Type", "End date"]}}]	9	1
798	2022-03-05 20:34:35.485832+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	2	[]	9	1
799	2022-03-05 20:34:48.620978+00	56	Mammadxanli Farid - Accent - 2017 - АІ 0312 MX  - Taxi	1	[{"added": {}}]	9	1
800	2022-03-05 20:35:04.576625+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	2	[{"changed": {"fields": ["End date"]}}]	9	1
801	2022-03-05 20:35:17.600988+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	3		9	1
802	2022-03-05 20:35:21.911337+00	56	Mammadxanli Farid - Accent - 2017 - АІ 0312 MX  - Taxi	3		9	1
803	2022-03-06 12:37:17.878355+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Taxi	1	[{"added": {}}]	9	1
804	2022-03-06 12:37:29.037225+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"added": {"name": "revenue", "object": "Revenue object (41)"}}]	34	1
805	2022-03-06 12:38:20.24871+00	41	Revenue object (41)	2	[{"added": {"name": "revenue item", "object": "RevenueItem object (99)"}}]	33	1
806	2022-03-06 12:38:46.248056+00	41	Revenue object (41)	2	[{"changed": {"name": "revenue item", "object": "RevenueItem object (99)", "fields": ["Amount cash", "Amount card"]}}]	33	1
807	2022-03-06 12:44:13.70529+00	41	Revenue object (41)	2	[{"added": {"name": "revenue item", "object": "RevenueItem object (100)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (101)"}}]	33	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	authtoken	token
7	authtoken	tokenproxy
8	account	customuser
9	account	contract
10	account	userphone
11	account	userimage
12	account	passportimage
13	account	licenseimage
14	car	brand
15	car	car
16	car	carclass
17	car	carimage
18	car	case
19	car	color
20	car	fuel
21	car	office
22	car	model
23	car	carpaper
24	car	carimagetask
25	core	administrativedeposit
26	core	administrativewithdraw
27	core	currency
28	core	balance
29	credit	credit
30	credit	creditpayment
31	credit	creditimage
32	taxi	company
33	taxi	revenue
34	taxi	taxi
35	taxi	revenueitem
36	rent_taxi	renttaxi
37	rent_taxi	revenue
38	rent_taxi	revenueitem
39	shtraf	shtraf
40	shtraf	shtrafimage
41	debt	debt
42	debt	debtimage
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	account	0001_initial	2022-02-23 13:23:55.37886+00
2	car	0001_initial	2022-02-23 13:23:55.438789+00
3	taxi	0001_initial	2022-02-23 13:23:55.500064+00
4	car	0002_initial	2022-02-23 13:23:55.637199+00
5	contenttypes	0001_initial	2022-02-23 13:23:55.644567+00
6	contenttypes	0002_remove_content_type_name	2022-02-23 13:23:55.664023+00
7	auth	0001_initial	2022-02-23 13:23:55.690406+00
8	auth	0002_alter_permission_name_max_length	2022-02-23 13:23:55.696179+00
9	auth	0003_alter_user_email_max_length	2022-02-23 13:23:55.70207+00
10	auth	0004_alter_user_username_opts	2022-02-23 13:23:55.707886+00
11	auth	0005_alter_user_last_login_null	2022-02-23 13:23:55.713343+00
12	auth	0006_require_contenttypes_0002	2022-02-23 13:23:55.71507+00
13	auth	0007_alter_validators_add_error_messages	2022-02-23 13:23:55.721655+00
14	auth	0008_alter_user_username_max_length	2022-02-23 13:23:55.726651+00
15	auth	0009_alter_user_last_name_max_length	2022-02-23 13:23:55.731625+00
16	auth	0010_alter_group_name_max_length	2022-02-23 13:23:55.737515+00
17	auth	0011_update_proxy_permissions	2022-02-23 13:23:55.753864+00
18	auth	0012_alter_user_first_name_max_length	2022-02-23 13:23:55.766857+00
19	account	0002_initial	2022-02-23 13:23:55.850255+00
20	admin	0001_initial	2022-02-23 13:23:55.879293+00
21	admin	0002_logentry_remove_auto_add	2022-02-23 13:23:55.895084+00
22	admin	0003_logentry_add_action_flag_choices	2022-02-23 13:23:55.910891+00
23	authtoken	0001_initial	2022-02-23 13:23:55.938869+00
24	authtoken	0002_auto_20160226_1747	2022-02-23 13:23:55.995449+00
25	authtoken	0003_tokenproxy	2022-02-23 13:23:55.998625+00
26	core	0001_initial	2022-02-23 13:23:56.01464+00
27	credit	0001_initial	2022-02-23 13:23:56.095891+00
28	debt	0001_initial	2022-02-23 13:23:56.158637+00
29	debt	0002_alter_debtimage_debt	2022-02-23 13:23:56.18483+00
30	taxi	0002_alter_revenue_taxi_alter_revenueitem_revenue	2022-02-23 13:23:56.293472+00
31	rent_taxi	0001_initial	2022-02-23 13:23:56.357087+00
32	rent_taxi	0002_rename_end_date_renttaxi_date_remove_renttaxi_driver_and_more	2022-02-23 13:23:56.46387+00
33	rent_taxi	0003_revenue	2022-02-23 13:23:56.498417+00
34	rent_taxi	0004_revenueitem	2022-02-23 13:23:56.52948+00
35	rent_taxi	0005_revenueitem_bolt_will_pay_revenueitem_debt_and_more	2022-02-23 13:23:56.558949+00
36	rent_taxi	0006_remove_revenueitem_debt_and_more	2022-02-23 13:23:56.650625+00
37	sessions	0001_initial	2022-02-23 13:23:56.658822+00
38	shtraf	0001_initial	2022-02-23 13:23:56.72552+00
39	shtraf	0002_shtraf_is_active	2022-02-23 13:23:56.750523+00
40	taxi	0003_revenueitem_debt_all_revenueitem_debt_today	2022-02-23 13:23:56.840355+00
41	taxi	0004_revenueitem_taxi	2022-02-23 13:23:56.868398+00
42	taxi	0005_alter_revenueitem_taxi	2022-02-23 13:23:56.893742+00
43	car	0003_remove_car_status_brand_logo_car_health_status_and_more	2022-03-02 21:04:11.223666+00
44	car	0004_car_status	2022-03-02 21:04:11.242483+00
45	car	0005_car_driver	2022-03-02 23:06:57.500461+00
46	account	0003_customuser_is_black_listed_alter_customuser_type	2022-03-03 08:45:45.600708+00
47	credit	0002_remove_credit_end_date_remove_credit_start_date	2022-03-03 18:43:05.867916+00
48	credit	0003_rename_total_credit_car_value_total_credit_expenses_and_more	2022-03-03 19:59:24.820108+00
49	credit	0004_alter_credit_payment_year	2022-03-03 21:55:00.945866+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
0znbpal655dl89vczbvm6vnqlitylvtq	.eJxVjEEOwiAQRe_C2hBhoIBL9z0DmWFAqoYmpV0Z765NutDtf-_9l4i4rTVuPS9xYnERSpx-N8L0yG0HfMd2m2Wa27pMJHdFHrTLceb8vB7u30HFXr81AnHySSNBVolDAS5nbQmV5-zVANoYa5ESIfgSOAzF2BzQOEXOgRPvDxPuOJI:1nMrdK:uXIYNhwrrrAE4oeFe-vL99UmTJCawjtgP_nwe8TDVRA	2022-03-09 13:24:42.89516+00
mcrd94ogk32siiw45fl80wf7zqgg87yn	.eJxVjEEOwiAQRe_C2hBhoIBL9z0DmWFAqoYmpV0Z765NutDtf-_9l4i4rTVuPS9xYnERSpx-N8L0yG0HfMd2m2Wa27pMJHdFHrTLceb8vB7u30HFXr81AnHySSNBVolDAS5nbQmV5-zVANoYa5ESIfgSOAzF2BzQOEXOgRPvDxPuOJI:1nNJna:TiPxhhNhiFBK0-ur3c4F4Ny4q_vtaXOgby1fB7nO9QQ	2022-03-10 19:29:10.499281+00
\.


--
-- Data for Name: rent_taxi_renttaxi; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_renttaxi (id, created_at, updated_at, date, car_id, is_active) FROM stdin;
\.


--
-- Data for Name: rent_taxi_revenue; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_revenue (id, created_at, updated_at, start_date, end_date, contract_id, driver_id, renttaxi_id) FROM stdin;
\.


--
-- Data for Name: rent_taxi_revenueitem; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_revenueitem (id, amount_cash, amount_card, order_count, paid, company_id, revenue_id, bolt_will_pay, rent_amount, bolt_will_pay_total, commission_from_driver, debt_all, debt_to_bolt, debt_today, driver_must_pay, rent_taxi_id) FROM stdin;
\.


--
-- Data for Name: shtraf_shtraf; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.shtraf_shtraf (id, created_at, updated_at, datetime, amount, paid, car_id, driver_id, is_active) FROM stdin;
\.


--
-- Data for Name: shtraf_shtrafimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.shtraf_shtrafimage (id, file, shtraf_id) FROM stdin;
\.


--
-- Data for Name: taxi_company; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.taxi_company (id, name, commission, tarif1_count, tarif1_pay, tarif2_count, tarif2_pay, tarif3_count, tarif3_pay) FROM stdin;
1	Bolt	25	90	12	120	13	150	14
2	Uber	25	90	12	120	13	150	14
3	Uklon	25	90	12	120	13	150	14
\.


--
-- Data for Name: taxi_revenue; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.taxi_revenue (id, created_at, updated_at, date, contract_id, driver_id, taxi_id) FROM stdin;
41	2022-03-06 12:37:29.034085+00	2022-03-06 12:44:13.69845+00	2022-03-06	57	1	10
\.


--
-- Data for Name: taxi_revenueitem; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.taxi_revenueitem (id, amount_cash, amount_card, order_count, paid, company_id, revenue_id, debt_all, debt_today, taxi_id) FROM stdin;
99	100	100	0	0	1	41	0	0	10
100	0	0	0	0	2	41	0	0	10
101	0	0	0	0	3	41	0	0	10
\.


--
-- Data for Name: taxi_taxi; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.taxi_taxi (id, created_at, updated_at, date, is_active, car_id) FROM stdin;
2	2022-02-23 13:35:46.329176+00	2022-02-23 13:35:46.329196+00	2022-02-23	t	2
3	2022-02-23 13:35:54.792596+00	2022-02-23 13:35:54.792617+00	2022-02-23	t	3
4	2022-02-23 13:35:59.740101+00	2022-02-23 13:35:59.740121+00	2022-02-23	t	4
5	2022-02-23 13:36:05.233848+00	2022-02-23 13:36:05.23387+00	2022-02-23	t	5
6	2022-02-23 13:36:10.565684+00	2022-02-23 13:36:10.56571+00	2022-02-23	t	6
7	2022-02-23 13:36:14.439632+00	2022-02-23 13:36:14.439653+00	2022-02-23	t	7
8	2022-02-23 13:36:19.314+00	2022-02-23 13:36:19.31403+00	2022-02-23	t	8
9	2022-02-23 13:36:22.91753+00	2022-02-23 13:36:22.917555+00	2022-02-23	t	9
1	2022-02-23 13:35:32.11413+00	2022-02-23 14:48:20.061217+00	2022-02-23	t	1
10	2022-02-23 13:36:29.116239+00	2022-03-06 12:37:29.032375+00	2022-02-23	t	10
\.


--
-- Name: account_contract_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_contract_id_seq', 57, true);


--
-- Name: account_customuser_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_customuser_groups_id_seq', 1, false);


--
-- Name: account_customuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_customuser_id_seq', 42, true);


--
-- Name: account_customuser_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_customuser_user_permissions_id_seq', 1, false);


--
-- Name: account_licenseimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_licenseimage_id_seq', 1, false);


--
-- Name: account_passportimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_passportimage_id_seq', 1, false);


--
-- Name: account_userimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_userimage_id_seq', 1, false);


--
-- Name: account_userphone_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_userphone_id_seq', 36, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 168, true);


--
-- Name: car_brand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_brand_id_seq', 9, true);


--
-- Name: car_car_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_car_id_seq', 140, true);


--
-- Name: car_carclass_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carclass_id_seq', 1, false);


--
-- Name: car_carimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carimage_id_seq', 133, true);


--
-- Name: car_carimagetask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carimagetask_id_seq', 1, false);


--
-- Name: car_carimagetask_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carimagetask_images_id_seq', 1, false);


--
-- Name: car_carpaper_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carpaper_id_seq', 1, true);


--
-- Name: car_case_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_case_id_seq', 1, false);


--
-- Name: car_color_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_color_id_seq', 1, false);


--
-- Name: car_fuel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_fuel_id_seq', 1, false);


--
-- Name: car_model_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_model_id_seq', 22, true);


--
-- Name: car_office_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_office_id_seq', 1, true);


--
-- Name: core_administrativedeposit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.core_administrativedeposit_id_seq', 1, false);


--
-- Name: core_administrativewithdraw_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.core_administrativewithdraw_id_seq', 1, false);


--
-- Name: core_balance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.core_balance_id_seq', 1, false);


--
-- Name: core_currency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.core_currency_id_seq', 1, true);


--
-- Name: credit_credit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.credit_credit_id_seq', 48, true);


--
-- Name: credit_creditimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.credit_creditimage_id_seq', 48, true);


--
-- Name: credit_creditpayment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.credit_creditpayment_id_seq', 23, true);


--
-- Name: debt_debt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.debt_debt_id_seq', 1, false);


--
-- Name: debt_debtimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.debt_debtimage_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 807, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 42, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 49, true);


--
-- Name: rent_taxi_renttaxi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_renttaxi_id_seq', 1, false);


--
-- Name: rent_taxi_revenue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_revenue_id_seq', 1, false);


--
-- Name: rent_taxi_revenueitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_revenueitem_id_seq', 1, false);


--
-- Name: shtraf_shtraf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.shtraf_shtraf_id_seq', 1, false);


--
-- Name: shtraf_shtrafimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.shtraf_shtrafimage_id_seq', 1, false);


--
-- Name: taxi_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.taxi_company_id_seq', 3, true);


--
-- Name: taxi_revenue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.taxi_revenue_id_seq', 41, true);


--
-- Name: taxi_revenueitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.taxi_revenueitem_id_seq', 101, true);


--
-- Name: taxi_taxi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.taxi_taxi_id_seq', 10, true);


--
-- Name: account_contract account_contract_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_contract
    ADD CONSTRAINT account_contract_pkey PRIMARY KEY (id);


--
-- Name: account_customuser_groups account_customuser_groups_customuser_id_group_id_7e51db7b_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups
    ADD CONSTRAINT account_customuser_groups_customuser_id_group_id_7e51db7b_uniq UNIQUE (customuser_id, group_id);


--
-- Name: account_customuser_groups account_customuser_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups
    ADD CONSTRAINT account_customuser_groups_pkey PRIMARY KEY (id);


--
-- Name: account_customuser account_customuser_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser
    ADD CONSTRAINT account_customuser_pkey PRIMARY KEY (id);


--
-- Name: account_customuser_user_permissions account_customuser_user__customuser_id_permission_650e378f_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions
    ADD CONSTRAINT account_customuser_user__customuser_id_permission_650e378f_uniq UNIQUE (customuser_id, permission_id);


--
-- Name: account_customuser_user_permissions account_customuser_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions
    ADD CONSTRAINT account_customuser_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: account_customuser account_customuser_username_key; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser
    ADD CONSTRAINT account_customuser_username_key UNIQUE (username);


--
-- Name: account_licenseimage account_licenseimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_licenseimage
    ADD CONSTRAINT account_licenseimage_pkey PRIMARY KEY (id);


--
-- Name: account_passportimage account_passportimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_passportimage
    ADD CONSTRAINT account_passportimage_pkey PRIMARY KEY (id);


--
-- Name: account_userimage account_userimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userimage
    ADD CONSTRAINT account_userimage_pkey PRIMARY KEY (id);


--
-- Name: account_userphone account_userphone_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userphone
    ADD CONSTRAINT account_userphone_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: car_brand car_brand_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_brand
    ADD CONSTRAINT car_brand_pkey PRIMARY KEY (id);


--
-- Name: car_car car_car_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_pkey PRIMARY KEY (id);


--
-- Name: car_carclass car_carclass_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carclass
    ADD CONSTRAINT car_carclass_pkey PRIMARY KEY (id);


--
-- Name: car_carimage car_carimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimage
    ADD CONSTRAINT car_carimage_pkey PRIMARY KEY (id);


--
-- Name: car_carimagetask_images car_carimagetask_images_carimagetask_id_carimage_79281200_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images
    ADD CONSTRAINT car_carimagetask_images_carimagetask_id_carimage_79281200_uniq UNIQUE (carimagetask_id, carimage_id);


--
-- Name: car_carimagetask_images car_carimagetask_images_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images
    ADD CONSTRAINT car_carimagetask_images_pkey PRIMARY KEY (id);


--
-- Name: car_carimagetask car_carimagetask_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask
    ADD CONSTRAINT car_carimagetask_pkey PRIMARY KEY (id);


--
-- Name: car_carpaper car_carpaper_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carpaper
    ADD CONSTRAINT car_carpaper_pkey PRIMARY KEY (id);


--
-- Name: car_case car_case_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_case
    ADD CONSTRAINT car_case_pkey PRIMARY KEY (id);


--
-- Name: car_color car_color_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_color
    ADD CONSTRAINT car_color_pkey PRIMARY KEY (id);


--
-- Name: car_fuel car_fuel_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_fuel
    ADD CONSTRAINT car_fuel_pkey PRIMARY KEY (id);


--
-- Name: car_model car_model_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_model
    ADD CONSTRAINT car_model_pkey PRIMARY KEY (id);


--
-- Name: car_office car_office_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_office
    ADD CONSTRAINT car_office_pkey PRIMARY KEY (id);


--
-- Name: core_administrativedeposit core_administrativedeposit_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_administrativedeposit
    ADD CONSTRAINT core_administrativedeposit_pkey PRIMARY KEY (id);


--
-- Name: core_administrativewithdraw core_administrativewithdraw_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_administrativewithdraw
    ADD CONSTRAINT core_administrativewithdraw_pkey PRIMARY KEY (id);


--
-- Name: core_balance core_balance_currency_type_5be3e167_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_balance
    ADD CONSTRAINT core_balance_currency_type_5be3e167_uniq UNIQUE (currency, type);


--
-- Name: core_balance core_balance_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_balance
    ADD CONSTRAINT core_balance_pkey PRIMARY KEY (id);


--
-- Name: core_currency core_currency_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_currency
    ADD CONSTRAINT core_currency_pkey PRIMARY KEY (id);


--
-- Name: credit_credit credit_credit_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_credit
    ADD CONSTRAINT credit_credit_pkey PRIMARY KEY (id);


--
-- Name: credit_creditimage credit_creditimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditimage
    ADD CONSTRAINT credit_creditimage_pkey PRIMARY KEY (id);


--
-- Name: credit_creditpayment credit_creditpayment_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditpayment
    ADD CONSTRAINT credit_creditpayment_pkey PRIMARY KEY (id);


--
-- Name: debt_debt debt_debt_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debt
    ADD CONSTRAINT debt_debt_pkey PRIMARY KEY (id);


--
-- Name: debt_debtimage debt_debtimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debtimage
    ADD CONSTRAINT debt_debtimage_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: rent_taxi_renttaxi rent_taxi_renttaxi_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_renttaxi
    ADD CONSTRAINT rent_taxi_renttaxi_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_revenue rent_taxi_revenue_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue
    ADD CONSTRAINT rent_taxi_revenue_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_revenueitem rent_taxi_revenueitem_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem
    ADD CONSTRAINT rent_taxi_revenueitem_pkey PRIMARY KEY (id);


--
-- Name: shtraf_shtraf shtraf_shtraf_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtraf
    ADD CONSTRAINT shtraf_shtraf_pkey PRIMARY KEY (id);


--
-- Name: shtraf_shtrafimage shtraf_shtrafimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtrafimage
    ADD CONSTRAINT shtraf_shtrafimage_pkey PRIMARY KEY (id);


--
-- Name: taxi_company taxi_company_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_company
    ADD CONSTRAINT taxi_company_pkey PRIMARY KEY (id);


--
-- Name: taxi_revenue taxi_revenue_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue
    ADD CONSTRAINT taxi_revenue_pkey PRIMARY KEY (id);


--
-- Name: taxi_revenueitem taxi_revenueitem_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem
    ADD CONSTRAINT taxi_revenueitem_pkey PRIMARY KEY (id);


--
-- Name: taxi_taxi taxi_taxi_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_taxi
    ADD CONSTRAINT taxi_taxi_pkey PRIMARY KEY (id);


--
-- Name: account_contract_car_id_5a2aec70; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_contract_car_id_5a2aec70 ON public.account_contract USING btree (car_id);


--
-- Name: account_contract_customer_id_034347f8; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_contract_customer_id_034347f8 ON public.account_contract USING btree (customer_id);


--
-- Name: account_customuser_groups_customuser_id_b6c60904; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_groups_customuser_id_b6c60904 ON public.account_customuser_groups USING btree (customuser_id);


--
-- Name: account_customuser_groups_group_id_2be9f6d7; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_groups_group_id_2be9f6d7 ON public.account_customuser_groups USING btree (group_id);


--
-- Name: account_customuser_user_permissions_customuser_id_03bcc114; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_user_permissions_customuser_id_03bcc114 ON public.account_customuser_user_permissions USING btree (customuser_id);


--
-- Name: account_customuser_user_permissions_permission_id_f4aec423; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_user_permissions_permission_id_f4aec423 ON public.account_customuser_user_permissions USING btree (permission_id);


--
-- Name: account_customuser_username_724ae020_like; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_username_724ae020_like ON public.account_customuser USING btree (username varchar_pattern_ops);


--
-- Name: account_licenseimage_user_id_2fa01b01; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_licenseimage_user_id_2fa01b01 ON public.account_licenseimage USING btree (user_id);


--
-- Name: account_passportimage_user_id_43c1cf55; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_passportimage_user_id_43c1cf55 ON public.account_passportimage USING btree (user_id);


--
-- Name: account_userimage_user_id_60023a82; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_userimage_user_id_60023a82 ON public.account_userimage USING btree (user_id);


--
-- Name: account_userphone_user_id_bf883813; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_userphone_user_id_bf883813 ON public.account_userphone USING btree (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: car_car_advertise_id_a27f1939; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_advertise_id_a27f1939 ON public.car_car USING btree (advertise_id);


--
-- Name: car_car_car_class_id_b7eb688e; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_car_class_id_b7eb688e ON public.car_car USING btree (car_class_id);


--
-- Name: car_car_case_id_b68dd611; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_case_id_b68dd611 ON public.car_car USING btree (case_id);


--
-- Name: car_car_color_id_f863d3f6; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_color_id_f863d3f6 ON public.car_car USING btree (color_id);


--
-- Name: car_car_driver_id_e2236767; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_driver_id_e2236767 ON public.car_car USING btree (driver_id);


--
-- Name: car_car_fuel_id_84981bd3; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_fuel_id_84981bd3 ON public.car_car USING btree (fuel_id);


--
-- Name: car_car_model_id_c6a3329e; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_model_id_c6a3329e ON public.car_car USING btree (model_id);


--
-- Name: car_car_office_id_93687ce9; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_office_id_93687ce9 ON public.car_car USING btree (office_id);


--
-- Name: car_carimage_car_id_da6e853f; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimage_car_id_da6e853f ON public.car_carimage USING btree (car_id);


--
-- Name: car_carimagetask_car_id_80477bac; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimagetask_car_id_80477bac ON public.car_carimagetask USING btree (car_id);


--
-- Name: car_carimagetask_images_carimage_id_b6bab1b6; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimagetask_images_carimage_id_b6bab1b6 ON public.car_carimagetask_images USING btree (carimage_id);


--
-- Name: car_carimagetask_images_carimagetask_id_ea5311ff; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimagetask_images_carimagetask_id_ea5311ff ON public.car_carimagetask_images USING btree (carimagetask_id);


--
-- Name: car_carimagetask_user_id_c86f4ff7; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimagetask_user_id_c86f4ff7 ON public.car_carimagetask USING btree (user_id);


--
-- Name: car_carpaper_car_id_1e838de9; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carpaper_car_id_1e838de9 ON public.car_carpaper USING btree (car_id);


--
-- Name: car_model_brand_id_a19b3370; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_model_brand_id_a19b3370 ON public.car_model USING btree (brand_id);


--
-- Name: credit_credit_contract_id_36edfa16; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX credit_credit_contract_id_36edfa16 ON public.credit_credit USING btree (contract_id);


--
-- Name: credit_creditimage_credit_id_6eb7d057; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX credit_creditimage_credit_id_6eb7d057 ON public.credit_creditimage USING btree (credit_id);


--
-- Name: credit_creditpayment_credit_id_0481595c; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX credit_creditpayment_credit_id_0481595c ON public.credit_creditpayment USING btree (credit_id);


--
-- Name: debt_debt_car_id_fe8a8c5b; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX debt_debt_car_id_fe8a8c5b ON public.debt_debt USING btree (car_id);


--
-- Name: debt_debt_driver_id_3af7225e; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX debt_debt_driver_id_3af7225e ON public.debt_debt USING btree (driver_id);


--
-- Name: debt_debtimage_debt_id_0e8cca69; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX debt_debtimage_debt_id_0e8cca69 ON public.debt_debtimage USING btree (debt_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: rent_taxi_renttaxi_car_id_c9a98fae; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_renttaxi_car_id_c9a98fae ON public.rent_taxi_renttaxi USING btree (car_id);


--
-- Name: rent_taxi_revenue_contract_id_ac7af62c; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenue_contract_id_ac7af62c ON public.rent_taxi_revenue USING btree (contract_id);


--
-- Name: rent_taxi_revenue_driver_id_975ee1ce; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenue_driver_id_975ee1ce ON public.rent_taxi_revenue USING btree (driver_id);


--
-- Name: rent_taxi_revenue_renttaxi_id_7f9b0691; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenue_renttaxi_id_7f9b0691 ON public.rent_taxi_revenue USING btree (renttaxi_id);


--
-- Name: rent_taxi_revenueitem_company_id_c6b8085b; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenueitem_company_id_c6b8085b ON public.rent_taxi_revenueitem USING btree (company_id);


--
-- Name: rent_taxi_revenueitem_rent_taxi_id_dd2412d5; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenueitem_rent_taxi_id_dd2412d5 ON public.rent_taxi_revenueitem USING btree (rent_taxi_id);


--
-- Name: rent_taxi_revenueitem_revenue_id_b8126f01; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenueitem_revenue_id_b8126f01 ON public.rent_taxi_revenueitem USING btree (revenue_id);


--
-- Name: shtraf_shtraf_car_id_fd95b8fd; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX shtraf_shtraf_car_id_fd95b8fd ON public.shtraf_shtraf USING btree (car_id);


--
-- Name: shtraf_shtraf_driver_id_8a231c53; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX shtraf_shtraf_driver_id_8a231c53 ON public.shtraf_shtraf USING btree (driver_id);


--
-- Name: shtraf_shtrafimage_shtraf_id_4de9eba2; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX shtraf_shtrafimage_shtraf_id_4de9eba2 ON public.shtraf_shtrafimage USING btree (shtraf_id);


--
-- Name: taxi_revenue_contract_id_6335489b; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenue_contract_id_6335489b ON public.taxi_revenue USING btree (contract_id);


--
-- Name: taxi_revenue_driver_id_8d1abf69; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenue_driver_id_8d1abf69 ON public.taxi_revenue USING btree (driver_id);


--
-- Name: taxi_revenue_taxi_id_b848c7a1; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenue_taxi_id_b848c7a1 ON public.taxi_revenue USING btree (taxi_id);


--
-- Name: taxi_revenueitem_company_id_185a75e6; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenueitem_company_id_185a75e6 ON public.taxi_revenueitem USING btree (company_id);


--
-- Name: taxi_revenueitem_revenue_id_cb85c3d8; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenueitem_revenue_id_cb85c3d8 ON public.taxi_revenueitem USING btree (revenue_id);


--
-- Name: taxi_revenueitem_taxi_id_5bff8b44; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenueitem_taxi_id_5bff8b44 ON public.taxi_revenueitem USING btree (taxi_id);


--
-- Name: taxi_taxi_car_id_3cbef2c9; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_taxi_car_id_3cbef2c9 ON public.taxi_taxi USING btree (car_id);


--
-- Name: account_contract account_contract_car_id_5a2aec70_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_contract
    ADD CONSTRAINT account_contract_car_id_5a2aec70_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_contract account_contract_customer_id_034347f8_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_contract
    ADD CONSTRAINT account_contract_customer_id_034347f8_fk_account_customuser_id FOREIGN KEY (customer_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_customuser_groups account_customuser_g_customuser_id_b6c60904_fk_account_c; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups
    ADD CONSTRAINT account_customuser_g_customuser_id_b6c60904_fk_account_c FOREIGN KEY (customuser_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_customuser_groups account_customuser_groups_group_id_2be9f6d7_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups
    ADD CONSTRAINT account_customuser_groups_group_id_2be9f6d7_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_customuser_user_permissions account_customuser_u_customuser_id_03bcc114_fk_account_c; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions
    ADD CONSTRAINT account_customuser_u_customuser_id_03bcc114_fk_account_c FOREIGN KEY (customuser_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_customuser_user_permissions account_customuser_u_permission_id_f4aec423_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions
    ADD CONSTRAINT account_customuser_u_permission_id_f4aec423_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_licenseimage account_licenseimage_user_id_2fa01b01_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_licenseimage
    ADD CONSTRAINT account_licenseimage_user_id_2fa01b01_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_passportimage account_passportimage_user_id_43c1cf55_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_passportimage
    ADD CONSTRAINT account_passportimage_user_id_43c1cf55_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_userimage account_userimage_user_id_60023a82_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userimage
    ADD CONSTRAINT account_userimage_user_id_60023a82_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_userphone account_userphone_user_id_bf883813_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userphone
    ADD CONSTRAINT account_userphone_user_id_bf883813_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_advertise_id_a27f1939_fk_taxi_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_advertise_id_a27f1939_fk_taxi_company_id FOREIGN KEY (advertise_id) REFERENCES public.taxi_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_car_class_id_b7eb688e_fk_car_carclass_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_car_class_id_b7eb688e_fk_car_carclass_id FOREIGN KEY (car_class_id) REFERENCES public.car_carclass(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_case_id_b68dd611_fk_car_case_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_case_id_b68dd611_fk_car_case_id FOREIGN KEY (case_id) REFERENCES public.car_case(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_color_id_f863d3f6_fk_car_color_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_color_id_f863d3f6_fk_car_color_id FOREIGN KEY (color_id) REFERENCES public.car_color(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_driver_id_e2236767_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_driver_id_e2236767_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_fuel_id_84981bd3_fk_car_fuel_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_fuel_id_84981bd3_fk_car_fuel_id FOREIGN KEY (fuel_id) REFERENCES public.car_fuel(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_model_id_c6a3329e_fk_car_model_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_model_id_c6a3329e_fk_car_model_id FOREIGN KEY (model_id) REFERENCES public.car_model(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_office_id_93687ce9_fk_car_office_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_office_id_93687ce9_fk_car_office_id FOREIGN KEY (office_id) REFERENCES public.car_office(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimage car_carimage_car_id_da6e853f_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimage
    ADD CONSTRAINT car_carimage_car_id_da6e853f_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimagetask car_carimagetask_car_id_80477bac_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask
    ADD CONSTRAINT car_carimagetask_car_id_80477bac_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimagetask_images car_carimagetask_ima_carimagetask_id_ea5311ff_fk_car_carim; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images
    ADD CONSTRAINT car_carimagetask_ima_carimagetask_id_ea5311ff_fk_car_carim FOREIGN KEY (carimagetask_id) REFERENCES public.car_carimagetask(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimagetask_images car_carimagetask_images_carimage_id_b6bab1b6_fk_car_carimage_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images
    ADD CONSTRAINT car_carimagetask_images_carimage_id_b6bab1b6_fk_car_carimage_id FOREIGN KEY (carimage_id) REFERENCES public.car_carimage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimagetask car_carimagetask_user_id_c86f4ff7_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask
    ADD CONSTRAINT car_carimagetask_user_id_c86f4ff7_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carpaper car_carpaper_car_id_1e838de9_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carpaper
    ADD CONSTRAINT car_carpaper_car_id_1e838de9_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_model car_model_brand_id_a19b3370_fk_car_brand_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_model
    ADD CONSTRAINT car_model_brand_id_a19b3370_fk_car_brand_id FOREIGN KEY (brand_id) REFERENCES public.car_brand(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: credit_credit credit_credit_contract_id_36edfa16_fk_account_contract_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_credit
    ADD CONSTRAINT credit_credit_contract_id_36edfa16_fk_account_contract_id FOREIGN KEY (contract_id) REFERENCES public.account_contract(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: credit_creditimage credit_creditimage_credit_id_6eb7d057_fk_credit_credit_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditimage
    ADD CONSTRAINT credit_creditimage_credit_id_6eb7d057_fk_credit_credit_id FOREIGN KEY (credit_id) REFERENCES public.credit_credit(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: credit_creditpayment credit_creditpayment_credit_id_0481595c_fk_credit_credit_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditpayment
    ADD CONSTRAINT credit_creditpayment_credit_id_0481595c_fk_credit_credit_id FOREIGN KEY (credit_id) REFERENCES public.credit_credit(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: debt_debt debt_debt_car_id_fe8a8c5b_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debt
    ADD CONSTRAINT debt_debt_car_id_fe8a8c5b_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: debt_debt debt_debt_driver_id_3af7225e_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debt
    ADD CONSTRAINT debt_debt_driver_id_3af7225e_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: debt_debtimage debt_debtimage_debt_id_0e8cca69_fk_debt_debt_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debtimage
    ADD CONSTRAINT debt_debtimage_debt_id_0e8cca69_fk_debt_debt_id FOREIGN KEY (debt_id) REFERENCES public.debt_debt(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_renttaxi rent_taxi_renttaxi_car_id_c9a98fae_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_renttaxi
    ADD CONSTRAINT rent_taxi_renttaxi_car_id_c9a98fae_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenue rent_taxi_revenue_contract_id_ac7af62c_fk_account_contract_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue
    ADD CONSTRAINT rent_taxi_revenue_contract_id_ac7af62c_fk_account_contract_id FOREIGN KEY (contract_id) REFERENCES public.account_contract(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenue rent_taxi_revenue_driver_id_975ee1ce_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue
    ADD CONSTRAINT rent_taxi_revenue_driver_id_975ee1ce_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenue rent_taxi_revenue_renttaxi_id_7f9b0691_fk_rent_taxi_renttaxi_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue
    ADD CONSTRAINT rent_taxi_revenue_renttaxi_id_7f9b0691_fk_rent_taxi_renttaxi_id FOREIGN KEY (renttaxi_id) REFERENCES public.rent_taxi_renttaxi(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenueitem rent_taxi_revenueite_rent_taxi_id_dd2412d5_fk_rent_taxi; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem
    ADD CONSTRAINT rent_taxi_revenueite_rent_taxi_id_dd2412d5_fk_rent_taxi FOREIGN KEY (rent_taxi_id) REFERENCES public.rent_taxi_renttaxi(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenueitem rent_taxi_revenueite_revenue_id_b8126f01_fk_rent_taxi; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem
    ADD CONSTRAINT rent_taxi_revenueite_revenue_id_b8126f01_fk_rent_taxi FOREIGN KEY (revenue_id) REFERENCES public.rent_taxi_revenue(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenueitem rent_taxi_revenueitem_company_id_c6b8085b_fk_taxi_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem
    ADD CONSTRAINT rent_taxi_revenueitem_company_id_c6b8085b_fk_taxi_company_id FOREIGN KEY (company_id) REFERENCES public.taxi_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shtraf_shtraf shtraf_shtraf_car_id_fd95b8fd_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtraf
    ADD CONSTRAINT shtraf_shtraf_car_id_fd95b8fd_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shtraf_shtraf shtraf_shtraf_driver_id_8a231c53_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtraf
    ADD CONSTRAINT shtraf_shtraf_driver_id_8a231c53_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shtraf_shtrafimage shtraf_shtrafimage_shtraf_id_4de9eba2_fk_shtraf_shtraf_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtrafimage
    ADD CONSTRAINT shtraf_shtrafimage_shtraf_id_4de9eba2_fk_shtraf_shtraf_id FOREIGN KEY (shtraf_id) REFERENCES public.shtraf_shtraf(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenue taxi_revenue_contract_id_6335489b_fk_account_contract_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue
    ADD CONSTRAINT taxi_revenue_contract_id_6335489b_fk_account_contract_id FOREIGN KEY (contract_id) REFERENCES public.account_contract(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenue taxi_revenue_driver_id_8d1abf69_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue
    ADD CONSTRAINT taxi_revenue_driver_id_8d1abf69_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenue taxi_revenue_taxi_id_b848c7a1_fk_taxi_taxi_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue
    ADD CONSTRAINT taxi_revenue_taxi_id_b848c7a1_fk_taxi_taxi_id FOREIGN KEY (taxi_id) REFERENCES public.taxi_taxi(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenueitem taxi_revenueitem_company_id_185a75e6_fk_taxi_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem
    ADD CONSTRAINT taxi_revenueitem_company_id_185a75e6_fk_taxi_company_id FOREIGN KEY (company_id) REFERENCES public.taxi_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenueitem taxi_revenueitem_revenue_id_cb85c3d8_fk_taxi_revenue_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem
    ADD CONSTRAINT taxi_revenueitem_revenue_id_cb85c3d8_fk_taxi_revenue_id FOREIGN KEY (revenue_id) REFERENCES public.taxi_revenue(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenueitem taxi_revenueitem_taxi_id_5bff8b44_fk_taxi_taxi_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem
    ADD CONSTRAINT taxi_revenueitem_taxi_id_5bff8b44_fk_taxi_taxi_id FOREIGN KEY (taxi_id) REFERENCES public.taxi_taxi(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_taxi taxi_taxi_car_id_3cbef2c9_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_taxi
    ADD CONSTRAINT taxi_taxi_car_id_3cbef2c9_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

