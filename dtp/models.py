from django.db import models
from car.models import Car
from account.mixins import CustomMixin


class Dtp(CustomMixin):
    INSURANCE_TYPES = (
        ('C','Casco'),
        ('O','OSAGO')
    )

    car = models.ForeignKey(Car, on_delete=models.CASCADE, related_name='dtps')
    driver = models.ForeignKey('account.CustomUser', on_delete=models.SET_NULL, null=True, blank=True, related_name='dtps')
    date = models.DateField()
    is_driver_fault = models.BooleanField(default=False)
    insurance = models.CharField(max_length=1,choices=INSURANCE_TYPES)
    insurance_company = models.ForeignKey('insurance.Company', on_delete=models.SET_NULL, null=True, blank=True)
    expert_observation = models.BooleanField(default=False)
    expert_inspection = models.BooleanField(default=False)
    insurance_back_status = models.BooleanField(default=False)
    insurance_back_amount = models.FloatField(null=True, blank=True)
    insurance_back_date = models.DateField(null=True, blank=True)
    insurance_back_staff = models.CharField(max_length=100, null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if self.expert_inspection:
            self.car.health_status = 'G'
        else:
            self.car.health_status = 'D'
        self.car.save()
        self.is_active = not self.insurance_back_status
        if self.pk is None and self.driver is None and self.car.driver is not None:
            self.driver = self.car.driver

        super(Dtp, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.car}'
    