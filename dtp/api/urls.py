from django.urls import path
from dtp.api import views as api_views


app_name = 'dtp'

urlpatterns = [
    path('list/',api_views.DtpListAPIView.as_view(),name='dtp-list'),
]
