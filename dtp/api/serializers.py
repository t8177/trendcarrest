from rest_framework import serializers
from car.api.serializers import CarImageSerializer, ModelSerializer
from account.api.serializers import CustomerBasicSerializer
from insurance.models import Casco, CascoImage
from dtp.models import Dtp
from car.api.serializers import BasicCarSerializer
from account.api.serializers import CustomerBasicSerializer
from insurance.api.serializers import CompanySerializer

class DtpListSerializer(serializers.ModelSerializer):
    driver = CustomerBasicSerializer()
    car = BasicCarSerializer()
    insurance_company = CompanySerializer()
    class Meta:
        model = Dtp
        fields = '__all__'

