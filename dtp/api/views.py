import datetime
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from rest_framework import status, viewsets
from rest_framework.views import APIView
from dtp.api.serializers import DtpListSerializer
from dtp.models import Dtp


class DtpListAPIView(ListAPIView):
    queryset = Dtp.objects.all().order_by('-id')
    serializer_class = DtpListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
