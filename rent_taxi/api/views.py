from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from account.api.permissions import CustomDjangoModelPermissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from rent_taxi.api.serializers import *
from rent_taxi.models import *
from credit.filters import CreditFilter, CreditPaymentFilter
from rest_framework.viewsets import ModelViewSet


class RentTaxiListAPIView(generics.ListAPIView):
    queryset = RentTaxi.objects.all().order_by('-id')
    serializer_class = RentListTaxiSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        renttaxis = RentTaxi.objects.filter(is_active=True)
        return renttaxis

class RentTaxiRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = RentTaxiRetrieveSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = RentTaxi.objects.all()


class RentTaxiRevenueListCreateAPIView(generics.ListCreateAPIView):
    queryset = Revenue.objects.all().order_by('-id')
    serializer_class = RentTaxiRevenueSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        taxi = RentTaxi.objects.get(pk=self.kwargs['taxi_pk'])
        revenues = Revenue.objects.filter()
        return revenues

class RentTaxiRevenueItemListCreateAPIView(generics.ListCreateAPIView):
    queryset = RevenueItem.objects.all().order_by('-id')
    serializer_class = RentTaxiRevenueItemSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        revenue = Revenue.objects.get(pk=self.kwargs['revenue_pk'])
        revenueItems = RevenueItem.objects.filter(revenue=revenue)
        return revenueItems