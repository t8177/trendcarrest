from django.urls import path
from django.urls.conf import include
from rent_taxi.api import views as api_views

app_name = 'rent_taxi'

urlpatterns = [
    path('list/', api_views.RentTaxiListAPIView.as_view(), name='renttaxis'),
    path('renttaxi/<int:pk>/', api_views.RentTaxiRetrieveAPIView.as_view(), name='renttaxi'),
    path('renttaxis/<int:taxi_pk>/revenues/', api_views.RentTaxiRevenueListCreateAPIView.as_view(), name='renttaxi-revenues'),
    path('renttaxis/revenues/<int:revenue_pk>/', api_views.RentTaxiRevenueItemListCreateAPIView.as_view(), name='renttaxi-revenue-items'),

]