# Generated by Django 4.0.3 on 2022-03-12 17:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rent_taxi', '0009_rename_bolt_will_pay_total_revenueitem_company_will_pay_total'),
    ]

    operations = [
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('commission', models.FloatField(default=5)),
                ('weekly_rent', models.FloatField(default=4500)),
            ],
        ),
        migrations.AddField(
            model_name='revenueitem',
            name='we_must_pay',
            field=models.FloatField(default=0),
        ),
    ]
