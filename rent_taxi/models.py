from django.db import models
from django.contrib.auth import get_user_model
from account.mixins import CustomMixin
from car.models import Car
from taxi.models import Company
from account.models import Contract

User = get_user_model()

class Settings(models.Model):
    commission_from_driver = models.FloatField(default=5)
    weekly_rent = models.FloatField(default=4500)

    def __str__(self):
        return f'Settings for Rent Taxi'


class Tariff(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='count_tariff')
    limit1 = models.PositiveIntegerField(default=90)
    limit2 = models.PositiveIntegerField(default=120)
    limit3 = models.PositiveIntegerField(default=150)
    limit1_tariff = models.FloatField(default=12)
    limit2_tariff = models.FloatField(default=13)
    limit3_tariff = models.FloatField(default=14)

    def __str__(self):
        return f'{self.company.name} - Tariffs'


class RentTaxi(CustomMixin):
    car = models.ForeignKey(Car,on_delete=models.CASCADE,null=True,blank=True,related_name='rent_taxis')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.car.model.name} - {self.car.year} - {self.car.number}'
 

class Revenue(CustomMixin):
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, related_name="rent_taxi_revenues")
    driver = models.ForeignKey(User, on_delete=models.CASCADE,related_name="rent_taxi_revenues")
    rent_taxi = models.ForeignKey(RentTaxi, on_delete=models.CASCADE,related_name="revenues")
    start_date = models.DateField()
    end_date = models.DateField()
 
    class Meta:
        ordering = ('start_date',)

    def __str__(self):
        return f'{self.driver.first_name} - {self.rent_taxi.car.number}  {self.start_date} - {self.end_date}'

    def save(self, *args, **kwargs):
        super(Revenue, self).save(*args, **kwargs)

        # First let's trigger al it's own sub records
        for item in self.items.all():
            item.save()

        # To trigger all sub records
        try:
            obj = self.rent_taxi.revenues.filter(start_date__gt=self.start_date).first()
            revenue_items = obj.items.all()
            for item in revenue_items:
                item.save()
        except:
            pass


class RevenueItem(models.Model):
    revenue = models.ForeignKey(Revenue,on_delete=models.CASCADE,related_name="items")
    company = models.ForeignKey(Company,on_delete=models.CASCADE,related_name='rent_taxi_revenue_items')
    rent_taxi = models.ForeignKey(RentTaxi,on_delete=models.CASCADE,null=True, blank=True,related_name='revenue_items')
    order_count = models.PositiveSmallIntegerField(default=0)
    company_will_pay_brand = models.FloatField(default=0)
    amount_cash = models.FloatField(default=0)
    amount_card = models.FloatField(default=0)
    company_commission = models.FloatField(default=0) 
    rent_amount = models.FloatField(default=0)
    commission_from_driver = models.FloatField(default=0)

    debt_to_bolt = models.FloatField(default=0)
    company_will_pay_total = models.FloatField(default=0) 

    driver_must_pay = models.FloatField(default=0)
    we_must_pay = models.FloatField(default=0)
    driver_paid = models.FloatField(default=0)
    we_paid = models.FloatField(default=0)

    debt_today = models.FloatField(default=0)
    debt_all = models.FloatField(default=0) #

    class Meta:
        ordering = ('revenue__start_date',)

    def save(self, *args, **kwargs):
        # Settings objects
        settings = Settings.objects.last()
        tariff = Tariff.objects.filter(company__pk=self.company.id).last()

        # Company Will Pay for Brand
        if self.order_count < tariff.limit1:
            self.company_will_pay_brand = 0
        elif self.order_count >= tariff.limit1 and self.order_count < tariff.limit2:
            self.company_will_pay_brand = self.order_count * tariff.limit1_tariff
        elif self.order_count >= tariff.limit2 and self.order_count < tariff.limit3:
            self.company_will_pay_brand = self.order_count * tariff.limit2_tariff
        elif self.order_count >= tariff.limit3:
            self.company_will_pay_brand = self.order_count * tariff.limit3_tariff


        

        # Company commission
        self.company_commission = (self.amount_card + self.amount_cash) * self.company.commission/100

        # Rent amount
        if self.rent_amount == 0:
            self.rent_amount = settings.weekly_rent

        # Commission from driver
        if self.order_count >= tariff.limit3:
            self.commission_from_driver = 0
        else:
            self.commission_from_driver = (self.amount_card + self.amount_cash - self.company_commission) * settings.commission_from_driver/100

        # Debt to bolt
        if ((self.amount_card + self.amount_cash) * self.company.commission/100 - self.amount_card) > 0:
            self.debt_to_bolt = ((self.amount_card + self.amount_cash) * self.company.commission/100 - self.amount_card)
        else:
            self.debt_to_bolt = 0

        # Company will pay
        self.company_will_pay_total = self.amount_card - self.company_commission + self.company_will_pay_brand

        # We or Driver must pay
        if (self.rent_amount+self.commission_from_driver+self.debt_to_bolt-self.amount_card) > 0:
            self.driver_must_pay = self.rent_amount+self.commission_from_driver+self.debt_to_bolt-self.amount_card
            self.we_must_pay = 0
        elif (self.rent_amount+self.commission_from_driver+self.debt_to_bolt-self.amount_card) < 0:
            self.we_must_pay = -(self.rent_amount+self.commission_from_driver+self.debt_to_bolt-self.amount_card)
            self.driver_must_pay = 0
            
        # Debt this week
        if self.we_must_pay > 0:
            self.debt_today = self.driver_paid - self.we_paid + self.we_must_pay
        elif self.driver_must_pay > 0:
            self.debt_today = self.driver_paid - self.driver_must_pay - self.we_paid

        # Debt All
        try:
            prev_revenue = self.revenue.rent_taxi.revenues.filter(start_date__lt=self.revenue.start_date).last()
            print(prev_revenue)
            print("-------------gg")
            prev_revenue_item = prev_revenue.items.filter(company__pk=self.company.id).last()
            self.debt_all = self.debt_today + prev_revenue_item.debt_all
        except:
            self.debt_all = self.debt_today
            print("-------------error")

        super(RevenueItem, self).save(*args, **kwargs)

        # To trigger all next records
        try:
            next_obj = self.revenue.rent_taxi.revenues.filter(start_date__gt=self.revenue.start_date).first()
            next_revenue_item = next_obj.items.filter(company__pk=self.company.id).last()
            next_revenue_item.save()
            #print(next_obj.date)
        except:
            pass

    def __str__(self):
        return f'{self.rent_taxi} - {self.revenue.start_date} - {self.company}' 





