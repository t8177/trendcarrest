from django.contrib import admin
from .models import *

admin.site.register(Tariff)
admin.site.register(Settings)
admin.site.register(RevenueItem)

class RevenueItemInline(admin.TabularInline):
    model = RevenueItem
    extra = 1

@admin.register(Revenue)
class RevenueAdmin(admin.ModelAdmin):
    inlines = [RevenueItemInline,]


class RevenueInline(admin.TabularInline):
    model = Revenue
    extra = 1

@admin.register(RentTaxi)
class RentTaxiAdmin(admin.ModelAdmin):
    inlines = [RevenueInline,]