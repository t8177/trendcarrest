import datetime
from xmlrpc.client import Boolean
from django.db import models
from django.contrib.auth import get_user_model
from car.models import Car
from account.mixins import CustomMixin
from account.models import Contract

User = get_user_model()

class Settings(models.Model):
    rent_per_day = models.FloatField(default=4500)
    deposit_amount = models.FloatField(default=1500)
    def __str__(self):
        return f'Settings for Rent'


class Rent(models.Model):
    car = models.ForeignKey(Car,on_delete=models.CASCADE,null=True,blank=True,related_name='rents')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.car.model.name} - {self.car.year} - {self.car.number}'


class Revenue(CustomMixin):
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, related_name="rent_revenues")
    driver = models.ForeignKey(User, on_delete=models.CASCADE,related_name="rent_revenues")
    rent = models.ForeignKey(Rent, on_delete=models.CASCADE,related_name="revenues")
    start_date = models.DateField()
    end_date = models.DateField(default=datetime.date.today)

    rented_days = models.PositiveIntegerField(default=0)
    rent_amount = models.FloatField(default=0)
    is_paid = models.BooleanField(default=True)
    
    is_deposit_in = models.BooleanField(default=True)
    is_deposit_out = models.BooleanField(default=False)
    deposit_amount = models.FloatField(default=0)

 
    class Meta:
        ordering = ('start_date',)

    def __str__(self):
        return f'{self.driver.first_name} - {self.rent.car.number}  {self.start_date}'

    def save(self, *args, **kwargs):
        self.deposit_amount = self.contract.deposit_amount

        self.rented_days = (self.end_date - self.start_date).days
        super(Revenue, self).save(*args, **kwargs)