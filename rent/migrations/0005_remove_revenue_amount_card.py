# Generated by Django 4.0.3 on 2022-03-19 21:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rent', '0004_revenue_end_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='revenue',
            name='amount_card',
        ),
    ]
