from django.contrib import admin
from .models import *

class RevenueInline(admin.TabularInline):
    model = Revenue
    extra = 1

@admin.register(Rent)
class RentAdmin(admin.ModelAdmin):
    inlines = [RevenueInline,]