import datetime
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from rest_framework import status, viewsets
from rest_framework.views import APIView
from rent.api.serializers import RentListSerializer
from rent.models import Rent


class RentListAPIView(ListAPIView):
    queryset = Rent.objects.all().order_by('-id')
    serializer_class = RentListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        today = datetime.datetime.now().strftime('%Y-%m-%d')
        rents = Rent.objects.prefetch_related('revenues').filter(revenues__end_date__gte=today)
        return rents