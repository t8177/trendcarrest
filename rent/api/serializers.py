import datetime
from rest_framework import serializers
from rent.models import Rent
from car.api.serializers import BasicCarSerializer

class RentListSerializer(serializers.ModelSerializer):
    car = BasicCarSerializer()
    customer = serializers.SerializerMethodField()
    deposit_amount = serializers.SerializerMethodField()
    rent_amount = serializers.SerializerMethodField()
    start_date = serializers.SerializerMethodField()
    end_date = serializers.SerializerMethodField()

    def get_start_date(self,obj,*args,**kwargs):
        today = datetime.datetime.now().strftime('%Y-%m-%d')
        revenue = obj.revenues.filter(end_date__gte=today).last()
        if revenue:
            return revenue.start_date
        else:
            return None

    def get_end_date(self,obj,*args,**kwargs):
        today = datetime.datetime.now().strftime('%Y-%m-%d')
        revenue = obj.revenues.filter(end_date__gte=today).last()
        if revenue:
            return revenue.end_date
        else:
            return None
            
    def get_deposit_amount(self,obj,*args,**kwargs):
        today = datetime.datetime.now().strftime('%Y-%m-%d')
        revenue = obj.revenues.filter(end_date__gte=today).last()
        if revenue:
            return revenue.deposit_amount
        else:
            return None

    def get_rent_amount(self,obj,*args,**kwargs):
        today = datetime.datetime.now().strftime('%Y-%m-%d')
        revenue = obj.revenues.filter(end_date__gte=today).last()
        if revenue:
            return revenue.rent_amount
        else:
            return None

    def get_customer(self,obj,*args,**kwargs):
        today = datetime.datetime.now().strftime('%Y-%m-%d')
        revenue = obj.revenues.filter(end_date__gte=today).last()
        if revenue:
            data = {
                "id":revenue.contract.customer.id,
                "first_name":revenue.contract.customer.first_name,
                "last_name":revenue.contract.customer.last_name,

            }
            return data
        else:
            return None

    class Meta:
        model = Rent
        fields = ['id','car','customer','deposit_amount','rent_amount','start_date','end_date','is_active',]