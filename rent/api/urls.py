from django.urls import path
from rent.api import views as api_views


app_name = 'rent'

urlpatterns = [
    path('list/',api_views.RentListAPIView.as_view(),name='car-list'),
]
