from django.db.models.signals import  post_delete, post_save
from django.dispatch import receiver
from .models import *


# For Company
@receiver(post_delete, sender=Revenue)
def calculate_payments_after_one_deleted(sender, instance, *args,**kwargs):

    try:
        obj = instance.taxi.revenues.filter(date__gt=instance.date).first()
        revenue_items = obj.items.all()
        for item in revenue_items:
            item.save()
    except:
        pass



@receiver(post_save, sender=Revenue)
def change_car_driver_when_create(sender, instance,created,**kwargs):
    if created:
        print('Hey')
        car = instance.contract.car
        car.driver = instance.contract.customer
        car.status = 'RTP'
        car.save()


@receiver(post_delete, sender=Revenue)
def change_car_driver_when_deleted(sender, instance, *args,**kwargs):
    car = instance.contract.car
    instance.contract.is_active = False
    instance.contract.save()
    print(car.contracts.filter(is_active=True).last())
    print('HHey')
    try:
        car.driver = car.contracts.filter(is_active=True).last().customer
        car.status = car.contracts.filter(is_active=True).last().type
        car.save()
    except:
        car.driver = None
        car.status = 'F'
        car.save()
    

@receiver(post_save, sender=Taxi)
def change_car_driver_when_create(sender, instance,created,**kwargs):
    if created:
        print('Hey')
        car = instance.car
        car.status = 'RTP'
        car.save()


@receiver(post_delete, sender=Taxi)
def change_car_driver_when_deleted(sender, instance, *args,**kwargs):
    car = instance.car
    print(car.contracts.filter(is_active=True).last())
    print('HHey')
    try:
        car.driver = car.contracts.filter(is_active=True).last().customer
        car.status = car.contracts.filter(is_active=True).last().type
        car.save()
    except:
        car.driver = None
        car.status = 'F'
        car.save()
    