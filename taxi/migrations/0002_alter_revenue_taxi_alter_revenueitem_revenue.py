# Generated by Django 4.0.2 on 2022-02-18 20:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('taxi', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='revenue',
            name='taxi',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='revenues', to='taxi.taxi'),
        ),
        migrations.AlterField(
            model_name='revenueitem',
            name='revenue',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='taxi.revenue'),
        ),
    ]
