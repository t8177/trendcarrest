# Generated by Django 4.0.2 on 2022-02-18 18:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('car', '0001_initial'),
        ('account', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('commission', models.FloatField()),
                ('tarif1_count', models.PositiveIntegerField()),
                ('tarif1_pay', models.FloatField()),
                ('tarif2_count', models.PositiveIntegerField()),
                ('tarif2_pay', models.FloatField()),
                ('tarif3_count', models.PositiveIntegerField()),
                ('tarif3_pay', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Revenue',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('date', models.DateField()),
                ('contract', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='account.contract')),
                ('driver', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Taxi',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('date', models.DateField()),
                ('is_active', models.BooleanField(default=True)),
                ('car', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='taxis', to='car.car')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RevenueItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount_cash', models.FloatField(default=0)),
                ('amount_card', models.FloatField(default=0)),
                ('order_count', models.PositiveSmallIntegerField(default=0)),
                ('paid', models.FloatField(default=0)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='revenues', to='taxi.company')),
                ('revenue', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='taxi.revenue')),
            ],
        ),
        migrations.AddField(
            model_name='revenue',
            name='taxi',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='taxi.taxi'),
        ),
    ]
