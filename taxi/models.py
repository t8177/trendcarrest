from django.db import models
from django.contrib.auth import get_user_model
from account.models import Contract
from account.mixins import CustomMixin
from car.models import Car

User = get_user_model()
class Company(models.Model):
    name = models.CharField(max_length=100)
    commission = models.FloatField()

    def __str__(self):
        return self.name
 
class Tariff(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='taxi_tariff')
    limit1 = models.FloatField(default=0)
    limit2 = models.FloatField(default=2000)
    limit3 = models.FloatField(default=2500)
    limit1_driver_percent = models.PositiveIntegerField(default=50)
    limit2_driver_percent = models.PositiveIntegerField(default=60)
    limit3_driver_percent = models.PositiveIntegerField(default=70)

    def __str__(self):
        return f'{self.company.name} - Tariffs'

class Taxi(CustomMixin):
    car = models.ForeignKey(Car,on_delete=models.CASCADE,null=True,blank=True,related_name='taxis')
    is_active = models.BooleanField(default=True)
    
    def __str__(self):
        return f'{self.car.model.name} - {self.car.year} - {self.car.number}'
 
 
class Revenue(CustomMixin):
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, related_name='taxi_revenues')
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='taxi_revenues')
    taxi = models.ForeignKey(Taxi, on_delete=models.CASCADE,related_name="revenues")
    date = models.DateField()

    class Meta:
        ordering = ('date',)

    def __str__(self):
        return f'{self.customer.username} - {self.taxi.car.number} - {self.date}'

    def save(self, *args, **kwargs):
        super(Revenue, self).save(*args, **kwargs)

        # First let's trigger al it's own sub records
        for item in self.items.all():
            item.save()

        # To trigger all sub records
        try:
            obj = self.taxi.revenues.filter(date__gt=self.date).first()
            revenue_items = obj.items.all()
            for item in revenue_items:
                item.save()
        except:
            pass


class RevenueItem(models.Model):
    revenue = models.ForeignKey(Revenue, on_delete=models.CASCADE,related_name="items")
    taxi = models.ForeignKey(Taxi, null=True, blank=True, on_delete=models.CASCADE,related_name="revenue_items")
    company = models.ForeignKey(Company,on_delete=models.CASCADE,related_name='taxi_revenue_items')
    amount_cash = models.FloatField(default=0)
    amount_card = models.FloatField(default=0)
    order_count = models.PositiveSmallIntegerField(default=0)
    driver_must_pay = models.FloatField(default=0) 
    we_must_pay = models.FloatField(default=0) 
    driver_paid = models.FloatField(default=0)
    we_paid = models.FloatField(default=0)

    debt_today = models.FloatField(default=0)
    debt_all = models.FloatField(default=0)

    class Meta:
        ordering = ('revenue__date',)

    def revenue_share(self, amount_cash,amount_card):
        tariff = Tariff.objects.filter(company__pk=self.company.id).last()
        total = amount_cash + amount_card
        driver_share = 0
        trendcar_share = 0
        if total < tariff.limit2:
            driver_share = total * tariff.limit1_driver_percent/100
            trendcar_share = total * (100 - tariff.limit1_driver_percent)/100
        elif total >= tariff.limit2 and total < tariff.limit3:
            driver_share = total * tariff.limit2_driver_percent/100
            trendcar_share = total * (100 - tariff.limit2_driver_percent)/100
        elif total >= tariff.limit3:
            driver_share = total * tariff.limit3_driver_percent/100
            trendcar_share = total * (100 - tariff.limit3_driver_percent)/100
        return driver_share,trendcar_share


    def revenues_payment(self, amount_cash,amount_card):
        driver, trendcar = self.revenue_share(amount_cash,amount_card)
        difference = amount_card - trendcar
        return difference

    def save(self, *args, **kwargs):
        # driver_must_pay and we_must_pay
        if self.revenues_payment(self.amount_cash, self.amount_card) > 0:
            self.driver_must_pay = 0
            self.we_must_pay = self.revenues_payment(self.amount_cash, self.amount_card)
        elif self.revenues_payment(self.amount_cash, self.amount_card) < 0:
            self.driver_must_pay = -(self.revenues_payment(self.amount_cash, self.amount_card))
            self.we_must_pay = 0

        # debt_today
        self.debt_today = (self.revenues_payment(self.amount_cash, self.amount_card)+self.driver_paid-self.we_paid)

        
        # debt_all
        try:
            prev_revenue = self.revenue.taxi.revenues.filter(date__lt=self.revenue.date).last()
            prev_revenue_item = prev_revenue.items.filter(company__pk=self.company.id).last()
            self.debt_all = self.debt_today + prev_revenue_item.debt_all
        except:
            self.debt_all = self.debt_today

        super(RevenueItem, self).save(*args, **kwargs)

        # To trigger all next records
        try:
            next_obj = self.revenue.taxi.revenues.filter(date__gt=self.revenue.date).first()
            next_revenue_item = next_obj.items.filter(company__pk=self.company.id).last()
            next_revenue_item.save()
            #print(next_obj.date)
        except:
            pass

    def __str__(self):
        return f'{self.taxi} - {self.revenue.date} - {self.company}'