from django.db import models
from django.db.models import fields
from taxi.models import *
from rest_framework import serializers
from account.api.serializers import ContractSerializer
from car.api.serializers import BasicCarSerializer, CarSerializer
from datetime import date, timedelta
from django.utils import timezone
from django.db.models import Q
from django.db.models import Count, Sum, F

class RevenueItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = RevenueItem
        fields = '__all__'

class RevenueSerializer(serializers.ModelSerializer):
    items = RevenueItemSerializer(many=True)
    class Meta: 
        model = Revenue
        fields = ['id','date','items']


class TaxiListSerializer(serializers.ModelSerializer):
    car = BasicCarSerializer()
    last_driver = serializers.SerializerMethodField()
    yesterday = serializers.SerializerMethodField()
    yesterday_compare = serializers.SerializerMethodField()
    lastweek = serializers.SerializerMethodField()
    lastweek_compare = serializers.SerializerMethodField()
    thisweek = serializers.SerializerMethodField()
    thisweek_compare = serializers.SerializerMethodField()
    debt_all = serializers.SerializerMethodField()
    this_week_count = serializers.SerializerMethodField()
    last_week_count = serializers.SerializerMethodField()
    class Meta:
        model = Taxi
        fields = ['id','is_active','car','last_driver','yesterday','yesterday_compare','lastweek','lastweek_compare','thisweek','thisweek_compare','debt_all','this_week_count','last_week_count']
    
    
    def get_last_driver(self, obj):
        last_revenue = obj.revenues.last()
        if last_revenue:
            contract = last_revenue.contract
            return str(contract.customer.get_full_name())
        else:
            return None
    def get_yesterday(self, obj):
        today = date.today()
        yesterday = today - timedelta(days = 1)
        revenue = Revenue.objects.filter(taxi=obj, date=yesterday).last()
        revenue_items = RevenueItem.objects.filter(revenue=revenue)
        total = 0
        for i in revenue_items:
            total += i.amount_cash
            total +=i.amount_card
        return total

    def get_yesterday_compare(self, obj):
        today = date.today()
        yesterday = today - timedelta(days = 1)
        two_day_before = today - timedelta(days = 2)
        yesterday_revenue = Revenue.objects.filter(taxi=obj, date=yesterday).last()
        yesterday_revenue_items = RevenueItem.objects.filter(revenue=yesterday_revenue)
        yesterday_total = 0
        for i in yesterday_revenue_items:
            yesterday_total += i.amount_cash
            yesterday_total += i.amount_card
        two_day_before_revenue = Revenue.objects.filter(taxi=obj, date=two_day_before).last()
        two_day_before_revenue_items = RevenueItem.objects.filter(revenue=two_day_before_revenue)
        two_day_before_total = 0
        for i in two_day_before_revenue_items:
            two_day_before_total += i.amount_cash
            two_day_before_total += i.amount_card
        if  yesterday_total == 0:
            return False
        else:
            return yesterday_total >= two_day_before_total
        
    def get_lastweek(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_last_week, date__lt=monday_of_this_week, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                total +=x.total_cash
                total +=x.total_card
        return total



    def get_lastweek_compare(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        
        some_day_two_weeks_ago = timezone.now().date() - timedelta(days=14)
        monday_of_two_weeks_ago = some_day_two_weeks_ago - timedelta(days=(some_day_two_weeks_ago.isocalendar()[2] - 1))
        
        last_week_revenues = Revenue.objects.filter(date__gte=monday_of_last_week, date__lt=monday_of_this_week, taxi=obj)
        last_week_total = 0
        for i in last_week_revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                last_week_total +=x.total_cash
                last_week_total +=x.total_card
        two_weeks_ago_revenues = Revenue.objects.filter(date__gte=monday_of_two_weeks_ago, date__lt=monday_of_last_week, taxi=obj)
        two_weeks_ago_total = 0
        for i in two_weeks_ago_revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                two_weeks_ago_total +=x.total_cash
                two_weeks_ago_total +=x.total_card     
        if  last_week_total == 0:
            return False
        else:     
            return last_week_total >= two_weeks_ago_total
        
    def get_thisweek(self, obj):
        today = date.today()
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_this_week, date__lt=today, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                total +=x.total_cash
                total +=x.total_card
        return total

    def get_thisweek_compare(self, obj):
        today = date.today()
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        last_week_revenues = Revenue.objects.filter(date__gte=monday_of_last_week, date__lt=monday_of_this_week, taxi=obj)
        last_week_total = 0
        for i in last_week_revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                last_week_total +=x.total_cash
                last_week_total +=x.total_card
        revenues = Revenue.objects.filter(date__gte=monday_of_this_week, date__lt=today, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                total +=x.total_cash
                total +=x.total_card
        if (total/(today.weekday()+1)) == 0:
            return False
        else:     
            if last_week_total == 0:
                return True
            else:
                return (total/(today.weekday()+1)) >= (last_week_total/7)
            
    def get_debt_all(self, obj):
        revenues = Revenue.objects.filter(taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i)
            for x in item:
                total +=x.debt_today    
        return total
    def get_this_week_count(self, obj):
        today = date.today()
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_this_week, date__lt=today, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i)
            for x in item:
                total +=x.order_count
        return total
    def get_last_week_count(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_last_week, date__lt=monday_of_this_week, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i)
            for x in item:
                total +=x.order_count
        return total

class TaxiRetrieveSerializer(serializers.ModelSerializer):
    car = CarSerializer() 
    last_driver = serializers.SerializerMethodField()
    yesterday = serializers.SerializerMethodField()
    yesterday_compare = serializers.SerializerMethodField()
    lastweek = serializers.SerializerMethodField()
    lastweek_compare = serializers.SerializerMethodField()
    thisweek = serializers.SerializerMethodField()
    thisweek_compare = serializers.SerializerMethodField()
    debt_all = serializers.SerializerMethodField()
    this_week_count = serializers.SerializerMethodField()
    last_week_count = serializers.SerializerMethodField()
    revenues = RevenueSerializer(many=True)
    class Meta:
        model = Taxi
        fields = ['id','is_active','car','revenues','last_driver','yesterday','yesterday_compare','lastweek','lastweek_compare','thisweek','thisweek_compare','debt_all','this_week_count','last_week_count']
    
    def get_last_driver(self, obj):
        today = date.today()
        contract = Contract.objects.filter(car=obj.car,is_active=True).filter(Q(start_date__lte=today)&Q(end_date__gte=today)&Q(type='TX')).last()
        if contract:
            return str(contract.customer.get_full_name())
        else:
            return None
    def get_yesterday(self, obj):
        today = date.today()
        yesterday = today - timedelta(days = 1)
        revenue = Revenue.objects.filter(taxi=obj, date=yesterday).last()
        revenue_items = RevenueItem.objects.filter(revenue=revenue)
        total = 0
        for i in revenue_items:
            total += i.amount_cash
            total +=i.amount_card
        return total

    def get_yesterday_compare(self, obj):
        today = date.today()
        yesterday = today - timedelta(days = 1)
        two_day_before = today - timedelta(days = 2)
        yesterday_revenue = Revenue.objects.filter(taxi=obj, date=yesterday).last()
        yesterday_revenue_items = RevenueItem.objects.filter(revenue=yesterday_revenue)
        yesterday_total = 0
        for i in yesterday_revenue_items:
            yesterday_total += i.amount_cash
            yesterday_total += i.amount_card
        two_day_before_revenue = Revenue.objects.filter(taxi=obj, date=two_day_before).last()
        two_day_before_revenue_items = RevenueItem.objects.filter(revenue=two_day_before_revenue)
        two_day_before_total = 0
        for i in two_day_before_revenue_items:
            two_day_before_total += i.amount_cash
            two_day_before_total += i.amount_card
        if  yesterday_total == 0:
            return False
        else:
            return yesterday_total >= two_day_before_total
        
    def get_lastweek(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_last_week, date__lt=monday_of_this_week, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                total +=x.total_cash
                total +=x.total_card
        return total



    def get_lastweek_compare(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        
        some_day_two_weeks_ago = timezone.now().date() - timedelta(days=14)
        monday_of_two_weeks_ago = some_day_two_weeks_ago - timedelta(days=(some_day_two_weeks_ago.isocalendar()[2] - 1))
        
        last_week_revenues = Revenue.objects.filter(date__gte=monday_of_last_week, date__lt=monday_of_this_week, taxi=obj)
        last_week_total = 0
        for i in last_week_revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                last_week_total +=x.total_cash
                last_week_total +=x.total_card
        two_weeks_ago_revenues = Revenue.objects.filter(date__gte=monday_of_two_weeks_ago, date__lt=monday_of_last_week, taxi=obj)
        two_weeks_ago_total = 0
        for i in two_weeks_ago_revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                two_weeks_ago_total +=x.total_cash
                two_weeks_ago_total +=x.total_card     
        if  last_week_total == 0:
            return False
        else:     
            return last_week_total >= two_weeks_ago_total
        
    def get_thisweek(self, obj):
        today = date.today()
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_this_week, date__lt=today, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                total +=x.total_cash
                total +=x.total_card
        return total

    def get_thisweek_compare(self, obj):
        today = date.today()
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        last_week_revenues = Revenue.objects.filter(date__gte=monday_of_last_week, date__lt=monday_of_this_week, taxi=obj)
        last_week_total = 0
        for i in last_week_revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                last_week_total +=x.total_cash
                last_week_total +=x.total_card
        revenues = Revenue.objects.filter(date__gte=monday_of_this_week, date__lt=today, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i).annotate(total_cash=Sum('amount_cash'),total_card=Sum('amount_card'))
            for x in item:
                total +=x.total_cash
                total +=x.total_card
        if (total/(today.weekday()+1)) == 0:
            return False
        else:     
            if last_week_total == 0:
                return True
            else:
                return (total/(today.weekday()+1)) >= (last_week_total/7)
            
    def get_debt_all(self, obj):
        revenues = Revenue.objects.filter(taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i)
            for x in item:
                total +=x.debt_today    
        return total
    def get_this_week_count(self, obj):
        today = date.today()
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_this_week, date__lt=today, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i)
            for x in item:
                total +=x.order_count
        return total
    def get_last_week_count(self, obj):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        revenues = Revenue.objects.filter(date__gte=monday_of_last_week, date__lt=monday_of_this_week, taxi=obj)
        total = 0
        for i in revenues:
            item = RevenueItem.objects.filter(revenue=i)
            for x in item:
                total +=x.order_count
        return total

class TaxiRevenueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Revenue
        fields = '__all__'
 
class TaxiRevenueItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = RevenueItem
        fields = '__all__'
