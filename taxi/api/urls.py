from django.urls import path
from taxi.api import views as api_views


app_name = 'taxi'

urlpatterns = [
    path('list/', api_views.TaxiListAPIView.as_view(), name='taxis'),
    path('taxi/<int:pk>/', api_views.TaxiRetrieveAPIView.as_view(), name='taxi'),
    path('taxis/<int:taxi_pk>/revenues/', api_views.TaxiRevenueListCreateAPIView.as_view(), name='taxi-revenues'),
    path('taxis/revenues/<int:revenue_pk>/', api_views.TaxiRevenueItemListCreateAPIView.as_view(), name='taxi-revenue-items'),
 
]
