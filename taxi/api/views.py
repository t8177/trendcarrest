from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from account.api.permissions import CustomDjangoModelPermissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from taxi.api.serializers import *
from taxi.models import *
from credit.filters import CreditFilter, CreditPaymentFilter
from rest_framework.viewsets import ModelViewSet


class TaxiListAPIView(generics.ListAPIView):
    queryset = Taxi.objects.all().order_by('-id')
    serializer_class = TaxiListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        taxis = Taxi.objects.filter(is_active=True)
        return taxis

class TaxiRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = TaxiRetrieveSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Taxi.objects.all()

class TaxiRevenueListCreateAPIView(generics.ListCreateAPIView):
    queryset = Revenue.objects.all().order_by('-id')
    serializer_class = TaxiRevenueSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
 
    def get_queryset(self):
        taxi = Taxi.objects.get(pk=self.kwargs['taxi_pk'])
        revenues = Revenue.objects.filter()
        return revenues

class TaxiRevenueItemListCreateAPIView(generics.ListCreateAPIView):
    queryset = RevenueItem.objects.all().order_by('-id')
    serializer_class = TaxiRevenueItemSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        revenue = Revenue.objects.get(pk=self.kwargs['revenue_pk'])
        revenueItems = RevenueItem.objects.filter(revenue=revenue)
        return revenueItems