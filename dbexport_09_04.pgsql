--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: account_contract; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_contract (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    type character varying(3) NOT NULL,
    signature character varying(100),
    start_date date NOT NULL,
    end_date date NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint NOT NULL,
    customer_id bigint,
    deposit_amount double precision NOT NULL
);


ALTER TABLE public.account_contract OWNER TO alvinseyidov;

--
-- Name: account_contract_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_contract_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_contract_id_seq OWNER TO alvinseyidov;

--
-- Name: account_contract_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_contract_id_seq OWNED BY public.account_contract.id;


--
-- Name: account_customuser; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_customuser (
    id bigint NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    username character varying(128) NOT NULL,
    father_name character varying(50),
    birth_date date,
    passport character varying(50),
    license_number character varying(50),
    type character varying(3) NOT NULL,
    is_black_listed boolean NOT NULL,
    note text,
    rating integer NOT NULL,
    CONSTRAINT account_customuser_rating_check CHECK ((rating >= 0))
);


ALTER TABLE public.account_customuser OWNER TO alvinseyidov;

--
-- Name: account_customuser_groups; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_customuser_groups (
    id bigint NOT NULL,
    customuser_id bigint NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.account_customuser_groups OWNER TO alvinseyidov;

--
-- Name: account_customuser_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_customuser_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_customuser_groups_id_seq OWNER TO alvinseyidov;

--
-- Name: account_customuser_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_customuser_groups_id_seq OWNED BY public.account_customuser_groups.id;


--
-- Name: account_customuser_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_customuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_customuser_id_seq OWNER TO alvinseyidov;

--
-- Name: account_customuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_customuser_id_seq OWNED BY public.account_customuser.id;


--
-- Name: account_customuser_user_permissions; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_customuser_user_permissions (
    id bigint NOT NULL,
    customuser_id bigint NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.account_customuser_user_permissions OWNER TO alvinseyidov;

--
-- Name: account_customuser_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_customuser_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_customuser_user_permissions_id_seq OWNER TO alvinseyidov;

--
-- Name: account_customuser_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_customuser_user_permissions_id_seq OWNED BY public.account_customuser_user_permissions.id;


--
-- Name: account_licenseimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_licenseimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    is_main boolean NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.account_licenseimage OWNER TO alvinseyidov;

--
-- Name: account_licenseimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_licenseimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_licenseimage_id_seq OWNER TO alvinseyidov;

--
-- Name: account_licenseimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_licenseimage_id_seq OWNED BY public.account_licenseimage.id;


--
-- Name: account_passportimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_passportimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    is_main boolean NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.account_passportimage OWNER TO alvinseyidov;

--
-- Name: account_passportimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_passportimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_passportimage_id_seq OWNER TO alvinseyidov;

--
-- Name: account_passportimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_passportimage_id_seq OWNED BY public.account_passportimage.id;


--
-- Name: account_score; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_score (
    id bigint NOT NULL,
    customer_id bigint,
    note text,
    score integer NOT NULL,
    CONSTRAINT account_score_score_check CHECK ((score >= 0))
);


ALTER TABLE public.account_score OWNER TO alvinseyidov;

--
-- Name: account_score_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_score_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_score_id_seq OWNER TO alvinseyidov;

--
-- Name: account_score_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_score_id_seq OWNED BY public.account_score.id;


--
-- Name: account_userimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_userimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    is_main boolean NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.account_userimage OWNER TO alvinseyidov;

--
-- Name: account_userimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_userimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_userimage_id_seq OWNER TO alvinseyidov;

--
-- Name: account_userimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_userimage_id_seq OWNED BY public.account_userimage.id;


--
-- Name: account_userphone; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.account_userphone (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    number character varying(30) NOT NULL,
    note character varying(150),
    is_main boolean NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.account_userphone OWNER TO alvinseyidov;

--
-- Name: account_userphone_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.account_userphone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_userphone_id_seq OWNER TO alvinseyidov;

--
-- Name: account_userphone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.account_userphone_id_seq OWNED BY public.account_userphone.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO alvinseyidov;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO alvinseyidov;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO alvinseyidov;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO alvinseyidov;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO alvinseyidov;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO alvinseyidov;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO alvinseyidov;

--
-- Name: car_brand; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_brand (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    logo character varying(100)
);


ALTER TABLE public.car_brand OWNER TO alvinseyidov;

--
-- Name: car_brand_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_brand_id_seq OWNER TO alvinseyidov;

--
-- Name: car_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_brand_id_seq OWNED BY public.car_brand.id;


--
-- Name: car_car; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_car (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    number character varying(50) NOT NULL,
    year smallint NOT NULL,
    vin_code integer,
    is_available boolean NOT NULL,
    is_active boolean NOT NULL,
    transmission character varying(1) NOT NULL,
    fuel_tank smallint,
    doors smallint,
    seats smallint,
    wheel_size smallint,
    current_tyre character varying(1) NOT NULL,
    bought_date date,
    bought_price double precision,
    current_km double precision,
    oil_km_limit smallint,
    climate boolean NOT NULL,
    climate_control boolean NOT NULL,
    is_verified boolean NOT NULL,
    reserve_key boolean NOT NULL,
    advertise_id bigint,
    car_class_id bigint,
    case_id bigint,
    color_id bigint,
    fuel_id bigint,
    model_id bigint NOT NULL,
    office_id bigint NOT NULL,
    health_status character varying(1) NOT NULL,
    status character varying(3) NOT NULL,
    driver_id bigint,
    CONSTRAINT car_car_doors_check CHECK ((doors >= 0)),
    CONSTRAINT car_car_fuel_tank_check CHECK ((fuel_tank >= 0)),
    CONSTRAINT car_car_oil_km_limit_check CHECK ((oil_km_limit >= 0)),
    CONSTRAINT car_car_seats_check CHECK ((seats >= 0)),
    CONSTRAINT car_car_wheel_size_check CHECK ((wheel_size >= 0)),
    CONSTRAINT car_car_year_check CHECK ((year >= 0))
);


ALTER TABLE public.car_car OWNER TO alvinseyidov;

--
-- Name: car_car_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_car_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_car_id_seq OWNER TO alvinseyidov;

--
-- Name: car_car_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_car_id_seq OWNED BY public.car_car.id;


--
-- Name: car_carclass; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carclass (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.car_carclass OWNER TO alvinseyidov;

--
-- Name: car_carclass_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carclass_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carclass_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carclass_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carclass_id_seq OWNED BY public.car_carclass.id;


--
-- Name: car_carimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    is_main boolean NOT NULL,
    car_id bigint NOT NULL
);


ALTER TABLE public.car_carimage OWNER TO alvinseyidov;

--
-- Name: car_carimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carimage_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carimage_id_seq OWNED BY public.car_carimage.id;


--
-- Name: car_carimagetask; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carimagetask (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    car_id bigint,
    user_id bigint
);


ALTER TABLE public.car_carimagetask OWNER TO alvinseyidov;

--
-- Name: car_carimagetask_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carimagetask_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carimagetask_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carimagetask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carimagetask_id_seq OWNED BY public.car_carimagetask.id;


--
-- Name: car_carimagetask_images; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carimagetask_images (
    id bigint NOT NULL,
    carimagetask_id bigint NOT NULL,
    carimage_id bigint NOT NULL
);


ALTER TABLE public.car_carimagetask_images OWNER TO alvinseyidov;

--
-- Name: car_carimagetask_images_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carimagetask_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carimagetask_images_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carimagetask_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carimagetask_images_id_seq OWNED BY public.car_carimagetask_images.id;


--
-- Name: car_carpaper; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_carpaper (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    car_id bigint NOT NULL
);


ALTER TABLE public.car_carpaper OWNER TO alvinseyidov;

--
-- Name: car_carpaper_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_carpaper_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_carpaper_id_seq OWNER TO alvinseyidov;

--
-- Name: car_carpaper_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_carpaper_id_seq OWNED BY public.car_carpaper.id;


--
-- Name: car_case; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_case (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.car_case OWNER TO alvinseyidov;

--
-- Name: car_case_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_case_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_case_id_seq OWNER TO alvinseyidov;

--
-- Name: car_case_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_case_id_seq OWNED BY public.car_case.id;


--
-- Name: car_color; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_color (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.car_color OWNER TO alvinseyidov;

--
-- Name: car_color_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_color_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_color_id_seq OWNER TO alvinseyidov;

--
-- Name: car_color_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_color_id_seq OWNED BY public.car_color.id;


--
-- Name: car_fuel; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_fuel (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.car_fuel OWNER TO alvinseyidov;

--
-- Name: car_fuel_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_fuel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_fuel_id_seq OWNER TO alvinseyidov;

--
-- Name: car_fuel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_fuel_id_seq OWNED BY public.car_fuel.id;


--
-- Name: car_model; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_model (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    brand_id bigint
);


ALTER TABLE public.car_model OWNER TO alvinseyidov;

--
-- Name: car_model_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_model_id_seq OWNER TO alvinseyidov;

--
-- Name: car_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_model_id_seq OWNED BY public.car_model.id;


--
-- Name: car_office; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.car_office (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    address text NOT NULL
);


ALTER TABLE public.car_office OWNER TO alvinseyidov;

--
-- Name: car_office_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.car_office_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_office_id_seq OWNER TO alvinseyidov;

--
-- Name: car_office_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.car_office_id_seq OWNED BY public.car_office.id;


--
-- Name: core_administrativedeposit; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.core_administrativedeposit (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    amount double precision NOT NULL,
    currency character varying(3) NOT NULL,
    note character varying(255),
    date date NOT NULL
);


ALTER TABLE public.core_administrativedeposit OWNER TO alvinseyidov;

--
-- Name: core_administrativedeposit_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.core_administrativedeposit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_administrativedeposit_id_seq OWNER TO alvinseyidov;

--
-- Name: core_administrativedeposit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.core_administrativedeposit_id_seq OWNED BY public.core_administrativedeposit.id;


--
-- Name: core_administrativewithdraw; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.core_administrativewithdraw (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    amount double precision NOT NULL,
    currency character varying(3) NOT NULL,
    note character varying(255),
    date date NOT NULL
);


ALTER TABLE public.core_administrativewithdraw OWNER TO alvinseyidov;

--
-- Name: core_administrativewithdraw_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.core_administrativewithdraw_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_administrativewithdraw_id_seq OWNER TO alvinseyidov;

--
-- Name: core_administrativewithdraw_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.core_administrativewithdraw_id_seq OWNED BY public.core_administrativewithdraw.id;


--
-- Name: core_balance; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.core_balance (
    id bigint NOT NULL,
    amount double precision NOT NULL,
    currency character varying(3) NOT NULL,
    type character varying(4) NOT NULL
);


ALTER TABLE public.core_balance OWNER TO alvinseyidov;

--
-- Name: core_balance_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.core_balance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_balance_id_seq OWNER TO alvinseyidov;

--
-- Name: core_balance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.core_balance_id_seq OWNED BY public.core_balance.id;


--
-- Name: core_currency; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.core_currency (
    id bigint NOT NULL,
    ratio double precision NOT NULL,
    date date NOT NULL
);


ALTER TABLE public.core_currency OWNER TO alvinseyidov;

--
-- Name: core_currency_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.core_currency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_currency_id_seq OWNER TO alvinseyidov;

--
-- Name: core_currency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.core_currency_id_seq OWNED BY public.core_currency.id;


--
-- Name: credit_credit; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.credit_credit (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    payment_count smallint NOT NULL,
    payment_year double precision NOT NULL,
    payment_date smallint NOT NULL,
    car_value_total double precision NOT NULL,
    first_payment double precision NOT NULL,
    type character varying(1) NOT NULL,
    balance double precision NOT NULL,
    percent_total integer NOT NULL,
    percent_expenses integer NOT NULL,
    is_completed boolean NOT NULL,
    is_permitted boolean NOT NULL,
    is_verified boolean NOT NULL,
    is_active boolean NOT NULL,
    payment double precision NOT NULL,
    notes text,
    contract_id bigint,
    expenses double precision NOT NULL,
    total_credit double precision NOT NULL,
    CONSTRAINT credit_credit_payment_count_check CHECK ((payment_count >= 0)),
    CONSTRAINT credit_credit_payment_date_check CHECK ((payment_date >= 0))
);


ALTER TABLE public.credit_credit OWNER TO alvinseyidov;

--
-- Name: credit_credit_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.credit_credit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credit_credit_id_seq OWNER TO alvinseyidov;

--
-- Name: credit_credit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.credit_credit_id_seq OWNED BY public.credit_credit.id;


--
-- Name: credit_creditimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.credit_creditimage (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    path character varying(100) NOT NULL,
    credit_id bigint
);


ALTER TABLE public.credit_creditimage OWNER TO alvinseyidov;

--
-- Name: credit_creditimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.credit_creditimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credit_creditimage_id_seq OWNER TO alvinseyidov;

--
-- Name: credit_creditimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.credit_creditimage_id_seq OWNED BY public.credit_creditimage.id;


--
-- Name: credit_creditpayment; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.credit_creditpayment (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    debt_week double precision NOT NULL,
    debt_total double precision NOT NULL,
    payment_left double precision NOT NULL,
    amount double precision NOT NULL,
    amount_uah double precision NOT NULL,
    total_amount double precision NOT NULL,
    is_verified boolean NOT NULL,
    credit_id bigint
);


ALTER TABLE public.credit_creditpayment OWNER TO alvinseyidov;

--
-- Name: credit_creditpayment_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.credit_creditpayment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.credit_creditpayment_id_seq OWNER TO alvinseyidov;

--
-- Name: credit_creditpayment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.credit_creditpayment_id_seq OWNED BY public.credit_creditpayment.id;


--
-- Name: debt_debt; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.debt_debt (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_date date NOT NULL,
    paid_date date,
    amount double precision NOT NULL,
    status character varying(1) NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint,
    customer_id bigint NOT NULL,
    note text
);


ALTER TABLE public.debt_debt OWNER TO alvinseyidov;

--
-- Name: debt_debt_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.debt_debt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.debt_debt_id_seq OWNER TO alvinseyidov;

--
-- Name: debt_debt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.debt_debt_id_seq OWNED BY public.debt_debt.id;


--
-- Name: debt_debtimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.debt_debtimage (
    id bigint NOT NULL,
    file character varying(100) NOT NULL,
    debt_id bigint NOT NULL
);


ALTER TABLE public.debt_debtimage OWNER TO alvinseyidov;

--
-- Name: debt_debtimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.debt_debtimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.debt_debtimage_id_seq OWNER TO alvinseyidov;

--
-- Name: debt_debtimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.debt_debtimage_id_seq OWNED BY public.debt_debtimage.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id bigint NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO alvinseyidov;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO alvinseyidov;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO alvinseyidov;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO alvinseyidov;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO alvinseyidov;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO alvinseyidov;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO alvinseyidov;

--
-- Name: dtp_dtp; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.dtp_dtp (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    is_driver_fault boolean NOT NULL,
    insurance character varying(1) NOT NULL,
    expert_observation boolean NOT NULL,
    expert_inspection boolean NOT NULL,
    insurance_back_status boolean NOT NULL,
    insurance_back_amount double precision,
    insurance_back_date date,
    insurance_back_staff character varying(100),
    note text,
    is_verified boolean NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint NOT NULL,
    insurance_company_id bigint,
    driver_id bigint
);


ALTER TABLE public.dtp_dtp OWNER TO alvinseyidov;

--
-- Name: dtp_dtp_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.dtp_dtp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dtp_dtp_id_seq OWNER TO alvinseyidov;

--
-- Name: dtp_dtp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.dtp_dtp_id_seq OWNED BY public.dtp_dtp.id;


--
-- Name: insurance_casco; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.insurance_casco (
    id bigint NOT NULL,
    amount_year double precision NOT NULL,
    compensation double precision NOT NULL,
    step smallint NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    is_verified boolean NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint NOT NULL,
    company_id bigint NOT NULL,
    CONSTRAINT insurance_casco_step_check CHECK ((step >= 0))
);


ALTER TABLE public.insurance_casco OWNER TO alvinseyidov;

--
-- Name: insurance_casco_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.insurance_casco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insurance_casco_id_seq OWNER TO alvinseyidov;

--
-- Name: insurance_casco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.insurance_casco_id_seq OWNED BY public.insurance_casco.id;


--
-- Name: insurance_cascoimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.insurance_cascoimage (
    id bigint NOT NULL,
    file character varying(100) NOT NULL,
    casco_id bigint NOT NULL
);


ALTER TABLE public.insurance_cascoimage OWNER TO alvinseyidov;

--
-- Name: insurance_cascoimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.insurance_cascoimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insurance_cascoimage_id_seq OWNER TO alvinseyidov;

--
-- Name: insurance_cascoimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.insurance_cascoimage_id_seq OWNED BY public.insurance_cascoimage.id;


--
-- Name: insurance_cascopayment; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.insurance_cascopayment (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    amount double precision NOT NULL,
    is_verified boolean NOT NULL,
    casco_id bigint NOT NULL,
    step integer NOT NULL,
    CONSTRAINT insurance_cascopayment_step_check CHECK ((step >= 0))
);


ALTER TABLE public.insurance_cascopayment OWNER TO alvinseyidov;

--
-- Name: insurance_cascopayment_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.insurance_cascopayment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insurance_cascopayment_id_seq OWNER TO alvinseyidov;

--
-- Name: insurance_cascopayment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.insurance_cascopayment_id_seq OWNED BY public.insurance_cascopayment.id;


--
-- Name: insurance_company; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.insurance_company (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.insurance_company OWNER TO alvinseyidov;

--
-- Name: insurance_company_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.insurance_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insurance_company_id_seq OWNER TO alvinseyidov;

--
-- Name: insurance_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.insurance_company_id_seq OWNED BY public.insurance_company.id;


--
-- Name: insurance_osago; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.insurance_osago (
    id bigint NOT NULL,
    amount_year double precision NOT NULL,
    compensation double precision NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    is_verified boolean NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint NOT NULL,
    company_id bigint NOT NULL
);


ALTER TABLE public.insurance_osago OWNER TO alvinseyidov;

--
-- Name: insurance_osago_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.insurance_osago_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insurance_osago_id_seq OWNER TO alvinseyidov;

--
-- Name: insurance_osago_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.insurance_osago_id_seq OWNED BY public.insurance_osago.id;


--
-- Name: insurance_osagoimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.insurance_osagoimage (
    id bigint NOT NULL,
    file character varying(100) NOT NULL,
    osago_id bigint NOT NULL
);


ALTER TABLE public.insurance_osagoimage OWNER TO alvinseyidov;

--
-- Name: insurance_osagoimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.insurance_osagoimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insurance_osagoimage_id_seq OWNER TO alvinseyidov;

--
-- Name: insurance_osagoimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.insurance_osagoimage_id_seq OWNED BY public.insurance_osagoimage.id;


--
-- Name: insurance_osagopayment; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.insurance_osagopayment (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    amount double precision NOT NULL,
    is_verified boolean NOT NULL,
    osago_id bigint NOT NULL
);


ALTER TABLE public.insurance_osagopayment OWNER TO alvinseyidov;

--
-- Name: insurance_osagopayment_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.insurance_osagopayment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insurance_osagopayment_id_seq OWNER TO alvinseyidov;

--
-- Name: insurance_osagopayment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.insurance_osagopayment_id_seq OWNED BY public.insurance_osagopayment.id;


--
-- Name: rent_rent; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_rent (
    id bigint NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint
);


ALTER TABLE public.rent_rent OWNER TO alvinseyidov;

--
-- Name: rent_rent_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_rent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_rent_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_rent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_rent_id_seq OWNED BY public.rent_rent.id;


--
-- Name: rent_revenue; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_revenue (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    start_date date NOT NULL,
    rented_days integer NOT NULL,
    rent_amount double precision NOT NULL,
    is_paid boolean NOT NULL,
    contract_id bigint NOT NULL,
    driver_id bigint NOT NULL,
    rent_id bigint NOT NULL,
    deposit_amount double precision NOT NULL,
    is_deposit_in boolean NOT NULL,
    is_deposit_out boolean NOT NULL,
    end_date date NOT NULL,
    CONSTRAINT rent_revenue_rented_days_check CHECK ((rented_days >= 0))
);


ALTER TABLE public.rent_revenue OWNER TO alvinseyidov;

--
-- Name: rent_revenue_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_revenue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_revenue_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_revenue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_revenue_id_seq OWNED BY public.rent_revenue.id;


--
-- Name: rent_settings; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_settings (
    id bigint NOT NULL,
    rent_per_day double precision NOT NULL,
    deposit_amount double precision NOT NULL
);


ALTER TABLE public.rent_settings OWNER TO alvinseyidov;

--
-- Name: rent_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_settings_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_settings_id_seq OWNED BY public.rent_settings.id;


--
-- Name: rent_taxi_daily_renttaxidaily; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_daily_renttaxidaily (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint
);


ALTER TABLE public.rent_taxi_daily_renttaxidaily OWNER TO alvinseyidov;

--
-- Name: rent_taxi_daily_renttaxidaily_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_daily_renttaxidaily_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_daily_renttaxidaily_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_daily_renttaxidaily_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_daily_renttaxidaily_id_seq OWNED BY public.rent_taxi_daily_renttaxidaily.id;


--
-- Name: rent_taxi_daily_revenue; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_daily_revenue (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    contract_id bigint NOT NULL,
    driver_id bigint NOT NULL,
    rent_taxi_daily_id bigint NOT NULL
);


ALTER TABLE public.rent_taxi_daily_revenue OWNER TO alvinseyidov;

--
-- Name: rent_taxi_daily_revenue_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_daily_revenue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_daily_revenue_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_daily_revenue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_daily_revenue_id_seq OWNED BY public.rent_taxi_daily_revenue.id;


--
-- Name: rent_taxi_daily_revenueitem; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_daily_revenueitem (
    id bigint NOT NULL,
    order_count smallint NOT NULL,
    company_will_pay_brand double precision NOT NULL,
    amount_cash double precision NOT NULL,
    amount_card double precision NOT NULL,
    company_commission double precision NOT NULL,
    rent_amount double precision NOT NULL,
    commission_from_driver double precision NOT NULL,
    debt_to_bolt double precision NOT NULL,
    company_will_pay_total double precision NOT NULL,
    driver_must_pay double precision NOT NULL,
    we_must_pay double precision NOT NULL,
    driver_paid double precision NOT NULL,
    we_paid double precision NOT NULL,
    debt_today double precision NOT NULL,
    debt_all double precision NOT NULL,
    company_id bigint NOT NULL,
    rent_taxi_daily_id bigint,
    revenue_id bigint NOT NULL,
    CONSTRAINT rent_taxi_daily_revenueitem_order_count_check CHECK ((order_count >= 0))
);


ALTER TABLE public.rent_taxi_daily_revenueitem OWNER TO alvinseyidov;

--
-- Name: rent_taxi_daily_revenueitem_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_daily_revenueitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_daily_revenueitem_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_daily_revenueitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_daily_revenueitem_id_seq OWNED BY public.rent_taxi_daily_revenueitem.id;


--
-- Name: rent_taxi_daily_settings; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_daily_settings (
    id bigint NOT NULL,
    commission_from_driver double precision NOT NULL,
    daily_rent double precision NOT NULL
);


ALTER TABLE public.rent_taxi_daily_settings OWNER TO alvinseyidov;

--
-- Name: rent_taxi_daily_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_daily_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_daily_settings_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_daily_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_daily_settings_id_seq OWNED BY public.rent_taxi_daily_settings.id;


--
-- Name: rent_taxi_renttaxi; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_renttaxi (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    car_id bigint,
    is_active boolean NOT NULL
);


ALTER TABLE public.rent_taxi_renttaxi OWNER TO alvinseyidov;

--
-- Name: rent_taxi_renttaxi_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_renttaxi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_renttaxi_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_renttaxi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_renttaxi_id_seq OWNED BY public.rent_taxi_renttaxi.id;


--
-- Name: rent_taxi_revenue; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_revenue (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    contract_id bigint NOT NULL,
    driver_id bigint NOT NULL,
    rent_taxi_id bigint NOT NULL
);


ALTER TABLE public.rent_taxi_revenue OWNER TO alvinseyidov;

--
-- Name: rent_taxi_revenue_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_revenue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_revenue_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_revenue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_revenue_id_seq OWNED BY public.rent_taxi_revenue.id;


--
-- Name: rent_taxi_revenueitem; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_revenueitem (
    id bigint NOT NULL,
    amount_cash double precision NOT NULL,
    amount_card double precision NOT NULL,
    order_count smallint NOT NULL,
    driver_paid double precision NOT NULL,
    company_id bigint NOT NULL,
    revenue_id bigint NOT NULL,
    company_will_pay_brand double precision NOT NULL,
    rent_amount double precision NOT NULL,
    company_will_pay_total double precision NOT NULL,
    commission_from_driver double precision NOT NULL,
    debt_all double precision NOT NULL,
    debt_to_bolt double precision NOT NULL,
    debt_today double precision NOT NULL,
    driver_must_pay double precision NOT NULL,
    rent_taxi_id bigint,
    we_must_pay double precision NOT NULL,
    company_commission double precision NOT NULL,
    we_paid double precision NOT NULL,
    CONSTRAINT rent_taxi_revenueitem_order_count_check CHECK ((order_count >= 0))
);


ALTER TABLE public.rent_taxi_revenueitem OWNER TO alvinseyidov;

--
-- Name: rent_taxi_revenueitem_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_revenueitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_revenueitem_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_revenueitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_revenueitem_id_seq OWNED BY public.rent_taxi_revenueitem.id;


--
-- Name: rent_taxi_settings; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_settings (
    id bigint NOT NULL,
    commission_from_driver double precision NOT NULL,
    weekly_rent double precision NOT NULL
);


ALTER TABLE public.rent_taxi_settings OWNER TO alvinseyidov;

--
-- Name: rent_taxi_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_settings_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_settings_id_seq OWNED BY public.rent_taxi_settings.id;


--
-- Name: rent_taxi_tariff; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.rent_taxi_tariff (
    id bigint NOT NULL,
    limit1 integer NOT NULL,
    limit2 integer NOT NULL,
    limit3 integer NOT NULL,
    limit1_tariff double precision NOT NULL,
    limit2_tariff double precision NOT NULL,
    limit3_tariff double precision NOT NULL,
    company_id bigint NOT NULL,
    CONSTRAINT rent_taxi_tariff_limit1_check CHECK ((limit1 >= 0)),
    CONSTRAINT rent_taxi_tariff_limit2_check CHECK ((limit2 >= 0)),
    CONSTRAINT rent_taxi_tariff_limit3_check CHECK ((limit3 >= 0))
);


ALTER TABLE public.rent_taxi_tariff OWNER TO alvinseyidov;

--
-- Name: rent_taxi_tariff_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.rent_taxi_tariff_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_taxi_tariff_id_seq OWNER TO alvinseyidov;

--
-- Name: rent_taxi_tariff_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.rent_taxi_tariff_id_seq OWNED BY public.rent_taxi_tariff.id;


--
-- Name: shtraf_shtraf; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.shtraf_shtraf (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    no character varying(255),
    datetime timestamp with time zone NOT NULL,
    created_date date NOT NULL,
    paid_date date,
    amount double precision NOT NULL,
    note text,
    status character varying(1) NOT NULL,
    debt_status character varying(1) NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint NOT NULL,
    driver_id bigint NOT NULL
);


ALTER TABLE public.shtraf_shtraf OWNER TO alvinseyidov;

--
-- Name: shtraf_shtraf_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.shtraf_shtraf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shtraf_shtraf_id_seq OWNER TO alvinseyidov;

--
-- Name: shtraf_shtraf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.shtraf_shtraf_id_seq OWNED BY public.shtraf_shtraf.id;


--
-- Name: shtraf_shtrafimage; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.shtraf_shtrafimage (
    id bigint NOT NULL,
    file character varying(100) NOT NULL,
    shtraf_id bigint NOT NULL
);


ALTER TABLE public.shtraf_shtrafimage OWNER TO alvinseyidov;

--
-- Name: shtraf_shtrafimage_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.shtraf_shtrafimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shtraf_shtrafimage_id_seq OWNER TO alvinseyidov;

--
-- Name: shtraf_shtrafimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.shtraf_shtrafimage_id_seq OWNED BY public.shtraf_shtrafimage.id;


--
-- Name: taxi_company; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.taxi_company (
    id bigint NOT NULL,
    name character varying(100) NOT NULL,
    commission double precision NOT NULL
);


ALTER TABLE public.taxi_company OWNER TO alvinseyidov;

--
-- Name: taxi_company_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.taxi_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxi_company_id_seq OWNER TO alvinseyidov;

--
-- Name: taxi_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.taxi_company_id_seq OWNED BY public.taxi_company.id;


--
-- Name: taxi_revenue; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.taxi_revenue (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date date NOT NULL,
    contract_id bigint NOT NULL,
    customer_id bigint NOT NULL,
    taxi_id bigint NOT NULL
);


ALTER TABLE public.taxi_revenue OWNER TO alvinseyidov;

--
-- Name: taxi_revenue_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.taxi_revenue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxi_revenue_id_seq OWNER TO alvinseyidov;

--
-- Name: taxi_revenue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.taxi_revenue_id_seq OWNED BY public.taxi_revenue.id;


--
-- Name: taxi_revenueitem; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.taxi_revenueitem (
    id bigint NOT NULL,
    amount_cash double precision NOT NULL,
    amount_card double precision NOT NULL,
    order_count smallint NOT NULL,
    driver_paid double precision NOT NULL,
    company_id bigint NOT NULL,
    revenue_id bigint NOT NULL,
    debt_all double precision NOT NULL,
    debt_today double precision NOT NULL,
    taxi_id bigint,
    driver_must_pay double precision NOT NULL,
    we_must_pay double precision NOT NULL,
    we_paid double precision NOT NULL,
    CONSTRAINT taxi_revenueitem_order_count_check CHECK ((order_count >= 0))
);


ALTER TABLE public.taxi_revenueitem OWNER TO alvinseyidov;

--
-- Name: taxi_revenueitem_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.taxi_revenueitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxi_revenueitem_id_seq OWNER TO alvinseyidov;

--
-- Name: taxi_revenueitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.taxi_revenueitem_id_seq OWNED BY public.taxi_revenueitem.id;


--
-- Name: taxi_tariff; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.taxi_tariff (
    id bigint NOT NULL,
    limit1 double precision NOT NULL,
    limit2 double precision NOT NULL,
    limit3 double precision NOT NULL,
    limit1_driver_percent integer NOT NULL,
    limit2_driver_percent integer NOT NULL,
    limit3_driver_percent integer NOT NULL,
    company_id bigint NOT NULL,
    CONSTRAINT taxi_tariff_limit1_driver_percent_check CHECK ((limit1_driver_percent >= 0)),
    CONSTRAINT taxi_tariff_limit2_driver_percent_check CHECK ((limit2_driver_percent >= 0)),
    CONSTRAINT taxi_tariff_limit3_driver_percent_check CHECK ((limit3_driver_percent >= 0))
);


ALTER TABLE public.taxi_tariff OWNER TO alvinseyidov;

--
-- Name: taxi_tariff_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.taxi_tariff_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxi_tariff_id_seq OWNER TO alvinseyidov;

--
-- Name: taxi_tariff_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.taxi_tariff_id_seq OWNED BY public.taxi_tariff.id;


--
-- Name: taxi_taxi; Type: TABLE; Schema: public; Owner: alvinseyidov
--

CREATE TABLE public.taxi_taxi (
    id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    is_active boolean NOT NULL,
    car_id bigint
);


ALTER TABLE public.taxi_taxi OWNER TO alvinseyidov;

--
-- Name: taxi_taxi_id_seq; Type: SEQUENCE; Schema: public; Owner: alvinseyidov
--

CREATE SEQUENCE public.taxi_taxi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxi_taxi_id_seq OWNER TO alvinseyidov;

--
-- Name: taxi_taxi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alvinseyidov
--

ALTER SEQUENCE public.taxi_taxi_id_seq OWNED BY public.taxi_taxi.id;


--
-- Name: account_contract id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_contract ALTER COLUMN id SET DEFAULT nextval('public.account_contract_id_seq'::regclass);


--
-- Name: account_customuser id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser ALTER COLUMN id SET DEFAULT nextval('public.account_customuser_id_seq'::regclass);


--
-- Name: account_customuser_groups id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups ALTER COLUMN id SET DEFAULT nextval('public.account_customuser_groups_id_seq'::regclass);


--
-- Name: account_customuser_user_permissions id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.account_customuser_user_permissions_id_seq'::regclass);


--
-- Name: account_licenseimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_licenseimage ALTER COLUMN id SET DEFAULT nextval('public.account_licenseimage_id_seq'::regclass);


--
-- Name: account_passportimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_passportimage ALTER COLUMN id SET DEFAULT nextval('public.account_passportimage_id_seq'::regclass);


--
-- Name: account_score id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_score ALTER COLUMN id SET DEFAULT nextval('public.account_score_id_seq'::regclass);


--
-- Name: account_userimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userimage ALTER COLUMN id SET DEFAULT nextval('public.account_userimage_id_seq'::regclass);


--
-- Name: account_userphone id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userphone ALTER COLUMN id SET DEFAULT nextval('public.account_userphone_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: car_brand id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_brand ALTER COLUMN id SET DEFAULT nextval('public.car_brand_id_seq'::regclass);


--
-- Name: car_car id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car ALTER COLUMN id SET DEFAULT nextval('public.car_car_id_seq'::regclass);


--
-- Name: car_carclass id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carclass ALTER COLUMN id SET DEFAULT nextval('public.car_carclass_id_seq'::regclass);


--
-- Name: car_carimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimage ALTER COLUMN id SET DEFAULT nextval('public.car_carimage_id_seq'::regclass);


--
-- Name: car_carimagetask id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask ALTER COLUMN id SET DEFAULT nextval('public.car_carimagetask_id_seq'::regclass);


--
-- Name: car_carimagetask_images id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images ALTER COLUMN id SET DEFAULT nextval('public.car_carimagetask_images_id_seq'::regclass);


--
-- Name: car_carpaper id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carpaper ALTER COLUMN id SET DEFAULT nextval('public.car_carpaper_id_seq'::regclass);


--
-- Name: car_case id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_case ALTER COLUMN id SET DEFAULT nextval('public.car_case_id_seq'::regclass);


--
-- Name: car_color id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_color ALTER COLUMN id SET DEFAULT nextval('public.car_color_id_seq'::regclass);


--
-- Name: car_fuel id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_fuel ALTER COLUMN id SET DEFAULT nextval('public.car_fuel_id_seq'::regclass);


--
-- Name: car_model id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_model ALTER COLUMN id SET DEFAULT nextval('public.car_model_id_seq'::regclass);


--
-- Name: car_office id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_office ALTER COLUMN id SET DEFAULT nextval('public.car_office_id_seq'::regclass);


--
-- Name: core_administrativedeposit id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_administrativedeposit ALTER COLUMN id SET DEFAULT nextval('public.core_administrativedeposit_id_seq'::regclass);


--
-- Name: core_administrativewithdraw id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_administrativewithdraw ALTER COLUMN id SET DEFAULT nextval('public.core_administrativewithdraw_id_seq'::regclass);


--
-- Name: core_balance id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_balance ALTER COLUMN id SET DEFAULT nextval('public.core_balance_id_seq'::regclass);


--
-- Name: core_currency id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_currency ALTER COLUMN id SET DEFAULT nextval('public.core_currency_id_seq'::regclass);


--
-- Name: credit_credit id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_credit ALTER COLUMN id SET DEFAULT nextval('public.credit_credit_id_seq'::regclass);


--
-- Name: credit_creditimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditimage ALTER COLUMN id SET DEFAULT nextval('public.credit_creditimage_id_seq'::regclass);


--
-- Name: credit_creditpayment id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditpayment ALTER COLUMN id SET DEFAULT nextval('public.credit_creditpayment_id_seq'::regclass);


--
-- Name: debt_debt id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debt ALTER COLUMN id SET DEFAULT nextval('public.debt_debt_id_seq'::regclass);


--
-- Name: debt_debtimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debtimage ALTER COLUMN id SET DEFAULT nextval('public.debt_debtimage_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: dtp_dtp id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.dtp_dtp ALTER COLUMN id SET DEFAULT nextval('public.dtp_dtp_id_seq'::regclass);


--
-- Name: insurance_casco id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_casco ALTER COLUMN id SET DEFAULT nextval('public.insurance_casco_id_seq'::regclass);


--
-- Name: insurance_cascoimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_cascoimage ALTER COLUMN id SET DEFAULT nextval('public.insurance_cascoimage_id_seq'::regclass);


--
-- Name: insurance_cascopayment id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_cascopayment ALTER COLUMN id SET DEFAULT nextval('public.insurance_cascopayment_id_seq'::regclass);


--
-- Name: insurance_company id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_company ALTER COLUMN id SET DEFAULT nextval('public.insurance_company_id_seq'::regclass);


--
-- Name: insurance_osago id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osago ALTER COLUMN id SET DEFAULT nextval('public.insurance_osago_id_seq'::regclass);


--
-- Name: insurance_osagoimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osagoimage ALTER COLUMN id SET DEFAULT nextval('public.insurance_osagoimage_id_seq'::regclass);


--
-- Name: insurance_osagopayment id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osagopayment ALTER COLUMN id SET DEFAULT nextval('public.insurance_osagopayment_id_seq'::regclass);


--
-- Name: rent_rent id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_rent ALTER COLUMN id SET DEFAULT nextval('public.rent_rent_id_seq'::regclass);


--
-- Name: rent_revenue id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_revenue ALTER COLUMN id SET DEFAULT nextval('public.rent_revenue_id_seq'::regclass);


--
-- Name: rent_settings id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_settings ALTER COLUMN id SET DEFAULT nextval('public.rent_settings_id_seq'::regclass);


--
-- Name: rent_taxi_daily_renttaxidaily id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_renttaxidaily ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_daily_renttaxidaily_id_seq'::regclass);


--
-- Name: rent_taxi_daily_revenue id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenue ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_daily_revenue_id_seq'::regclass);


--
-- Name: rent_taxi_daily_revenueitem id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenueitem ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_daily_revenueitem_id_seq'::regclass);


--
-- Name: rent_taxi_daily_settings id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_settings ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_daily_settings_id_seq'::regclass);


--
-- Name: rent_taxi_renttaxi id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_renttaxi ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_renttaxi_id_seq'::regclass);


--
-- Name: rent_taxi_revenue id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_revenue_id_seq'::regclass);


--
-- Name: rent_taxi_revenueitem id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_revenueitem_id_seq'::regclass);


--
-- Name: rent_taxi_settings id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_settings ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_settings_id_seq'::regclass);


--
-- Name: rent_taxi_tariff id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_tariff ALTER COLUMN id SET DEFAULT nextval('public.rent_taxi_tariff_id_seq'::regclass);


--
-- Name: shtraf_shtraf id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtraf ALTER COLUMN id SET DEFAULT nextval('public.shtraf_shtraf_id_seq'::regclass);


--
-- Name: shtraf_shtrafimage id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtrafimage ALTER COLUMN id SET DEFAULT nextval('public.shtraf_shtrafimage_id_seq'::regclass);


--
-- Name: taxi_company id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_company ALTER COLUMN id SET DEFAULT nextval('public.taxi_company_id_seq'::regclass);


--
-- Name: taxi_revenue id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue ALTER COLUMN id SET DEFAULT nextval('public.taxi_revenue_id_seq'::regclass);


--
-- Name: taxi_revenueitem id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem ALTER COLUMN id SET DEFAULT nextval('public.taxi_revenueitem_id_seq'::regclass);


--
-- Name: taxi_tariff id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_tariff ALTER COLUMN id SET DEFAULT nextval('public.taxi_tariff_id_seq'::regclass);


--
-- Name: taxi_taxi id; Type: DEFAULT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_taxi ALTER COLUMN id SET DEFAULT nextval('public.taxi_taxi_id_seq'::regclass);


--
-- Data for Name: account_contract; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_contract (id, created_at, updated_at, type, signature, start_date, end_date, is_active, car_id, customer_id, deposit_amount) FROM stdin;
25	2022-03-03 17:16:05.030885+00	2022-03-19 13:39:43.759491+00	F		2021-11-23	2022-11-23	t	54	15	0
26	2022-03-03 17:19:55.500845+00	2022-03-19 13:39:46.967716+00	F		2021-12-03	2023-12-07	t	91	16	0
27	2022-03-03 17:20:34.9768+00	2022-03-19 13:39:51.077955+00	F		2022-01-01	2023-01-01	t	80	17	0
29	2022-03-03 17:23:53.504474+00	2022-03-19 13:39:54.273749+00	F		2021-03-10	2022-09-06	t	3	19	0
28	2022-03-03 17:21:43.413707+00	2022-03-03 17:21:43.413729+00	CR		2021-02-15	2022-01-10	t	58	18	0
30	2022-03-03 17:24:44.616141+00	2022-03-19 13:39:57.695295+00	F		2020-12-29	2022-12-29	t	45	20	0
31	2022-03-03 17:25:42.259243+00	2022-03-19 13:40:01.308853+00	F		2022-01-15	2023-01-15	t	55	21	0
32	2022-03-03 17:26:20.433388+00	2022-03-19 13:40:04.387929+00	F		2020-06-30	2022-06-30	t	56	22	0
33	2022-03-03 17:27:04.330652+00	2022-03-19 13:40:07.755067+00	F		2020-12-29	2022-12-29	t	59	23	0
34	2022-03-03 17:28:12.030155+00	2022-03-19 13:40:11.279924+00	F		2021-11-09	2023-11-09	t	95	24	0
35	2022-03-03 17:28:45.446464+00	2022-03-19 13:40:14.757537+00	F		2021-06-02	2022-06-02	t	61	25	0
57	2022-03-06 12:37:17.875357+00	2022-03-21 10:16:29.57459+00	RTW		2022-03-01	2022-03-20	t	78	1	0
58	2022-03-19 22:41:44.045351+00	2022-03-21 18:54:15.872788+00	RTD		2022-03-19	2022-03-19	t	132	1	1500
13	2022-03-03 16:57:58.280435+00	2022-03-19 13:38:51.044663+00	F		2021-09-21	2022-09-21	t	78	3	0
14	2022-03-03 16:59:03.182095+00	2022-03-19 13:39:00.222071+00	F		2021-05-10	2023-05-10	t	83	4	0
15	2022-03-03 17:00:06.904862+00	2022-03-19 13:39:03.758913+00	F		2020-10-26	2022-10-26	t	53	5	0
16	2022-03-03 17:01:24.505755+00	2022-03-19 13:39:07.087275+00	F		2020-07-17	2022-07-19	t	85	6	0
17	2022-03-03 17:03:12.216391+00	2022-03-19 13:39:10.759993+00	F		2020-09-04	2022-09-04	t	126	7	0
18	2022-03-03 17:04:06.761409+00	2022-03-19 13:39:14.087763+00	F		2021-01-03	2023-01-03	t	88	8	0
19	2022-03-03 17:07:48.523149+00	2022-03-19 13:39:17.591833+00	F		2021-04-19	2022-04-17	t	5	9	0
20	2022-03-03 17:08:35.355072+00	2022-03-19 13:39:21.668152+00	F		2021-08-03	2022-08-03	t	13	10	0
21	2022-03-03 17:09:22.431095+00	2022-03-19 13:39:24.872005+00	F		2021-12-07	2023-12-07	t	14	11	0
22	2022-03-03 17:10:17.848362+00	2022-03-19 13:39:28.922468+00	F		2021-07-26	2023-07-27	t	23	12	0
23	2022-03-03 17:11:16.85102+00	2022-03-19 13:39:32.670365+00	F		2021-09-13	2022-09-13	t	43	13	0
24	2022-03-03 17:12:02.250645+00	2022-03-19 13:39:36.205552+00	F		2021-10-25	2023-10-25	t	65	14	0
36	2022-03-03 17:29:25.609285+00	2022-03-19 13:40:18.006696+00	F		2021-09-01	2023-09-01	t	81	26	0
37	2022-03-03 17:30:16.108586+00	2022-03-19 13:40:21.283377+00	F		2022-01-13	2024-01-13	t	64	26	0
38	2022-03-03 17:31:44.376693+00	2022-03-19 13:40:24.340356+00	F		2020-09-16	2022-03-16	t	52	27	0
39	2022-03-03 17:32:16.591558+00	2022-03-19 13:40:27.441888+00	F		2021-09-09	2022-09-09	t	87	28	0
40	2022-03-03 17:33:24.37982+00	2022-03-19 13:40:31.08843+00	F		2021-09-02	2022-09-02	t	6	29	0
41	2022-03-03 17:34:07.08881+00	2022-03-19 13:40:34.717607+00	F		2021-10-28	2022-10-28	t	17	30	0
42	2022-03-03 17:34:49.837542+00	2022-03-19 13:40:38.111475+00	F		2020-10-23	2022-10-23	t	60	31	0
43	2022-03-03 17:35:30.72855+00	2022-03-19 13:40:41.383536+00	F		2021-11-11	2022-11-11	t	67	32	0
44	2022-03-03 17:36:16.189977+00	2022-03-19 13:40:45.105129+00	F		2021-12-10	2022-12-10	t	82	33	0
45	2022-03-03 17:37:09.620298+00	2022-03-19 13:40:48.768448+00	F		2021-09-24	2022-09-24	t	84	34	0
46	2022-03-03 17:38:05.224916+00	2022-03-19 13:40:52.2947+00	F		2021-04-15	2022-10-13	t	86	35	0
47	2022-03-03 17:39:14.163555+00	2022-03-19 13:40:56.183432+00	F		2021-05-20	2023-05-20	t	131	36	0
48	2022-03-03 17:40:14.437916+00	2022-03-19 13:41:00.20602+00	F		2020-10-08	2022-10-08	t	93	37	0
49	2022-03-03 17:41:53.583605+00	2022-03-19 13:41:03.984646+00	F		2020-05-29	2022-05-29	t	130	38	0
50	2022-03-03 17:43:00.112132+00	2022-03-19 13:41:07.247076+00	F		2021-10-02	2023-10-02	t	90	39	0
51	2022-03-03 17:44:02.01858+00	2022-03-19 13:41:11.136371+00	F		2021-01-29	2023-01-29	t	76	40	0
52	2022-03-03 17:44:43.158026+00	2022-03-19 13:41:14.571264+00	F		2021-12-17	2022-12-17	t	68	41	0
53	2022-03-03 17:45:48.867113+00	2022-03-19 13:41:18.209809+00	F		2021-12-16	2023-12-16	t	77	42	0
54	2022-03-03 17:46:41.421173+00	2022-03-19 13:41:21.86499+00	F		2022-02-17	2024-02-17	t	18	17	0
\.


--
-- Data for Name: account_customuser; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_customuser (id, password, last_login, is_superuser, first_name, last_name, email, is_staff, is_active, date_joined, username, father_name, birth_date, passport, license_number, type, is_black_listed, note, rating) FROM stdin;
1	pbkdf2_sha256$260000$hDChMxXLeX4DjlrpXqFmHZ$izEYcGe43bv0gMB9+1hxBB4uqNV8ELt59QKkhNhqur4=	2022-04-04 19:54:36.399433+00	t	Elvin	Seyidov		t	t	2022-02-23 13:24:25+00	alvinseyidov	\N	\N	\N	\N	R	f	\N	10
11	pbkdf2_sha256$260000$zcc5XNbO44w23sDSONnJHB$WqSxcPsQuIhMbwUQ5SErMjSo3YvR4qzgOmHnItyvEEw=	\N	f	Леонид	Романчук		f	t	2022-03-03 09:39:40+00	user9	\N	\N	\N	\N	C	f	\N	10
3	pbkdf2_sha256$260000$Z3y7F1MQMFAouFnTgsLEcJ$zvaMaf8BG/ouK5UzDUKvWtuGhuqWUFaauG9jbBfmeCc=	\N	f	Каюн	Дмитрий		f	t	2022-03-03 09:22:41+00	user1	\N	\N	\N	\N	C	f	\N	10
13	pbkdf2_sha256$260000$fAbdVu4E1mHHpoB8dfzGp1$GRi3GZ5lw61SJRMPDstFDYu/78sdBLpSHD8Kt/cZ7vQ=	\N	f	Ойвен	Исмайлов		f	t	2022-03-03 09:40:38+00	user11	\N	\N	\N	\N	C	f	\N	10
23	pbkdf2_sha256$260000$IYQM3rEVutjxEdYPSEYmox$W0YEZK2c0fhR65KIm5aAHogv58E6gqTco82+zcti6Mc=	\N	f	Сергей	Гончаров		f	t	2022-03-03 12:09:37+00	user21	\N	\N	\N	\N	C	f	\N	10
33	pbkdf2_sha256$260000$stLA0QnruUFpykXWAvMkb9$xoK/QSe0lVt3No6WfEHCLyO9z9iyXiT0WYifqJ4KuZs=	\N	f	Нургелди	Бегишов		f	t	2022-03-03 12:13:19+00	user31	\N	\N	\N	\N	C	f	\N	10
8	pbkdf2_sha256$260000$8ltOCbNNJL34ZYNpH87cbZ$gnQQRNUeSKGIKkCwAIhOQr4o+94PGikCepctxPM1imQ=	\N	f	Артём	Айрапетов		f	t	2022-03-03 09:34:17+00	user6	\N	\N	\N	\N	C	f	\N	10
9	pbkdf2_sha256$260000$xrNckhkItK4DnOAcXfQgCp$qqqwhRkVQySGldI5IGMLYaFTlYE2ctl2m6KUcFQpst0=	\N	f	Гурбан	Батыров		f	t	2022-03-03 09:34:51+00	user7	\N	\N	\N	\N	C	f	\N	10
10	pbkdf2_sha256$260000$us1hHDykUSBbrtduUtqE3q$j79iDmEEKkY7OA81jrvP4n5wlSHqZaai+qhvvYnb0vE=	\N	f	ВЫКУП	(без договора)		f	t	2022-03-03 09:38:59+00	user8	\N	\N	\N	\N	C	f	\N	10
2	pbkdf2_sha256$260000$PmrY4xGAJ2IqDXzt9gKa7D$rKOEfmxDlor2G4lQN1De9Vb/yfWcWix5lDBuTAgq73I=	\N	f	Farid	Mammadxanli		f	t	2022-02-23 14:28:15+00	farid	\N	\N	\N	\N	S	f	\N	10
12	pbkdf2_sha256$260000$CG5qiQqtqLVsgt2B9rDnPw$Ph3rD6V3qk2WwBeH+Ky/nxvQupBILAms1idcqcuomzU=	\N	f	Сергей	Бережной		f	t	2022-03-03 09:40:12+00	user10	\N	\N	\N	\N	C	f	\N	10
14	pbkdf2_sha256$260000$MutI2CiXvVwwAIkWiAoOrp$DBWPOfBK7vniZD37TW9mLeY7gmPANzb70f3wxMSEmLI=	\N	f	Заур	выкуп		f	t	2022-03-03 09:41:42+00	user12	\N	\N	\N	\N	F	f	\N	10
15	pbkdf2_sha256$260000$qaT6DFj5YYzqevzW8rXhaW$CIYSxFwPQnaCJ5bZxX0zKX2CY2G0DiTbbFuvOTDOc98=	\N	f	Олег	Лаптев		f	t	2022-03-03 09:43:12+00	user13	\N	\N	\N	\N	C	f	\N	10
17	pbkdf2_sha256$260000$4l0vr7HrAgxeAu2sYfxfyq$mNQ05/3U0OL+q9KCEt1NkAgUeJC2J7Yx4RlLEoU49TM=	\N	f	Азат	Кулиев		f	t	2022-03-03 09:44:24+00	user15	\N	\N	\N	\N	C	f	\N	10
16	pbkdf2_sha256$260000$36eiypWJMLTwN0Co1mXqkm$xUfhoEEGWBLWxinGMHUzvqZ30ol8nzmvNVTC5RGF/0M=	\N	f	Эрхан	Кара		f	t	2022-03-03 09:43:42+00	user14	\N	\N	\N	\N	C	f	\N	10
18	pbkdf2_sha256$260000$1Ax4VV20ic9wb41ROWH8R6$a5Q5LPtxH2QZgzpihLre9n+u/t+YZEy0bQ5y2HRcrYM=	\N	f	Али	выкуп		f	t	2022-03-03 12:05:57+00	user16	\N	\N	\N	\N	C	f	\N	10
19	pbkdf2_sha256$260000$pVqOXS6JmOP9690ZuEGizX$rvcmQSqwcuwW2ao6Ekhq9qsLhpS1XKqE03jDE0GT4vM=	\N	f	Рустем	Чолиев		f	t	2022-03-03 12:06:16+00	user17	\N	\N	\N	\N	F	f	\N	10
20	pbkdf2_sha256$260000$Z0S461XcGZz2Zvj7IQrjBc$znTQadOAjg3BPjZ0I+V2VOV9RA6UlVGEWVJebkQDZGo=	\N	f	Рахман	Абдыресулов		f	t	2022-03-03 12:06:49+00	user18	\N	\N	\N	\N	C	f	\N	10
22	pbkdf2_sha256$260000$8VplipG7baPUPBCOvshkwB$VGcRmzlbmtF5oH4evrKpWFECvpgNMVeu5ERT6c2uaTI=	\N	f	Юрий	Москалик		f	t	2022-03-03 12:09:08+00	user20	\N	\N	\N	\N	C	f	\N	10
24	pbkdf2_sha256$260000$l3H9nQwL02dymu45qbpfEA$BykiqUCtVtv0PAwtifBZ0t2UzyaCxfZw0gW5f98nrto=	\N	f	Фарух	Фархатов		f	t	2022-03-03 12:09:58+00	user22	\N	\N	\N	\N	C	f	\N	10
25	pbkdf2_sha256$260000$bKfo2a1a4J2wBbankuIpYh$2lOqhXdJMOeNot8nfCihzEX1ozdhQPGUnya9hyUthps=	\N	f	Бегенч	Атаджанов		f	t	2022-03-03 12:10:18+00	user23	\N	\N	\N	\N	C	f	\N	10
26	pbkdf2_sha256$260000$OPIotuxTe65AKApto117XC$iP/RgiBL4O2WTcbx4CmTaMuXSlSOe61isIJEh/23YQ4=	\N	f	Эльвин	Валиев		f	t	2022-03-03 12:10:41+00	user24	\N	\N	\N	\N	C	f	\N	10
27	pbkdf2_sha256$260000$zLuJVnxssNIcxtnhJ5INyt$67HZZvvvgCLt3TrA7bZv4M47/lCkPCUauneZ1qFaysM=	\N	f	Шамиль	Яхяев		f	t	2022-03-03 12:11:04+00	user25	\N	\N	\N	\N	C	f	\N	10
28	pbkdf2_sha256$260000$ihtg7XzGiwSI1hQRvQU8FH$iD0cDiDeZMkDaKLoIQ5Mtq9hyxUr32dNQye7kxB5Y84=	\N	f	Мухамметсердар	Оммадов		f	t	2022-03-03 12:11:25+00	user26	\N	\N	\N	\N	C	f	\N	10
29	pbkdf2_sha256$260000$CirGMt1GxEJBAXsCMt23yZ$ELThSE3oP9SShR//mA9STkb52noL0hR37bNgu/GNBFE=	\N	f	Довлет	Аманов		f	t	2022-03-03 12:11:46+00	user27	\N	\N	\N	\N	C	f	\N	10
30	pbkdf2_sha256$260000$QQ8L6xj0mNTim9jYlPTaRe$vrW/G3OCQ/wRsG7MmwS0PW9pvVohDioECk7eMi60sc8=	\N	f	Умитджан	Бахтияров		f	t	2022-03-03 12:12:06+00	user28	\N	\N	\N	\N	C	f	\N	10
31	pbkdf2_sha256$260000$FWf7EMtkH2s8XR5huOdnTh$jc3T4uVjc585+mGM/vWlatIv7n2ZcEZQlBNxAmuIpVU=	\N	f	Сердар	Бердиев		f	t	2022-03-03 12:12:32+00	user29	\N	\N	\N	\N	C	f	\N	10
5	pbkdf2_sha256$260000$PMgl9hglGtZHFLBHYL0e3H$zq92lbVNr4jEk31aRvF5xPvNOWD0YHaR5I5kb6YszWg=	\N	f	Юнус	Чолиев		f	t	2022-03-03 09:25:38+00	user3	\N	\N	\N	\N	C	f	\N	10
32	pbkdf2_sha256$260000$Wpq7HBdi4Wzp3ILB1nyDjl$ZDkTg2MWzCoAr48YoD2sgsZaJtt+50d4rUE3Ho65mpk=	\N	f	Сердар	Халилов		f	t	2022-03-03 12:13:00+00	user30	\N	\N	\N	\N	C	f	\N	10
34	pbkdf2_sha256$260000$pLaqNhesGSOXQaIfWW5TF5$yniTVLyUB9FHg/c7xm26W+/ZV6OP4eSxW+JBH768bGc=	\N	f	Байрам	Аманов		f	t	2022-03-03 12:18:26+00	user32	\N	\N	\N	\N	C	f	\N	10
35	pbkdf2_sha256$260000$yOsTLXpgr86zBmOuTBvyec$JWaQOs51NzFHgjr701H9fvGzdRYRG1B0VtVOfWGAY8M=	\N	f	Богдан	Голяченко		f	t	2022-03-03 12:18:46+00	user33	\N	\N	\N	\N	C	f	\N	10
36	pbkdf2_sha256$260000$GmcJBj474tSaKyvy3VnhoJ$Ge/ZAIEPuKkePVxryYXwC+IY9yNBosdtSiEKUXowYvU=	\N	f	Василий	Сладкевич		f	t	2022-03-03 12:19:04+00	user34	\N	\N	\N	\N	C	f	\N	10
37	pbkdf2_sha256$260000$WLii6bhmdS6NFLw0DVuMmI$JIOM6KV9xfLEPBX/4rs9YxvcB8jP7gD3qchs7hN5g3c=	\N	f	Бегенч	Говкиев		f	t	2022-03-03 12:19:24+00	user35	\N	\N	\N	\N	C	f	\N	10
38	pbkdf2_sha256$260000$5SDoqX9VY5xSGAWe8Y3NVP$moEth9/WdF2C1eJC5fODicQ8hBzHJ0rw3Z1TiPEo6qE=	\N	f	Александр	Волошин		f	t	2022-03-03 12:19:50+00	user36	\N	\N	\N	\N	C	f	\N	10
39	pbkdf2_sha256$260000$nEQJX208tfsouO9SJnZoMq$I1DUWxtw5OQ0MOE0sCaaxjjAISoKHHK629qg5/qc9/U=	\N	f	Ярослав	Ященко		f	t	2022-03-03 12:20:11+00	user37	\N	\N	\N	\N	C	f	\N	10
40	pbkdf2_sha256$260000$dpPIKdmgtgM6PgtzdcQSpo$rMM3XCxKqSLhUVBYNlnnlGIbH3FqFV1tNSqBoP+F4uI=	\N	f	МОЙСИК			f	t	2022-03-03 12:20:35+00	user38	\N	\N	\N	\N	C	f	\N	10
6	pbkdf2_sha256$260000$7qE7sjcxbs32ssC0OirUwm$XWzmTWzlKNXYK+454lmVcJuQFvLYDNwJDruqcHh7OwI=	\N	f	Мукхриддин	Акрамов		f	t	2022-03-03 09:26:06+00	user4	\N	\N	\N	\N	C	f	\N	10
7	pbkdf2_sha256$260000$srzBEAkB1VoImQKl77a7Ox$skry7bPVOxHbEvnapRBk28giwi5rDCx1NrUqrbq9cww=	\N	f	Бабагелди	Авдиев		f	t	2022-03-03 09:26:52+00	user5	\N	\N	\N	\N	C	f	\N	10
21	pbkdf2_sha256$260000$LRPYLmD0W0fXO1FrCfoLbK$iT3fyCkg0CjPKEbj13M7uuJbHuAdqP91flJEgTdT8CM=	\N	f	Довлетгельди	Абдиев		f	t	2022-03-03 12:07:29+00	user19	\N	\N	\N	\N	C	f	\N	10
4	pbkdf2_sha256$260000$YmP4EAOeK85Nbt43rmoX6O$kkEja2hmhvOx8akMdViQzDXVFL4q89dcxep7GGda/h8=	\N	f	Шохрат	Язгулиев		f	t	2022-03-03 09:24:35+00	user2	\N	\N	\N	\N	C	f	Примечание об этом водитель.	10
41	pbkdf2_sha256$260000$SSSYu7NxUuAh0a7V1GCdWb$SYI9/51RK6uTZ//pWarWkhIQsjX2AH+XZULoYzGb+uE=	\N	f	Шохрат	Пенджиев		f	t	2022-03-03 12:20:59+00	user39	\N	\N	\N	\N	C	f	Примечание об этом водитель.	7
42	pbkdf2_sha256$260000$hZYSADs7pdTkNsGkXXo7H8$SbLzwOSrbTXDksu+5Gg/OEoLKZSv4xswC32bVmbRFDY=	\N	f	Савченко	Юрий		f	t	2022-03-03 12:21:30+00	user40	\N	\N	\N	\N	C	f	Примечание об этом водитель.	4
\.


--
-- Data for Name: account_customuser_groups; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_customuser_groups (id, customuser_id, group_id) FROM stdin;
\.


--
-- Data for Name: account_customuser_user_permissions; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_customuser_user_permissions (id, customuser_id, permission_id) FROM stdin;
\.


--
-- Data for Name: account_licenseimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_licenseimage (id, created_at, updated_at, path, is_main, user_id) FROM stdin;
\.


--
-- Data for Name: account_passportimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_passportimage (id, created_at, updated_at, path, is_main, user_id) FROM stdin;
\.


--
-- Data for Name: account_score; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_score (id, customer_id, note, score) FROM stdin;
1	11	\N	10
2	1	\N	10
3	2	\N	10
4	3	\N	10
5	12	\N	10
6	13	\N	10
7	14	\N	10
8	15	\N	10
10	16	\N	10
11	17	\N	10
12	18	\N	10
13	19	\N	10
14	20	\N	10
15	21	\N	10
16	4	\N	10
17	22	\N	10
18	23	\N	10
19	24	\N	10
20	25	\N	10
21	26	\N	10
22	27	\N	10
23	28	\N	10
24	29	\N	10
25	30	\N	10
26	31	\N	10
27	5	\N	10
28	32	\N	10
29	33	\N	10
30	34	\N	10
31	35	\N	10
32	36	\N	10
33	37	\N	10
34	38	\N	10
35	39	\N	10
36	40	\N	10
37	41	\N	10
38	6	\N	10
39	42	\N	10
40	7	\N	10
41	8	\N	10
42	9	\N	10
43	10	\N	10
44	42		1
45	41		4
46	42		1
\.


--
-- Data for Name: account_userimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_userimage (id, created_at, updated_at, path, is_main, user_id) FROM stdin;
1	2022-03-19 17:31:19.192573+00	2022-03-19 17:31:19.192602+00	accounts/user_images/istockphoto-1163700146-612x612.jpg	f	21
2	2022-03-19 17:33:30.935264+00	2022-03-19 17:33:30.935289+00	accounts/user_images/istockphoto-1163700146-612x612_7TBxwsL.jpg	f	4
3	2022-03-19 18:11:54.512768+00	2022-03-19 18:11:54.51279+00	accounts/user_images/istockphoto-1163700146-612x612_IX1Iccb.jpg	f	42
4	2022-03-19 18:12:37.337816+00	2022-03-19 18:12:37.33784+00	accounts/user_images/istockphoto-1138561454-612x612.jpg	f	41
\.


--
-- Data for Name: account_userphone; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.account_userphone (id, created_at, updated_at, number, note, is_main, user_id) FROM stdin;
1	2022-02-23 13:33:41.144924+00	2022-02-23 13:33:41.144944+00	0933936397	\N	f	1
2	2022-02-23 14:28:15.164109+00	2022-02-23 14:28:15.16413+00	0509809228	\N	f	2
3	2022-03-03 09:22:41.916224+00	2022-03-03 09:22:41.916251+00	098-757-17-67	\N	f	3
4	2022-03-03 09:24:35.799749+00	2022-03-03 09:24:35.799772+00	063-430-63-73	\N	f	4
5	2022-03-03 09:25:38.750653+00	2022-03-03 09:25:38.750683+00	095-770-37-95	\N	f	5
6	2022-03-03 09:26:06.554101+00	2022-03-03 09:26:06.554125+00	097-520-62-95	\N	f	6
7	2022-03-03 09:26:52.604399+00	2022-03-03 09:26:52.604422+00	066-420-33-81	\N	f	7
8	2022-03-03 09:34:17.669564+00	2022-03-03 09:34:17.669597+00	067-743-27-19	\N	f	8
9	2022-03-03 09:34:51.623832+00	2022-03-03 09:34:51.623854+00	093-211-60-14	\N	f	9
10	2022-03-03 09:40:12.405574+00	2022-03-03 09:40:12.405596+00	063-890-08-79	\N	f	12
11	2022-03-03 09:40:38.663283+00	2022-03-03 09:40:38.663309+00	073-040-40-45	\N	f	13
12	2022-03-03 09:43:42.265544+00	2022-03-03 09:43:42.265567+00	066-876-00-50	\N	f	16
13	2022-03-03 09:44:24.617577+00	2022-03-03 09:44:24.617605+00	063-070-08-25	\N	f	17
14	2022-03-03 12:06:16.890485+00	2022-03-03 12:06:16.890507+00	063-503-76-01	\N	f	19
15	2022-03-03 12:06:49.393308+00	2022-03-03 12:06:49.393331+00	093-035-20-02	\N	f	20
16	2022-03-03 12:07:29.468975+00	2022-03-03 12:07:29.469003+00	066-990-98-98	\N	f	21
17	2022-03-03 12:09:08.817833+00	2022-03-03 12:09:08.817854+00	063-828-39-44	\N	f	22
18	2022-03-03 12:09:37.69763+00	2022-03-03 12:09:37.697652+00	098-201-201-2	\N	f	23
19	2022-03-03 12:09:58.368308+00	2022-03-03 12:09:58.36833+00	063-895-58-62	\N	f	24
20	2022-03-03 12:10:19.095685+00	2022-03-03 12:10:19.095717+00	063-597-01-21	\N	f	25
21	2022-03-03 12:11:04.819665+00	2022-03-03 12:11:04.819689+00	095-383-08-41	\N	f	27
22	2022-03-03 12:11:25.237654+00	2022-03-03 12:11:25.237674+00	095-706-07-97	\N	f	28
23	2022-03-03 12:12:07.086708+00	2022-03-03 12:12:07.086729+00	067-282-05-82	\N	f	30
24	2022-03-03 12:12:32.508585+00	2022-03-03 12:12:32.508606+00	066-919-32-97	\N	f	31
25	2022-03-03 12:12:32.509339+00	2022-03-03 12:12:32.509353+00	063-198-48-65	\N	f	31
26	2022-03-03 12:13:01.083696+00	2022-03-03 12:13:01.083718+00	063-864-15-41	\N	f	32
27	2022-03-03 12:13:19.385774+00	2022-03-03 12:13:19.3858+00	093-381-18-52	\N	f	33
28	2022-03-03 12:18:26.923884+00	2022-03-03 12:18:26.923906+00	093-050-25-02	\N	f	34
29	2022-03-03 12:18:47.107238+00	2022-03-03 12:18:47.107259+00	096-676-71-02	\N	f	35
30	2022-03-03 12:19:04.586595+00	2022-03-03 12:19:04.586618+00	098-464-78-33	\N	f	36
31	2022-03-03 12:19:25.088238+00	2022-03-03 12:19:25.088263+00	093-066-56-60	\N	f	37
32	2022-03-03 12:19:50.955015+00	2022-03-03 12:19:50.955038+00	063-668-93-28	\N	f	38
33	2022-03-03 12:20:11.597065+00	2022-03-03 12:20:11.597105+00	095-070-37-32	\N	f	39
34	2022-03-03 12:20:59.228402+00	2022-03-03 12:20:59.228427+00	063-509-64-70	\N	f	41
35	2022-03-03 12:20:59.229144+00	2022-03-03 12:20:59.229157+00	093-422-54-64	\N	f	41
36	2022-03-03 12:21:30.849227+00	2022-03-03 12:21:30.849258+00	093-515-15-95	\N	f	42
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add Token	6	add_token
22	Can change Token	6	change_token
23	Can delete Token	6	delete_token
24	Can view Token	6	view_token
25	Can add token	7	add_tokenproxy
26	Can change token	7	change_tokenproxy
27	Can delete token	7	delete_tokenproxy
28	Can view token	7	view_tokenproxy
29	Can add user	8	add_customuser
30	Can change user	8	change_customuser
31	Can delete user	8	delete_customuser
32	Can view user	8	view_customuser
33	Can add contract	9	add_contract
34	Can change contract	9	change_contract
35	Can delete contract	9	delete_contract
36	Can view contract	9	view_contract
37	Can add user phone	10	add_userphone
38	Can change user phone	10	change_userphone
39	Can delete user phone	10	delete_userphone
40	Can view user phone	10	view_userphone
41	Can add user image	11	add_userimage
42	Can change user image	11	change_userimage
43	Can delete user image	11	delete_userimage
44	Can view user image	11	view_userimage
45	Can add passport image	12	add_passportimage
46	Can change passport image	12	change_passportimage
47	Can delete passport image	12	delete_passportimage
48	Can view passport image	12	view_passportimage
49	Can add license image	13	add_licenseimage
50	Can change license image	13	change_licenseimage
51	Can delete license image	13	delete_licenseimage
52	Can view license image	13	view_licenseimage
53	Can add brand	14	add_brand
54	Can change brand	14	change_brand
55	Can delete brand	14	delete_brand
56	Can view brand	14	view_brand
57	Can add car	15	add_car
58	Can change car	15	change_car
59	Can delete car	15	delete_car
60	Can view car	15	view_car
61	Can add car class	16	add_carclass
62	Can change car class	16	change_carclass
63	Can delete car class	16	delete_carclass
64	Can view car class	16	view_carclass
65	Can add car image	17	add_carimage
66	Can change car image	17	change_carimage
67	Can delete car image	17	delete_carimage
68	Can view car image	17	view_carimage
69	Can add case	18	add_case
70	Can change case	18	change_case
71	Can delete case	18	delete_case
72	Can view case	18	view_case
73	Can add color	19	add_color
74	Can change color	19	change_color
75	Can delete color	19	delete_color
76	Can view color	19	view_color
77	Can add fuel	20	add_fuel
78	Can change fuel	20	change_fuel
79	Can delete fuel	20	delete_fuel
80	Can view fuel	20	view_fuel
81	Can add office	21	add_office
82	Can change office	21	change_office
83	Can delete office	21	delete_office
84	Can view office	21	view_office
85	Can add model	22	add_model
86	Can change model	22	change_model
87	Can delete model	22	delete_model
88	Can view model	22	view_model
89	Can add car paper	23	add_carpaper
90	Can change car paper	23	change_carpaper
91	Can delete car paper	23	delete_carpaper
92	Can view car paper	23	view_carpaper
93	Can add car image task	24	add_carimagetask
94	Can change car image task	24	change_carimagetask
95	Can delete car image task	24	delete_carimagetask
96	Can view car image task	24	view_carimagetask
97	Can add administrative deposit	25	add_administrativedeposit
98	Can change administrative deposit	25	change_administrativedeposit
99	Can delete administrative deposit	25	delete_administrativedeposit
100	Can view administrative deposit	25	view_administrativedeposit
101	Can add administrative withdraw	26	add_administrativewithdraw
102	Can change administrative withdraw	26	change_administrativewithdraw
103	Can delete administrative withdraw	26	delete_administrativewithdraw
104	Can view administrative withdraw	26	view_administrativewithdraw
105	Can add Currency	27	add_currency
106	Can change Currency	27	change_currency
107	Can delete Currency	27	delete_currency
108	Can view Currency	27	view_currency
109	Can add balance	28	add_balance
110	Can change balance	28	change_balance
111	Can delete balance	28	delete_balance
112	Can view balance	28	view_balance
113	Can add credit	29	add_credit
114	Can change credit	29	change_credit
115	Can delete credit	29	delete_credit
116	Can view credit	29	view_credit
117	Can add credit payment	30	add_creditpayment
118	Can change credit payment	30	change_creditpayment
119	Can delete credit payment	30	delete_creditpayment
120	Can view credit payment	30	view_creditpayment
121	Can add credit image	31	add_creditimage
122	Can change credit image	31	change_creditimage
123	Can delete credit image	31	delete_creditimage
124	Can view credit image	31	view_creditimage
125	Can add company	32	add_company
126	Can change company	32	change_company
127	Can delete company	32	delete_company
128	Can view company	32	view_company
129	Can add revenue	33	add_revenue
130	Can change revenue	33	change_revenue
131	Can delete revenue	33	delete_revenue
132	Can view revenue	33	view_revenue
133	Can add taxi	34	add_taxi
134	Can change taxi	34	change_taxi
135	Can delete taxi	34	delete_taxi
136	Can view taxi	34	view_taxi
137	Can add revenue item	35	add_revenueitem
138	Can change revenue item	35	change_revenueitem
139	Can delete revenue item	35	delete_revenueitem
140	Can view revenue item	35	view_revenueitem
141	Can add rent taxi	36	add_renttaxi
142	Can change rent taxi	36	change_renttaxi
143	Can delete rent taxi	36	delete_renttaxi
144	Can view rent taxi	36	view_renttaxi
145	Can add revenue	37	add_revenue
146	Can change revenue	37	change_revenue
147	Can delete revenue	37	delete_revenue
148	Can view revenue	37	view_revenue
149	Can add revenue item	38	add_revenueitem
150	Can change revenue item	38	change_revenueitem
151	Can delete revenue item	38	delete_revenueitem
152	Can view revenue item	38	view_revenueitem
153	Can add shtraf	39	add_shtraf
154	Can change shtraf	39	change_shtraf
155	Can delete shtraf	39	delete_shtraf
156	Can view shtraf	39	view_shtraf
157	Can add shtraf image	40	add_shtrafimage
158	Can change shtraf image	40	change_shtrafimage
159	Can delete shtraf image	40	delete_shtrafimage
160	Can view shtraf image	40	view_shtrafimage
161	Can add debt	41	add_debt
162	Can change debt	41	change_debt
163	Can delete debt	41	delete_debt
164	Can view debt	41	view_debt
165	Can add debt image	42	add_debtimage
166	Can change debt image	42	change_debtimage
167	Can delete debt image	42	delete_debtimage
168	Can view debt image	42	view_debtimage
169	Can add tariff	43	add_tariff
170	Can change tariff	43	change_tariff
171	Can delete tariff	43	delete_tariff
172	Can view tariff	43	view_tariff
173	Can add tariff	44	add_tariff
174	Can change tariff	44	change_tariff
175	Can delete tariff	44	delete_tariff
176	Can view tariff	44	view_tariff
177	Can add settings	45	add_settings
178	Can change settings	45	change_settings
179	Can delete settings	45	delete_settings
180	Can view settings	45	view_settings
181	Can add settings	46	add_settings
182	Can change settings	46	change_settings
183	Can delete settings	46	delete_settings
184	Can view settings	46	view_settings
185	Can add revenue	47	add_revenue
186	Can change revenue	47	change_revenue
187	Can delete revenue	47	delete_revenue
188	Can view revenue	47	view_revenue
189	Can add revenue item	48	add_revenueitem
190	Can change revenue item	48	change_revenueitem
191	Can delete revenue item	48	delete_revenueitem
192	Can view revenue item	48	view_revenueitem
193	Can add rent taxi	49	add_renttaxi
194	Can change rent taxi	49	change_renttaxi
195	Can delete rent taxi	49	delete_renttaxi
196	Can view rent taxi	49	view_renttaxi
197	Can add rent taxi daily	50	add_renttaxidaily
198	Can change rent taxi daily	50	change_renttaxidaily
199	Can delete rent taxi daily	50	delete_renttaxidaily
200	Can view rent taxi daily	50	view_renttaxidaily
201	Can add score	51	add_score
202	Can change score	51	change_score
203	Can delete score	51	delete_score
204	Can view score	51	view_score
205	Can add casco image	52	add_cascoimage
206	Can change casco image	52	change_cascoimage
207	Can delete casco image	52	delete_cascoimage
208	Can view casco image	52	view_cascoimage
209	Can add osago payment	53	add_osagopayment
210	Can change osago payment	53	change_osagopayment
211	Can delete osago payment	53	delete_osagopayment
212	Can view osago payment	53	view_osagopayment
213	Can add casco payment	54	add_cascopayment
214	Can change casco payment	54	change_cascopayment
215	Can delete casco payment	54	delete_cascopayment
216	Can view casco payment	54	view_cascopayment
217	Can add casco	55	add_casco
218	Can change casco	55	change_casco
219	Can delete casco	55	delete_casco
220	Can view casco	55	view_casco
221	Can add company	56	add_company
222	Can change company	56	change_company
223	Can delete company	56	delete_company
224	Can view company	56	view_company
225	Can add osago	57	add_osago
226	Can change osago	57	change_osago
227	Can delete osago	57	delete_osago
228	Can view osago	57	view_osago
229	Can add osago image	58	add_osagoimage
230	Can change osago image	58	change_osagoimage
231	Can delete osago image	58	delete_osagoimage
232	Can view osago image	58	view_osagoimage
233	Can add rent	59	add_rent
234	Can change rent	59	change_rent
235	Can delete rent	59	delete_rent
236	Can view rent	59	view_rent
237	Can add settings	60	add_settings
238	Can change settings	60	change_settings
239	Can delete settings	60	delete_settings
240	Can view settings	60	view_settings
241	Can add revenue	61	add_revenue
242	Can change revenue	61	change_revenue
243	Can delete revenue	61	delete_revenue
244	Can view revenue	61	view_revenue
245	Can add dtp	62	add_dtp
246	Can change dtp	62	change_dtp
247	Can delete dtp	62	delete_dtp
248	Can view dtp	62	view_dtp
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
990c9d166decaad5c05138bd06d12e60267d0a06	2022-02-23 13:40:33.157976+00	1
\.


--
-- Data for Name: car_brand; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_brand (id, name, logo) FROM stdin;
1	Hyundai	\N
2	BMW	\N
3	Toyota	\N
4	Skoda	\N
5	Volkswagen	\N
6	Kia	\N
7	Mazda	\N
8	Opel	\N
9	Subaru	\N
\.


--
-- Data for Name: car_car; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_car (id, created_at, updated_at, number, year, vin_code, is_available, is_active, transmission, fuel_tank, doors, seats, wheel_size, current_tyre, bought_date, bought_price, current_km, oil_km_limit, climate, climate_control, is_verified, reserve_key, advertise_id, car_class_id, case_id, color_id, fuel_id, model_id, office_id, health_status, status, driver_id) FROM stdin;
74	2022-03-01 21:40:23.256218+00	2022-03-17 22:50:16.639971+00	АІ 7328 MР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	10	1	G	F	\N
75	2022-03-01 21:40:42.392485+00	2022-03-17 22:50:23.239128+00	АІ 7652 MТ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	10	1	G	F	\N
13	2022-03-01 20:16:18.827728+00	2022-03-18 22:49:00.699968+00	AІ 9149 МН	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	2	1	G	C	10
18	2022-03-01 20:21:54.717071+00	2022-03-18 22:38:48.730335+00	АІ 6453 ОО	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	4	1	G	C	17
53	2022-03-01 21:20:19.859002+00	2022-03-18 22:47:04.983514+00	АА 8986 СС	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	7	1	G	C	5
14	2022-03-01 20:19:42.701591+00	2022-03-18 22:49:26.523762+00	AI 3839 ОС	2002	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	3	1	G	C	11
9	2022-02-23 13:28:26.857143+00	2022-03-17 22:36:00.395065+00	АІ 8064 MO	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	2	1	G	F	\N
12	2022-03-01 19:46:43.548763+00	2022-03-17 22:35:37.086642+00	KA 9980 AT	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	2	1	G	F	\N
25	2022-03-01 20:24:27.524002+00	2022-03-17 22:44:18.357725+00	ВМ 0610 СМ	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
8	2022-02-23 13:28:06.234911+00	2022-03-17 22:36:07.898351+00	AE 7193 KO	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	1	1	G	F	\N
1	2022-02-23 13:26:18.450592+00	2022-03-17 22:34:09.202571+00	АІ 0315 MX	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	1	1	G	F	\N
19	2022-03-01 20:22:22.556291+00	2022-03-17 22:34:40.920701+00	АІ 8891 ІК	2007	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	4	1	G	F	\N
7	2022-02-23 13:27:53.89819+00	2022-03-17 22:36:14.881989+00	КA 2219 ВВ	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	1	1	G	F	\N
23	2022-03-01 20:23:42.931269+00	2022-03-18 22:49:40.630516+00	АІ 5508 МР	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	C	12
21	2022-03-01 20:22:55.244143+00	2022-03-17 22:37:17.204554+00	КА 2026 ВВ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	4	1	G	F	\N
47	2022-03-01 20:36:02.526874+00	2022-03-17 22:46:59.107853+00	AІ 7645 МТ	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
65	2022-03-01 21:36:22.360222+00	2022-03-18 22:51:04.003469+00	AI 0712 OВ	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	11	1	G	C	14
58	2022-03-01 21:25:07.644675+00	2022-03-18 22:51:56.235296+00	АА 4934 ВР	2010	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	8	1	G	C	18
3	2022-02-23 13:26:48.662705+00	2022-03-18 22:52:09.750961+00	АI 4463 MC	2019	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	1	1	G	C	19
45	2022-03-01 20:32:52.721982+00	2022-03-18 22:52:24.464426+00	AІ 7365 МI	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	C	20
55	2022-03-01 21:22:38.910492+00	2022-03-18 22:52:36.897009+00	АІ 7166 ІМ	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	7	1	G	C	21
56	2022-03-01 21:24:33.336905+00	2022-03-18 22:52:50.534119+00	АА 3967 ТТ	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	8	1	G	C	22
64	2022-03-01 21:35:59.11692+00	2022-03-18 22:54:01.721628+00	AI 5266 OO	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	10	1	G	C	26
52	2022-03-01 21:20:03.010299+00	2022-03-18 22:54:17.837278+00	АА 8037 РР	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	7	1	G	C	27
6	2022-02-23 13:27:42.245522+00	2022-03-18 22:54:53.582163+00	КА 0729 АР	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	1	1	G	C	29
17	2022-03-01 20:21:19.071816+00	2022-03-18 22:55:08.387442+00	АІ 7462 ОВ	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	4	1	G	C	30
60	2022-03-01 21:25:58.933691+00	2022-03-18 22:55:23.493258+00	АІ 6723 ІН	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	8	1	G	C	31
66	2022-03-01 21:37:30.978341+00	2022-03-19 00:21:36.105991+00	AI 3724 OС	2011	\N	t	t	A	\N	\N	\N	\N	W	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	12	1	D	F	\N
71	2022-03-01 21:38:48.293285+00	2022-03-19 00:22:09.134299+00	АІ 5563 MP	2016	\N	t	t	A	\N	\N	\N	\N	W	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	10	1	D	F	\N
70	2022-03-01 21:38:34.839039+00	2022-03-21 18:54:00.528926+00	АІ 3620 MP	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	10	1	G	RTD	\N
2	2022-02-23 13:26:34.993018+00	2022-03-22 01:27:52.4166+00	АІ 0312 MX	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	1	1	D	F	\N
72	2022-03-01 21:39:13.709312+00	2022-03-22 01:04:54.137441+00	АІ 5564 MP	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	10	1	D	F	\N
29	2022-03-01 20:25:34.365363+00	2022-03-17 22:44:50.117909+00	KA 2853 ВВ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
30	2022-03-01 20:25:51.309237+00	2022-03-17 22:44:59.830996+00	KA 3564 ВI	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
31	2022-03-01 20:26:03.409279+00	2022-03-17 22:45:07.085806+00	KA 5754 ВI	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
34	2022-03-01 20:28:05.42193+00	2022-03-17 22:45:23.802922+00	KA 9864 AO	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
36	2022-03-01 20:28:50.703095+00	2022-03-17 22:45:39.805876+00	АІ 0368 MX	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
32	2022-03-01 20:27:19.317995+00	2022-03-17 21:49:48.695186+00	KA 9134 ВЕ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
38	2022-03-01 20:29:52.643421+00	2022-03-17 22:45:54.772637+00	АІ 3167 MМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
39	2022-03-01 20:30:09.81938+00	2022-03-17 22:46:01.835679+00	АІ 3168 MМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
41	2022-03-01 20:30:39.529455+00	2022-03-17 22:46:15.786851+00	АІ 3422 MР	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
24	2022-03-01 20:23:59.29255+00	2022-03-17 22:44:09.675222+00	ВМ 0588 СМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
26	2022-03-01 20:24:41.292642+00	2022-03-17 22:44:26.661029+00	KA 0514 ВН	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
42	2022-03-01 20:32:12.08395+00	2022-03-17 22:46:23.607922+00	АІ 6304 МР	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
62	2022-03-01 21:28:03.192083+00	2022-03-17 21:57:55.676296+00	АI 3439 MP	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	9	1	G	F	\N
16	2022-03-01 20:21:01.378+00	2022-03-17 22:34:59.934373+00	АІ 2541 КО	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	4	1	G	F	\N
57	2022-03-01 21:24:53.715485+00	2022-03-17 22:48:04.251497+00	АА 4421 ЕІ	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	8	1	G	F	\N
48	2022-03-01 20:36:16.929474+00	2022-03-17 22:47:07.473888+00	AІ 8132 МТ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
50	2022-03-01 20:36:58.856062+00	2022-03-17 22:47:22.671628+00	AМ 6023 ЕТ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
51	2022-03-01 20:37:11.395975+00	2022-03-17 21:54:16.762823+00	KA 6328 ВI	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	6	1	G	F	\N
46	2022-03-01 20:33:07.564279+00	2022-03-17 22:46:50.778246+00	AІ 7453 ОВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
69	2022-03-01 21:38:21.949365+00	2022-03-17 22:00:00.327957+00	АІ 0724 MІ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	10	1	G	F	\N
5	2022-02-23 13:27:14.669674+00	2022-03-18 22:48:42.62727+00	АI 9756 IH	2010	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	1	1	G	C	9
43	2022-03-01 20:32:27.115632+00	2022-03-18 22:50:46.335454+00	АІ 6703 МР	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	C	13
95	2022-03-01 21:53:22.705699+00	2022-03-18 22:53:16.088683+00	АI 0954 МB	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	C	24
61	2022-03-01 21:26:47.320921+00	2022-03-18 22:53:36.549826+00	АІ 2258 MМ	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	9	1	G	C	25
81	2022-03-01 21:46:15.596364+00	2022-03-18 22:53:49.561272+00	AI 1495 MO	2006	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	16	1	G	C	26
87	2022-03-01 21:51:09.056853+00	2022-03-18 22:54:38.579453+00	КА 3306 ВМ	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	19	1	G	C	28
82	2022-03-01 21:48:34.887621+00	2022-03-18 22:56:14.993061+00	AА 1556 ТT	2004	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	17	1	G	C	33
86	2022-03-01 21:50:48.13225+00	2022-03-18 22:56:54.957956+00	АІ 1893 ІІ	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	19	1	G	C	35
90	2022-03-01 21:52:05.281037+00	2022-03-18 22:58:16.363448+00	АI 6786 ОА	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	C	39
76	2022-03-01 21:40:56.303129+00	2022-03-18 22:58:28.138688+00	АІ 8089 MI	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	10	1	G	C	40
68	2022-03-01 21:38:10.07538+00	2022-03-18 22:58:45.48734+00	АІ 0589 MІ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	10	1	G	C	41
77	2022-03-01 21:41:37.183756+00	2022-03-18 22:59:00.637776+00	AI 2111 MB	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	13	1	G	C	42
130	2022-03-01 22:02:47.225411+00	2022-03-19 17:24:22.992179+00	АI 3427 MP	2015	\N	t	t	A	\N	\N	\N	\N	W	\N	\N	\N	\N	f	f	f	t	\N	1	1	1	1	20	1	D	C	38
133	2022-03-01 22:04:32.031175+00	2022-03-19 00:32:09.637411+00	AI 7103 OB	2011	\N	t	t	A	\N	\N	\N	\N	W	\N	\N	\N	\N	f	f	f	t	\N	1	1	1	1	21	1	G	RTD	1
129	2022-03-01 22:02:35.106282+00	2022-03-19 00:32:26.624704+00	АI 8894 ММ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	t	\N	1	1	1	1	20	1	G	F	\N
125	2022-03-01 22:01:40.316635+00	2022-03-19 00:31:59.21244+00	КА 8962 ВМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	t	\N	1	1	1	1	20	1	G	F	\N
132	2022-03-01 22:04:05.760465+00	2022-03-26 16:23:54.189394+00	АI 0886 KP	2016	\N	t	t	A	\N	\N	\N	\N	W	\N	\N	\N	\N	f	f	f	t	\N	1	1	1	1	20	1	D	R	1
128	2022-03-01 22:02:22.3944+00	2022-03-19 00:32:31.520231+00	СА 5408 ІВ	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	t	\N	1	1	1	1	20	1	G	F	\N
127	2022-03-01 22:02:07.985132+00	2022-03-19 00:32:36.859952+00	СА 5407 ІВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	t	\N	1	1	1	1	20	1	G	F	\N
134	2022-03-01 22:04:58.074612+00	2022-03-19 17:20:50.135648+00	AI 8500 MT	2007	\N	t	t	A	\N	\N	\N	\N	W	\N	\N	\N	\N	f	f	f	t	\N	1	1	1	1	22	1	O	F	\N
78	2022-03-01 21:42:01.261454+00	2022-03-29 12:53:06.278333+00	KА 6164 EC	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	14	1	G	RTP	1
109	2022-03-01 21:57:20.344383+00	2022-03-17 22:54:28.596054+00	АI 7923 MТ	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
89	2022-03-01 21:51:51.112186+00	2022-03-17 22:52:05.612085+00	АI 6782 ОА	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
92	2022-03-01 21:52:40.628802+00	2022-03-17 22:52:27.42875+00	АI 6809 ОА	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
94	2022-03-01 21:53:09.020769+00	2022-03-17 22:52:41.815898+00	АI 0253 МР	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
96	2022-03-01 21:53:38.218569+00	2022-03-17 22:52:48.67239+00	АI 0962 МB	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
97	2022-03-01 21:53:52.987791+00	2022-03-17 22:52:56.27251+00	АI 1256 ММ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
99	2022-03-01 21:54:19.85852+00	2022-03-17 22:53:11.597039+00	АI 2519 МР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
100	2022-03-01 21:54:42.795693+00	2022-03-17 22:53:18.208818+00	АI 2846 МР	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
101	2022-03-01 21:55:24.310133+00	2022-03-17 22:53:34.154293+00	АI 2875 ММ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
102	2022-03-01 21:55:37.421868+00	2022-03-17 22:53:41.523722+00	АI 2953 ММ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
104	2022-03-01 21:56:11.501856+00	2022-03-17 22:53:56.143634+00	АI 3922 MТ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
107	2022-03-01 21:56:58.212519+00	2022-03-17 22:54:11.50861+00	АI 5691 MP	2013	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
105	2022-03-01 21:56:25.296676+00	2022-03-17 22:54:02.617623+00	АI 4668 МЕ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
110	2022-03-01 21:58:02.065732+00	2022-03-17 22:54:36.931581+00	АI 8861 MТ	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
111	2022-03-01 21:58:22.602034+00	2022-03-17 22:54:44.396548+00	АI 9104 МР	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
113	2022-03-01 21:58:50.045624+00	2022-03-17 22:54:57.711679+00	АI 9364 МР	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
114	2022-03-01 21:59:01.980033+00	2022-03-17 22:55:05.843687+00	АI 9681 МР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
115	2022-03-01 21:59:16.702716+00	2022-03-17 22:55:12.901237+00	АI 9702 МР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
116	2022-03-01 21:59:37.066433+00	2022-03-17 22:55:19.572297+00	АI 9814 МВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
118	2022-03-01 22:00:03.059744+00	2022-03-17 22:55:36.734275+00	ВМ 0584 СМ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
119	2022-03-01 22:00:15.762339+00	2022-03-17 22:55:44.045444+00	КА 0583 ВM	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
120	2022-03-01 22:00:28.656404+00	2022-03-17 22:55:52.758664+00	КА 0941 ВМ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
122	2022-03-01 22:00:57.944905+00	2022-03-17 22:56:06.276575+00	КА 2641 ВВ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
123	2022-03-01 22:01:15.043877+00	2022-03-17 22:56:13.616091+00	КА 3426 ВМ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
124	2022-03-01 22:01:28.150868+00	2022-03-17 22:56:19.93374+00	КА 6210 ET	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
106	2022-03-01 21:56:42.76448+00	2022-03-17 21:32:45.153219+00	АI 5684 MP	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	20	1	G	F	\N
28	2022-03-01 20:25:14.381425+00	2022-03-17 22:44:41.672608+00	KA 2816 ВВ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
83	2022-03-01 21:49:00.248677+00	2022-03-18 22:46:09.20504+00	KA 0728 AP	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	18	1	G	C	4
79	2022-03-01 21:44:35.786749+00	2022-03-17 22:02:38.005369+00	АА 1066 СС	2007	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	\N	\N	\N	\N	15	1	G	F	\N
35	2022-03-01 20:28:20.704567+00	2022-03-17 22:45:31.715271+00	KA 9865 AO	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
37	2022-03-01 20:29:32.747683+00	2022-03-17 22:45:47.296472+00	АІ 3159 MМ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
20	2022-03-01 20:22:36.413883+00	2022-03-17 22:34:23.993708+00	АІ 9423 ІМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	4	1	G	F	\N
15	2022-03-01 20:20:43.075461+00	2022-03-17 22:35:07.847166+00	AI 6531 OC	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	5	1	G	F	\N
11	2022-03-01 19:46:19.885957+00	2022-03-17 22:35:45.327743+00	КА 3676 ВО	2014	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	2	1	G	F	\N
10	2022-02-23 13:28:39.610487+00	2022-03-17 22:35:53.520549+00	АІ 8243 OВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	2	1	G	F	\N
4	2022-02-23 13:27:00.29139+00	2022-03-17 22:36:35.423291+00	АI 8250 OB	2008	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	1	1	G	F	\N
22	2022-03-01 20:23:30.79391+00	2022-03-17 22:37:27.472831+00	АІ 3605 ОС	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
27	2022-03-01 20:24:58.633026+00	2022-03-17 22:44:33.439456+00	KA 0574 ВМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
33	2022-03-01 20:27:52.029885+00	2022-03-17 22:45:15.264352+00	KA 9236 ВЕ	2018	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
40	2022-03-01 20:30:25.05629+00	2022-03-17 22:46:09.299933+00	АІ 3351 MA	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
44	2022-03-01 20:32:39.090013+00	2022-03-17 22:46:37.230272+00	АІ 6896 ОВ	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
49	2022-03-01 20:36:31.563805+00	2022-03-17 22:47:14.98514+00	AІ 8829 ММ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	6	1	G	F	\N
63	2022-03-01 21:28:17.418829+00	2022-03-17 22:48:51.035417+00	КА 0345 ВМ	2010	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	9	1	G	F	\N
73	2022-03-01 21:40:07.8131+00	2022-03-17 22:50:00.795641+00	АІ 7045 MP	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	10	1	G	F	\N
98	2022-03-01 21:54:05.788776+00	2022-03-17 22:53:05.256476+00	АI 2518 МР	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
103	2022-03-01 21:55:49.268453+00	2022-03-17 22:53:48.072575+00	АI 3702 MP	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
108	2022-03-01 21:57:06.363266+00	2022-03-17 22:54:19.293041+00	АI 6429 MP	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
112	2022-03-01 21:58:36.208945+00	2022-03-17 22:54:51.113785+00	АI 9361 МР	2017	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
117	2022-03-01 21:59:50.274519+00	2022-03-17 22:55:27.985648+00	ВА 7546 ЕМ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
121	2022-03-01 22:00:46.190102+00	2022-03-17 22:55:59.732678+00	КА 2479 ВВ	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	F	\N
88	2022-03-01 21:51:24.947691+00	2022-03-18 22:48:24.569084+00	СА 3701 СК	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	19	1	G	C	8
85	2022-03-01 21:50:31.264483+00	2022-03-18 22:47:18.768677+00	АА 6038 РР	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	19	1	G	C	6
54	2022-03-01 21:22:18.728738+00	2022-03-18 22:51:18.633924+00	АІ 2803 СС	2013	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	7	1	G	C	15
91	2022-03-01 21:52:27.493327+00	2022-03-18 22:51:29.535215+00	АI 6805 ОА	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	C	16
80	2022-03-01 21:45:50.769643+00	2022-03-18 22:51:43.24615+00	АІ 8006 МА	2013	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	15	1	G	C	17
59	2022-03-01 21:25:21.946529+00	2022-03-18 22:53:04.812609+00	АА 6079 СС	2012	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	\N	1	1	8	1	G	C	23
67	2022-03-01 21:37:57.061228+00	2022-03-18 22:55:37.35398+00	AI 9766 OA	2011	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	12	1	G	C	32
84	2022-03-01 21:49:56.468223+00	2022-03-18 22:56:39.268445+00	АЕ 9200 IM	2013	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	18	1	G	C	34
131	2022-03-01 22:03:50.10662+00	2022-03-18 22:57:09.122097+00	АА 1058 ЕМ	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	C	36
93	2022-03-01 21:52:53.191374+00	2022-03-18 22:57:41.204006+00	АМ 3560 ЕО	2015	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	f	\N	1	1	1	1	20	1	G	C	37
126	2022-03-01 22:01:54.365927+00	2022-03-19 00:32:42.807146+00	КА 9531 ВК	2016	\N	t	t	A	\N	\N	\N	\N	S	\N	\N	\N	\N	f	f	f	t	\N	1	1	1	1	20	1	G	C	7
\.


--
-- Data for Name: car_carclass; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carclass (id, name) FROM stdin;
1	Эконом
\.


--
-- Data for Name: car_carimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carimage (id, created_at, updated_at, path, is_main, car_id) FROM stdin;
5	2022-02-23 13:54:43.656928+00	2022-03-17 21:41:27.449089+00	cars/m_16082309500140819.jpg	f	7
3	2022-02-23 13:54:11.325309+00	2022-03-17 21:39:08.630092+00	cars/hyundai-accent__270037636-460x345.jpg	f	1
2	2022-02-23 13:53:58.256381+00	2022-03-17 21:41:49.023528+00	cars/BF538031_6af24a.jpg	f	9
11	2022-03-01 22:28:41.848392+00	2022-03-17 21:42:15.499705+00	cars/BH246446_c452d5.jpg	f	11
12	2022-03-01 22:28:56.596485+00	2022-03-17 21:42:28.701322+00	cars/2018-Hyundai-Elantra-review-white-1001x565p_15.webp	f	12
13	2022-03-01 22:29:07.215691+00	2022-03-17 21:42:49.216467+00	cars/2018_hyundai_elantra_sedan_eco_fq_oem_1_600.webp	f	13
1	2022-02-23 13:53:50.051592+00	2022-03-17 21:42:03.67327+00	cars/BH246446_5aef71.jpg	f	10
10	2022-02-23 14:02:42.091759+00	2022-03-17 21:40:26.880414+00	cars/20210528_134301278_ios.jpg	f	4
7	2022-02-23 13:54:58.366175+00	2022-03-17 21:40:46.988374+00	cars/Hyundai-Accent-IV_1.jpg	f	5
6	2022-02-23 13:54:51.534767+00	2022-03-17 21:41:10.290815+00	cars/20515031_2f88f5.jpg	f	6
34	2022-03-01 22:46:39.980494+00	2022-03-17 21:44:13.296708+00	cars/97391aa-1.jpg	f	15
25	2022-03-01 22:43:56.755811+00	2022-03-17 21:46:56.993359+00	cars/2015_hyundai_elantra_sport-pic-7907303392934580073-640x480.jpeg	f	22
9	2022-02-23 13:55:24.895688+00	2022-03-17 21:39:28.775415+00	cars/hyundai_accent__407514610bx.jpg	f	2
8	2022-02-23 13:55:14.015669+00	2022-03-17 21:39:55.60963+00	cars/hyundai_accent__407514610bx_1.jpg	f	3
14	2022-03-01 22:29:16.548086+00	2022-03-17 21:43:41.704701+00	cars/bmw-7-series-back-side-2-308345.jpg	f	14
33	2022-03-01 22:46:23.690586+00	2022-03-17 21:44:35.070047+00	cars/2015_Toyota_Corolla_ZRE172R_Ascent_sedan_2015-11-11_01.jpg	f	16
32	2022-03-01 22:46:17.816121+00	2022-03-17 21:44:59.672416+00	cars/c6d9b6d9-4882-4905-b7b4-922097ff89a7.webp	f	17
31	2022-03-01 22:45:53.094809+00	2022-03-17 21:45:20.322051+00	cars/90811a.jpg	f	18
30	2022-03-01 22:45:43.672454+00	2022-03-17 21:45:36.93115+00	cars/2bdbdcf7dbb828dae85e3a97c1f1e25b75a229dc_med.jpg	f	19
29	2022-03-01 22:45:36.666053+00	2022-03-17 21:45:49.682577+00	cars/IMG_6160.jpg	f	20
28	2022-03-01 22:44:29.073327+00	2022-03-17 21:46:05.172476+00	cars/IMG_6154.jpg	f	21
27	2022-03-01 22:44:12.349298+00	2022-03-17 21:47:09.31333+00	cars/3483ef809acf46da7f2b6d055a2406ed.jpg	f	23
26	2022-03-01 22:44:02.94792+00	2022-03-17 21:47:24.714618+00	cars/hyundai-elantra-2015-5npdh4ae3fh631106-img1.jpg	f	24
15	2022-03-01 22:41:50.923778+00	2022-03-17 21:47:38.97591+00	cars/hyundai_elantra__324856662f.jpg	f	25
16	2022-03-01 22:42:48.056383+00	2022-03-17 21:47:52.327689+00	cars/Hyundai_Elantra__293415308f.jpg	f	26
17	2022-03-01 22:42:54.115739+00	2022-03-17 21:48:05.344781+00	cars/hyundai-elantra__36370-460x345.jpg	f	27
18	2022-03-01 22:43:00.503434+00	2022-03-17 21:48:16.417451+00	cars/125809_CvW6z7hMgE4PlW0VCC2t0iWIK_small.jpg	f	28
19	2022-03-01 22:43:07.627926+00	2022-03-17 21:48:43.387415+00	cars/hyundai-elantra-2015-5-1200x580.jpg	f	29
20	2022-03-01 22:43:16.607529+00	2022-03-17 21:49:25.730611+00	cars/hyundai-elantra-se-2015-5npdh4ae1fh551481-img1.jpg	f	30
21	2022-03-01 22:43:23.187595+00	2022-03-17 21:49:38.367156+00	cars/1695422-134406-baabdb3a-3cbc-55dd-a5e7-a045611cd20c.jpg	f	31
22	2022-03-01 22:43:29.226048+00	2022-03-17 21:49:48.70309+00	cars/BH610320_d0840e.jpg	f	32
23	2022-03-01 22:43:35.935803+00	2022-03-17 21:50:00.58406+00	cars/10527813_847775091907201_391069862483871979_n.webp	f	33
24	2022-03-01 22:43:41.889888+00	2022-03-17 21:50:14.266273+00	cars/hyundai_elantra__411340125f.jpg	f	34
45	2022-03-01 22:49:14.613971+00	2022-03-17 21:50:24.030351+00	cars/Hyundai_Elantra__286963889f.jpg	f	35
46	2022-03-01 22:49:21.574872+00	2022-03-17 21:50:39.258524+00	cars/1819764_vVKAuuTxz1fhZqobAR3H2oLCH_small.jpg	f	36
47	2022-03-01 22:49:28.775547+00	2022-03-17 21:50:49.698521+00	cars/139575_5I8MFHSwpapygJr1HwPupiHVn_small.jpg	f	37
48	2022-03-01 22:49:44.899859+00	2022-03-17 21:50:59.10631+00	cars/slider_hyundai_elantra_2016_2018.jpeg	f	38
49	2022-03-01 22:49:52.890265+00	2022-03-17 21:51:08.147397+00	cars/hyundai-elantra__271581946-460x345.jpg	f	39
50	2022-03-01 22:49:59.95607+00	2022-03-17 21:51:22.6244+00	cars/phpThumb_generated_thumbnail_7.jpeg	f	40
51	2022-03-01 22:50:08.798181+00	2022-03-17 21:51:32.254597+00	cars/phpThumb_generated_thumbnail_8.jpeg	f	41
52	2022-03-01 22:50:20.899406+00	2022-03-17 21:51:45.837245+00	cars/2017_Hyundai_Elantra_AD_Elite_sedan_2017-12-09_01.jpg	f	42
53	2022-03-01 22:50:37.448698+00	2022-03-17 21:51:57.429653+00	cars/maxresdefault_3.jpg	f	43
54	2022-03-01 22:51:01.467524+00	2022-03-17 21:52:08.268454+00	cars/2017-hyundai-elantra-limi_1440x1080.webp	f	44
35	2022-03-01 22:47:32.330637+00	2022-03-17 21:52:21.122476+00	cars/x5_981527947l4b.jpg	f	45
36	2022-03-01 22:47:39.045302+00	2022-03-17 21:52:37.351407+00	cars/l-1605498503.0316-5fb1f68707b91.webp	f	46
37	2022-03-01 22:47:47.361965+00	2022-03-17 21:52:57.052658+00	cars/248fec2356b96e900fb0a36189ebd3225308d999.jpg	f	47
38	2022-03-01 22:47:55.443509+00	2022-03-17 21:53:14.761681+00	cars/1071535.jpg	f	48
39	2022-03-01 22:48:11.051008+00	2022-03-17 21:53:29.599482+00	cars/9a5b792d14b8a48a21b0402700f0254e.png	f	49
40	2022-03-01 22:48:18.151521+00	2022-03-17 21:54:03.251879+00	cars/hyundai_elantra__285234089f.jpg	f	50
41	2022-03-01 22:48:25.675828+00	2022-03-17 21:54:16.764307+00	cars/hyundai-elantra-2015-7.1200x1000.jpg	f	51
42	2022-03-01 22:48:41.596545+00	2022-03-17 21:54:52.620446+00	cars/7C918096-6B8D-4D0B-A2D9-FF4EDA78D21A.jpeg.jpg	f	52
43	2022-03-01 22:48:48.561599+00	2022-03-17 21:55:09.203679+00	cars/Skoda_Fabia__305491380f.jpg	f	53
44	2022-03-01 22:48:56.198781+00	2022-03-17 21:55:21.172679+00	cars/113368_tNMEq9ItQ9P6Ia8jbRTcDsFm9_small.jpg	f	54
65	2022-03-01 22:55:15.139071+00	2022-03-17 21:58:54.115816+00	cars/Kia_Carens_LX_2.0_2012.jpg	f	65
66	2022-03-01 22:55:34.639503+00	2022-03-17 21:59:11.122967+00	cars/kia_forte__431755868f.jpg	f	66
67	2022-03-01 22:55:52.715145+00	2022-03-17 21:59:20.167927+00	cars/2010kiafortereview_00.jpg	f	67
64	2022-03-01 22:55:06.9958+00	2022-03-17 21:59:37.072956+00	cars/BH418099_6a2a2d.jpg	f	68
69	2022-03-01 22:56:14.549301+00	2022-03-17 22:00:00.32936+00	cars/BF777945_e3677b.jpg	f	69
70	2022-03-01 22:56:49.253722+00	2022-03-17 22:00:24.045947+00	cars/maxresdefault_4.jpg	f	70
71	2022-03-01 22:56:59.633123+00	2022-03-17 22:00:32.113092+00	cars/BK242506_a91fec.jpg	f	71
72	2022-03-01 22:57:21.202919+00	2022-03-17 22:00:42.259919+00	cars/20073100011.webp	f	72
73	2022-03-01 22:57:31.032187+00	2022-03-17 22:00:51.453162+00	cars/BF661909_283d05.jpg	f	73
74	2022-03-01 22:57:46.030418+00	2022-03-17 22:01:03.063822+00	cars/Kia_K5_TF_02_China_2012-04-22.jpg	f	74
55	2022-03-01 22:52:25.641715+00	2022-03-17 22:01:24.919876+00	cars/998E1C3359ABDE5733.jpg	f	75
56	2022-03-01 22:52:35.638337+00	2022-03-17 22:01:57.074253+00	cars/kia-sorento__307386659-460x345.jpg	f	77
57	2022-03-01 22:53:04.02702+00	2022-03-17 22:02:18.908409+00	cars/kia_sportage__440431253-620x415x70.webp	f	78
58	2022-03-01 22:53:51.079827+00	2022-03-17 22:02:38.006593+00	cars/Mazda-3-II-Sedan-BL-facelift-2011.jpg	f	79
59	2022-03-01 22:53:59.483972+00	2022-03-17 22:02:51.28663+00	cars/2013_Mazda3_BL_Series_2_MY13_Neo_sedan_2016-01-04_02.jpg	f	80
60	2022-03-01 22:54:13.473611+00	2022-03-17 22:03:09.855528+00	cars/image.webp	f	81
61	2022-03-01 22:54:30.247717+00	2022-03-17 22:03:50.721562+00	cars/cc7d5044-aef7-4367-a303-09cd9df420db_c2f0a4ab-e3e2-4cf6-81e2-e970df7f001d.jpg	f	82
62	2022-03-01 22:54:37.675465+00	2022-03-17 22:04:06.378971+00	cars/volkswagen-polo__252738177-460x345.jpg	f	83
63	2022-03-01 22:54:46.157547+00	2022-03-17 22:04:16.607203+00	cars/Volkswagen_Polo__268720220f.jpg	f	84
4	2022-02-23 13:54:33.306099+00	2022-03-17 22:05:17.423259+00	cars/danh-gia-xe-hyundai-accent-2016-cu-uu-diem-nhieu-nhuoc-diem-cung-khong-it-e1578383154925.jpg	f	8
126	2022-03-01 23:09:33.813476+00	2022-03-17 21:20:22.477745+00	cars/000082e3dbfa-2444-4761-a.jpg	f	133
127	2022-03-01 23:09:40.43446+00	2022-03-17 21:21:04.318181+00	cars/phpThumb_generated_thumbnail.jpeg	f	132
133	2022-03-01 23:10:22.147064+00	2022-03-17 21:22:48.304545+00	cars/maxresdefault.jpg	f	126
132	2022-03-01 23:10:15.206334+00	2022-03-17 21:22:54.117258+00	cars/4293.jpg	f	127
131	2022-03-01 23:10:08.217661+00	2022-03-17 21:22:59.480133+00	cars/23cfb4616a3ea91f7af1409d8a46fa457e567235e035a2321c0892ff066e2b45.jpg	f	128
130	2022-03-01 23:10:00.24484+00	2022-03-17 21:23:04.022461+00	cars/6952342-4.jpg	f	129
87	2022-03-01 23:02:52.594233+00	2022-03-17 21:28:44.353192+00	cars/maxresdefault_1.jpg	f	93
129	2022-03-01 23:09:53.525232+00	2022-03-17 21:23:42.732862+00	cars/WhatsApp-Image-2021-01-06-at-4-16-17-PM-1.jpeg	f	130
128	2022-03-01 23:09:46.880068+00	2022-03-17 21:23:47.543985+00	cars/6958985-2.jpg	f	131
85	2022-03-01 23:02:38.140838+00	2022-03-17 21:24:24.47663+00	cars/15849811360253905_1.jpg	f	125
134	2022-03-17 21:25:50.372371+00	2022-03-17 21:25:50.37239+00	cars/02-2012-kia-rio-5-front-left-view.webp	f	85
135	2022-03-17 21:26:27.721768+00	2022-03-17 21:26:27.721788+00	cars/2012_Kia_Rio_SX_sedan_--_06-28-2012_front.jpg	f	86
93	2022-03-01 23:03:48.389861+00	2022-03-17 21:26:45.435182+00	cars/Kia-Rio-sedan-w.webp	f	87
92	2022-03-01 23:03:39.05918+00	2022-03-17 21:27:20.887363+00	cars/1200px-2012_Kia_Rio_SX_sedan_--_06-28-2012_rear.jpg	f	88
91	2022-03-01 23:03:29.96995+00	2022-03-17 21:27:41.37456+00	cars/hyundai_sonata__343859223-620x415x70.webp	f	89
90	2022-03-01 23:03:20.670333+00	2022-03-17 21:27:56.119567+00	cars/Hyundai_Sonata__335727213f.jpg	f	90
89	2022-03-01 23:03:12.339005+00	2022-03-17 21:28:11.859581+00	cars/1375893951_2013-hyundai-sonata-front.jpg	f	91
88	2022-03-01 23:03:00.982004+00	2022-03-17 21:28:30.764171+00	cars/hyundai-sonata-spo-2016-5npe34af0gh414464-img1.jpg	f	92
86	2022-03-01 23:02:45.342097+00	2022-03-17 21:28:54.111873+00	cars/hyundai-sonata-spo-2016-5npe34af0gh414464-img1_1.jpg	f	94
103	2022-03-01 23:05:15.728823+00	2022-03-17 21:29:05.263388+00	cars/5NPE24AF0GH353278_1.jpg	f	95
102	2022-03-01 23:05:07.586952+00	2022-03-17 21:29:17.820215+00	cars/20190312210522781_DSC00027.jpg	f	96
101	2022-03-01 23:04:58.711749+00	2022-03-17 21:29:35.82942+00	cars/4ec24cc5-4e7c-4274-ae04-6242f384bd03.jpg	f	97
100	2022-03-01 23:04:51.315159+00	2022-03-17 21:29:54.339691+00	cars/bccc72e1-822e-4d94-b8f4-10279e7efddc.jpg	f	98
99	2022-03-01 23:04:43.578333+00	2022-03-17 21:30:09.04621+00	cars/hyundai_sonata__322445220f.jpg	f	99
98	2022-03-01 23:04:35.064195+00	2022-03-17 21:30:25.259888+00	cars/hyundai_sonata__360965530f.jpg	f	100
97	2022-03-01 23:04:25.505405+00	2022-03-17 21:30:37.5532+00	cars/image_1633075068267-62acdaa8.jpg	f	101
96	2022-03-01 23:04:16.51274+00	2022-03-17 21:30:47.286145+00	cars/Hyundai_Sonata__272240293f.jpg	f	102
95	2022-03-01 23:04:08.332064+00	2022-03-17 21:31:10.477426+00	cars/1603994827.jpg	f	103
94	2022-03-01 23:03:59.84812+00	2022-03-17 21:32:13.088837+00	cars/Sonata-7-g.jpg	f	104
114	2022-03-01 23:07:01.879186+00	2022-03-17 21:32:33.398493+00	cars/Sonata-7.jpg	f	105
112	2022-03-01 23:06:42.255772+00	2022-03-17 21:32:45.154557+00	cars/sonata-7-sport-old1.jpg	f	106
111	2022-03-01 23:06:27.754588+00	2022-03-17 21:33:12.854631+00	cars/phpThumb_generated_thumbnail_1.jpeg	f	107
110	2022-03-01 23:06:19.489726+00	2022-03-17 21:33:23.096357+00	cars/phpThumb_generated_thumbnail_2.jpeg	f	108
109	2022-03-01 23:06:10.49066+00	2022-03-17 21:33:36.473019+00	cars/hyundai_sonata__240867153f.jpg	f	109
108	2022-03-01 23:06:02.051111+00	2022-03-17 21:33:47.954317+00	cars/hyundai_sonata__356390140f.jpg	f	110
107	2022-03-01 23:05:52.520977+00	2022-03-17 21:34:04.675336+00	cars/2035561_BpQHThoAPXpdSk8A4CsdQX0Tz.jpg	f	111
106	2022-03-01 23:05:45.090553+00	2022-03-17 21:34:15.275791+00	cars/maxresdefault_2.jpg	f	112
105	2022-03-01 23:05:36.56757+00	2022-03-17 21:34:25.584323+00	cars/phpThumb_generated_thumbnail_3.jpeg	f	113
104	2022-03-01 23:05:23.174763+00	2022-03-17 21:34:35.887094+00	cars/phpThumb_generated_thumbnail_4.jpeg	f	114
115	2022-03-01 23:07:17.130404+00	2022-03-17 21:34:47.913044+00	cars/Hyundai_Sonata__280993482f.jpg	f	115
124	2022-03-01 23:09:01.046406+00	2022-03-17 21:35:00.629975+00	cars/Hyundai_Sonata__307314383f.jpg	f	116
123	2022-03-01 23:08:45.825497+00	2022-03-17 21:35:11.058993+00	cars/phpThumb_generated_thumbnail_5.jpeg	f	117
122	2022-03-01 23:08:38.997895+00	2022-03-17 21:35:27.220164+00	cars/phpThumb_generated_thumbnail_6.jpeg	f	118
121	2022-03-01 23:08:32.789157+00	2022-03-17 21:35:40.350377+00	cars/18cfa0d72e29357b72ae348e92581325.jpg	f	119
120	2022-03-01 23:08:21.520489+00	2022-03-17 21:35:53.371203+00	cars/BH617633_0c8bc0.jpg	f	120
119	2022-03-01 23:08:08.561495+00	2022-03-17 21:36:05.062942+00	cars/a980c2b09b5d9afe58013cf59591fc5f.jpg	f	121
118	2022-03-01 23:07:59.952238+00	2022-03-17 21:36:17.003934+00	cars/4ec24cc5-4e7c-4274-ae04-6242f384bd03_1.jpg	f	122
117	2022-03-01 23:07:40.725888+00	2022-03-17 21:36:32.911018+00	cars/hyundai-sonata-spo-2016-5npe34af0gh414464-img1_2.jpg	f	123
116	2022-03-01 23:07:24.545814+00	2022-03-17 21:36:50.318851+00	cars/IMG_3857-1350x900.jpg	f	124
75	2022-03-01 22:57:55.803505+00	2022-03-17 21:55:36.776861+00	cars/Skoda_Fabia__344700639f.jpg	f	55
76	2022-03-01 22:58:18.618275+00	2022-03-17 21:56:07.684075+00	cars/1889427_NCbAqTG3NARrSBQnzdPSWcZkR.jpg	f	56
77	2022-03-01 22:58:27.690567+00	2022-03-17 21:56:21.999535+00	cars/13648661.jpg	f	57
78	2022-03-01 22:58:33.876951+00	2022-03-17 21:56:37.411684+00	cars/68051982.jpg	f	58
79	2022-03-01 22:58:58.927758+00	2022-03-17 21:57:01.301512+00	cars/hyundai-i30-2012-prata-85473-0.jpg	f	59
80	2022-03-01 22:59:31.880501+00	2022-03-17 21:57:21.559559+00	cars/1786846_g3s1zuwSfOJMHSOkxDsImEUtR_small.jpg	f	60
81	2022-03-01 22:59:42.241329+00	2022-03-17 21:57:45.013497+00	cars/img-1-600x400.webp	f	61
82	2022-03-01 22:59:55.372595+00	2022-03-17 21:57:55.677619+00	cars/1200px-VW_Jetta_V__Frontansicht_6._Mai_2011_Velbert.jpg	f	62
83	2022-03-01 23:00:04.024135+00	2022-03-17 21:58:06.956075+00	cars/08-jetta-hero.jpg	f	63
84	2022-03-01 23:00:19.382543+00	2022-03-17 21:58:31.513752+00	cars/Kia_K5__279765813f.jpg	f	64
136	2022-03-17 22:01:37.995457+00	2022-03-17 22:01:37.995475+00	cars/K-20180419-576527.png	f	76
\.


--
-- Data for Name: car_carimagetask; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carimagetask (id, created_at, updated_at, car_id, user_id) FROM stdin;
\.


--
-- Data for Name: car_carimagetask_images; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carimagetask_images (id, carimagetask_id, carimage_id) FROM stdin;
\.


--
-- Data for Name: car_carpaper; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_carpaper (id, created_at, updated_at, path, car_id) FROM stdin;
1	2022-02-23 13:55:07.860397+00	2022-02-23 13:55:07.860414+00	cars/papers/9.jpg	4
\.


--
-- Data for Name: car_case; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_case (id, name) FROM stdin;
1	Седан
\.


--
-- Data for Name: car_color; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_color (id, name) FROM stdin;
1	Белый
\.


--
-- Data for Name: car_fuel; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_fuel (id, name) FROM stdin;
1	Газ
\.


--
-- Data for Name: car_model; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_model (id, name, brand_id) FROM stdin;
1	Accent	1
2	Avante	1
3	735i	2
4	Corolla	3
5	Camry	3
6	Elantra	1
7	Fabia	4
8	I30	1
9	Jetta	5
10	K5	6
11	Carens	6
12	Forte	6
13	Sorento	6
14	Sportage	6
15	Mazda	7
16	Insignia	8
17	Passat	5
18	Polo	5
19	Rio	6
20	Sonata	1
21	Subaru	9
22	Forester	9
\.


--
-- Data for Name: car_office; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.car_office (id, name, address) FROM stdin;
1	Главный	Kyiv
\.


--
-- Data for Name: core_administrativedeposit; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.core_administrativedeposit (id, created_at, updated_at, amount, currency, note, date) FROM stdin;
\.


--
-- Data for Name: core_administrativewithdraw; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.core_administrativewithdraw (id, created_at, updated_at, amount, currency, note, date) FROM stdin;
\.


--
-- Data for Name: core_balance; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.core_balance (id, amount, currency, type) FROM stdin;
\.


--
-- Data for Name: core_currency; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.core_currency (id, ratio, date) FROM stdin;
1	28.5	2022-02-23
\.


--
-- Data for Name: credit_credit; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.credit_credit (id, created_at, updated_at, payment_count, payment_year, payment_date, car_value_total, first_payment, type, balance, percent_total, percent_expenses, is_completed, is_permitted, is_verified, is_active, payment, notes, contract_id, expenses, total_credit) FROM stdin;
21	2022-03-05 18:56:26.184742+00	2022-03-05 20:18:04.185257+00	52	1	5	5244.7	1000	W	0	30	10	f	f	f	t	0		27	524.47	6200
33	2022-03-05 19:27:41.423366+00	2022-03-05 20:17:02.102879+00	52	1	4	6622.3	500	W	0	30	10	f	f	f	t	0		39	662.23	8820
7	2022-03-03 18:54:36.321365+00	2022-03-03 22:37:28.108872+00	12	1	21	14200	1000	M	0	30	10	f	f	f	t	0		13	1420	19006
22	2022-03-05 18:58:20.672845+00	2022-03-05 20:17:58.442947+00	47	47	2	5377.6	1300	W	0	30	10	f	f	f	t	0		28	537.76	6000
8	2022-03-03 20:34:22.139376+00	2022-03-03 22:39:10.245751+00	104	2	1	7718.75	500	W	0	60	20	f	f	f	t	0		14	1543.75	14020
23	2022-03-05 18:59:45.420472+00	2022-03-05 20:17:53.563542+00	78	1.5	3	7343.3	700	W	0	45	15	f	f	f	t	0		29	1101.495	11230
9	2022-03-03 20:46:03.296158+00	2022-03-03 22:40:09.709016+00	104	2	1	6364.5	300	W	0	60	20	f	f	f	t	0		15	1272.9	11740
10	2022-03-03 20:48:18.94188+00	2022-03-03 22:40:47.145293+00	104	2	1	8125	2000	W	0	60	20	f	f	f	t	0		16	1625	12400
11	2022-03-03 20:54:13.815807+00	2022-03-03 22:41:19.657189+00	104	2	1	15537.5	2000	W	0	60	20	f	f	f	t	0		17	3107.5	26632
24	2022-03-05 19:01:38.820098+00	2022-03-05 20:17:48.38121+00	104	2	3	12187.5	1000	W	0	60	20	f	f	f	t	0		30	2437.5	21800
12	2022-03-03 21:48:39.598806+00	2022-03-05 20:18:53.033366+00	104	2	1	7041.6	400	W	0	60	20	f	f	f	t	0		18	1408.32	12880
13	2022-03-03 21:52:26.336844+00	2022-03-05 20:18:47.029717+00	78	1.5	2	7811	700	W	0	45	15	f	f	f	t	0		19	1171.65	12010
14	2022-03-05 17:28:39.602288+00	2022-03-05 20:18:40.67909+00	52	1	2	5927	0	W	0	30	10	f	f	f	t	0		20	592.7	8476
25	2022-03-05 19:02:58.437093+00	2022-03-05 20:17:43.123996+00	52	1	2	8258.7	500	W	0	30	10	f	f	f	t	0		31	825.87	11160
15	2022-03-05 17:37:44.325802+00	2022-03-05 20:18:35.543325+00	104	2	2	5241	0	W	0	60	20	f	f	f	t	0		21	1048.2	10063
16	2022-03-05 17:38:57.588665+00	2022-03-05 20:18:29.802149+00	104	2	2	8260.5	500	W	0	60	20	f	f	f	t	0		22	1652.1	15060
17	2022-03-05 17:45:38.563719+00	2022-03-05 20:18:24.505757+00	104	2	2	12187.5	1000	W	0	60	20	f	f	f	t	0		23	2437.5	21800
18	2022-03-05 17:46:55.51578+00	2022-03-05 20:18:19.124151+00	104	2	2	4279	0	W	0	60	20	f	f	f	t	0		24	855.8	8216
19	2022-03-05 18:52:48.302375+00	2022-03-05 20:18:13.784718+00	52	1	2	5454.5	0	W	0	30	10	f	f	f	t	0		25	545.45	7800
20	2022-03-05 18:54:12.877393+00	2022-03-05 20:18:09.11781+00	52	1	2	16146.8	4500	W	0	30	10	f	f	f	t	0		26	1614.68	17240
26	2022-03-05 19:04:21.806877+00	2022-03-05 20:17:37.525638+00	104	2	3	7447.9	300	W	0	60	20	f	f	f	t	0		32	1489.58	13820
27	2022-03-05 19:05:19.774066+00	2022-03-05 20:17:32.814841+00	104	2	3	5822.9	300	W	0	60	20	f	f	f	t	0		33	1164.58	10700
28	2022-03-05 19:06:39.462689+00	2022-03-05 20:17:28.308464+00	104	2	3	10833.3	800	W	0	60	20	f	f	f	t	0		34	2166.66	19520
29	2022-03-05 19:08:07.911181+00	2022-03-05 20:17:22.764058+00	52	1	4	7499.3	1000	W	0	30	10	f	f	f	t	0		35	749.93	9424
30	2022-03-05 19:23:33.38139+00	2022-03-05 20:17:17.574498+00	104	2	4	8612.5	1000	W	0	60	20	f	f	f	t	0		36	1722.5	14936
31	2022-03-05 19:24:26.748883+00	2022-03-05 20:17:12.374864+00	104	2	4	10331.25	1000	W	0	60	20	f	f	f	t	0		37	2066.25	18236
32	2022-03-05 19:26:10.34021+00	2022-03-05 20:17:07.247482+00	78	1.5	4	6795.2	900	W	0	45	15	f	f	f	t	0		38	1019.28	10026
34	2022-03-05 19:41:51.983096+00	2022-03-05 20:16:56.680569+00	52	1	5	6986	500	W	0	30	10	f	f	f	t	0		40	698.6	9340
35	2022-03-05 19:43:04.845762+00	2022-03-05 20:16:51.326591+00	52	1	5	6986	500	W	0	30	10	f	f	f	t	0		41	698.6	9340
39	2022-03-05 19:48:22.166765+00	2022-03-05 20:16:28.587925+00	52	1	5	6664.3	300	W	0	30	10	f	f	f	t	0		45	666.43	9140
36	2022-03-05 19:44:32.257939+00	2022-03-05 20:16:45.854991+00	104	2	5	9354	500	W	0	60	20	f	f	f	t	0		42	1870.8	17160
37	2022-03-05 19:45:46.201508+00	2022-03-05 20:16:40.0656+00	52	1	5	9090.9	0	W	0	30	10	f	f	f	t	0		43	909.09	13000
38	2022-03-05 19:47:07.625145+00	2022-03-05 20:16:34.113924+00	52	1	5	8849.6	350	W	0	30	10	f	f	f	t	0		44	884.96	12200
40	2022-03-05 19:49:38.217551+00	2022-03-05 20:16:23.268798+00	78	1.5	5	7470.4	500	W	0	45	15	f	f	f	t	0		46	1120.56	11732
41	2022-03-05 19:51:01.04473+00	2022-03-05 20:16:17.847952+00	104	2	5	12187.5	1000	W	0	60	20	f	f	f	t	0		47	2437.5	21800
42	2022-03-05 19:51:48.990162+00	2022-03-05 20:16:12.333382+00	104	2	5	10562.5	1000	W	0	60	20	f	f	f	t	0		48	2112.5	18680
43	2022-03-05 19:52:45.271687+00	2022-03-05 20:16:07.346023+00	104	2	5	8125	400	W	0	60	20	f	f	f	t	0		49	1625	14960
44	2022-03-05 19:57:51.390975+00	2022-03-05 20:16:01.979426+00	104	2	5	10833.3	800	W	0	60	20	f	f	f	t	0		50	2166.66	19520
45	2022-03-05 19:59:02.93484+00	2022-03-05 20:15:56.49549+00	104	2	5	7312.5	0	W	0	60	20	f	f	f	t	0		51	1462.5	14040
46	2022-03-05 20:00:53.200524+00	2022-03-05 20:15:51.162797+00	52	1	5	12117.5	1000	W	0	30	10	f	f	f	t	0		52	1211.75	16028
47	2022-03-05 20:05:23.506905+00	2022-03-13 13:32:31.778505+00	104	2	2	5416.6	0	W	0	60	20	f	f	f	t	0		53	1083.32	10400
48	2022-03-05 20:07:14.654492+00	2022-03-12 10:56:48.691208+00	52	1	4	20000	1000	W	0	30	10	f	f	f	t	0		54	2000	27300
\.


--
-- Data for Name: credit_creditimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.credit_creditimage (id, created_at, updated_at, path, credit_id) FROM stdin;
2	2022-02-23 22:12:33.14151+00	2022-02-23 22:12:33.141536+00	credit_images/1_1ciMZRs.jpg	\N
3	2022-02-23 22:17:16.397411+00	2022-02-23 22:17:16.39743+00	credit_images/6.jpg	\N
4	2022-02-23 22:17:42.443917+00	2022-02-23 22:17:42.443935+00	credit_images/7.jpg	\N
5	2022-02-23 22:18:07.637458+00	2022-02-23 22:18:07.637475+00	credit_images/5.webp	\N
6	2022-02-23 22:18:39.160458+00	2022-02-23 22:18:39.160474+00	credit_images/8.jpg	\N
7	2022-03-03 20:50:56.683687+00	2022-03-03 20:50:56.683705+00	credit_images/3.png	10
8	2022-03-03 20:51:18.415201+00	2022-03-03 20:51:18.415221+00	credit_images/3_w03m1rM.png	7
9	2022-03-03 20:51:29.439262+00	2022-03-03 20:51:29.439281+00	credit_images/3_GcxR9UB.png	9
10	2022-03-03 20:51:35.711713+00	2022-03-03 20:51:35.711732+00	credit_images/3_q3mxgc5.png	8
11	2022-03-03 20:54:29.931057+00	2022-03-03 20:54:29.931074+00	credit_images/3_bVQxT0W.png	11
12	2022-03-05 20:15:39.644186+00	2022-03-05 20:15:39.644204+00	credit_images/1_ujOc4Lh.jpg	48
13	2022-03-05 20:15:45.150346+00	2022-03-05 20:15:45.150368+00	credit_images/1_6QWP39a.jpg	47
14	2022-03-05 20:15:51.164146+00	2022-03-05 20:15:51.164163+00	credit_images/1_ZhtLwMK.jpg	46
15	2022-03-05 20:15:56.496884+00	2022-03-05 20:15:56.496902+00	credit_images/1_O6tyEGj.jpg	45
16	2022-03-05 20:16:01.980846+00	2022-03-05 20:16:01.980865+00	credit_images/1_Zb8DIGF.jpg	44
17	2022-03-05 20:16:07.347128+00	2022-03-05 20:16:07.347145+00	credit_images/1_V9TXVIH.jpg	43
18	2022-03-05 20:16:12.335866+00	2022-03-05 20:16:12.335886+00	credit_images/1_Tioo4oY.jpg	42
19	2022-03-05 20:16:17.84948+00	2022-03-05 20:16:17.849505+00	credit_images/1_SMfebwY.jpg	41
20	2022-03-05 20:16:23.269986+00	2022-03-05 20:16:23.270004+00	credit_images/1_ih1k3vl.jpg	40
21	2022-03-05 20:16:28.589183+00	2022-03-05 20:16:28.5892+00	credit_images/1_frnev9m.jpg	39
22	2022-03-05 20:16:34.115048+00	2022-03-05 20:16:34.115075+00	credit_images/1_ForRyHy.jpg	38
23	2022-03-05 20:16:40.066745+00	2022-03-05 20:16:40.066764+00	credit_images/1_Ah65W4a.jpg	37
24	2022-03-05 20:16:45.856499+00	2022-03-05 20:16:45.856518+00	credit_images/1_uytCHTi.jpg	36
25	2022-03-05 20:16:51.32776+00	2022-03-05 20:16:51.327776+00	credit_images/1_xrcYROY.jpg	35
26	2022-03-05 20:16:56.681716+00	2022-03-05 20:16:56.681733+00	credit_images/1_Xfww5if.jpg	34
27	2022-03-05 20:17:02.104263+00	2022-03-05 20:17:02.104282+00	credit_images/1_5f47aQW.jpg	33
28	2022-03-05 20:17:07.249037+00	2022-03-05 20:17:07.249063+00	credit_images/1_XqtbwVz.jpg	32
29	2022-03-05 20:17:12.376283+00	2022-03-05 20:17:12.376302+00	credit_images/1_2T9DbQa.jpg	31
30	2022-03-05 20:17:17.575765+00	2022-03-05 20:17:17.575783+00	credit_images/1_8rRx2Ud.jpg	30
31	2022-03-05 20:17:22.766194+00	2022-03-05 20:17:22.766217+00	credit_images/1_LWR6hHM.jpg	29
32	2022-03-05 20:17:28.309703+00	2022-03-05 20:17:28.30972+00	credit_images/1_lvk4vvv.jpg	28
33	2022-03-05 20:17:32.816068+00	2022-03-05 20:17:32.816085+00	credit_images/1_NhllJ0N.jpg	27
34	2022-03-05 20:17:37.526858+00	2022-03-05 20:17:37.526875+00	credit_images/1_HCx7W5o.jpg	26
35	2022-03-05 20:17:43.125381+00	2022-03-05 20:17:43.125399+00	credit_images/1_mPpAhd2.jpg	25
36	2022-03-05 20:17:48.382471+00	2022-03-05 20:17:48.382488+00	credit_images/1_Ua1XZ54.jpg	24
37	2022-03-05 20:17:53.564643+00	2022-03-05 20:17:53.56466+00	credit_images/1_OThRuvU.jpg	23
38	2022-03-05 20:17:58.444176+00	2022-03-05 20:17:58.444193+00	credit_images/1_5viNcJM.jpg	22
39	2022-03-05 20:18:04.186656+00	2022-03-05 20:18:04.186676+00	credit_images/1_0AbHvWF.jpg	21
40	2022-03-05 20:18:09.119403+00	2022-03-05 20:18:09.119423+00	credit_images/1_gu7C7v7.jpg	20
41	2022-03-05 20:18:13.785907+00	2022-03-05 20:18:13.785925+00	credit_images/1_EGrReXS.jpg	19
42	2022-03-05 20:18:19.125524+00	2022-03-05 20:18:19.125542+00	credit_images/1_zp6vDV4.jpg	18
43	2022-03-05 20:18:24.507077+00	2022-03-05 20:18:24.507096+00	credit_images/1_mlwuSbf.jpg	17
44	2022-03-05 20:18:29.803982+00	2022-03-05 20:18:29.804002+00	credit_images/1_bPLlxAx.jpg	16
45	2022-03-05 20:18:35.544475+00	2022-03-05 20:18:35.544492+00	credit_images/1_nt7FTWl.jpg	15
46	2022-03-05 20:18:40.680241+00	2022-03-05 20:18:40.680258+00	credit_images/1_uaOiKqd.jpg	14
47	2022-03-05 20:18:47.030901+00	2022-03-05 20:18:47.030919+00	credit_images/1_92txmvA.jpg	13
48	2022-03-05 20:18:53.034481+00	2022-03-05 20:18:53.034497+00	credit_images/1_eSZEDUW.jpg	12
\.


--
-- Data for Name: credit_creditpayment; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.credit_creditpayment (id, created_at, updated_at, date, debt_week, debt_total, payment_left, amount, amount_uah, total_amount, is_verified, credit_id) FROM stdin;
1	2022-02-23 22:12:33.144479+00	2022-02-23 22:12:33.144495+00	2022-02-23	0	0	0	500	0	500	f	\N
2	2022-02-23 22:17:16.401037+00	2022-02-23 22:17:16.401054+00	2022-02-23	0	0	0	1000	0	1000	f	\N
3	2022-02-23 22:17:42.447028+00	2022-02-23 22:17:42.447045+00	2022-02-23	0	0	0	500	0	500	f	\N
4	2022-02-23 22:18:07.639992+00	2022-02-23 22:18:07.640015+00	2022-02-23	0	0	0	500	0	500	f	\N
6	2022-02-23 22:20:01.651914+00	2022-02-23 22:20:01.65195+00	2022-02-24	-480	-480	600	100	0	100	f	\N
7	2022-02-24 21:46:55.436413+00	2022-02-24 21:46:55.436442+00	2022-02-24	20	-460	700	600	0	600	f	\N
8	2022-02-24 21:50:05.541956+00	2022-02-24 21:50:05.541984+00	2022-02-24	-20	-480	1260	560	0	560	f	\N
9	2022-02-24 21:50:27.730658+00	2022-02-24 21:50:27.7307+00	2022-02-24	20	-460	1860	600	0	600	f	\N
10	2022-02-24 21:50:33.851021+00	2022-02-24 21:50:33.851056+00	2022-02-24	0	-460	2440	580	0	580	f	\N
11	2022-02-24 21:50:47.393425+00	2022-02-24 21:50:47.393456+00	2022-02-24	440	-20	3460	1020	0	1020	f	\N
12	2022-02-24 21:51:46.297487+00	2022-02-24 21:51:46.297509+00	2022-02-24	163	163	1326	826	0	826	f	\N
13	2022-02-24 21:53:46.605408+00	2022-02-24 21:53:46.605446+00	2022-02-24	-163	0	1826	500	0	500	f	\N
14	2022-02-24 21:53:55.702508+00	2022-02-24 21:53:55.702533+00	2022-02-24	0	0	2489	663	0	663	f	\N
15	2022-02-24 21:54:09.186007+00	2022-02-24 21:54:09.186036+00	2022-02-24	0	0	3152	663	0	663	f	\N
16	2022-02-24 21:54:18.414328+00	2022-02-24 21:54:18.414351+00	2022-02-24	-1	-1	3814	662	0	662	f	\N
17	2022-02-24 21:54:38.413138+00	2022-02-24 21:54:38.413158+00	2022-02-24	0	-1	4477	663	0	663	f	\N
18	2022-02-24 21:54:45.654138+00	2022-02-24 21:54:45.65416+00	2022-02-24	1	0	5141	664	0	664	f	\N
19	2022-02-24 21:55:24.693223+00	2022-02-24 21:55:24.693251+00	2022-02-24	28646	28646	34450	29309	0	29309	f	\N
44	2022-03-12 10:47:46.008881+00	2022-03-12 10:47:46.008902+00	2022-03-01	-225	-225	27000	300	0	300	f	48
45	2022-03-12 10:47:59.848455+00	2022-03-12 10:47:59.848479+00	2022-03-02	-125	-350	26600	400	0	400	f	48
46	2022-03-12 10:48:19.139566+00	2022-03-12 10:48:19.139592+00	2022-03-03	-325	-675	26400	200	0	200	f	48
47	2022-03-12 10:48:24.536459+00	2022-03-12 10:48:24.536497+00	2022-03-04	-25	-700	25900	500	0	500	f	48
48	2022-03-12 10:48:28.793894+00	2022-03-12 10:48:28.793927+00	2022-03-05	75	-625	25300	600	0	600	f	48
49	2022-03-12 10:48:46.349788+00	2022-03-12 10:48:46.349806+00	2022-03-06	775	150	24000	1300	0	1300	f	48
50	2022-03-12 10:50:02.302543+00	2022-03-12 10:50:02.302577+00	2022-03-07	-25	125	23500	500	0	500	f	48
51	2022-03-12 10:50:15.689539+00	2022-03-12 10:50:15.689562+00	2022-03-08	75	200	22900	600	0	600	f	48
52	2022-03-12 10:53:17.547735+00	2022-03-12 10:53:17.547768+00	2022-03-09	75	275	22300	600	0	600	f	48
59	2022-03-13 13:33:01.817237+00	2022-03-13 13:33:01.81726+00	2022-03-01	0	0	10300	100	0	100	f	47
60	2022-03-13 13:33:12.774301+00	2022-03-13 13:33:12.774321+00	2022-03-03	-50	-50	10250	50	0	50	f	47
63	2022-03-13 13:35:04.247815+00	2022-03-13 13:35:04.247845+00	2022-03-04	-50	-100	10200	50	0	50	f	47
61	2022-03-13 13:33:25.849116+00	2022-03-13 13:35:04.253082+00	2022-03-05	50	-50	10050	150	0	150	f	47
62	2022-03-13 13:33:38.027347+00	2022-03-13 13:35:04.258027+00	2022-03-07	-90	-140	10040	10	0	10	f	47
64	2022-03-17 22:30:27.666925+00	2022-03-17 22:30:27.666948+00	2022-03-18	-208.23	-208.23	14928	100	0	100	f	46
65	2022-03-19 15:09:35.434868+00	2022-03-19 15:09:35.434904+00	2022-03-19	-425	-150	22200	100	0	100	f	48
66	2022-03-24 20:39:07.541103+00	2022-03-24 20:39:07.541151+00	2022-03-24	9475	9325	12200	10000	0	10000	f	48
67	2022-03-24 20:52:57.076915+00	2022-03-24 20:52:57.076941+00	2022-03-24	4900	4760	5040	5000	0	5000	f	47
\.


--
-- Data for Name: debt_debt; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.debt_debt (id, created_at, updated_at, created_date, paid_date, amount, status, is_active, car_id, customer_id, note) FROM stdin;
1	2022-03-19 17:47:06.685919+00	2022-03-24 18:33:11.548194+00	2022-03-19	\N	10000	N	t	25	41	Долг от штрафа.
4	2022-03-24 16:59:04.254468+00	2022-03-24 18:33:17.152341+00	2022-03-24	\N	5000	N	t	47	42	Долг от штрафа.
3	2022-03-24 16:58:46.868243+00	2022-03-24 18:33:19.673495+00	2022-03-24	2022-03-24	1000	P	t	8	42	Долг от штрафа.
2	2022-03-19 17:49:30.20818+00	2022-03-24 18:34:13.442859+00	2022-03-19	\N	2500	N	t	111	42	Долг из выкуп.
5	2022-03-24 18:35:02.608948+00	2022-03-24 18:37:03.153901+00	2022-03-24	\N	5000	N	t	9	16	Долг от аренды
6	2022-03-24 18:37:44.814217+00	2022-03-24 18:37:44.814237+00	2022-03-24	\N	550	N	t	18	19	Долг от аренды такси.
7	2022-03-24 18:38:39.614455+00	2022-03-24 18:38:39.614476+00	2022-03-24	\N	1500	N	t	9	16	Долг от аренды такси.
8	2022-03-24 18:39:09.599715+00	2022-03-24 18:39:09.599739+00	2022-03-24	\N	600	N	t	19	19	Долг от аренды такси.
\.


--
-- Data for Name: debt_debtimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.debt_debtimage (id, file, debt_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2022-02-23 13:25:23.264617+00	1	Hyundai	1	[{"added": {}}]	14	1
2	2022-02-23 13:25:31.674956+00	1	Accent	1	[{"added": {}}]	22	1
3	2022-02-23 13:26:02.851035+00	1	Главный	1	[{"added": {}}]	21	1
4	2022-02-23 13:26:18.453732+00	1	Accent - 2011 - АІ 0315 MX 	1	[{"added": {}}]	15	1
5	2022-02-23 13:26:34.994616+00	2	Accent - 2017 - АІ 0312 MX 	1	[{"added": {}}]	15	1
6	2022-02-23 13:26:48.66439+00	3	Accent - 2019 - АI 4463 MC 	1	[{"added": {}}]	15	1
7	2022-02-23 13:27:00.292952+00	4	Accent - 2008 - АI 8250 OB 	1	[{"added": {}}]	15	1
8	2022-02-23 13:27:14.671143+00	5	Accent - 9756 - АI 9756 IH 	1	[{"added": {}}]	15	1
9	2022-02-23 13:27:35.235462+00	5	Accent - 2010 - АI 9756 IH 	2	[{"changed": {"fields": ["Year"]}}]	15	1
10	2022-02-23 13:27:42.246976+00	6	Accent - 2008 - КА 0729 АР 	1	[{"added": {}}]	15	1
11	2022-02-23 13:27:53.899655+00	7	Accent - 2011 - КA 2219 ВВ 	1	[{"added": {}}]	15	1
12	2022-02-23 13:28:06.236759+00	8	Accent - 2016 - AE 7193 KO 	1	[{"added": {}}]	15	1
13	2022-02-23 13:28:17.774702+00	2	Avente	1	[{"added": {}}]	22	1
14	2022-02-23 13:28:26.859559+00	9	Avente - 2017 - АІ 8064 MO 	1	[{"added": {}}]	15	1
15	2022-02-23 13:28:39.612415+00	10	Avente - 2016 - АІ 8243 OВ 	1	[{"added": {}}]	15	1
16	2022-02-23 13:33:41.14651+00	1	Seyidov Elvin	2	[{"changed": {"fields": ["First name", "Last name"]}}, {"added": {"name": "user phone", "object": ""}}]	8	1
17	2022-02-23 13:34:01.286603+00	1	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	1	[{"added": {}}]	9	1
18	2022-02-23 13:34:08.305951+00	2	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	1	[{"added": {}}]	9	1
19	2022-02-23 13:34:15.369993+00	3	Seyidov Elvin - Accent - 2019 - АI 4463 MC  - Taxi	1	[{"added": {}}]	9	1
20	2022-02-23 13:34:24.270435+00	4	Seyidov Elvin - Accent - 2008 - АI 8250 OB  - Taxi	1	[{"added": {}}]	9	1
21	2022-02-23 13:34:34.061906+00	5	Seyidov Elvin - Accent - 2010 - АI 9756 IH  - Taxi	1	[{"added": {}}]	9	1
22	2022-02-23 13:34:42.911911+00	6	Seyidov Elvin - Accent - 2008 - КА 0729 АР  - Taxi	1	[{"added": {}}]	9	1
23	2022-02-23 13:34:51.785734+00	7	Seyidov Elvin - Accent - 2011 - КA 2219 ВВ  - Taxi	1	[{"added": {}}]	9	1
24	2022-02-23 13:35:00.150711+00	8	Seyidov Elvin - Accent - 2016 - AE 7193 KO  - Taxi	1	[{"added": {}}]	9	1
25	2022-02-23 13:35:07.930723+00	9	Seyidov Elvin - Avente - 2017 - АІ 8064 MO  - Taxi	1	[{"added": {}}]	9	1
26	2022-02-23 13:35:15.631519+00	10	Seyidov Elvin - Avente - 2016 - АІ 8243 OВ  - Taxi	1	[{"added": {}}]	9	1
27	2022-02-23 13:35:32.115294+00	1	Taxi object (1)	1	[{"added": {}}]	34	1
28	2022-02-23 13:35:40.899248+00	1	Taxi object (1)	2	[]	34	1
29	2022-02-23 13:35:46.330166+00	2	Taxi object (2)	1	[{"added": {}}]	34	1
30	2022-02-23 13:35:54.793656+00	3	Taxi object (3)	1	[{"added": {}}]	34	1
31	2022-02-23 13:35:59.74103+00	4	Taxi object (4)	1	[{"added": {}}]	34	1
32	2022-02-23 13:36:05.234824+00	5	Taxi object (5)	1	[{"added": {}}]	34	1
33	2022-02-23 13:36:10.566835+00	6	Taxi object (6)	1	[{"added": {}}]	34	1
34	2022-02-23 13:36:14.440729+00	7	Taxi object (7)	1	[{"added": {}}]	34	1
35	2022-02-23 13:36:19.315024+00	8	Taxi object (8)	1	[{"added": {}}]	34	1
36	2022-02-23 13:36:22.918662+00	9	Taxi object (9)	1	[{"added": {}}]	34	1
37	2022-02-23 13:36:29.117188+00	10	Taxi object (10)	1	[{"added": {}}]	34	1
38	2022-02-23 13:40:33.15992+00	1	990c9d166decaad5c05138bd06d12e60267d0a06	1	[{"added": {}}]	7	1
39	2022-02-23 13:53:02.544742+00	2	Avante	2	[{"changed": {"fields": ["Name"]}}]	22	1
40	2022-02-23 13:53:06.140376+00	10	Avante - 2016 - АІ 8243 OВ 	2	[]	15	1
41	2022-02-23 13:53:50.057967+00	10	Avante - 2016 - АІ 8243 OВ 	2	[{"added": {"name": "car image", "object": "Avante - 2016 - \\u0410\\u0406 8243 O\\u0412 "}}]	15	1
42	2022-02-23 13:53:58.259549+00	9	Avante - 2017 - АІ 8064 MO 	2	[{"added": {"name": "car image", "object": "Avante - 2017 - \\u0410\\u0406 8064 MO "}}]	15	1
43	2022-02-23 13:54:11.327492+00	1	Accent - 2011 - АІ 0315 MX 	2	[{"added": {"name": "car image", "object": "Accent - 2011 - \\u0410\\u0406 0315 MX "}}]	15	1
44	2022-02-23 13:54:33.30774+00	8	Accent - 2016 - AE 7193 KO 	2	[{"added": {"name": "car image", "object": "Accent - 2016 - AE 7193 KO "}}]	15	1
45	2022-02-23 13:54:43.658556+00	7	Accent - 2011 - КA 2219 ВВ 	2	[{"added": {"name": "car image", "object": "Accent - 2011 - \\u041aA 2219 \\u0412\\u0412 "}}]	15	1
46	2022-02-23 13:54:51.536728+00	6	Accent - 2008 - КА 0729 АР 	2	[{"added": {"name": "car image", "object": "Accent - 2008 - \\u041a\\u0410 0729 \\u0410\\u0420 "}}]	15	1
47	2022-02-23 13:54:58.367787+00	5	Accent - 2010 - АI 9756 IH 	2	[{"added": {"name": "car image", "object": "Accent - 2010 - \\u0410I 9756 IH "}}]	15	1
48	2022-02-23 13:55:07.863643+00	4	Accent - 2008 - АI 8250 OB 	2	[{"added": {"name": "car paper", "object": "Accent - 2008 - \\u0410I 8250 OB "}}]	15	1
49	2022-02-23 13:55:14.0175+00	3	Accent - 2019 - АI 4463 MC 	2	[{"added": {"name": "car image", "object": "Accent - 2019 - \\u0410I 4463 MC "}}]	15	1
50	2022-02-23 13:55:24.897475+00	2	Accent - 2017 - АІ 0312 MX 	2	[{"added": {"name": "car image", "object": "Accent - 2017 - \\u0410\\u0406 0312 MX "}}]	15	1
51	2022-02-23 14:02:42.09367+00	4	Accent - 2008 - АI 8250 OB 	2	[{"added": {"name": "car image", "object": "Accent - 2008 - \\u0410I 8250 OB "}}]	15	1
52	2022-02-23 14:28:15.165262+00	2	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
53	2022-02-23 14:28:29.350068+00	2	Mammadxanli Farid	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
54	2022-02-23 14:28:52.685932+00	1	Mammadxanli Farid - Accent - 2011 - АІ 0315 MX  - Taxi	2	[{"changed": {"fields": ["Customer", "Start date", "End date"]}}]	9	1
55	2022-02-23 14:28:58.820402+00	1	Mammadxanli Farid - Accent - 2011 - АІ 0315 MX  - Taxi	2	[]	9	1
56	2022-02-23 14:29:13.399077+00	11	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	1	[{"added": {}}]	9	1
57	2022-02-23 14:29:29.426695+00	11	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	2	[{"changed": {"fields": ["Start date"]}}]	9	1
58	2022-02-23 14:29:42.56526+00	11	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	2	[{"changed": {"fields": ["Start date"]}}]	9	1
59	2022-02-23 14:31:58.531438+00	1	Mammadxanli Farid - Accent - 2011 - АІ 0315 MX  - Taxi	2	[{"changed": {"fields": ["Start date"]}}]	9	1
60	2022-02-23 14:48:20.068805+00	1	Taxi object (1)	2	[{"added": {"name": "revenue", "object": "Revenue object (1)"}}, {"added": {"name": "revenue", "object": "Revenue object (2)"}}, {"added": {"name": "revenue", "object": "Revenue object (3)"}}, {"added": {"name": "revenue", "object": "Revenue object (4)"}}, {"added": {"name": "revenue", "object": "Revenue object (5)"}}]	34	1
61	2022-02-23 14:56:06.29238+00	1	Bolt	1	[{"added": {}}]	32	1
62	2022-02-23 14:56:34.508608+00	2	Uber	1	[{"added": {}}]	32	1
63	2022-02-23 14:56:53.400956+00	3	Uklon	1	[{"added": {}}]	32	1
64	2022-02-23 14:57:55.170546+00	1	Revenue object (1)	2	[{"added": {"name": "revenue item", "object": "RevenueItem object (1)"}}]	33	1
65	2022-02-23 14:58:22.512854+00	1	Revenue object (1)	3		33	1
66	2022-02-23 14:58:29.262659+00	5	Revenue object (5)	3		33	1
67	2022-02-23 14:58:29.264756+00	4	Revenue object (4)	3		33	1
68	2022-02-23 14:58:29.265809+00	3	Revenue object (3)	3		33	1
69	2022-02-23 14:58:29.266702+00	2	Revenue object (2)	3		33	1
70	2022-02-23 14:59:46.145791+00	7	Revenue object (7)	3		33	1
71	2022-02-23 14:59:46.147441+00	6	Revenue object (6)	3		33	1
72	2022-02-23 15:06:56.164633+00	8	Revenue object (8)	2	[{"deleted": {"name": "revenue item", "object": "RevenueItem object (None)"}}]	33	1
73	2022-02-23 15:07:00.196466+00	8	Revenue object (8)	3		33	1
74	2022-02-23 15:13:15.775288+00	12	Revenue object (12)	3		33	1
75	2022-02-23 15:13:15.777106+00	11	Revenue object (11)	3		33	1
76	2022-02-23 15:13:15.777937+00	10	Revenue object (10)	3		33	1
77	2022-02-23 15:13:15.778786+00	9	Revenue object (9)	3		33	1
78	2022-02-23 15:39:39.337202+00	22	Revenue object (22)	3		33	1
79	2022-02-23 15:39:39.338945+00	21	Revenue object (21)	3		33	1
80	2022-02-23 15:39:39.339783+00	20	Revenue object (20)	3		33	1
81	2022-02-23 15:39:39.340568+00	19	Revenue object (19)	3		33	1
82	2022-02-23 15:39:39.341344+00	18	Revenue object (18)	3		33	1
83	2022-02-23 15:39:39.342165+00	17	Revenue object (17)	3		33	1
84	2022-02-23 15:39:39.342953+00	16	Revenue object (16)	3		33	1
85	2022-02-23 15:39:39.343677+00	15	Revenue object (15)	3		33	1
86	2022-02-23 15:39:39.344364+00	14	Revenue object (14)	3		33	1
87	2022-02-23 15:39:39.345098+00	13	Revenue object (13)	3		33	1
88	2022-02-23 15:44:56.337361+00	25	Revenue object (25)	1	[{"added": {}}, {"added": {"name": "revenue item", "object": "RevenueItem object (51)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (52)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (53)"}}]	33	1
89	2022-02-23 15:49:35.417962+00	25	Revenue object (25)	3		33	1
90	2022-02-23 15:49:35.419751+00	24	Revenue object (24)	3		33	1
91	2022-02-23 15:49:35.42067+00	23	Revenue object (23)	3		33	1
92	2022-02-23 15:49:41.785951+00	26	Revenue object (26)	3		33	1
93	2022-02-23 15:50:25.486029+00	28	Revenue object (28)	3		33	1
94	2022-02-23 15:50:25.48774+00	27	Revenue object (27)	3		33	1
95	2022-02-23 15:51:44.625038+00	29	Revenue object (29)	3		33	1
96	2022-02-23 15:52:47.306266+00	30	Revenue object (30)	3		33	1
97	2022-02-23 18:28:04.859043+00	33	Revenue object (33)	3		33	1
98	2022-02-23 18:28:04.861018+00	32	Revenue object (32)	3		33	1
99	2022-02-23 18:28:04.861928+00	31	Revenue object (31)	3		33	1
100	2022-02-23 22:08:27.077776+00	12	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Credit	1	[{"added": {}}]	9	1
101	2022-02-23 22:12:30.70697+00	1	28.5	1	[{"added": {}}]	27	1
102	2022-02-23 22:12:33.150092+00	2	Avante - 2016 - АІ 8243 OВ  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (2)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
103	2022-02-23 22:16:45.878708+00	2	Avante - 2016 - АІ 8243 OВ  - W	2	[]	29	1
104	2022-02-23 22:17:16.404871+00	3	Accent - 2017 - АІ 0312 MX  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (3)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
105	2022-02-23 22:17:42.450483+00	4	Accent - 2010 - АI 9756 IH  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (4)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
106	2022-02-23 22:18:07.643549+00	5	Accent - 2011 - КA 2219 ВВ  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (5)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
107	2022-02-23 22:18:39.16612+00	6	Accent - 2011 - АІ 0315 MX  - W	1	[{"added": {}}, {"added": {"name": "credit image", "object": "CreditImage object (6)"}}, {"added": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
108	2022-02-23 22:27:24.388815+00	6	Accent - 2011 - АІ 0315 MX  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-02-23"}}]	29	1
109	2022-03-01 19:46:19.888296+00	11	Avante - 2014 - КА 3676 ВО 	1	[{"added": {}}]	15	1
110	2022-03-01 19:46:43.550415+00	12	Avante - 2016 - KA 9980 AT 	1	[{"added": {}}]	15	1
111	2022-03-01 19:47:53.709534+00	12	Avante - 2016 - KA 9980 AT 	2	[{"changed": {"fields": ["Office"]}}]	15	1
112	2022-03-01 20:16:18.829882+00	13	Avante - 2012 - AІ 9149 МН 	1	[{"added": {}}]	15	1
113	2022-03-01 20:19:17.076007+00	2	BMW	1	[{"added": {}}]	14	1
114	2022-03-01 20:19:27.090161+00	3	735i	1	[{"added": {}}]	22	1
115	2022-03-01 20:19:42.7032+00	14	735i - 2002 - AI 3839 ОС 	1	[{"added": {}}]	15	1
116	2022-03-01 20:20:14.392499+00	3	Toyota	1	[{"added": {}}]	14	1
117	2022-03-01 20:20:17.687662+00	4	Corolla	1	[{"added": {}}]	22	1
118	2022-03-01 20:20:38.247813+00	5	Camry	1	[{"added": {}}]	22	1
119	2022-03-01 20:20:43.076891+00	15	Camry - 2015 - AI 6531 OC 	1	[{"added": {}}]	15	1
120	2022-03-01 20:21:01.379677+00	16	Corolla - 2014 - АІ 2541 КО 	1	[{"added": {}}]	15	1
121	2022-03-01 20:21:19.073393+00	17	Corolla - 2018 - АІ 7462 ОВ 	1	[{"added": {}}]	15	1
122	2022-03-01 20:21:54.718697+00	18	Corolla - 2011 - АІ 6453 ОО 	1	[{"added": {}}]	15	1
123	2022-03-01 20:22:22.558068+00	19	Corolla - 2007 - АІ 8891 ІК 	1	[{"added": {}}]	15	1
124	2022-03-01 20:22:36.415763+00	20	Corolla - 2016 - АІ 9423 ІМ 	1	[{"added": {}}]	15	1
125	2022-03-01 20:22:55.246238+00	21	Corolla - 2015 - КА 2026 ВВ 	1	[{"added": {}}]	15	1
126	2022-03-01 20:23:12.362465+00	6	Elantra	1	[{"added": {}}]	22	1
127	2022-03-01 20:23:30.795742+00	22	Elantra - 2016 - АІ 3605 ОС 	1	[{"added": {}}]	15	1
128	2022-03-01 20:23:42.932868+00	23	Elantra - 2017 - АІ 5508 МР 	1	[{"added": {}}]	15	1
129	2022-03-01 20:23:59.294083+00	24	Elantra - 2016 - ВМ 0588 СМ 	1	[{"added": {}}]	15	1
130	2022-03-01 20:24:27.525851+00	25	Elantra - 2018 - ВМ 0610 СМ 	1	[{"added": {}}]	15	1
131	2022-03-01 20:24:41.294319+00	26	Elantra - 2017 - KA 0514 ВН 	1	[{"added": {}}]	15	1
132	2022-03-01 20:24:58.634674+00	27	Elantra - 2016 - KA 0574 ВМ 	1	[{"added": {}}]	15	1
133	2022-03-01 20:25:14.384286+00	28	Elantra - 2017 - KA 2816 ВВ 	1	[{"added": {}}]	15	1
134	2022-03-01 20:25:34.366863+00	29	Elantra - 2017 - KA 2853 ВВ 	1	[{"added": {}}]	15	1
135	2022-03-01 20:25:51.311077+00	30	Elantra - 2017 - KA 3564 ВI 	1	[{"added": {}}]	15	1
136	2022-03-01 20:26:03.410989+00	31	Elantra - 2017 - KA 5754 ВI 	1	[{"added": {}}]	15	1
137	2022-03-01 20:27:19.319968+00	32	Elantra - 2016 - KA 9134 ВЕ 	1	[{"added": {}}]	15	1
138	2022-03-01 20:27:52.031729+00	33	Elantra - 2018 - KA 9236 ВЕ 	1	[{"added": {}}]	15	1
139	2022-03-01 20:28:05.42354+00	34	Elantra - 2015 - KA 9864 AO 	1	[{"added": {}}]	15	1
140	2022-03-01 20:28:20.706389+00	35	Elantra - 2016 - KA 9865 AO 	1	[{"added": {}}]	15	1
141	2022-03-01 20:28:50.704657+00	36	Elantra - 2011 - АІ 0368 MX 	1	[{"added": {}}]	15	1
142	2022-03-01 20:29:32.749336+00	37	Elantra - 2017 - АІ 3159 MМ 	1	[{"added": {}}]	15	1
143	2022-03-01 20:29:52.644958+00	38	Elantra - 2016 - АІ 3167 MМ 	1	[{"added": {}}]	15	1
144	2022-03-01 20:30:09.820951+00	39	Elantra - 2016 - АІ 3168 MМ 	1	[{"added": {}}]	15	1
145	2022-03-01 20:30:25.058128+00	40	Elantra - 2016 - АІ 3351 MA 	1	[{"added": {}}]	15	1
146	2022-03-01 20:30:39.53109+00	41	Elantra - 2018 - АІ 3422 MР 	1	[{"added": {}}]	15	1
147	2022-03-01 20:32:12.085865+00	42	Elantra - 2018 - АІ 6304 МР 	1	[{"added": {}}]	15	1
148	2022-03-01 20:32:27.118516+00	43	Elantra - 2017 - АІ 6703 МР 	1	[{"added": {}}]	15	1
149	2022-03-01 20:32:39.091682+00	44	Elantra - 2017 - АІ 6896 ОВ 	1	[{"added": {}}]	15	1
150	2022-03-01 20:32:52.723702+00	45	Elantra - 2017 - AІ 7365 МI 	1	[{"added": {}}]	15	1
151	2022-03-01 20:33:07.565863+00	46	Elantra - 2016 - AІ 7453 ОВ 	1	[{"added": {}}]	15	1
152	2022-03-01 20:36:02.528643+00	47	Elantra - 2018 - AІ 7645 МТ 	1	[{"added": {}}]	15	1
153	2022-03-01 20:36:16.930958+00	48	Elantra - 2016 - AІ 8132 МТ 	1	[{"added": {}}]	15	1
154	2022-03-01 20:36:31.565346+00	49	Elantra - 2016 - AІ 8829 ММ 	1	[{"added": {}}]	15	1
155	2022-03-01 20:36:58.858097+00	50	Elantra - 2017 - AМ 6023 ЕТ 	1	[{"added": {}}]	15	1
156	2022-03-01 20:37:11.397631+00	51	Elantra - 2017 - KA 6328 ВI 	1	[{"added": {}}]	15	1
157	2022-03-01 21:19:54.757912+00	4	Skoda	1	[{"added": {}}]	14	1
158	2022-03-01 21:19:56.601048+00	7	Fabia	1	[{"added": {}}]	22	1
159	2022-03-01 21:20:03.012765+00	52	Fabia - 2008 - АА 8037 РР 	1	[{"added": {}}]	15	1
160	2022-03-01 21:20:19.860614+00	53	Fabia - 2015 - АА 8986 СС 	1	[{"added": {}}]	15	1
161	2022-03-01 21:22:18.731516+00	54	Fabia - 2013 - АІ 2803 СС 	1	[{"added": {}}]	15	1
162	2022-03-01 21:22:38.912003+00	55	Fabia - 2008 - АІ 7166 ІМ 	1	[{"added": {}}]	15	1
163	2022-03-01 21:23:02.113949+00	8	I30	1	[{"added": {}}]	22	1
164	2022-03-01 21:24:33.33848+00	56	I30 - 2011 - АА 3967 ТТ 	1	[{"added": {}}]	15	1
165	2022-03-01 21:24:53.71705+00	57	I30 - 2011 - АА 4421 ЕІ 	1	[{"added": {}}]	15	1
166	2022-03-01 21:25:07.646196+00	58	I30 - 2010 - АА 4934 ВР 	1	[{"added": {}}]	15	1
167	2022-03-01 21:25:21.948664+00	59	I30 - 2012 - АА 6079 СС 	1	[{"added": {}}]	15	1
168	2022-03-01 21:25:58.935218+00	60	I30 - 2008 - АІ 6723 ІН 	1	[{"added": {}}]	15	1
169	2022-03-01 21:26:23.233284+00	5	Volkswagen	1	[{"added": {}}]	14	1
170	2022-03-01 21:26:29.417988+00	9	Jetta	1	[{"added": {}}]	22	1
171	2022-03-01 21:26:47.322466+00	61	Jetta - 2008 - АІ 2258 MМ 	1	[{"added": {}}]	15	1
172	2022-03-01 21:28:03.193728+00	62	Jetta - 2011 - АI 3439 MP 	1	[{"added": {}}]	15	1
173	2022-03-01 21:28:17.420536+00	63	Jetta - 2010 - КА 0345 ВМ 	1	[{"added": {}}]	15	1
174	2022-03-01 21:28:53.483749+00	6	Kia	1	[{"added": {}}]	14	1
175	2022-03-01 21:28:57.041202+00	10	K5	1	[{"added": {}}]	22	1
176	2022-03-01 21:35:59.118714+00	64	K5 - 2012 - AI 5266 OO 	1	[{"added": {}}]	15	1
177	2022-03-01 21:36:15.887614+00	11	Carens	1	[{"added": {}}]	22	1
178	2022-03-01 21:36:22.361983+00	65	Carens - 2012 - AI 0712 OВ 	1	[{"added": {}}]	15	1
179	2022-03-01 21:36:41.108802+00	12	Forte	1	[{"added": {}}]	22	1
180	2022-03-01 21:37:30.979958+00	66	Forte - 2011 - AI 3724 OС 	1	[{"added": {}}]	15	1
181	2022-03-01 21:37:57.062743+00	67	Forte - 2011 - AI 9766 OA 	1	[{"added": {}}]	15	1
182	2022-03-01 21:38:10.077057+00	68	K5 - 2015 - АІ 0589 MІ 	1	[{"added": {}}]	15	1
183	2022-03-01 21:38:21.951044+00	69	K5 - 2015 - АІ 0724 MІ 	1	[{"added": {}}]	15	1
184	2022-03-01 21:38:34.840631+00	70	K5 - 2016 - АІ 3620 MP 	1	[{"added": {}}]	15	1
185	2022-03-01 21:38:48.294995+00	71	K5 - 2016 - АІ 5563 MP 	1	[{"added": {}}]	15	1
186	2022-03-01 21:39:13.710953+00	72	K5 - 2017 - АІ 5564 MP 	1	[{"added": {}}]	15	1
187	2022-03-01 21:40:07.814817+00	73	K5 - 2017 - АІ 7045 MP 	1	[{"added": {}}]	15	1
188	2022-03-01 21:40:23.257747+00	74	K5 - 2016 - АІ 7328 MР 	1	[{"added": {}}]	15	1
189	2022-03-01 21:40:42.394126+00	75	K5 - 2015 - АІ 7652 MТ 	1	[{"added": {}}]	15	1
190	2022-03-01 21:40:56.305232+00	76	K5 - 2015 - АІ 8089 MI 	1	[{"added": {}}]	15	1
191	2022-03-01 21:41:34.664305+00	13	Sorento	1	[{"added": {}}]	22	1
192	2022-03-01 21:41:37.185285+00	77	Sorento - 2016 - AI 2111 MB 	1	[{"added": {}}]	15	1
193	2022-03-01 21:41:57.78849+00	14	Sportage	1	[{"added": {}}]	22	1
194	2022-03-01 21:42:01.263021+00	78	Sportage - 2011 - KА 6164 EC 	1	[{"added": {}}]	15	1
195	2022-03-01 21:44:20.416045+00	7	Mazda	1	[{"added": {}}]	14	1
196	2022-03-01 21:44:26.37033+00	15	Mazda 6	1	[{"added": {}}]	22	1
197	2022-03-01 21:44:35.788901+00	79	Mazda 6 - 2007 - АА 1066 СС 	1	[{"added": {}}]	15	1
198	2022-03-01 21:45:41.251422+00	15	Mazda	2	[{"changed": {"fields": ["Name"]}}]	22	1
199	2022-03-01 21:45:50.771155+00	80	Mazda - 2013 - АІ 8006 МА 	1	[{"added": {}}]	15	1
200	2022-03-01 21:46:09.789239+00	8	Opel	1	[{"added": {}}]	14	1
201	2022-03-01 21:46:12.956454+00	16	Insignia	1	[{"added": {}}]	22	1
202	2022-03-01 21:46:15.598657+00	81	Insignia - 2006 - AI 1495 MO 	1	[{"added": {}}]	15	1
203	2022-03-01 21:48:24.413675+00	17	Passat	1	[{"added": {}}]	22	1
204	2022-03-01 21:48:34.889247+00	82	Passat - 2004 - AА 1556 ТT 	1	[{"added": {}}]	15	1
205	2022-03-01 21:48:53.493928+00	18	Polo	1	[{"added": {}}]	22	1
206	2022-03-01 21:49:00.250197+00	83	Polo - 2015 - KA 0728 AP 	1	[{"added": {}}]	15	1
207	2022-03-01 21:49:56.469763+00	84	Polo - 2013 - АЕ 9200 IM 	1	[{"added": {}}]	15	1
208	2022-03-01 21:50:13.054107+00	19	Rio	1	[{"added": {}}]	22	1
209	2022-03-01 21:50:31.266+00	85	Rio - 2012 - АА 6038 РР 	1	[{"added": {}}]	15	1
210	2022-03-01 21:50:48.134111+00	86	Rio - 2012 - АІ 1893 ІІ 	1	[{"added": {}}]	15	1
211	2022-03-01 21:51:09.058404+00	87	Rio - 2012 - КА 3306 ВМ 	1	[{"added": {}}]	15	1
212	2022-03-01 21:51:24.949448+00	88	Rio - 2011 - СА 3701 СК 	1	[{"added": {}}]	15	1
213	2022-03-01 21:51:44.72901+00	20	Sonata	1	[{"added": {}}]	22	1
214	2022-03-01 21:51:51.113678+00	89	Sonata - 2012 - АI 6782 ОА 	1	[{"added": {}}]	15	1
215	2022-03-01 21:52:05.282731+00	90	Sonata - 2012 - АI 6786 ОА 	1	[{"added": {}}]	15	1
216	2022-03-01 21:52:27.495258+00	91	Sonata - 2012 - АI 6805 ОА 	1	[{"added": {}}]	15	1
217	2022-03-01 21:52:40.630424+00	92	Sonata - 2016 - АI 6809 ОА 	1	[{"added": {}}]	15	1
218	2022-03-01 21:52:53.193077+00	93	Sonata - 2015 - АМ 3560 ЕО 	1	[{"added": {}}]	15	1
219	2022-03-01 21:53:09.022291+00	94	Sonata - 2015 - АI 0253 МР 	1	[{"added": {}}]	15	1
220	2022-03-01 21:53:22.707225+00	95	Sonata - 2015 - АI 0954 МB 	1	[{"added": {}}]	15	1
221	2022-03-01 21:53:38.220191+00	96	Sonata - 2015 - АI 0962 МB 	1	[{"added": {}}]	15	1
222	2022-03-01 21:53:52.989244+00	97	Sonata - 2016 - АI 1256 ММ 	1	[{"added": {}}]	15	1
223	2022-03-01 21:54:05.7903+00	98	Sonata - 2016 - АI 2518 МР 	1	[{"added": {}}]	15	1
224	2022-03-01 21:54:19.86006+00	99	Sonata - 2016 - АI 2519 МР 	1	[{"added": {}}]	15	1
225	2022-03-01 21:54:42.797082+00	100	Sonata - 2015 - АI 2846 МР 	1	[{"added": {}}]	15	1
226	2022-03-01 21:55:24.311708+00	101	Sonata - 2015 - АI 2875 ММ 	1	[{"added": {}}]	15	1
227	2022-03-01 21:55:37.423432+00	102	Sonata - 2017 - АI 2953 ММ 	1	[{"added": {}}]	15	1
228	2022-03-01 21:55:49.269871+00	103	Sonata - 2016 - АI 3702 MP 	1	[{"added": {}}]	15	1
229	2022-03-01 21:56:11.503314+00	104	Sonata - 2016 - АI 3922 MТ 	1	[{"added": {}}]	15	1
230	2022-03-01 21:56:25.298509+00	105	Sonata - 2016 - АI 4668 МЕ 	1	[{"added": {}}]	15	1
231	2022-03-01 21:56:42.76616+00	106	Sonata - 2014 - АI 5684 MP 	1	[{"added": {}}]	15	1
232	2022-03-01 21:56:58.21395+00	107	Sonata - 2013 - АI 5691 MP 	1	[{"added": {}}]	15	1
233	2022-03-01 21:57:06.366131+00	108	Sonata - 2015 - АI 6429 MP 	1	[{"added": {}}]	15	1
234	2022-03-01 21:57:20.346202+00	109	Sonata - 2015 - АI 6429 MP 	1	[{"added": {}}]	15	1
235	2022-03-01 21:57:46.362453+00	109	Sonata - 2008 - АI 7923 MТ 	2	[{"changed": {"fields": ["Number", "Year"]}}]	15	1
236	2022-03-01 21:58:02.067534+00	110	Sonata - 2012 - АI 8861 MТ 	1	[{"added": {}}]	15	1
237	2022-03-01 21:58:22.603815+00	111	Sonata - 2017 - АI 9104 МР 	1	[{"added": {}}]	15	1
238	2022-03-01 21:58:36.210375+00	112	Sonata - 2017 - АI 9361 МР 	1	[{"added": {}}]	15	1
239	2022-03-01 21:58:50.047518+00	113	Sonata - 2014 - АI 9364 МР 	1	[{"added": {}}]	15	1
240	2022-03-01 21:59:01.981749+00	114	Sonata - 2016 - АI 9681 МР 	1	[{"added": {}}]	15	1
241	2022-03-01 21:59:16.704463+00	115	Sonata - 2016 - АI 9702 МР 	1	[{"added": {}}]	15	1
242	2022-03-01 21:59:37.067863+00	116	Sonata - 2016 - АI 9814 МВ 	1	[{"added": {}}]	15	1
243	2022-03-01 21:59:50.275905+00	117	Sonata - 2016 - ВА 7546 ЕМ 	1	[{"added": {}}]	15	1
244	2022-03-01 22:00:03.062687+00	118	Sonata - 2017 - ВМ 0584 СМ 	1	[{"added": {}}]	15	1
245	2022-03-01 22:00:15.764028+00	119	Sonata - 2016 - КА 0583 ВM 	1	[{"added": {}}]	15	1
246	2022-03-01 22:00:28.657876+00	120	Sonata - 2015 - КА 0941 ВМ 	1	[{"added": {}}]	15	1
247	2022-03-01 22:00:46.19291+00	121	Sonata - 2016 - КА 2479 ВВ 	1	[{"added": {}}]	15	1
248	2022-03-01 22:00:57.946539+00	122	Sonata - 2015 - КА 2641 ВВ 	1	[{"added": {}}]	15	1
249	2022-03-01 22:01:15.046903+00	123	Sonata - 2015 - КА 3426 ВМ 	1	[{"added": {}}]	15	1
250	2022-03-01 22:01:28.152747+00	124	Sonata - 2017 - КА 6210 ET 	1	[{"added": {}}]	15	1
251	2022-03-01 22:01:40.318226+00	125	Sonata - 2016 - КА 8962 ВМ 	1	[{"added": {}}]	15	1
252	2022-03-01 22:01:54.367761+00	126	Sonata - 2016 - КА 9531 ВК 	1	[{"added": {}}]	15	1
253	2022-03-01 22:02:07.98755+00	127	Sonata - 2016 - СА 5407 ІВ 	1	[{"added": {}}]	15	1
254	2022-03-01 22:02:22.396194+00	128	Sonata - 2014 - СА 5408 ІВ 	1	[{"added": {}}]	15	1
255	2022-03-01 22:02:35.108051+00	129	Sonata - 2015 - АI 8894 ММ 	1	[{"added": {}}]	15	1
256	2022-03-01 22:02:47.227091+00	130	Sonata - 2015 - АI 3427 MP 	1	[{"added": {}}]	15	1
257	2022-03-01 22:03:50.108273+00	131	Sonata - 2015 - АА 1058 ЕМ 	1	[{"added": {}}]	15	1
258	2022-03-01 22:04:05.762004+00	132	Sonata - 2016 - АI 0886 KP 	1	[{"added": {}}]	15	1
259	2022-03-01 22:04:20.908305+00	9	Subaru	1	[{"added": {}}]	14	1
260	2022-03-01 22:04:22.552504+00	21	Subaru	1	[{"added": {}}]	22	1
261	2022-03-01 22:04:32.032823+00	133	Subaru - 2011 - AI 7103 OB 	1	[{"added": {}}]	15	1
262	2022-03-01 22:04:52.965921+00	22	Forester	1	[{"added": {}}]	22	1
263	2022-03-01 22:04:58.076358+00	134	Forester - 2007 - AI 8500 MT 	1	[{"added": {}}]	15	1
264	2022-03-01 22:10:57.058846+00	135	Rio - 1 - 1 	1	[{"added": {}}]	15	1
265	2022-03-01 22:11:06.101066+00	136	Polo - 1 - 1 	1	[{"added": {}}]	15	1
266	2022-03-01 22:11:18.553298+00	137	Mazda - 1 - 1 	1	[{"added": {}}]	15	1
267	2022-03-01 22:11:25.4707+00	138	Sportage - 1 - 1 	1	[{"added": {}}]	15	1
268	2022-03-01 22:11:34.54661+00	139	Passat - 1 - 1 	1	[{"added": {}}]	15	1
269	2022-03-01 22:11:46.672526+00	140	Insignia - 1 - 1 	1	[{"added": {}}]	15	1
270	2022-03-01 22:17:18.59739+00	140	Insignia - 1 - 1 	3		15	1
271	2022-03-01 22:17:18.599677+00	139	Passat - 1 - 1 	3		15	1
272	2022-03-01 22:17:18.600575+00	138	Sportage - 1 - 1 	3		15	1
273	2022-03-01 22:17:18.601456+00	137	Mazda - 1 - 1 	3		15	1
274	2022-03-01 22:17:18.602333+00	136	Polo - 1 - 1 	3		15	1
275	2022-03-01 22:17:18.603118+00	135	Rio - 1 - 1 	3		15	1
276	2022-03-01 22:27:32.993971+00	5	Accent - 2010 - АI 9756 IH 	2	[{"changed": {"name": "car image", "object": "Accent - 2010 - \\u0410I 9756 IH ", "fields": ["Path"]}}]	15	1
277	2022-03-01 22:27:43.750836+00	6	Accent - 2008 - КА 0729 АР 	2	[{"changed": {"name": "car image", "object": "Accent - 2008 - \\u041a\\u0410 0729 \\u0410\\u0420 ", "fields": ["Path"]}}]	15	1
278	2022-03-01 22:27:43.956031+00	6	Accent - 2008 - КА 0729 АР 	2	[{"changed": {"name": "car image", "object": "Accent - 2008 - \\u041a\\u0410 0729 \\u0410\\u0420 ", "fields": ["Path"]}}]	15	1
279	2022-03-01 22:27:54.027739+00	7	Accent - 2011 - КA 2219 ВВ 	2	[{"changed": {"name": "car image", "object": "Accent - 2011 - \\u041aA 2219 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
280	2022-03-01 22:28:05.361983+00	8	Accent - 2016 - AE 7193 KO 	2	[{"changed": {"name": "car image", "object": "Accent - 2016 - AE 7193 KO ", "fields": ["Path"]}}]	15	1
281	2022-03-01 22:28:16.77714+00	9	Avante - 2017 - АІ 8064 MO 	2	[{"changed": {"name": "car image", "object": "Avante - 2017 - \\u0410\\u0406 8064 MO ", "fields": ["Path"]}}]	15	1
282	2022-03-01 22:28:24.844791+00	10	Avante - 2016 - АІ 8243 OВ 	2	[{"changed": {"name": "car image", "object": "Avante - 2016 - \\u0410\\u0406 8243 O\\u0412 ", "fields": ["Path"]}}]	15	1
283	2022-03-01 22:28:41.849982+00	11	Avante - 2014 - КА 3676 ВО 	2	[{"added": {"name": "car image", "object": "Avante - 2014 - \\u041a\\u0410 3676 \\u0412\\u041e "}}]	15	1
284	2022-03-01 22:28:56.598252+00	12	Avante - 2016 - KA 9980 AT 	2	[{"added": {"name": "car image", "object": "Avante - 2016 - KA 9980 AT "}}]	15	1
285	2022-03-01 22:29:07.217368+00	13	Avante - 2012 - AІ 9149 МН 	2	[{"added": {"name": "car image", "object": "Avante - 2012 - A\\u0406 9149 \\u041c\\u041d "}}]	15	1
286	2022-03-01 22:29:16.549853+00	14	735i - 2002 - AI 3839 ОС 	2	[{"added": {"name": "car image", "object": "735i - 2002 - AI 3839 \\u041e\\u0421 "}}]	15	1
287	2022-03-01 22:29:26.510781+00	4	Accent - 2008 - АI 8250 OB 	2	[{"changed": {"name": "car image", "object": "Accent - 2008 - \\u0410I 8250 OB ", "fields": ["Path"]}}]	15	1
288	2022-03-01 22:29:35.630639+00	3	Accent - 2019 - АI 4463 MC 	2	[{"changed": {"name": "car image", "object": "Accent - 2019 - \\u0410I 4463 MC ", "fields": ["Path"]}}]	15	1
289	2022-03-01 22:29:48.812334+00	2	Accent - 2017 - АІ 0312 MX 	2	[{"changed": {"name": "car image", "object": "Accent - 2017 - \\u0410\\u0406 0312 MX ", "fields": ["Path"]}}]	15	1
290	2022-03-01 22:29:58.830606+00	1	Accent - 2011 - АІ 0315 MX 	2	[{"changed": {"name": "car image", "object": "Accent - 2011 - \\u0410\\u0406 0315 MX ", "fields": ["Path"]}}]	15	1
291	2022-03-01 22:40:59.597884+00	14	735i - 2002 - AI 3839 ОС 	2	[{"changed": {"name": "car image", "object": "735i - 2002 - AI 3839 \\u041e\\u0421 ", "fields": ["Path"]}}]	15	1
292	2022-03-01 22:41:50.925739+00	25	Elantra - 2018 - ВМ 0610 СМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - \\u0412\\u041c 0610 \\u0421\\u041c "}}]	15	1
293	2022-03-01 22:42:48.058579+00	26	Elantra - 2017 - KA 0514 ВН 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 0514 \\u0412\\u041d "}}]	15	1
294	2022-03-01 22:42:54.117758+00	27	Elantra - 2016 - KA 0574 ВМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - KA 0574 \\u0412\\u041c "}}]	15	1
295	2022-03-01 22:43:00.505592+00	28	Elantra - 2017 - KA 2816 ВВ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 2816 \\u0412\\u0412 "}}]	15	1
296	2022-03-01 22:43:07.630083+00	29	Elantra - 2017 - KA 2853 ВВ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 2853 \\u0412\\u0412 "}}]	15	1
297	2022-03-01 22:43:16.609644+00	30	Elantra - 2017 - KA 3564 ВI 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 3564 \\u0412I "}}]	15	1
298	2022-03-01 22:43:23.18949+00	31	Elantra - 2017 - KA 5754 ВI 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 5754 \\u0412I "}}]	15	1
299	2022-03-01 22:43:29.227717+00	32	Elantra - 2016 - KA 9134 ВЕ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - KA 9134 \\u0412\\u0415 "}}]	15	1
300	2022-03-01 22:43:35.937509+00	33	Elantra - 2018 - KA 9236 ВЕ 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - KA 9236 \\u0412\\u0415 "}}]	15	1
301	2022-03-01 22:43:41.891547+00	34	Elantra - 2015 - KA 9864 AO 	2	[{"added": {"name": "car image", "object": "Elantra - 2015 - KA 9864 AO "}}]	15	1
302	2022-03-01 22:43:56.75758+00	22	Elantra - 2016 - АІ 3605 ОС 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3605 \\u041e\\u0421 "}}]	15	1
303	2022-03-01 22:44:02.949876+00	24	Elantra - 2016 - ВМ 0588 СМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0412\\u041c 0588 \\u0421\\u041c "}}]	15	1
304	2022-03-01 22:44:12.351141+00	23	Elantra - 2017 - АІ 5508 МР 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 5508 \\u041c\\u0420 "}}]	15	1
305	2022-03-01 22:44:29.080334+00	21	Corolla - 2015 - КА 2026 ВВ 	2	[{"added": {"name": "car image", "object": "Corolla - 2015 - \\u041a\\u0410 2026 \\u0412\\u0412 "}}]	15	1
306	2022-03-01 22:45:30.741316+00	21	Corolla - 2015 - КА 2026 ВВ 	2	[{"changed": {"name": "car image", "object": "Corolla - 2015 - \\u041a\\u0410 2026 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
307	2022-03-01 22:45:36.667787+00	20	Corolla - 2016 - АІ 9423 ІМ 	2	[{"added": {"name": "car image", "object": "Corolla - 2016 - \\u0410\\u0406 9423 \\u0406\\u041c "}}]	15	1
308	2022-03-01 22:45:43.674228+00	19	Corolla - 2007 - АІ 8891 ІК 	2	[{"added": {"name": "car image", "object": "Corolla - 2007 - \\u0410\\u0406 8891 \\u0406\\u041a "}}]	15	1
309	2022-03-01 22:45:53.096722+00	18	Corolla - 2011 - АІ 6453 ОО 	2	[{"added": {"name": "car image", "object": "Corolla - 2011 - \\u0410\\u0406 6453 \\u041e\\u041e "}}]	15	1
310	2022-03-01 22:46:17.818112+00	17	Corolla - 2018 - АІ 7462 ОВ 	2	[{"added": {"name": "car image", "object": "Corolla - 2018 - \\u0410\\u0406 7462 \\u041e\\u0412 "}}]	15	1
311	2022-03-01 22:46:23.69235+00	16	Corolla - 2014 - АІ 2541 КО 	2	[{"added": {"name": "car image", "object": "Corolla - 2014 - \\u0410\\u0406 2541 \\u041a\\u041e "}}]	15	1
312	2022-03-01 22:46:39.982289+00	15	Camry - 2015 - AI 6531 OC 	2	[{"added": {"name": "car image", "object": "Camry - 2015 - AI 6531 OC "}}]	15	1
313	2022-03-01 22:47:32.332386+00	45	Elantra - 2017 - AІ 7365 МI 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - A\\u0406 7365 \\u041cI "}}]	15	1
314	2022-03-01 22:47:39.047285+00	46	Elantra - 2016 - AІ 7453 ОВ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - A\\u0406 7453 \\u041e\\u0412 "}}]	15	1
315	2022-03-01 22:47:47.363856+00	47	Elantra - 2018 - AІ 7645 МТ 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - A\\u0406 7645 \\u041c\\u0422 "}}]	15	1
316	2022-03-01 22:47:55.445327+00	48	Elantra - 2016 - AІ 8132 МТ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - A\\u0406 8132 \\u041c\\u0422 "}}]	15	1
317	2022-03-01 22:48:11.052916+00	49	Elantra - 2016 - AІ 8829 ММ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - A\\u0406 8829 \\u041c\\u041c "}}]	15	1
318	2022-03-01 22:48:18.153574+00	50	Elantra - 2017 - AМ 6023 ЕТ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - A\\u041c 6023 \\u0415\\u0422 "}}]	15	1
319	2022-03-01 22:48:25.677577+00	51	Elantra - 2017 - KA 6328 ВI 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - KA 6328 \\u0412I "}}]	15	1
320	2022-03-01 22:48:41.598347+00	52	Fabia - 2008 - АА 8037 РР 	2	[{"added": {"name": "car image", "object": "Fabia - 2008 - \\u0410\\u0410 8037 \\u0420\\u0420 "}}]	15	1
321	2022-03-01 22:48:48.563334+00	53	Fabia - 2015 - АА 8986 СС 	2	[{"added": {"name": "car image", "object": "Fabia - 2015 - \\u0410\\u0410 8986 \\u0421\\u0421 "}}]	15	1
322	2022-03-01 22:48:56.200696+00	54	Fabia - 2013 - АІ 2803 СС 	2	[{"added": {"name": "car image", "object": "Fabia - 2013 - \\u0410\\u0406 2803 \\u0421\\u0421 "}}]	15	1
323	2022-03-01 22:49:14.615827+00	35	Elantra - 2016 - KA 9865 AO 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - KA 9865 AO "}}]	15	1
324	2022-03-01 22:49:21.576706+00	36	Elantra - 2011 - АІ 0368 MX 	2	[{"added": {"name": "car image", "object": "Elantra - 2011 - \\u0410\\u0406 0368 MX "}}]	15	1
325	2022-03-01 22:49:28.777238+00	37	Elantra - 2017 - АІ 3159 MМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 3159 M\\u041c "}}]	15	1
326	2022-03-01 22:49:44.901405+00	38	Elantra - 2016 - АІ 3167 MМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3167 M\\u041c "}}]	15	1
327	2022-03-01 22:49:52.891951+00	39	Elantra - 2016 - АІ 3168 MМ 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3168 M\\u041c "}}]	15	1
328	2022-03-01 22:49:59.957671+00	40	Elantra - 2016 - АІ 3351 MA 	2	[{"added": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3351 MA "}}]	15	1
329	2022-03-01 22:50:08.800015+00	41	Elantra - 2018 - АІ 3422 MР 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - \\u0410\\u0406 3422 M\\u0420 "}}]	15	1
330	2022-03-01 22:50:20.901393+00	42	Elantra - 2018 - АІ 6304 МР 	2	[{"added": {"name": "car image", "object": "Elantra - 2018 - \\u0410\\u0406 6304 \\u041c\\u0420 "}}]	15	1
331	2022-03-01 22:50:37.450462+00	43	Elantra - 2017 - АІ 6703 МР 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 6703 \\u041c\\u0420 "}}]	15	1
332	2022-03-01 22:51:01.469665+00	44	Elantra - 2017 - АІ 6896 ОВ 	2	[{"added": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 6896 \\u041e\\u0412 "}}]	15	1
333	2022-03-01 22:52:25.644016+00	75	K5 - 2015 - АІ 7652 MТ 	2	[{"added": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 7652 M\\u0422 "}}]	15	1
334	2022-03-01 22:52:35.640524+00	77	Sorento - 2016 - AI 2111 MB 	2	[{"added": {"name": "car image", "object": "Sorento - 2016 - AI 2111 MB "}}]	15	1
335	2022-03-01 22:53:04.029222+00	78	Sportage - 2011 - KА 6164 EC 	2	[{"added": {"name": "car image", "object": "Sportage - 2011 - K\\u0410 6164 EC "}}]	15	1
336	2022-03-01 22:53:51.081641+00	79	Mazda - 2007 - АА 1066 СС 	2	[{"added": {"name": "car image", "object": "Mazda - 2007 - \\u0410\\u0410 1066 \\u0421\\u0421 "}}]	15	1
337	2022-03-01 22:53:59.485635+00	80	Mazda - 2013 - АІ 8006 МА 	2	[{"added": {"name": "car image", "object": "Mazda - 2013 - \\u0410\\u0406 8006 \\u041c\\u0410 "}}]	15	1
338	2022-03-01 22:54:13.47992+00	81	Insignia - 2006 - AI 1495 MO 	2	[{"added": {"name": "car image", "object": "Insignia - 2006 - AI 1495 MO "}}]	15	1
339	2022-03-01 22:54:30.25009+00	82	Passat - 2004 - AА 1556 ТT 	2	[{"added": {"name": "car image", "object": "Passat - 2004 - A\\u0410 1556 \\u0422T "}}]	15	1
340	2022-03-01 22:54:37.677241+00	83	Polo - 2015 - KA 0728 AP 	2	[{"added": {"name": "car image", "object": "Polo - 2015 - KA 0728 AP "}}]	15	1
341	2022-03-01 22:54:46.159216+00	84	Polo - 2013 - АЕ 9200 IM 	2	[{"added": {"name": "car image", "object": "Polo - 2013 - \\u0410\\u0415 9200 IM "}}]	15	1
342	2022-03-01 22:55:06.997944+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"added": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 0589 M\\u0406 "}}]	15	1
343	2022-03-01 22:55:15.141148+00	65	Carens - 2012 - AI 0712 OВ 	2	[{"added": {"name": "car image", "object": "Carens - 2012 - AI 0712 O\\u0412 "}}]	15	1
344	2022-03-01 22:55:34.641329+00	66	Forte - 2011 - AI 3724 OС 	2	[{"added": {"name": "car image", "object": "Forte - 2011 - AI 3724 O\\u0421 "}}]	15	1
345	2022-03-01 22:55:52.716992+00	67	Forte - 2011 - AI 9766 OA 	2	[{"added": {"name": "car image", "object": "Forte - 2011 - AI 9766 OA "}}]	15	1
346	2022-03-01 22:56:02.654722+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"added": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 0589 M\\u0406 "}}]	15	1
347	2022-03-01 22:56:14.551171+00	69	K5 - 2015 - АІ 0724 MІ 	2	[{"added": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 0724 M\\u0406 "}}]	15	1
348	2022-03-01 22:56:49.255672+00	70	K5 - 2016 - АІ 3620 MP 	2	[{"added": {"name": "car image", "object": "K5 - 2016 - \\u0410\\u0406 3620 MP "}}]	15	1
349	2022-03-01 22:56:59.635971+00	71	K5 - 2016 - АІ 5563 MP 	2	[{"added": {"name": "car image", "object": "K5 - 2016 - \\u0410\\u0406 5563 MP "}}]	15	1
350	2022-03-01 22:57:21.204911+00	72	K5 - 2017 - АІ 5564 MP 	2	[{"added": {"name": "car image", "object": "K5 - 2017 - \\u0410\\u0406 5564 MP "}}]	15	1
351	2022-03-01 22:57:31.034796+00	73	K5 - 2017 - АІ 7045 MP 	2	[{"added": {"name": "car image", "object": "K5 - 2017 - \\u0410\\u0406 7045 MP "}}]	15	1
352	2022-03-01 22:57:46.032861+00	74	K5 - 2016 - АІ 7328 MР 	2	[{"added": {"name": "car image", "object": "K5 - 2016 - \\u0410\\u0406 7328 M\\u0420 "}}]	15	1
353	2022-03-01 22:57:55.805618+00	55	Fabia - 2008 - АІ 7166 ІМ 	2	[{"added": {"name": "car image", "object": "Fabia - 2008 - \\u0410\\u0406 7166 \\u0406\\u041c "}}]	15	1
354	2022-03-01 22:58:18.619945+00	56	I30 - 2011 - АА 3967 ТТ 	2	[{"added": {"name": "car image", "object": "I30 - 2011 - \\u0410\\u0410 3967 \\u0422\\u0422 "}}]	15	1
355	2022-03-01 22:58:27.692394+00	57	I30 - 2011 - АА 4421 ЕІ 	2	[{"added": {"name": "car image", "object": "I30 - 2011 - \\u0410\\u0410 4421 \\u0415\\u0406 "}}]	15	1
356	2022-03-01 22:58:33.878677+00	58	I30 - 2010 - АА 4934 ВР 	2	[{"added": {"name": "car image", "object": "I30 - 2010 - \\u0410\\u0410 4934 \\u0412\\u0420 "}}]	15	1
357	2022-03-01 22:58:58.92934+00	59	I30 - 2012 - АА 6079 СС 	2	[{"added": {"name": "car image", "object": "I30 - 2012 - \\u0410\\u0410 6079 \\u0421\\u0421 "}}]	15	1
358	2022-03-01 22:59:31.882229+00	60	I30 - 2008 - АІ 6723 ІН 	2	[{"added": {"name": "car image", "object": "I30 - 2008 - \\u0410\\u0406 6723 \\u0406\\u041d "}}]	15	1
359	2022-03-01 22:59:42.243225+00	61	Jetta - 2008 - АІ 2258 MМ 	2	[{"added": {"name": "car image", "object": "Jetta - 2008 - \\u0410\\u0406 2258 M\\u041c "}}]	15	1
360	2022-03-01 22:59:55.374385+00	62	Jetta - 2011 - АI 3439 MP 	2	[{"added": {"name": "car image", "object": "Jetta - 2011 - \\u0410I 3439 MP "}}]	15	1
361	2022-03-01 23:00:04.025923+00	63	Jetta - 2010 - КА 0345 ВМ 	2	[{"added": {"name": "car image", "object": "Jetta - 2010 - \\u041a\\u0410 0345 \\u0412\\u041c "}}]	15	1
362	2022-03-01 23:00:19.385057+00	64	K5 - 2012 - AI 5266 OO 	2	[{"added": {"name": "car image", "object": "K5 - 2012 - AI 5266 OO "}}]	15	1
363	2022-03-01 23:02:38.143218+00	125	Sonata - 2016 - КА 8962 ВМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 8962 \\u0412\\u041c "}}]	15	1
364	2022-03-01 23:02:45.344265+00	94	Sonata - 2015 - АI 0253 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 0253 \\u041c\\u0420 "}}]	15	1
365	2022-03-01 23:02:52.596183+00	93	Sonata - 2015 - АМ 3560 ЕО 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410\\u041c 3560 \\u0415\\u041e "}}]	15	1
366	2022-03-01 23:03:00.984658+00	92	Sonata - 2016 - АI 6809 ОА 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 6809 \\u041e\\u0410 "}}]	15	1
367	2022-03-01 23:03:12.340848+00	91	Sonata - 2012 - АI 6805 ОА 	2	[{"added": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 6805 \\u041e\\u0410 "}}]	15	1
368	2022-03-01 23:03:20.67202+00	90	Sonata - 2012 - АI 6786 ОА 	2	[{"added": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 6786 \\u041e\\u0410 "}}]	15	1
369	2022-03-01 23:03:29.971644+00	89	Sonata - 2012 - АI 6782 ОА 	2	[{"added": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 6782 \\u041e\\u0410 "}}]	15	1
414	2022-03-02 21:11:22.454488+00	53	Fabia - 2015 - АА 8986 СС 	2	[{"changed": {"fields": ["Status"]}}]	15	1
370	2022-03-01 23:03:39.061141+00	88	Rio - 2011 - СА 3701 СК 	2	[{"added": {"name": "car image", "object": "Rio - 2011 - \\u0421\\u0410 3701 \\u0421\\u041a "}}]	15	1
371	2022-03-01 23:03:48.391841+00	87	Rio - 2012 - КА 3306 ВМ 	2	[{"added": {"name": "car image", "object": "Rio - 2012 - \\u041a\\u0410 3306 \\u0412\\u041c "}}]	15	1
372	2022-03-01 23:03:59.849655+00	104	Sonata - 2016 - АI 3922 MТ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 3922 M\\u0422 "}}]	15	1
373	2022-03-01 23:04:08.333769+00	103	Sonata - 2016 - АI 3702 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 3702 MP "}}]	15	1
374	2022-03-01 23:04:16.514565+00	102	Sonata - 2017 - АI 2953 ММ 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u0410I 2953 \\u041c\\u041c "}}]	15	1
375	2022-03-01 23:04:25.507085+00	101	Sonata - 2015 - АI 2875 ММ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 2875 \\u041c\\u041c "}}]	15	1
376	2022-03-01 23:04:35.065813+00	100	Sonata - 2015 - АI 2846 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 2846 \\u041c\\u0420 "}}]	15	1
377	2022-03-01 23:04:43.580012+00	99	Sonata - 2016 - АI 2519 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 2519 \\u041c\\u0420 "}}]	15	1
378	2022-03-01 23:04:51.316847+00	98	Sonata - 2016 - АI 2518 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 2518 \\u041c\\u0420 "}}]	15	1
379	2022-03-01 23:04:58.713333+00	97	Sonata - 2016 - АI 1256 ММ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 1256 \\u041c\\u041c "}}]	15	1
380	2022-03-01 23:05:07.588549+00	96	Sonata - 2015 - АI 0962 МB 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 0962 \\u041cB "}}]	15	1
381	2022-03-01 23:05:15.730522+00	95	Sonata - 2015 - АI 0954 МB 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 0954 \\u041cB "}}]	15	1
382	2022-03-01 23:05:23.176419+00	114	Sonata - 2016 - АI 9681 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 9681 \\u041c\\u0420 "}}]	15	1
383	2022-03-01 23:05:36.569166+00	113	Sonata - 2014 - АI 9364 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2014 - \\u0410I 9364 \\u041c\\u0420 "}}]	15	1
384	2022-03-01 23:05:45.092277+00	112	Sonata - 2017 - АI 9361 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u0410I 9361 \\u041c\\u0420 "}}]	15	1
385	2022-03-01 23:05:52.522486+00	111	Sonata - 2017 - АI 9104 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u0410I 9104 \\u041c\\u0420 "}}]	15	1
386	2022-03-01 23:06:02.05283+00	110	Sonata - 2012 - АI 8861 MТ 	2	[{"added": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 8861 M\\u0422 "}}]	15	1
387	2022-03-01 23:06:10.492417+00	109	Sonata - 2008 - АI 7923 MТ 	2	[{"added": {"name": "car image", "object": "Sonata - 2008 - \\u0410I 7923 M\\u0422 "}}]	15	1
388	2022-03-01 23:06:19.491483+00	108	Sonata - 2015 - АI 6429 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 6429 MP "}}]	15	1
389	2022-03-01 23:06:27.756977+00	107	Sonata - 2013 - АI 5691 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2013 - \\u0410I 5691 MP "}}]	15	1
390	2022-03-01 23:06:42.257336+00	106	Sonata - 2014 - АI 5684 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2014 - \\u0410I 5684 MP "}}]	15	1
391	2022-03-01 23:06:50.588132+00	106	Sonata - 2014 - АI 5684 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2014 - \\u0410I 5684 MP "}}]	15	1
392	2022-03-01 23:07:01.881766+00	105	Sonata - 2016 - АI 4668 МЕ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 4668 \\u041c\\u0415 "}}]	15	1
393	2022-03-01 23:07:17.132313+00	115	Sonata - 2016 - АI 9702 МР 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 9702 \\u041c\\u0420 "}}]	15	1
394	2022-03-01 23:07:24.547849+00	124	Sonata - 2017 - КА 6210 ET 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u041a\\u0410 6210 ET "}}]	15	1
395	2022-03-01 23:07:40.72789+00	123	Sonata - 2015 - КА 3426 ВМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u041a\\u0410 3426 \\u0412\\u041c "}}]	15	1
396	2022-03-01 23:07:59.954129+00	122	Sonata - 2015 - КА 2641 ВВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u041a\\u0410 2641 \\u0412\\u0412 "}}]	15	1
397	2022-03-01 23:08:08.563233+00	121	Sonata - 2016 - КА 2479 ВВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 2479 \\u0412\\u0412 "}}]	15	1
398	2022-03-01 23:08:21.522226+00	120	Sonata - 2015 - КА 0941 ВМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u041a\\u0410 0941 \\u0412\\u041c "}}]	15	1
399	2022-03-01 23:08:32.791178+00	119	Sonata - 2016 - КА 0583 ВM 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 0583 \\u0412M "}}]	15	1
400	2022-03-01 23:08:38.999641+00	118	Sonata - 2017 - ВМ 0584 СМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2017 - \\u0412\\u041c 0584 \\u0421\\u041c "}}]	15	1
401	2022-03-01 23:08:45.827078+00	117	Sonata - 2016 - ВА 7546 ЕМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0412\\u0410 7546 \\u0415\\u041c "}}]	15	1
402	2022-03-01 23:09:01.048432+00	116	Sonata - 2016 - АI 9814 МВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 9814 \\u041c\\u0412 "}}]	15	1
403	2022-03-01 23:09:22.644664+00	134	Forester - 2007 - AI 8500 MT 	2	[{"added": {"name": "car image", "object": "Forester - 2007 - AI 8500 MT "}}]	15	1
404	2022-03-01 23:09:33.818384+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"added": {"name": "car image", "object": "Subaru - 2011 - AI 7103 OB "}}]	15	1
405	2022-03-01 23:09:40.43609+00	132	Sonata - 2016 - АI 0886 KP 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 0886 KP "}}]	15	1
406	2022-03-01 23:09:46.881786+00	131	Sonata - 2015 - АА 1058 ЕМ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410\\u0410 1058 \\u0415\\u041c "}}]	15	1
407	2022-03-01 23:09:53.526777+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 3427 MP "}}]	15	1
408	2022-03-01 23:10:00.246464+00	129	Sonata - 2015 - АI 8894 ММ 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 8894 \\u041c\\u041c "}}]	15	1
409	2022-03-01 23:10:08.21981+00	128	Sonata - 2014 - СА 5408 ІВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2014 - \\u0421\\u0410 5408 \\u0406\\u0412 "}}]	15	1
410	2022-03-01 23:10:15.208008+00	127	Sonata - 2016 - СА 5407 ІВ 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u0421\\u0410 5407 \\u0406\\u0412 "}}]	15	1
411	2022-03-01 23:10:22.148896+00	126	Sonata - 2016 - КА 9531 ВК 	2	[{"added": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 9531 \\u0412\\u041a "}}]	15	1
412	2022-03-02 21:11:04.541337+00	78	Sportage - 2011 - KА 6164 EC 	2	[{"changed": {"fields": ["Status"]}}]	15	1
413	2022-03-02 21:11:15.3773+00	83	Polo - 2015 - KA 0728 AP 	2	[{"changed": {"fields": ["Status"]}}]	15	1
415	2022-03-02 21:11:27.558166+00	85	Rio - 2012 - АА 6038 РР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
416	2022-03-02 21:11:32.998139+00	126	Sonata - 2016 - КА 9531 ВК 	2	[{"changed": {"fields": ["Status"]}}]	15	1
417	2022-03-02 21:11:36.890054+00	88	Rio - 2011 - СА 3701 СК 	2	[{"changed": {"fields": ["Status"]}}]	15	1
418	2022-03-02 21:11:41.458011+00	5	Accent - 2010 - АI 9756 IH 	2	[{"changed": {"fields": ["Status"]}}]	15	1
419	2022-03-02 21:11:45.3175+00	13	Avante - 2012 - AІ 9149 МН 	2	[{"changed": {"fields": ["Status"]}}]	15	1
420	2022-03-02 21:11:49.2553+00	14	735i - 2002 - AI 3839 ОС 	2	[{"changed": {"fields": ["Status"]}}]	15	1
421	2022-03-02 21:11:53.072789+00	23	Elantra - 2017 - АІ 5508 МР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
422	2022-03-02 21:11:56.659472+00	43	Elantra - 2017 - АІ 6703 МР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
423	2022-03-02 21:12:00.145793+00	65	Carens - 2012 - AI 0712 OВ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
424	2022-03-02 21:12:04.42617+00	54	Fabia - 2013 - АІ 2803 СС 	2	[{"changed": {"fields": ["Status"]}}]	15	1
425	2022-03-02 21:12:08.638068+00	91	Sonata - 2012 - АI 6805 ОА 	2	[{"changed": {"fields": ["Status"]}}]	15	1
426	2022-03-02 21:12:12.17699+00	80	Mazda - 2013 - АІ 8006 МА 	2	[{"changed": {"fields": ["Status"]}}]	15	1
427	2022-03-02 21:12:15.672421+00	58	I30 - 2010 - АА 4934 ВР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
428	2022-03-02 21:12:19.414398+00	3	Accent - 2019 - АI 4463 MC 	2	[{"changed": {"fields": ["Status"]}}]	15	1
429	2022-03-02 21:12:22.265861+00	45	Elantra - 2017 - AІ 7365 МI 	2	[{"changed": {"fields": ["Status"]}}]	15	1
430	2022-03-02 21:12:25.326788+00	55	Fabia - 2008 - АІ 7166 ІМ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
431	2022-03-02 21:12:28.548297+00	56	I30 - 2011 - АА 3967 ТТ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
432	2022-03-02 21:12:31.738111+00	59	I30 - 2012 - АА 6079 СС 	2	[{"changed": {"fields": ["Status"]}}]	15	1
433	2022-03-02 21:12:34.977128+00	95	Sonata - 2015 - АI 0954 МB 	2	[{"changed": {"fields": ["Status"]}}]	15	1
434	2022-03-02 21:12:39.157191+00	61	Jetta - 2008 - АІ 2258 MМ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
435	2022-03-02 21:12:42.832462+00	81	Insignia - 2006 - AI 1495 MO 	2	[{"changed": {"fields": ["Status"]}}]	15	1
436	2022-03-02 21:12:46.018313+00	64	K5 - 2012 - AI 5266 OO 	2	[{"changed": {"fields": ["Status"]}}]	15	1
437	2022-03-02 21:12:49.441096+00	52	Fabia - 2008 - АА 8037 РР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
438	2022-03-02 21:12:53.620228+00	87	Rio - 2012 - КА 3306 ВМ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
439	2022-03-02 21:12:57.105092+00	6	Accent - 2008 - КА 0729 АР 	2	[{"changed": {"fields": ["Status"]}}]	15	1
440	2022-03-02 21:13:00.532713+00	17	Corolla - 2018 - АІ 7462 ОВ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
441	2022-03-02 21:13:04.355397+00	60	I30 - 2008 - АІ 6723 ІН 	2	[{"changed": {"fields": ["Status"]}}]	15	1
442	2022-03-02 21:13:07.638468+00	67	Forte - 2011 - AI 9766 OA 	2	[{"changed": {"fields": ["Status"]}}]	15	1
443	2022-03-02 21:13:11.153118+00	82	Passat - 2004 - AА 1556 ТT 	2	[{"changed": {"fields": ["Status"]}}]	15	1
444	2022-03-02 21:13:15.826065+00	84	Polo - 2013 - АЕ 9200 IM 	2	[{"changed": {"fields": ["Status"]}}]	15	1
445	2022-03-02 21:13:19.799509+00	86	Rio - 2012 - АІ 1893 ІІ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
446	2022-03-02 21:13:23.277397+00	131	Sonata - 2015 - АА 1058 ЕМ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
447	2022-03-02 21:13:26.564813+00	93	Sonata - 2015 - АМ 3560 ЕО 	2	[{"changed": {"fields": ["Status"]}}]	15	1
448	2022-03-02 21:13:30.30976+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"changed": {"fields": ["Status"]}}]	15	1
449	2022-03-02 21:13:33.792423+00	90	Sonata - 2012 - АI 6786 ОА 	2	[{"changed": {"fields": ["Status"]}}]	15	1
450	2022-03-02 21:13:37.369868+00	76	K5 - 2015 - АІ 8089 MI 	2	[{"changed": {"fields": ["Status"]}}]	15	1
451	2022-03-02 21:13:40.673451+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"changed": {"fields": ["Status"]}}]	15	1
452	2022-03-02 21:13:44.201344+00	77	Sorento - 2016 - AI 2111 MB 	2	[{"changed": {"fields": ["Status"]}}]	15	1
453	2022-03-02 21:13:48.393292+00	18	Corolla - 2011 - АІ 6453 ОО 	2	[{"changed": {"fields": ["Status"]}}]	15	1
454	2022-03-02 23:24:33.691778+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Status"]}}]	15	1
455	2022-03-02 23:24:49.11714+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Status"]}}]	15	1
456	2022-03-02 23:28:08.654852+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
457	2022-03-03 00:08:07.415006+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Current tyre"]}}]	15	1
458	2022-03-03 09:17:41.66814+00	1	Seyidov Elvin	2	[{"changed": {"fields": ["Type"]}}]	8	1
459	2022-03-03 09:17:50.451692+00	2	Mammadxanli Farid	2	[{"changed": {"fields": ["Type"]}}]	8	1
460	2022-03-03 09:22:41.919139+00	3	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
461	2022-03-03 09:23:01.876508+00	3	Дмитрий Каюн	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
462	2022-03-03 09:24:35.800924+00	4	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
463	2022-03-03 09:24:57.826395+00	4	Язгулиев Шохрат	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
464	2022-03-03 09:25:38.751893+00	5	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
465	2022-03-03 09:25:53.175003+00	5	Чолиев Юнус	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
466	2022-03-03 09:26:06.555209+00	6	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
467	2022-03-03 09:26:26.123009+00	6	Акрамов Мукхриддин	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
468	2022-03-03 09:26:39.097161+00	6	Акрамов Мукхриддин	2	[]	8	1
469	2022-03-03 09:26:52.605459+00	7	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
470	2022-03-03 09:27:01.797316+00	7	Авдиев Бабагелди	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
471	2022-03-03 09:34:17.670961+00	8	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
472	2022-03-03 09:34:27.096061+00	8	Айрапетов Артём	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
473	2022-03-03 09:34:51.624974+00	9	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
474	2022-03-03 09:34:59.94593+00	9	Батыров Гурбан	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
475	2022-03-03 09:38:59.48564+00	10	 	1	[{"added": {}}]	8	1
1326	2022-03-19 20:42:25.971792+00	45	Шохрат Пенджиев - 4	2	[]	51	1
476	2022-03-03 09:39:18.158143+00	10	(без договора) ВЫКУП	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
477	2022-03-03 09:39:40.54883+00	11	 	1	[{"added": {}}]	8	1
478	2022-03-03 09:39:48.88261+00	11	Романчук Леонид	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
479	2022-03-03 09:40:12.406818+00	12	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
480	2022-03-03 09:40:20.402643+00	12	Бережной Сергей	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
481	2022-03-03 09:40:38.664562+00	13	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
482	2022-03-03 09:41:04.281816+00	13	Исмайлов Ойвен	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
483	2022-03-03 09:41:42.889231+00	14	 	1	[{"added": {}}]	8	1
484	2022-03-03 09:41:51.366027+00	14	выкуп Заур	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
485	2022-03-03 09:43:12.163164+00	15	 	1	[{"added": {}}]	8	1
486	2022-03-03 09:43:22.071094+00	15	Лаптев Олег	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
487	2022-03-03 09:43:42.266699+00	16	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
488	2022-03-03 09:44:12.009513+00	16	Кара Эрхан	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
489	2022-03-03 09:44:24.618818+00	17	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
490	2022-03-03 11:42:51.148193+00	17	Кулиев Азат	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
491	2022-03-03 12:05:57.67428+00	18	 	1	[{"added": {}}]	8	1
492	2022-03-03 12:06:04.266447+00	18	выкуп Али	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
493	2022-03-03 12:06:16.891804+00	19	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
494	2022-03-03 12:06:35.811283+00	19	Чолиев Рустем	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
495	2022-03-03 12:06:49.394345+00	20	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
496	2022-03-03 12:07:10.502188+00	20	Абдыресулов Рахман	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
497	2022-03-03 12:07:29.470313+00	21	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
498	2022-03-03 12:08:54.244115+00	21	Абдиев Довлетгельди	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
499	2022-03-03 12:09:08.81928+00	22	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
500	2022-03-03 12:09:17.576706+00	22	Москалик Юрий	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
501	2022-03-03 12:09:37.698784+00	23	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
502	2022-03-03 12:09:45.618147+00	23	Гончаров Сергей	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
503	2022-03-03 12:09:58.369359+00	24	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
504	2022-03-03 12:10:06.260634+00	24	Фархатов Фарух	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
505	2022-03-03 12:10:19.096858+00	25	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
506	2022-03-03 12:10:30.73299+00	25	Атаджанов Бегенч	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
507	2022-03-03 12:10:41.718452+00	26	 	1	[{"added": {}}]	8	1
508	2022-03-03 12:10:46.97698+00	26	Валиев Эльвин	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
509	2022-03-03 12:11:04.820851+00	27	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
510	2022-03-03 12:11:13.343649+00	27	Яхяев Шамиль	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
511	2022-03-03 12:11:25.238771+00	28	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
512	2022-03-03 12:11:33.520157+00	28	Оммадов Мухамметсердар	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
513	2022-03-03 12:11:47.147617+00	29	 	1	[{"added": {}}]	8	1
514	2022-03-03 12:11:53.314882+00	29	Аманов Довлет	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
515	2022-03-03 12:12:07.087858+00	30	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
516	2022-03-03 12:12:14.659945+00	30	Бахтияров Умитджан	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
517	2022-03-03 12:12:32.510211+00	31	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}, {"added": {"name": "user phone", "object": ""}}]	8	1
518	2022-03-03 12:12:39.930561+00	31	Бердиев Сердар	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
519	2022-03-03 12:13:01.0849+00	32	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
520	2022-03-03 12:13:08.207441+00	32	Халилов Сердар	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
521	2022-03-03 12:13:19.386935+00	33	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
522	2022-03-03 12:13:40.773912+00	33	Бегишов Нургелди	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
523	2022-03-03 12:18:26.925191+00	34	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
524	2022-03-03 12:18:35.949391+00	34	Аманов Байрам	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
525	2022-03-03 12:18:47.108438+00	35	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
526	2022-03-03 12:18:54.504467+00	35	Голяченко Богдан	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
527	2022-03-03 12:19:04.587771+00	36	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
528	2022-03-03 12:19:13.871027+00	36	Сладкевич Василий	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
529	2022-03-03 12:19:25.089378+00	37	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
530	2022-03-03 12:19:33.39088+00	37	Говкиев Бегенч	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
531	2022-03-03 12:19:50.95615+00	38	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
532	2022-03-03 12:19:59.566495+00	38	Волошин Александр	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
533	2022-03-03 12:20:11.598346+00	39	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
534	2022-03-03 12:20:20.150003+00	39	Ященко Ярослав	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
535	2022-03-03 12:20:35.928325+00	40	 	1	[{"added": {}}]	8	1
536	2022-03-03 12:20:39.276722+00	40	 МОЙСИК	2	[{"changed": {"fields": ["First name"]}}]	8	1
537	2022-03-03 12:20:59.230052+00	41	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}, {"added": {"name": "user phone", "object": ""}}]	8	1
538	2022-03-03 12:21:18.044198+00	41	Пенджиев Шохрат	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
539	2022-03-03 12:21:30.850494+00	42	 	1	[{"added": {}}, {"added": {"name": "user phone", "object": ""}}]	8	1
540	2022-03-03 12:21:43.18562+00	42	Юрий Савченко	2	[{"changed": {"fields": ["First name", "Last name"]}}]	8	1
541	2022-03-03 13:19:44.532663+00	1	Seyidov Elvin	2	[{"changed": {"fields": ["Type"]}}]	8	1
542	2022-03-03 13:20:12.909923+00	1	Seyidov Elvin	2	[{"changed": {"fields": ["Type"]}}]	8	1
543	2022-03-03 13:24:18.334381+00	11	Романчук Леонид	2	[{"changed": {"fields": ["Type"]}}]	8	1
544	2022-03-03 13:24:22.996823+00	3	Дмитрий Каюн	2	[{"changed": {"fields": ["Type"]}}]	8	1
545	2022-03-03 13:24:26.512982+00	12	Бережной Сергей	2	[{"changed": {"fields": ["Type"]}}]	8	1
546	2022-03-03 13:24:33.63749+00	13	Исмайлов Ойвен	2	[{"changed": {"fields": ["Type"]}}]	8	1
547	2022-03-03 13:24:37.566796+00	14	выкуп Заур	2	[]	8	1
548	2022-03-03 13:24:41.250973+00	15	Лаптев Олег	2	[{"changed": {"fields": ["Type"]}}]	8	1
549	2022-03-03 13:24:44.523309+00	16	Кара Эрхан	2	[{"changed": {"fields": ["Type"]}}]	8	1
550	2022-03-03 13:24:47.39463+00	17	Кулиев Азат	2	[{"changed": {"fields": ["Type"]}}]	8	1
551	2022-03-03 13:24:50.836808+00	18	выкуп Али	2	[{"changed": {"fields": ["Type"]}}]	8	1
552	2022-03-03 13:24:54.096223+00	19	Чолиев Рустем	2	[]	8	1
553	2022-03-03 13:24:57.086792+00	20	Абдыресулов Рахман	2	[{"changed": {"fields": ["Type"]}}]	8	1
554	2022-03-03 13:25:00.065987+00	21	Абдиев Довлетгельди	2	[{"changed": {"fields": ["Type"]}}]	8	1
555	2022-03-03 13:25:03.073409+00	4	Язгулиев Шохрат	2	[{"changed": {"fields": ["Type"]}}]	8	1
556	2022-03-03 13:25:05.957928+00	22	Москалик Юрий	2	[{"changed": {"fields": ["Type"]}}]	8	1
557	2022-03-03 13:25:09.181982+00	23	Гончаров Сергей	2	[{"changed": {"fields": ["Type"]}}]	8	1
558	2022-03-03 13:25:12.075462+00	24	Фархатов Фарух	2	[{"changed": {"fields": ["Type"]}}]	8	1
559	2022-03-03 13:25:14.889336+00	25	Атаджанов Бегенч	2	[{"changed": {"fields": ["Type"]}}]	8	1
560	2022-03-03 13:25:17.781559+00	26	Валиев Эльвин	2	[{"changed": {"fields": ["Type"]}}]	8	1
561	2022-03-03 13:25:22.228807+00	27	Яхяев Шамиль	2	[{"changed": {"fields": ["Type"]}}]	8	1
562	2022-03-03 13:25:25.430817+00	28	Оммадов Мухамметсердар	2	[{"changed": {"fields": ["Type"]}}]	8	1
563	2022-03-03 13:25:28.444757+00	29	Аманов Довлет	2	[{"changed": {"fields": ["Type"]}}]	8	1
564	2022-03-03 13:25:31.711983+00	30	Бахтияров Умитджан	2	[{"changed": {"fields": ["Type"]}}]	8	1
565	2022-03-03 13:25:34.973417+00	31	Бердиев Сердар	2	[{"changed": {"fields": ["Type"]}}]	8	1
566	2022-03-03 13:25:39.238814+00	5	Чолиев Юнус	2	[{"changed": {"fields": ["Type"]}}]	8	1
567	2022-03-03 13:25:43.06186+00	32	Халилов Сердар	2	[{"changed": {"fields": ["Type"]}}]	8	1
568	2022-03-03 13:25:47.033502+00	33	Бегишов Нургелди	2	[{"changed": {"fields": ["Type"]}}]	8	1
569	2022-03-03 13:25:50.671052+00	34	Аманов Байрам	2	[{"changed": {"fields": ["Type"]}}]	8	1
570	2022-03-03 13:25:54.14732+00	35	Голяченко Богдан	2	[{"changed": {"fields": ["Type"]}}]	8	1
571	2022-03-03 13:25:57.235219+00	36	Сладкевич Василий	2	[{"changed": {"fields": ["Type"]}}]	8	1
572	2022-03-03 13:26:00.679813+00	37	Говкиев Бегенч	2	[{"changed": {"fields": ["Type"]}}]	8	1
573	2022-03-03 13:26:03.560895+00	38	Волошин Александр	2	[{"changed": {"fields": ["Type"]}}]	8	1
574	2022-03-03 13:26:06.288408+00	39	Ященко Ярослав	2	[{"changed": {"fields": ["Type"]}}]	8	1
575	2022-03-03 13:26:09.809887+00	40	 МОЙСИК	2	[{"changed": {"fields": ["Type"]}}]	8	1
576	2022-03-03 13:26:13.165274+00	41	Пенджиев Шохрат	2	[{"changed": {"fields": ["Type"]}}]	8	1
577	2022-03-03 13:26:16.933958+00	6	Акрамов Мукхриддин	2	[{"changed": {"fields": ["Type"]}}]	8	1
578	2022-03-03 13:26:19.901881+00	42	Юрий Савченко	2	[{"changed": {"fields": ["Type"]}}]	8	1
579	2022-03-03 13:26:23.309196+00	7	Авдиев Бабагелди	2	[{"changed": {"fields": ["Type"]}}]	8	1
580	2022-03-03 13:26:28.00859+00	8	Айрапетов Артём	2	[{"changed": {"fields": ["Type"]}}]	8	1
581	2022-03-03 13:26:30.920328+00	9	Батыров Гурбан	2	[{"changed": {"fields": ["Type"]}}]	8	1
582	2022-03-03 13:26:34.256973+00	10	(без договора) ВЫКУП	2	[{"changed": {"fields": ["Type"]}}]	8	1
583	2022-03-03 16:51:46.046353+00	6	Accent - 2011 - АІ 0315 MX  - W	3		29	1
584	2022-03-03 16:51:46.04872+00	5	Accent - 2011 - КA 2219 ВВ  - W	3		29	1
585	2022-03-03 16:51:46.04966+00	4	Accent - 2010 - АI 9756 IH  - W	3		29	1
586	2022-03-03 16:51:46.050512+00	3	Accent - 2017 - АІ 0312 MX  - W	3		29	1
587	2022-03-03 16:51:46.051382+00	2	Avante - 2016 - АІ 8243 OВ  - W	3		29	1
588	2022-03-03 16:55:45.571464+00	12	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Credit	3		9	1
589	2022-03-03 16:55:45.57379+00	11	Seyidov Elvin - Accent - 2011 - АІ 0315 MX  - Taxi	3		9	1
590	2022-03-03 16:55:45.57471+00	10	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Taxi	3		9	1
591	2022-03-03 16:55:45.575534+00	9	Seyidov Elvin - Avante - 2017 - АІ 8064 MO  - Taxi	3		9	1
592	2022-03-03 16:55:45.576355+00	8	Seyidov Elvin - Accent - 2016 - AE 7193 KO  - Taxi	3		9	1
593	2022-03-03 16:55:45.577155+00	7	Seyidov Elvin - Accent - 2011 - КA 2219 ВВ  - Taxi	3		9	1
594	2022-03-03 16:55:45.577992+00	6	Seyidov Elvin - Accent - 2008 - КА 0729 АР  - Taxi	3		9	1
595	2022-03-03 16:55:45.578776+00	5	Seyidov Elvin - Accent - 2010 - АI 9756 IH  - Taxi	3		9	1
596	2022-03-03 16:55:45.579565+00	4	Seyidov Elvin - Accent - 2008 - АI 8250 OB  - Taxi	3		9	1
597	2022-03-03 16:55:45.580312+00	3	Seyidov Elvin - Accent - 2019 - АI 4463 MC  - Taxi	3		9	1
598	2022-03-03 16:55:45.581067+00	2	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	3		9	1
599	2022-03-03 16:55:45.581807+00	1	Mammadxanli Farid - Accent - 2011 - АІ 0315 MX  - Taxi	3		9	1
600	2022-03-03 16:57:58.283+00	13	Дмитрий Каюн - Sportage - 2011 - KА 6164 EC  - Credit	1	[{"added": {}}]	9	1
601	2022-03-03 16:59:03.186093+00	14	Язгулиев Шохрат - Polo - 2015 - KA 0728 AP  - Credit	1	[{"added": {}}]	9	1
602	2022-03-03 17:00:06.907056+00	15	Чолиев Юнус - Fabia - 2015 - АА 8986 СС  - Credit	1	[{"added": {}}]	9	1
603	2022-03-03 17:01:24.507984+00	16	Акрамов Мукхриддин - Rio - 2012 - АА 6038 РР  - Credit	1	[{"added": {}}]	9	1
604	2022-03-03 17:03:12.219882+00	17	Авдиев Бабагелди - Sonata - 2016 - КА 9531 ВК  - Credit	1	[{"added": {}}]	9	1
605	2022-03-03 17:04:06.763902+00	18	Айрапетов Артём - Rio - 2011 - СА 3701 СК  - Credit	1	[{"added": {}}]	9	1
606	2022-03-03 17:07:48.526173+00	19	Батыров Гурбан - Accent - 2010 - АI 9756 IH  - Credit	1	[{"added": {}}]	9	1
607	2022-03-03 17:08:35.357636+00	20	(без договора) ВЫКУП - Avante - 2012 - AІ 9149 МН  - Credit	1	[{"added": {}}]	9	1
608	2022-03-03 17:09:22.434501+00	21	Романчук Леонид - 735i - 2002 - AI 3839 ОС  - Credit	1	[{"added": {}}]	9	1
609	2022-03-03 17:10:17.850421+00	22	Бережной Сергей - Elantra - 2017 - АІ 5508 МР  - Credit	1	[{"added": {}}]	9	1
610	2022-03-03 17:11:16.853343+00	23	Исмайлов Ойвен - Elantra - 2017 - АІ 6703 МР  - Credit	1	[{"added": {}}]	9	1
611	2022-03-03 17:12:02.252995+00	24	выкуп Заур - Carens - 2012 - AI 0712 OВ  - Credit	1	[{"added": {}}]	9	1
612	2022-03-03 17:16:05.033893+00	25	Лаптев Олег - Fabia - 2013 - АІ 2803 СС  - Credit	1	[{"added": {}}]	9	1
613	2022-03-03 17:19:55.50387+00	26	Кара Эрхан - Sonata - 2012 - АI 6805 ОА  - Credit	1	[{"added": {}}]	9	1
614	2022-03-03 17:20:34.979028+00	27	Кулиев Азат - Mazda - 2013 - АІ 8006 МА  - Credit	1	[{"added": {}}]	9	1
615	2022-03-03 17:21:43.416083+00	28	выкуп Али - I30 - 2010 - АА 4934 ВР  - Credit	1	[{"added": {}}]	9	1
616	2022-03-03 17:23:53.507028+00	29	Чолиев Рустем - Accent - 2019 - АI 4463 MC  - Credit	1	[{"added": {}}]	9	1
617	2022-03-03 17:24:44.618569+00	30	Абдыресулов Рахман - Elantra - 2017 - AІ 7365 МI  - Credit	1	[{"added": {}}]	9	1
618	2022-03-03 17:25:42.261865+00	31	Абдиев Довлетгельди - Fabia - 2008 - АІ 7166 ІМ  - Credit	1	[{"added": {}}]	9	1
619	2022-03-03 17:26:20.435944+00	32	Москалик Юрий - I30 - 2011 - АА 3967 ТТ  - Credit	1	[{"added": {}}]	9	1
620	2022-03-03 17:27:04.333382+00	33	Гончаров Сергей - I30 - 2012 - АА 6079 СС  - Credit	1	[{"added": {}}]	9	1
621	2022-03-03 17:28:12.032671+00	34	Фархатов Фарух - Sonata - 2015 - АI 0954 МB  - Credit	1	[{"added": {}}]	9	1
622	2022-03-03 17:28:45.448772+00	35	Атаджанов Бегенч - Jetta - 2008 - АІ 2258 MМ  - Credit	1	[{"added": {}}]	9	1
623	2022-03-03 17:29:25.611694+00	36	Валиев Эльвин - Insignia - 2006 - AI 1495 MO  - Credit	1	[{"added": {}}]	9	1
624	2022-03-03 17:30:16.111272+00	37	Валиев Эльвин - K5 - 2012 - AI 5266 OO  - Credit	1	[{"added": {}}]	9	1
625	2022-03-03 17:31:44.379444+00	38	Яхяев Шамиль - Fabia - 2008 - АА 8037 РР  - Credit	1	[{"added": {}}]	9	1
626	2022-03-03 17:32:16.593774+00	39	Оммадов Мухамметсердар - Rio - 2012 - КА 3306 ВМ  - Credit	1	[{"added": {}}]	9	1
627	2022-03-03 17:33:24.382527+00	40	Аманов Довлет - Accent - 2008 - КА 0729 АР  - Credit	1	[{"added": {}}]	9	1
628	2022-03-03 17:34:07.09137+00	41	Бахтияров Умитджан - Corolla - 2018 - АІ 7462 ОВ  - Credit	1	[{"added": {}}]	9	1
629	2022-03-03 17:34:49.839805+00	42	Бердиев Сердар - I30 - 2008 - АІ 6723 ІН  - Credit	1	[{"added": {}}]	9	1
630	2022-03-03 17:35:30.731044+00	43	Халилов Сердар - Forte - 2011 - AI 9766 OA  - Credit	1	[{"added": {}}]	9	1
631	2022-03-03 17:36:16.192621+00	44	Бегишов Нургелди - Passat - 2004 - AА 1556 ТT  - Credit	1	[{"added": {}}]	9	1
632	2022-03-03 17:37:09.622473+00	45	Аманов Байрам - Polo - 2013 - АЕ 9200 IM  - Credit	1	[{"added": {}}]	9	1
633	2022-03-03 17:38:05.227537+00	46	Голяченко Богдан - Rio - 2012 - АІ 1893 ІІ  - Credit	1	[{"added": {}}]	9	1
634	2022-03-03 17:39:14.165816+00	47	Сладкевич Василий - Sonata - 2015 - АА 1058 ЕМ  - Credit	1	[{"added": {}}]	9	1
635	2022-03-03 17:40:14.440708+00	48	Говкиев Бегенч - Sonata - 2015 - АМ 3560 ЕО  - Credit	1	[{"added": {}}]	9	1
636	2022-03-03 17:41:53.58605+00	49	Волошин Александр - Sonata - 2015 - АI 3427 MP  - Credit	1	[{"added": {}}]	9	1
637	2022-03-03 17:43:00.114618+00	50	Ященко Ярослав - Sonata - 2012 - АI 6786 ОА  - Credit	1	[{"added": {}}]	9	1
638	2022-03-03 17:44:02.021086+00	51	 МОЙСИК - K5 - 2015 - АІ 8089 MI  - Credit	1	[{"added": {}}]	9	1
639	2022-03-03 17:44:43.160692+00	52	Пенджиев Шохрат - K5 - 2015 - АІ 0589 MІ  - Credit	1	[{"added": {}}]	9	1
640	2022-03-03 17:45:48.869399+00	53	Юрий Савченко - Sorento - 2016 - AI 2111 MB  - Credit	1	[{"added": {}}]	9	1
641	2022-03-03 17:46:41.423364+00	54	Кулиев Азат - Corolla - 2011 - АІ 6453 ОО  - Credit	1	[{"added": {}}]	9	1
642	2022-03-03 18:54:36.327558+00	7	Sportage - 2011 - KА 6164 EC  - M	1	[{"added": {}}]	29	1
643	2022-03-03 18:56:35.895939+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[{"added": {"name": "credit payment", "object": "2022-03-03"}}]	29	1
644	2022-03-03 19:13:26.336389+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[{"changed": {"fields": ["Total"]}}, {"deleted": {"name": "credit payment", "object": "2022-03-03"}}]	29	1
645	2022-03-03 19:17:44.484177+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[{"changed": {"fields": ["Payment count"]}}]	29	1
646	2022-03-03 20:00:02.445949+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[]	29	1
647	2022-03-03 20:34:22.146251+00	8	Polo - 2015 - KA 0728 AP  - W	1	[{"added": {}}]	29	1
648	2022-03-03 20:41:14.687311+00	8	Polo - 2015 - KA 0728 AP  - W	2	[]	29	1
649	2022-03-03 20:42:10.365673+00	8	Polo - 2015 - KA 0728 AP  - W	2	[]	29	1
650	2022-03-03 20:46:03.30131+00	9	Fabia - 2015 - АА 8986 СС  - W	1	[{"added": {}}]	29	1
651	2022-03-03 20:46:21.506109+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
652	2022-03-03 20:47:08.919434+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[]	29	1
653	2022-03-03 20:48:18.946622+00	10	Rio - 2012 - АА 6038 РР  - W	1	[{"added": {}}]	29	1
654	2022-03-03 20:48:30.818228+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
655	2022-03-03 20:48:51.337049+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
656	2022-03-03 20:49:03.133682+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
657	2022-03-03 20:50:56.693195+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (7)"}}]	29	1
658	2022-03-03 20:51:18.420586+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[{"added": {"name": "credit image", "object": "CreditImage object (8)"}}]	29	1
659	2022-03-03 20:51:29.444466+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (9)"}}]	29	1
660	2022-03-03 20:51:35.715945+00	8	Polo - 2015 - KA 0728 AP  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (10)"}}]	29	1
661	2022-03-03 20:54:13.820942+00	11	Sonata - 2016 - КА 9531 ВК  - W	1	[{"added": {}}]	29	1
662	2022-03-03 20:54:29.935405+00	11	Sonata - 2016 - КА 9531 ВК  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (11)"}}]	29	1
663	2022-03-03 21:47:27.848582+00	11	Sonata - 2016 - КА 9531 ВК  - W	2	[]	29	1
664	2022-03-03 21:48:39.603037+00	12	Rio - 2011 - СА 3701 СК  - W	1	[{"added": {}}]	29	1
665	2022-03-03 21:48:58.300311+00	12	Rio - 2011 - СА 3701 СК  - W	2	[]	29	1
666	2022-03-03 21:52:26.341117+00	13	Accent - 2010 - АI 9756 IH  - W	1	[{"added": {}}]	29	1
667	2022-03-03 21:56:05.806246+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"changed": {"fields": ["Payment year"]}}]	29	1
668	2022-03-03 22:03:01.018292+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
669	2022-03-03 22:37:22.360536+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[]	29	1
670	2022-03-03 22:37:28.112949+00	7	Sportage - 2011 - KА 6164 EC  - M	2	[]	29	1
671	2022-03-03 22:38:53.500793+00	8	Polo - 2015 - KA 0728 AP  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
672	2022-03-03 22:39:01.557186+00	8	Polo - 2015 - KA 0728 AP  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
673	2022-03-03 22:39:10.25001+00	8	Polo - 2015 - KA 0728 AP  - W	2	[]	29	1
674	2022-03-03 22:39:50.496199+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
675	2022-03-03 22:40:01.385884+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
676	2022-03-03 22:40:09.71282+00	9	Fabia - 2015 - АА 8986 СС  - W	2	[]	29	1
677	2022-03-03 22:40:47.148975+00	10	Rio - 2012 - АА 6038 РР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
678	2022-03-03 22:41:19.661497+00	11	Sonata - 2016 - КА 9531 ВК  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
679	2022-03-03 22:42:04.144025+00	12	Rio - 2011 - СА 3701 СК  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
680	2022-03-03 22:42:07.670723+00	12	Rio - 2011 - СА 3701 СК  - W	2	[]	29	1
681	2022-03-03 22:43:07.301254+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
682	2022-03-03 22:43:54.30903+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
683	2022-03-05 17:28:39.607512+00	14	Avante - 2012 - AІ 9149 МН  - W	1	[{"added": {}}]	29	1
684	2022-03-05 17:36:44.842926+00	14	Avante - 2012 - AІ 9149 МН  - W	2	[]	29	1
685	2022-03-05 17:37:44.330418+00	15	735i - 2002 - AI 3839 ОС  - W	1	[{"added": {}}]	29	1
686	2022-03-05 17:37:53.870923+00	15	735i - 2002 - AI 3839 ОС  - W	2	[]	29	1
687	2022-03-05 17:38:57.592586+00	16	Elantra - 2017 - АІ 5508 МР  - W	1	[{"added": {}}]	29	1
688	2022-03-05 17:43:05.51233+00	16	Elantra - 2017 - АІ 5508 МР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
689	2022-03-05 17:43:15.713046+00	16	Elantra - 2017 - АІ 5508 МР  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
690	2022-03-05 17:44:10.228238+00	16	Elantra - 2017 - АІ 5508 МР  - W	2	[]	29	1
691	2022-03-05 17:45:38.56776+00	17	Elantra - 2017 - АІ 6703 МР  - W	1	[{"added": {}}]	29	1
692	2022-03-05 17:45:47.568407+00	17	Elantra - 2017 - АІ 6703 МР  - W	2	[]	29	1
693	2022-03-05 17:46:55.51965+00	18	Carens - 2012 - AI 0712 OВ  - W	1	[{"added": {}}]	29	1
694	2022-03-05 17:47:02.994106+00	18	Carens - 2012 - AI 0712 OВ  - W	2	[]	29	1
695	2022-03-05 18:52:48.307465+00	19	Fabia - 2013 - АІ 2803 СС  - W	1	[{"added": {}}]	29	1
696	2022-03-05 18:53:02.648658+00	19	Fabia - 2013 - АІ 2803 СС  - W	2	[]	29	1
697	2022-03-05 18:54:12.882532+00	20	Sonata - 2012 - АI 6805 ОА  - W	1	[{"added": {}}]	29	1
698	2022-03-05 18:54:29.503049+00	20	Sonata - 2012 - АI 6805 ОА  - W	2	[]	29	1
699	2022-03-05 18:56:26.189352+00	21	Mazda - 2013 - АІ 8006 МА  - W	1	[{"added": {}}]	29	1
700	2022-03-05 18:56:32.7022+00	21	Mazda - 2013 - АІ 8006 МА  - W	2	[]	29	1
701	2022-03-05 18:58:20.676933+00	22	I30 - 2010 - АА 4934 ВР  - W	1	[{"added": {}}]	29	1
702	2022-03-05 18:58:24.835139+00	22	I30 - 2010 - АА 4934 ВР  - W	2	[]	29	1
703	2022-03-05 18:59:45.425229+00	23	Accent - 2019 - АI 4463 MC  - W	1	[{"added": {}}]	29	1
704	2022-03-05 19:00:01.007641+00	23	Accent - 2019 - АI 4463 MC  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
705	2022-03-05 19:00:04.811918+00	23	Accent - 2019 - АI 4463 MC  - W	2	[]	29	1
706	2022-03-05 19:01:38.824596+00	24	Elantra - 2017 - AІ 7365 МI  - W	1	[{"added": {}}]	29	1
707	2022-03-05 19:01:50.379698+00	24	Elantra - 2017 - AІ 7365 МI  - W	2	[]	29	1
708	2022-03-05 19:02:58.442605+00	25	Fabia - 2008 - АІ 7166 ІМ  - W	1	[{"added": {}}]	29	1
709	2022-03-05 19:03:02.662422+00	25	Fabia - 2008 - АІ 7166 ІМ  - W	2	[]	29	1
710	2022-03-05 19:04:21.811256+00	26	I30 - 2011 - АА 3967 ТТ  - W	1	[{"added": {}}]	29	1
711	2022-03-05 19:04:27.972062+00	26	I30 - 2011 - АА 3967 ТТ  - W	2	[]	29	1
712	2022-03-05 19:05:19.778854+00	27	I30 - 2012 - АА 6079 СС  - W	1	[{"added": {}}]	29	1
713	2022-03-05 19:05:23.897659+00	27	I30 - 2012 - АА 6079 СС  - W	2	[]	29	1
714	2022-03-05 19:06:39.467063+00	28	Sonata - 2015 - АI 0954 МB  - W	1	[{"added": {}}]	29	1
715	2022-03-05 19:06:47.898589+00	28	Sonata - 2015 - АI 0954 МB  - W	2	[]	29	1
716	2022-03-05 19:08:07.915225+00	29	Jetta - 2008 - АІ 2258 MМ  - W	1	[{"added": {}}]	29	1
717	2022-03-05 19:08:13.458036+00	29	Jetta - 2008 - АІ 2258 MМ  - W	2	[]	29	1
718	2022-03-05 19:23:33.386493+00	30	Insignia - 2006 - AI 1495 MO  - W	1	[{"added": {}}]	29	1
719	2022-03-05 19:23:38.480816+00	30	Insignia - 2006 - AI 1495 MO  - W	2	[]	29	1
720	2022-03-05 19:24:26.753363+00	31	K5 - 2012 - AI 5266 OO  - W	1	[{"added": {}}]	29	1
721	2022-03-05 19:24:34.176554+00	31	K5 - 2012 - AI 5266 OO  - W	2	[]	29	1
722	2022-03-05 19:26:10.344706+00	32	Fabia - 2008 - АА 8037 РР  - W	1	[{"added": {}}]	29	1
723	2022-03-05 19:26:14.961984+00	32	Fabia - 2008 - АА 8037 РР  - W	2	[]	29	1
724	2022-03-05 19:27:41.429342+00	33	Rio - 2012 - КА 3306 ВМ  - W	1	[{"added": {}}]	29	1
725	2022-03-05 19:27:48.800539+00	33	Rio - 2012 - КА 3306 ВМ  - W	2	[]	29	1
726	2022-03-05 19:41:51.988105+00	34	Accent - 2008 - КА 0729 АР  - W	1	[{"added": {}}]	29	1
727	2022-03-05 19:41:59.859833+00	34	Accent - 2008 - КА 0729 АР  - W	2	[]	29	1
728	2022-03-05 19:43:04.850505+00	35	Corolla - 2018 - АІ 7462 ОВ  - W	1	[{"added": {}}]	29	1
729	2022-03-05 19:43:15.459934+00	35	Corolla - 2018 - АІ 7462 ОВ  - W	2	[]	29	1
730	2022-03-05 19:44:32.262125+00	36	I30 - 2008 - АІ 6723 ІН  - W	1	[{"added": {}}]	29	1
731	2022-03-05 19:44:44.049666+00	36	I30 - 2008 - АІ 6723 ІН  - W	2	[]	29	1
732	2022-03-05 19:45:46.205286+00	37	Forte - 2011 - AI 9766 OA  - W	1	[{"added": {}}]	29	1
733	2022-03-05 19:45:57.601501+00	37	Forte - 2011 - AI 9766 OA  - W	2	[{"changed": {"fields": ["Car value total"]}}]	29	1
734	2022-03-05 19:46:05.279299+00	37	Forte - 2011 - AI 9766 OA  - W	2	[]	29	1
735	2022-03-05 19:47:07.629066+00	38	Passat - 2004 - AА 1556 ТT  - W	1	[{"added": {}}]	29	1
736	2022-03-05 19:47:14.219733+00	38	Passat - 2004 - AА 1556 ТT  - W	2	[]	29	1
737	2022-03-05 19:48:22.170759+00	39	Polo - 2013 - АЕ 9200 IM  - W	1	[{"added": {}}]	29	1
738	2022-03-05 19:48:29.814009+00	39	Polo - 2013 - АЕ 9200 IM  - W	2	[]	29	1
739	2022-03-05 19:49:38.221405+00	40	Rio - 2012 - АІ 1893 ІІ  - W	1	[{"added": {}}]	29	1
740	2022-03-05 19:49:42.355917+00	40	Rio - 2012 - АІ 1893 ІІ  - W	2	[]	29	1
741	2022-03-05 19:51:01.049293+00	41	Sonata - 2015 - АА 1058 ЕМ  - W	1	[{"added": {}}]	29	1
742	2022-03-05 19:51:06.115833+00	41	Sonata - 2015 - АА 1058 ЕМ  - W	2	[]	29	1
743	2022-03-05 19:51:48.994371+00	42	Sonata - 2015 - АМ 3560 ЕО  - W	1	[{"added": {}}]	29	1
744	2022-03-05 19:51:56.832357+00	42	Sonata - 2015 - АМ 3560 ЕО  - W	2	[]	29	1
745	2022-03-05 19:52:45.276287+00	43	Sonata - 2015 - АI 3427 MP  - W	1	[{"added": {}}]	29	1
746	2022-03-05 19:52:49.170708+00	43	Sonata - 2015 - АI 3427 MP  - W	2	[]	29	1
747	2022-03-05 19:57:51.395442+00	44	Sonata - 2012 - АI 6786 ОА  - W	1	[{"added": {}}]	29	1
748	2022-03-05 19:57:56.867214+00	44	Sonata - 2012 - АI 6786 ОА  - W	2	[]	29	1
749	2022-03-05 19:59:02.941164+00	45	K5 - 2015 - АІ 8089 MI  - W	1	[{"added": {}}]	29	1
750	2022-03-05 19:59:08.27087+00	45	K5 - 2015 - АІ 8089 MI  - W	2	[]	29	1
751	2022-03-05 20:00:53.205009+00	46	K5 - 2015 - АІ 0589 MІ  - W	1	[{"added": {}}]	29	1
752	2022-03-05 20:01:03.368443+00	46	K5 - 2015 - АІ 0589 MІ  - W	2	[]	29	1
753	2022-03-05 20:05:23.511566+00	47	Sorento - 2016 - AI 2111 MB  - W	1	[{"added": {}}]	29	1
754	2022-03-05 20:05:32.838204+00	47	Sorento - 2016 - AI 2111 MB  - W	2	[]	29	1
755	2022-03-05 20:06:01.207649+00	47	Sorento - 2016 - AI 2111 MB  - W	2	[{"changed": {"fields": ["Payment date"]}}]	29	1
756	2022-03-05 20:07:14.658935+00	48	Corolla - 2011 - АІ 6453 ОО  - W	1	[{"added": {}}]	29	1
757	2022-03-05 20:07:31.747399+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[]	29	1
758	2022-03-05 20:15:39.661488+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (12)"}}]	29	1
759	2022-03-05 20:15:45.155179+00	47	Sorento - 2016 - AI 2111 MB  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (13)"}}]	29	1
760	2022-03-05 20:15:51.16831+00	46	K5 - 2015 - АІ 0589 MІ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (14)"}}]	29	1
761	2022-03-05 20:15:56.501604+00	45	K5 - 2015 - АІ 8089 MI  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (15)"}}]	29	1
762	2022-03-05 20:16:01.98531+00	44	Sonata - 2012 - АI 6786 ОА  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (16)"}}]	29	1
763	2022-03-05 20:16:07.351229+00	43	Sonata - 2015 - АI 3427 MP  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (17)"}}]	29	1
764	2022-03-05 20:16:12.341527+00	42	Sonata - 2015 - АМ 3560 ЕО  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (18)"}}]	29	1
765	2022-03-05 20:16:17.854575+00	41	Sonata - 2015 - АА 1058 ЕМ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (19)"}}]	29	1
766	2022-03-05 20:16:23.274201+00	40	Rio - 2012 - АІ 1893 ІІ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (20)"}}]	29	1
767	2022-03-05 20:16:28.593736+00	39	Polo - 2013 - АЕ 9200 IM  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (21)"}}]	29	1
768	2022-03-05 20:16:34.118886+00	38	Passat - 2004 - AА 1556 ТT  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (22)"}}]	29	1
769	2022-03-05 20:16:40.070803+00	37	Forte - 2011 - AI 9766 OA  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (23)"}}]	29	1
770	2022-03-05 20:16:45.861245+00	36	I30 - 2008 - АІ 6723 ІН  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (24)"}}]	29	1
771	2022-03-05 20:16:51.331724+00	35	Corolla - 2018 - АІ 7462 ОВ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (25)"}}]	29	1
772	2022-03-05 20:16:56.685814+00	34	Accent - 2008 - КА 0729 АР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (26)"}}]	29	1
773	2022-03-05 20:17:02.108713+00	33	Rio - 2012 - КА 3306 ВМ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (27)"}}]	29	1
774	2022-03-05 20:17:07.253445+00	32	Fabia - 2008 - АА 8037 РР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (28)"}}]	29	1
775	2022-03-05 20:17:12.380713+00	31	K5 - 2012 - AI 5266 OO  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (29)"}}]	29	1
776	2022-03-05 20:17:17.580226+00	30	Insignia - 2006 - AI 1495 MO  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (30)"}}]	29	1
777	2022-03-05 20:17:22.771222+00	29	Jetta - 2008 - АІ 2258 MМ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (31)"}}]	29	1
778	2022-03-05 20:17:28.314141+00	28	Sonata - 2015 - АI 0954 МB  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (32)"}}]	29	1
779	2022-03-05 20:17:32.820392+00	27	I30 - 2012 - АА 6079 СС  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (33)"}}]	29	1
780	2022-03-05 20:17:37.531125+00	26	I30 - 2011 - АА 3967 ТТ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (34)"}}]	29	1
781	2022-03-05 20:17:43.130225+00	25	Fabia - 2008 - АІ 7166 ІМ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (35)"}}]	29	1
782	2022-03-05 20:17:48.386591+00	24	Elantra - 2017 - AІ 7365 МI  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (36)"}}]	29	1
783	2022-03-05 20:17:53.568453+00	23	Accent - 2019 - АI 4463 MC  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (37)"}}]	29	1
784	2022-03-05 20:17:58.448439+00	22	I30 - 2010 - АА 4934 ВР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (38)"}}]	29	1
785	2022-03-05 20:18:04.191664+00	21	Mazda - 2013 - АІ 8006 МА  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (39)"}}]	29	1
786	2022-03-05 20:18:09.123957+00	20	Sonata - 2012 - АI 6805 ОА  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (40)"}}]	29	1
787	2022-03-05 20:18:13.790121+00	19	Fabia - 2013 - АІ 2803 СС  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (41)"}}]	29	1
788	2022-03-05 20:18:19.130371+00	18	Carens - 2012 - AI 0712 OВ  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (42)"}}]	29	1
789	2022-03-05 20:18:24.511609+00	17	Elantra - 2017 - АІ 6703 МР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (43)"}}]	29	1
790	2022-03-05 20:18:29.808944+00	16	Elantra - 2017 - АІ 5508 МР  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (44)"}}]	29	1
791	2022-03-05 20:18:35.548329+00	15	735i - 2002 - AI 3839 ОС  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (45)"}}]	29	1
792	2022-03-05 20:18:40.684377+00	14	Avante - 2012 - AІ 9149 МН  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (46)"}}]	29	1
793	2022-03-05 20:18:47.035054+00	13	Accent - 2010 - АI 9756 IH  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (47)"}}]	29	1
794	2022-03-05 20:18:53.038349+00	12	Rio - 2011 - СА 3701 СК  - W	2	[{"added": {"name": "credit image", "object": "CreditImage object (48)"}}]	29	1
795	2022-03-05 20:33:04.606853+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	1	[{"added": {}}]	9	1
796	2022-03-05 20:33:17.064938+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Credit	2	[{"changed": {"fields": ["Type"]}}]	9	1
797	2022-03-05 20:33:30.292626+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	2	[{"changed": {"fields": ["Type", "End date"]}}]	9	1
798	2022-03-05 20:34:35.485832+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	2	[]	9	1
799	2022-03-05 20:34:48.620978+00	56	Mammadxanli Farid - Accent - 2017 - АІ 0312 MX  - Taxi	1	[{"added": {}}]	9	1
800	2022-03-05 20:35:04.576625+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	2	[{"changed": {"fields": ["End date"]}}]	9	1
801	2022-03-05 20:35:17.600988+00	55	Seyidov Elvin - Accent - 2017 - АІ 0312 MX  - Taxi	3		9	1
802	2022-03-05 20:35:21.911337+00	56	Mammadxanli Farid - Accent - 2017 - АІ 0312 MX  - Taxi	3		9	1
803	2022-03-06 12:37:17.878355+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Taxi	1	[{"added": {}}]	9	1
804	2022-03-06 12:37:29.037225+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"added": {"name": "revenue", "object": "Revenue object (41)"}}]	34	1
805	2022-03-06 12:38:20.24871+00	41	Revenue object (41)	2	[{"added": {"name": "revenue item", "object": "RevenueItem object (99)"}}]	33	1
806	2022-03-06 12:38:46.248056+00	41	Revenue object (41)	2	[{"changed": {"name": "revenue item", "object": "RevenueItem object (99)", "fields": ["Amount cash", "Amount card"]}}]	33	1
807	2022-03-06 12:44:13.70529+00	41	Revenue object (41)	2	[{"added": {"name": "revenue item", "object": "RevenueItem object (100)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (101)"}}]	33	1
808	2022-03-11 12:59:05.02306+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-06"}}, {"deleted": {"name": "credit payment", "object": "2022-03-05"}}, {"deleted": {"name": "credit payment", "object": "2022-03-05"}}]	29	1
809	2022-03-11 12:59:17.309232+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"added": {"name": "credit payment", "object": "2022-03-11"}}]	29	1
810	2022-03-11 13:00:57.510406+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"added": {"name": "credit payment", "object": "2022-03-11"}}]	29	1
811	2022-03-11 13:11:28.436096+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-11"}}, {"deleted": {"name": "credit payment", "object": "2022-03-11"}}]	29	1
812	2022-03-11 13:11:37.67826+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"added": {"name": "credit payment", "object": "2022-03-11"}}]	29	1
813	2022-03-11 14:24:20.996924+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-11"}}]	29	1
814	2022-03-11 20:40:37.217895+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"changed": {"name": "credit payment", "object": "2022-03-11", "fields": ["Is verified"]}}]	29	1
815	2022-03-11 20:40:41.506442+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"changed": {"name": "credit payment", "object": "2022-03-11", "fields": ["Is verified"]}}]	29	1
816	2022-03-11 20:40:46.213324+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"changed": {"name": "credit payment", "object": "2022-03-11", "fields": ["Is verified"]}}]	29	1
817	2022-03-12 09:22:17.994302+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"changed": {"fields": ["Payment count", "Payment year", "Car value total", "First payment", "Percent total", "Percent expenses"]}}]	29	1
818	2022-03-12 09:24:21.979764+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-12"}}, {"deleted": {"name": "credit payment", "object": "2022-03-11"}}, {"deleted": {"name": "credit payment", "object": "2022-03-11"}}]	29	1
819	2022-03-12 10:11:32.574358+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-12"}}]	29	1
820	2022-03-12 10:12:04.600456+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-12"}}]	29	1
821	2022-03-12 10:23:03.876873+00	1	Sedan	1	[{"added": {}}]	18	1
822	2022-03-12 10:23:06.557667+00	18	Corolla - 2011 - АІ 6453 ОО 	2	[{"changed": {"fields": ["Case"]}}]	15	1
823	2022-03-12 10:23:08.386628+00	54	Кулиев Азат - Corolla - 2011 - АІ 6453 ОО  - Credit	2	[]	9	1
824	2022-03-12 10:40:40.01507+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-09"}}, {"deleted": {"name": "credit payment", "object": "2022-03-08"}}, {"deleted": {"name": "credit payment", "object": "2022-03-07"}}, {"deleted": {"name": "credit payment", "object": "2022-03-06"}}, {"deleted": {"name": "credit payment", "object": "2022-03-05"}}, {"deleted": {"name": "credit payment", "object": "2022-03-04"}}, {"deleted": {"name": "credit payment", "object": "2022-03-03"}}, {"deleted": {"name": "credit payment", "object": "2022-03-02"}}, {"deleted": {"name": "credit payment", "object": "2022-03-01"}}]	29	1
825	2022-03-12 10:42:58.360169+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-01"}}]	29	1
826	2022-03-12 10:45:09.404547+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-01"}}]	29	1
827	2022-03-12 10:47:31.9452+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-01"}}]	29	1
828	2022-03-12 10:56:48.696761+00	48	Corolla - 2011 - АІ 6453 ОО  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-10"}}, {"deleted": {"name": "credit payment", "object": "2022-03-10"}}]	29	1
829	2022-03-12 10:59:06.657096+00	1	White	1	[{"added": {}}]	19	1
830	2022-03-12 10:59:09.386827+00	18	Corolla - 2011 - АІ 6453 ОО 	2	[{"changed": {"fields": ["Color"]}}]	15	1
831	2022-03-12 10:59:11.270287+00	54	Кулиев Азат - Corolla - 2011 - АІ 6453 ОО  - Credit	2	[]	9	1
832	2022-03-12 11:10:58.095976+00	9	Avante - 2017 - АІ 8064 MO	3		34	1
833	2022-03-12 11:10:58.098205+00	8	Accent - 2016 - AE 7193 KO	3		34	1
834	2022-03-12 11:10:58.099089+00	7	Accent - 2011 - КA 2219 ВВ	3		34	1
835	2022-03-12 11:10:58.099903+00	6	Accent - 2008 - КА 0729 АР	3		34	1
836	2022-03-12 11:10:58.100704+00	5	Accent - 2010 - АI 9756 IH	3		34	1
837	2022-03-12 11:10:58.101596+00	4	Accent - 2008 - АI 8250 OB	3		34	1
838	2022-03-12 11:10:58.102702+00	3	Accent - 2019 - АI 4463 MC	3		34	1
839	2022-03-12 11:10:58.103453+00	2	Accent - 2017 - АІ 0312 MX	3		34	1
840	2022-03-12 11:10:58.10417+00	1	Accent - 2011 - АІ 0315 MX	3		34	1
841	2022-03-12 11:32:52.598879+00	41	Revenue object (41)	3		33	1
842	2022-03-12 14:19:34.383268+00	1	Tariffs	1	[{"added": {}}]	43	1
843	2022-03-12 14:20:19.163336+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}]	34	1
844	2022-03-12 14:22:01.588314+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}]	34	1
845	2022-03-12 14:52:54.610394+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}]	34	1
846	2022-03-12 15:01:39.61375+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}, {"deleted": {"name": "revenue", "object": "Revenue object (None)"}}]	34	1
847	2022-03-12 15:14:55.582086+00	1	RentTaxi object (1)	1	[{"added": {}}, {"added": {"name": "revenue", "object": "Revenue object (1)"}}]	36	1
848	2022-03-12 15:34:53.262025+00	1	Tariffs	2	[]	43	1
849	2022-03-12 15:37:35.818067+00	1	Tariffs	2	[]	43	1
850	2022-03-12 15:37:43.701715+00	2	Tariffs	1	[{"added": {}}]	43	1
851	2022-03-12 15:37:48.498047+00	3	Tariffs	1	[{"added": {}}]	43	1
852	2022-03-12 15:38:00.065516+00	1	Tariffs	1	[{"added": {}}]	44	1
853	2022-03-12 15:38:05.367474+00	2	Tariffs	1	[{"added": {}}]	44	1
854	2022-03-12 15:38:08.504749+00	3	Tariffs	1	[{"added": {}}]	44	1
855	2022-03-12 18:54:42.779996+00	64	Revenue object (64)	2	[{"deleted": {"name": "revenue item", "object": "RevenueItem object (None)"}}, {"deleted": {"name": "revenue item", "object": "RevenueItem object (None)"}}, {"deleted": {"name": "revenue item", "object": "RevenueItem object (None)"}}]	33	1
856	2022-03-12 18:58:20.262719+00	1	Revenue object (1)	2	[{"added": {"name": "revenue item", "object": "RevenueItem object (1)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (2)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (3)"}}]	37	1
857	2022-03-12 19:00:35.593277+00	64	Revenue object (64)	2	[{"added": {"name": "revenue item", "object": "RevenueItem object (177)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (178)"}}, {"added": {"name": "revenue item", "object": "RevenueItem object (179)"}}]	33	1
858	2022-03-12 19:01:08.452134+00	66	Revenue object (66)	3		33	1
859	2022-03-12 19:01:08.454434+00	65	Revenue object (65)	3		33	1
860	2022-03-12 19:01:08.455333+00	64	Revenue object (64)	3		33	1
861	2022-03-12 19:20:04.013834+00	1	Revenue object (1)	2	[{"deleted": {"name": "revenue item", "object": "RevenueItem object (None)"}}, {"deleted": {"name": "revenue item", "object": "RevenueItem object (None)"}}, {"deleted": {"name": "revenue item", "object": "RevenueItem object (None)"}}]	37	1
862	2022-03-12 19:21:30.763531+00	1	Revenue object (1)	2	[{"added": {"name": "revenue item", "object": "RevenueItem object (4)"}}]	37	1
863	2022-03-13 12:05:34.300866+00	77	Sorento - 2016 - AI 2111 MB 	2	[{"changed": {"fields": ["Case", "Color"]}}]	15	1
864	2022-03-13 12:05:36.089082+00	53	Юрий Савченко - Sorento - 2016 - AI 2111 MB  - Credit	2	[]	9	1
865	2022-03-13 12:05:38.852224+00	47	Sorento - 2016 - AI 2111 MB  - W	2	[]	29	1
866	2022-03-13 13:32:31.787262+00	47	Sorento - 2016 - AI 2111 MB  - W	2	[{"deleted": {"name": "credit payment", "object": "2022-03-15"}}, {"deleted": {"name": "credit payment", "object": "2022-03-13"}}, {"deleted": {"name": "credit payment", "object": "2022-03-11"}}, {"deleted": {"name": "credit payment", "object": "2022-03-09"}}]	29	1
867	2022-03-17 21:18:54.511236+00	134	Forester - 2007 - AI 8500 MT 	2	[{"changed": {"name": "car image", "object": "Forester - 2007 - AI 8500 MT ", "fields": ["Path"]}}]	15	1
868	2022-03-17 21:20:22.481127+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"name": "car image", "object": "Subaru - 2011 - AI 7103 OB ", "fields": ["Path"]}}]	15	1
869	2022-03-17 21:21:04.319845+00	132	Sonata - 2016 - АI 0886 KP 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 0886 KP ", "fields": ["Path"]}}]	15	1
870	2022-03-17 21:22:48.306423+00	126	Sonata - 2016 - КА 9531 ВК 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 9531 \\u0412\\u041a ", "fields": ["Path"]}}]	15	1
871	2022-03-17 21:22:54.120026+00	127	Sonata - 2016 - СА 5407 ІВ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0421\\u0410 5407 \\u0406\\u0412 ", "fields": ["Path"]}}]	15	1
872	2022-03-17 21:22:59.486163+00	128	Sonata - 2014 - СА 5408 ІВ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2014 - \\u0421\\u0410 5408 \\u0406\\u0412 ", "fields": ["Path"]}}]	15	1
1142	2022-03-18 22:47:18.770935+00	85	Rio - 2012 - АА 6038 РР 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
873	2022-03-17 21:23:04.024343+00	129	Sonata - 2015 - АI 8894 ММ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 8894 \\u041c\\u041c ", "fields": ["Path"]}}]	15	1
874	2022-03-17 21:23:37.427682+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 3427 MP ", "fields": ["Path"]}}]	15	1
875	2022-03-17 21:23:42.734857+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 3427 MP ", "fields": ["Path"]}}]	15	1
876	2022-03-17 21:23:47.545931+00	131	Sonata - 2015 - АА 1058 ЕМ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410\\u0410 1058 \\u0415\\u041c ", "fields": ["Path"]}}]	15	1
877	2022-03-17 21:24:24.478548+00	125	Sonata - 2016 - КА 8962 ВМ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 8962 \\u0412\\u041c ", "fields": ["Path"]}}]	15	1
878	2022-03-17 21:25:50.375469+00	85	Rio - 2012 - АА 6038 РР 	2	[{"added": {"name": "car image", "object": "Rio - 2012 - \\u0410\\u0410 6038 \\u0420\\u0420 "}}]	15	1
879	2022-03-17 21:26:27.723666+00	86	Rio - 2012 - АІ 1893 ІІ 	2	[{"added": {"name": "car image", "object": "Rio - 2012 - \\u0410\\u0406 1893 \\u0406\\u0406 "}}]	15	1
880	2022-03-17 21:26:45.436833+00	87	Rio - 2012 - КА 3306 ВМ 	2	[{"changed": {"name": "car image", "object": "Rio - 2012 - \\u041a\\u0410 3306 \\u0412\\u041c ", "fields": ["Path"]}}]	15	1
881	2022-03-17 21:27:20.889374+00	88	Rio - 2011 - СА 3701 СК 	2	[{"changed": {"name": "car image", "object": "Rio - 2011 - \\u0421\\u0410 3701 \\u0421\\u041a ", "fields": ["Path"]}}]	15	1
882	2022-03-17 21:27:41.376318+00	89	Sonata - 2012 - АI 6782 ОА 	2	[{"changed": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 6782 \\u041e\\u0410 ", "fields": ["Path"]}}]	15	1
883	2022-03-17 21:27:56.12129+00	90	Sonata - 2012 - АI 6786 ОА 	2	[{"changed": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 6786 \\u041e\\u0410 ", "fields": ["Path"]}}]	15	1
884	2022-03-17 21:28:11.861329+00	91	Sonata - 2012 - АI 6805 ОА 	2	[{"changed": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 6805 \\u041e\\u0410 ", "fields": ["Path"]}}]	15	1
885	2022-03-17 21:28:30.766073+00	92	Sonata - 2016 - АI 6809 ОА 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 6809 \\u041e\\u0410 ", "fields": ["Path"]}}]	15	1
886	2022-03-17 21:28:44.354916+00	93	Sonata - 2015 - АМ 3560 ЕО 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410\\u041c 3560 \\u0415\\u041e ", "fields": ["Path"]}}]	15	1
887	2022-03-17 21:28:54.113704+00	94	Sonata - 2015 - АI 0253 МР 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 0253 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
888	2022-03-17 21:29:05.264928+00	95	Sonata - 2015 - АI 0954 МB 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 0954 \\u041cB ", "fields": ["Path"]}}]	15	1
889	2022-03-17 21:29:17.822666+00	96	Sonata - 2015 - АI 0962 МB 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 0962 \\u041cB ", "fields": ["Path"]}}]	15	1
890	2022-03-17 21:29:35.831098+00	97	Sonata - 2016 - АI 1256 ММ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 1256 \\u041c\\u041c ", "fields": ["Path"]}}]	15	1
891	2022-03-17 21:29:54.341243+00	98	Sonata - 2016 - АI 2518 МР 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 2518 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
892	2022-03-17 21:30:09.047918+00	99	Sonata - 2016 - АI 2519 МР 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 2519 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
893	2022-03-17 21:30:25.261971+00	100	Sonata - 2015 - АI 2846 МР 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 2846 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
894	2022-03-17 21:30:37.554989+00	101	Sonata - 2015 - АI 2875 ММ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 2875 \\u041c\\u041c ", "fields": ["Path"]}}]	15	1
895	2022-03-17 21:30:47.287764+00	102	Sonata - 2017 - АI 2953 ММ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2017 - \\u0410I 2953 \\u041c\\u041c ", "fields": ["Path"]}}]	15	1
896	2022-03-17 21:31:10.480149+00	103	Sonata - 2016 - АI 3702 MP 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 3702 MP ", "fields": ["Path"]}}]	15	1
897	2022-03-17 21:32:13.091655+00	104	Sonata - 2016 - АI 3922 MТ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 3922 M\\u0422 ", "fields": ["Path"]}}]	15	1
898	2022-03-17 21:32:33.400976+00	105	Sonata - 2016 - АI 4668 МЕ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 4668 \\u041c\\u0415 ", "fields": ["Path"]}}]	15	1
899	2022-03-17 21:32:45.158933+00	106	Sonata - 2014 - АI 5684 MP 	2	[{"changed": {"name": "car image", "object": "Sonata - 2014 - \\u0410I 5684 MP ", "fields": ["Path"]}}, {"deleted": {"name": "car image", "object": "Sonata - 2014 - \\u0410I 5684 MP "}}]	15	1
900	2022-03-17 21:33:12.856541+00	107	Sonata - 2013 - АI 5691 MP 	2	[{"changed": {"name": "car image", "object": "Sonata - 2013 - \\u0410I 5691 MP ", "fields": ["Path"]}}]	15	1
901	2022-03-17 21:33:23.098058+00	108	Sonata - 2015 - АI 6429 MP 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 6429 MP ", "fields": ["Path"]}}]	15	1
902	2022-03-17 21:33:36.474568+00	109	Sonata - 2008 - АI 7923 MТ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2008 - \\u0410I 7923 M\\u0422 ", "fields": ["Path"]}}]	15	1
903	2022-03-17 21:33:47.956072+00	110	Sonata - 2012 - АI 8861 MТ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2012 - \\u0410I 8861 M\\u0422 ", "fields": ["Path"]}}]	15	1
904	2022-03-17 21:34:04.677167+00	111	Sonata - 2017 - АI 9104 МР 	2	[{"changed": {"name": "car image", "object": "Sonata - 2017 - \\u0410I 9104 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
905	2022-03-17 21:34:15.277597+00	112	Sonata - 2017 - АI 9361 МР 	2	[{"changed": {"name": "car image", "object": "Sonata - 2017 - \\u0410I 9361 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
906	2022-03-17 21:34:25.586085+00	113	Sonata - 2014 - АI 9364 МР 	2	[{"changed": {"name": "car image", "object": "Sonata - 2014 - \\u0410I 9364 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
907	2022-03-17 21:34:35.88922+00	114	Sonata - 2016 - АI 9681 МР 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 9681 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
908	2022-03-17 21:34:47.914627+00	115	Sonata - 2016 - АI 9702 МР 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 9702 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
909	2022-03-17 21:35:00.63161+00	116	Sonata - 2016 - АI 9814 МВ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0410I 9814 \\u041c\\u0412 ", "fields": ["Path"]}}]	15	1
910	2022-03-17 21:35:11.060929+00	117	Sonata - 2016 - ВА 7546 ЕМ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u0412\\u0410 7546 \\u0415\\u041c ", "fields": ["Path"]}}]	15	1
950	2022-03-17 21:50:14.268022+00	34	Elantra - 2015 - KA 9864 AO 	2	[{"changed": {"name": "car image", "object": "Elantra - 2015 - KA 9864 AO ", "fields": ["Path"]}}]	15	1
911	2022-03-17 21:35:27.221863+00	118	Sonata - 2017 - ВМ 0584 СМ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2017 - \\u0412\\u041c 0584 \\u0421\\u041c ", "fields": ["Path"]}}]	15	1
912	2022-03-17 21:35:40.351961+00	119	Sonata - 2016 - КА 0583 ВM 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 0583 \\u0412M ", "fields": ["Path"]}}]	15	1
913	2022-03-17 21:35:53.372849+00	120	Sonata - 2015 - КА 0941 ВМ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u041a\\u0410 0941 \\u0412\\u041c ", "fields": ["Path"]}}]	15	1
914	2022-03-17 21:36:05.064679+00	121	Sonata - 2016 - КА 2479 ВВ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2016 - \\u041a\\u0410 2479 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
915	2022-03-17 21:36:17.006145+00	122	Sonata - 2015 - КА 2641 ВВ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u041a\\u0410 2641 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
916	2022-03-17 21:36:32.912717+00	123	Sonata - 2015 - КА 3426 ВМ 	2	[{"changed": {"name": "car image", "object": "Sonata - 2015 - \\u041a\\u0410 3426 \\u0412\\u041c ", "fields": ["Path"]}}]	15	1
917	2022-03-17 21:36:50.320993+00	124	Sonata - 2017 - КА 6210 ET 	2	[{"changed": {"name": "car image", "object": "Sonata - 2017 - \\u041a\\u0410 6210 ET ", "fields": ["Path"]}}]	15	1
918	2022-03-17 21:39:08.631994+00	1	Accent - 2011 - АІ 0315 MX 	2	[{"changed": {"name": "car image", "object": "Accent - 2011 - \\u0410\\u0406 0315 MX ", "fields": ["Path"]}}]	15	1
919	2022-03-17 21:39:28.777176+00	2	Accent - 2017 - АІ 0312 MX 	2	[{"changed": {"name": "car image", "object": "Accent - 2017 - \\u0410\\u0406 0312 MX ", "fields": ["Path"]}}]	15	1
920	2022-03-17 21:39:55.611432+00	3	Accent - 2019 - АI 4463 MC 	2	[{"changed": {"name": "car image", "object": "Accent - 2019 - \\u0410I 4463 MC ", "fields": ["Path"]}}]	15	1
921	2022-03-17 21:40:26.882743+00	4	Accent - 2008 - АI 8250 OB 	2	[{"changed": {"name": "car image", "object": "Accent - 2008 - \\u0410I 8250 OB ", "fields": ["Path"]}}]	15	1
922	2022-03-17 21:40:46.989819+00	5	Accent - 2010 - АI 9756 IH 	2	[{"changed": {"name": "car image", "object": "Accent - 2010 - \\u0410I 9756 IH ", "fields": ["Path"]}}]	15	1
923	2022-03-17 21:41:10.292446+00	6	Accent - 2008 - КА 0729 АР 	2	[{"changed": {"name": "car image", "object": "Accent - 2008 - \\u041a\\u0410 0729 \\u0410\\u0420 ", "fields": ["Path"]}}]	15	1
924	2022-03-17 21:41:27.450888+00	7	Accent - 2011 - КA 2219 ВВ 	2	[{"changed": {"name": "car image", "object": "Accent - 2011 - \\u041aA 2219 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
925	2022-03-17 21:41:49.025193+00	9	Avante - 2017 - АІ 8064 MO 	2	[{"changed": {"name": "car image", "object": "Avante - 2017 - \\u0410\\u0406 8064 MO ", "fields": ["Path"]}}]	15	1
926	2022-03-17 21:42:03.674858+00	10	Avante - 2016 - АІ 8243 OВ 	2	[{"changed": {"name": "car image", "object": "Avante - 2016 - \\u0410\\u0406 8243 O\\u0412 ", "fields": ["Path"]}}]	15	1
927	2022-03-17 21:42:15.501428+00	11	Avante - 2014 - КА 3676 ВО 	2	[{"changed": {"name": "car image", "object": "Avante - 2014 - \\u041a\\u0410 3676 \\u0412\\u041e ", "fields": ["Path"]}}]	15	1
928	2022-03-17 21:42:28.703159+00	12	Avante - 2016 - KA 9980 AT 	2	[{"changed": {"name": "car image", "object": "Avante - 2016 - KA 9980 AT ", "fields": ["Path"]}}]	15	1
929	2022-03-17 21:42:49.21823+00	13	Avante - 2012 - AІ 9149 МН 	2	[{"changed": {"name": "car image", "object": "Avante - 2012 - A\\u0406 9149 \\u041c\\u041d ", "fields": ["Path"]}}]	15	1
930	2022-03-17 21:43:41.707067+00	14	735i - 2002 - AI 3839 ОС 	2	[{"changed": {"name": "car image", "object": "735i - 2002 - AI 3839 \\u041e\\u0421 ", "fields": ["Path"]}}]	15	1
931	2022-03-17 21:44:13.298603+00	15	Camry - 2015 - AI 6531 OC 	2	[{"changed": {"name": "car image", "object": "Camry - 2015 - AI 6531 OC ", "fields": ["Path"]}}]	15	1
932	2022-03-17 21:44:35.072899+00	16	Corolla - 2014 - АІ 2541 КО 	2	[{"changed": {"name": "car image", "object": "Corolla - 2014 - \\u0410\\u0406 2541 \\u041a\\u041e ", "fields": ["Path"]}}]	15	1
933	2022-03-17 21:44:59.674383+00	17	Corolla - 2018 - АІ 7462 ОВ 	2	[{"changed": {"name": "car image", "object": "Corolla - 2018 - \\u0410\\u0406 7462 \\u041e\\u0412 ", "fields": ["Path"]}}]	15	1
934	2022-03-17 21:45:20.323713+00	18	Corolla - 2011 - АІ 6453 ОО 	2	[{"changed": {"name": "car image", "object": "Corolla - 2011 - \\u0410\\u0406 6453 \\u041e\\u041e ", "fields": ["Path"]}}]	15	1
935	2022-03-17 21:45:36.932689+00	19	Corolla - 2007 - АІ 8891 ІК 	2	[{"changed": {"name": "car image", "object": "Corolla - 2007 - \\u0410\\u0406 8891 \\u0406\\u041a ", "fields": ["Path"]}}]	15	1
936	2022-03-17 21:45:49.684249+00	20	Corolla - 2016 - АІ 9423 ІМ 	2	[{"changed": {"name": "car image", "object": "Corolla - 2016 - \\u0410\\u0406 9423 \\u0406\\u041c ", "fields": ["Path"]}}]	15	1
937	2022-03-17 21:46:05.174448+00	21	Corolla - 2015 - КА 2026 ВВ 	2	[{"changed": {"name": "car image", "object": "Corolla - 2015 - \\u041a\\u0410 2026 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
938	2022-03-17 21:46:56.995306+00	22	Elantra - 2016 - АІ 3605 ОС 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3605 \\u041e\\u0421 ", "fields": ["Path"]}}]	15	1
939	2022-03-17 21:47:09.315321+00	23	Elantra - 2017 - АІ 5508 МР 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 5508 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
940	2022-03-17 21:47:24.716402+00	24	Elantra - 2016 - ВМ 0588 СМ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - \\u0412\\u041c 0588 \\u0421\\u041c ", "fields": ["Path"]}}]	15	1
941	2022-03-17 21:47:38.978008+00	25	Elantra - 2018 - ВМ 0610 СМ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2018 - \\u0412\\u041c 0610 \\u0421\\u041c ", "fields": ["Path"]}}]	15	1
942	2022-03-17 21:47:52.329619+00	26	Elantra - 2017 - KA 0514 ВН 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - KA 0514 \\u0412\\u041d ", "fields": ["Path"]}}]	15	1
943	2022-03-17 21:48:05.346329+00	27	Elantra - 2016 - KA 0574 ВМ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - KA 0574 \\u0412\\u041c ", "fields": ["Path"]}}]	15	1
944	2022-03-17 21:48:16.419242+00	28	Elantra - 2017 - KA 2816 ВВ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - KA 2816 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
945	2022-03-17 21:48:43.389183+00	29	Elantra - 2017 - KA 2853 ВВ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - KA 2853 \\u0412\\u0412 ", "fields": ["Path"]}}]	15	1
946	2022-03-17 21:49:25.732524+00	30	Elantra - 2017 - KA 3564 ВI 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - KA 3564 \\u0412I ", "fields": ["Path"]}}]	15	1
947	2022-03-17 21:49:38.369361+00	31	Elantra - 2017 - KA 5754 ВI 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - KA 5754 \\u0412I ", "fields": ["Path"]}}]	15	1
948	2022-03-17 21:49:48.706241+00	32	Elantra - 2016 - KA 9134 ВЕ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - KA 9134 \\u0412\\u0415 ", "fields": ["Path"]}}]	15	1
949	2022-03-17 21:50:00.585777+00	33	Elantra - 2018 - KA 9236 ВЕ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2018 - KA 9236 \\u0412\\u0415 ", "fields": ["Path"]}}]	15	1
951	2022-03-17 21:50:24.032135+00	35	Elantra - 2016 - KA 9865 AO 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - KA 9865 AO ", "fields": ["Path"]}}]	15	1
952	2022-03-17 21:50:39.260204+00	36	Elantra - 2011 - АІ 0368 MX 	2	[{"changed": {"name": "car image", "object": "Elantra - 2011 - \\u0410\\u0406 0368 MX ", "fields": ["Path"]}}]	15	1
953	2022-03-17 21:50:49.700118+00	37	Elantra - 2017 - АІ 3159 MМ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 3159 M\\u041c ", "fields": ["Path"]}}]	15	1
954	2022-03-17 21:50:59.107966+00	38	Elantra - 2016 - АІ 3167 MМ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3167 M\\u041c ", "fields": ["Path"]}}]	15	1
955	2022-03-17 21:51:08.14899+00	39	Elantra - 2016 - АІ 3168 MМ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3168 M\\u041c ", "fields": ["Path"]}}]	15	1
956	2022-03-17 21:51:22.626536+00	40	Elantra - 2016 - АІ 3351 MA 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - \\u0410\\u0406 3351 MA ", "fields": ["Path"]}}]	15	1
957	2022-03-17 21:51:32.25622+00	41	Elantra - 2018 - АІ 3422 MР 	2	[{"changed": {"name": "car image", "object": "Elantra - 2018 - \\u0410\\u0406 3422 M\\u0420 ", "fields": ["Path"]}}]	15	1
958	2022-03-17 21:51:45.839069+00	42	Elantra - 2018 - АІ 6304 МР 	2	[{"changed": {"name": "car image", "object": "Elantra - 2018 - \\u0410\\u0406 6304 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
959	2022-03-17 21:51:57.431366+00	43	Elantra - 2017 - АІ 6703 МР 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 6703 \\u041c\\u0420 ", "fields": ["Path"]}}]	15	1
960	2022-03-17 21:52:08.2702+00	44	Elantra - 2017 - АІ 6896 ОВ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - \\u0410\\u0406 6896 \\u041e\\u0412 ", "fields": ["Path"]}}]	15	1
961	2022-03-17 21:52:21.124533+00	45	Elantra - 2017 - AІ 7365 МI 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - A\\u0406 7365 \\u041cI ", "fields": ["Path"]}}]	15	1
962	2022-03-17 21:52:37.352978+00	46	Elantra - 2016 - AІ 7453 ОВ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - A\\u0406 7453 \\u041e\\u0412 ", "fields": ["Path"]}}]	15	1
963	2022-03-17 21:52:57.054695+00	47	Elantra - 2018 - AІ 7645 МТ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2018 - A\\u0406 7645 \\u041c\\u0422 ", "fields": ["Path"]}}]	15	1
964	2022-03-17 21:53:14.764187+00	48	Elantra - 2016 - AІ 8132 МТ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - A\\u0406 8132 \\u041c\\u0422 ", "fields": ["Path"]}}]	15	1
965	2022-03-17 21:53:29.602489+00	49	Elantra - 2016 - AІ 8829 ММ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2016 - A\\u0406 8829 \\u041c\\u041c ", "fields": ["Path"]}}]	15	1
966	2022-03-17 21:54:03.253738+00	50	Elantra - 2017 - AМ 6023 ЕТ 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - A\\u041c 6023 \\u0415\\u0422 ", "fields": ["Path"]}}]	15	1
967	2022-03-17 21:54:16.76623+00	51	Elantra - 2017 - KA 6328 ВI 	2	[{"changed": {"name": "car image", "object": "Elantra - 2017 - KA 6328 \\u0412I ", "fields": ["Path"]}}]	15	1
968	2022-03-17 21:54:52.62207+00	52	Fabia - 2008 - АА 8037 РР 	2	[{"changed": {"name": "car image", "object": "Fabia - 2008 - \\u0410\\u0410 8037 \\u0420\\u0420 ", "fields": ["Path"]}}]	15	1
969	2022-03-17 21:55:09.205411+00	53	Fabia - 2015 - АА 8986 СС 	2	[{"changed": {"name": "car image", "object": "Fabia - 2015 - \\u0410\\u0410 8986 \\u0421\\u0421 ", "fields": ["Path"]}}]	15	1
970	2022-03-17 21:55:21.174245+00	54	Fabia - 2013 - АІ 2803 СС 	2	[{"changed": {"name": "car image", "object": "Fabia - 2013 - \\u0410\\u0406 2803 \\u0421\\u0421 ", "fields": ["Path"]}}]	15	1
971	2022-03-17 21:55:36.778572+00	55	Fabia - 2008 - АІ 7166 ІМ 	2	[{"changed": {"name": "car image", "object": "Fabia - 2008 - \\u0410\\u0406 7166 \\u0406\\u041c ", "fields": ["Path"]}}]	15	1
972	2022-03-17 21:56:07.685847+00	56	I30 - 2011 - АА 3967 ТТ 	2	[{"changed": {"name": "car image", "object": "I30 - 2011 - \\u0410\\u0410 3967 \\u0422\\u0422 ", "fields": ["Path"]}}]	15	1
973	2022-03-17 21:56:22.0013+00	57	I30 - 2011 - АА 4421 ЕІ 	2	[{"changed": {"name": "car image", "object": "I30 - 2011 - \\u0410\\u0410 4421 \\u0415\\u0406 ", "fields": ["Path"]}}]	15	1
974	2022-03-17 21:56:37.413218+00	58	I30 - 2010 - АА 4934 ВР 	2	[{"changed": {"name": "car image", "object": "I30 - 2010 - \\u0410\\u0410 4934 \\u0412\\u0420 ", "fields": ["Path"]}}]	15	1
975	2022-03-17 21:57:01.303168+00	59	I30 - 2012 - АА 6079 СС 	2	[{"changed": {"name": "car image", "object": "I30 - 2012 - \\u0410\\u0410 6079 \\u0421\\u0421 ", "fields": ["Path"]}}]	15	1
976	2022-03-17 21:57:21.561271+00	60	I30 - 2008 - АІ 6723 ІН 	2	[{"changed": {"name": "car image", "object": "I30 - 2008 - \\u0410\\u0406 6723 \\u0406\\u041d ", "fields": ["Path"]}}]	15	1
977	2022-03-17 21:57:45.015056+00	61	Jetta - 2008 - АІ 2258 MМ 	2	[{"changed": {"name": "car image", "object": "Jetta - 2008 - \\u0410\\u0406 2258 M\\u041c ", "fields": ["Path"]}}]	15	1
978	2022-03-17 21:57:55.67945+00	62	Jetta - 2011 - АI 3439 MP 	2	[{"changed": {"name": "car image", "object": "Jetta - 2011 - \\u0410I 3439 MP ", "fields": ["Path"]}}]	15	1
979	2022-03-17 21:58:06.957639+00	63	Jetta - 2010 - КА 0345 ВМ 	2	[{"changed": {"name": "car image", "object": "Jetta - 2010 - \\u041a\\u0410 0345 \\u0412\\u041c ", "fields": ["Path"]}}]	15	1
980	2022-03-17 21:58:31.515447+00	64	K5 - 2012 - AI 5266 OO 	2	[{"changed": {"name": "car image", "object": "K5 - 2012 - AI 5266 OO ", "fields": ["Path"]}}]	15	1
981	2022-03-17 21:58:54.117883+00	65	Carens - 2012 - AI 0712 OВ 	2	[{"changed": {"name": "car image", "object": "Carens - 2012 - AI 0712 O\\u0412 ", "fields": ["Path"]}}]	15	1
982	2022-03-17 21:59:11.124607+00	66	Forte - 2011 - AI 3724 OС 	2	[{"changed": {"name": "car image", "object": "Forte - 2011 - AI 3724 O\\u0421 ", "fields": ["Path"]}}]	15	1
983	2022-03-17 21:59:20.169499+00	67	Forte - 2011 - AI 9766 OA 	2	[{"changed": {"name": "car image", "object": "Forte - 2011 - AI 9766 OA ", "fields": ["Path"]}}]	15	1
984	2022-03-17 21:59:37.076323+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"changed": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 0589 M\\u0406 ", "fields": ["Path"]}}, {"deleted": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 0589 M\\u0406 "}}]	15	1
985	2022-03-17 22:00:00.331131+00	69	K5 - 2015 - АІ 0724 MІ 	2	[{"changed": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 0724 M\\u0406 ", "fields": ["Path"]}}]	15	1
986	2022-03-17 22:00:24.04772+00	70	K5 - 2016 - АІ 3620 MP 	2	[{"changed": {"name": "car image", "object": "K5 - 2016 - \\u0410\\u0406 3620 MP ", "fields": ["Path"]}}]	15	1
987	2022-03-17 22:00:32.11482+00	71	K5 - 2016 - АІ 5563 MP 	2	[{"changed": {"name": "car image", "object": "K5 - 2016 - \\u0410\\u0406 5563 MP ", "fields": ["Path"]}}]	15	1
988	2022-03-17 22:00:42.261742+00	72	K5 - 2017 - АІ 5564 MP 	2	[{"changed": {"name": "car image", "object": "K5 - 2017 - \\u0410\\u0406 5564 MP ", "fields": ["Path"]}}]	15	1
989	2022-03-17 22:00:51.454777+00	73	K5 - 2017 - АІ 7045 MP 	2	[{"changed": {"name": "car image", "object": "K5 - 2017 - \\u0410\\u0406 7045 MP ", "fields": ["Path"]}}]	15	1
990	2022-03-17 22:01:03.065735+00	74	K5 - 2016 - АІ 7328 MР 	2	[{"changed": {"name": "car image", "object": "K5 - 2016 - \\u0410\\u0406 7328 M\\u0420 ", "fields": ["Path"]}}]	15	1
991	2022-03-17 22:01:24.921516+00	75	K5 - 2015 - АІ 7652 MТ 	2	[{"changed": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 7652 M\\u0422 ", "fields": ["Path"]}}]	15	1
992	2022-03-17 22:01:37.997803+00	76	K5 - 2015 - АІ 8089 MI 	2	[{"added": {"name": "car image", "object": "K5 - 2015 - \\u0410\\u0406 8089 MI "}}]	15	1
993	2022-03-17 22:01:57.07619+00	77	Sorento - 2016 - AI 2111 MB 	2	[{"changed": {"name": "car image", "object": "Sorento - 2016 - AI 2111 MB ", "fields": ["Path"]}}]	15	1
994	2022-03-17 22:02:18.910109+00	78	Sportage - 2011 - KА 6164 EC 	2	[{"changed": {"name": "car image", "object": "Sportage - 2011 - K\\u0410 6164 EC ", "fields": ["Path"]}}]	15	1
995	2022-03-17 22:02:38.008379+00	79	Mazda - 2007 - АА 1066 СС 	2	[{"changed": {"name": "car image", "object": "Mazda - 2007 - \\u0410\\u0410 1066 \\u0421\\u0421 ", "fields": ["Path"]}}]	15	1
996	2022-03-17 22:02:51.289605+00	80	Mazda - 2013 - АІ 8006 МА 	2	[{"changed": {"name": "car image", "object": "Mazda - 2013 - \\u0410\\u0406 8006 \\u041c\\u0410 ", "fields": ["Path"]}}]	15	1
997	2022-03-17 22:03:09.857262+00	81	Insignia - 2006 - AI 1495 MO 	2	[{"changed": {"name": "car image", "object": "Insignia - 2006 - AI 1495 MO ", "fields": ["Path"]}}]	15	1
998	2022-03-17 22:03:50.723162+00	82	Passat - 2004 - AА 1556 ТT 	2	[{"changed": {"name": "car image", "object": "Passat - 2004 - A\\u0410 1556 \\u0422T ", "fields": ["Path"]}}]	15	1
999	2022-03-17 22:04:06.380742+00	83	Polo - 2015 - KA 0728 AP 	2	[{"changed": {"name": "car image", "object": "Polo - 2015 - KA 0728 AP ", "fields": ["Path"]}}]	15	1
1000	2022-03-17 22:04:16.609078+00	84	Polo - 2013 - АЕ 9200 IM 	2	[{"changed": {"name": "car image", "object": "Polo - 2013 - \\u0410\\u0415 9200 IM ", "fields": ["Path"]}}]	15	1
1001	2022-03-17 22:05:17.429852+00	8	Accent - 2016 - AE 7193 KO 	2	[{"changed": {"name": "car image", "object": "Accent - 2016 - AE 7193 KO ", "fields": ["Path"]}}]	15	1
1002	2022-03-17 22:29:52.577255+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"changed": {"fields": ["Case", "Color"]}}]	15	1
1003	2022-03-17 22:31:35.001906+00	1	Эконом	1	[{"added": {}}]	16	1
1004	2022-03-17 22:31:50.038633+00	1	белый	2	[{"changed": {"fields": ["Name"]}}]	19	1
1005	2022-03-17 22:31:54.961532+00	1	Белый	2	[{"changed": {"fields": ["Name"]}}]	19	1
1006	2022-03-17 22:32:04.219861+00	1	Газ	1	[{"added": {}}]	20	1
1007	2022-03-17 22:32:11.827454+00	134	Forester - 2007 - AI 8500 MT 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1008	2022-03-17 22:34:09.204644+00	1	Accent - 2011 - АІ 0315 MX 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1009	2022-03-17 22:34:23.995859+00	20	Corolla - 2016 - АІ 9423 ІМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1010	2022-03-17 22:34:31.335818+00	18	Corolla - 2011 - АІ 6453 ОО 	2	[{"changed": {"fields": ["Car class", "Fuel"]}}]	15	1
1011	2022-03-17 22:34:40.922353+00	19	Corolla - 2007 - АІ 8891 ІК 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1012	2022-03-17 22:34:51.374423+00	17	Corolla - 2018 - АІ 7462 ОВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1013	2022-03-17 22:34:59.936088+00	16	Corolla - 2014 - АІ 2541 КО 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1014	2022-03-17 22:35:07.849407+00	15	Camry - 2015 - AI 6531 OC 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1015	2022-03-17 22:35:18.796914+00	14	735i - 2002 - AI 3839 ОС 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1016	2022-03-17 22:35:29.6452+00	13	Avante - 2012 - AІ 9149 МН 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1017	2022-03-17 22:35:37.088707+00	12	Avante - 2016 - KA 9980 AT 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1018	2022-03-17 22:35:45.329714+00	11	Avante - 2014 - КА 3676 ВО 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1019	2022-03-17 22:35:53.522279+00	10	Avante - 2016 - АІ 8243 OВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1020	2022-03-17 22:36:00.396968+00	9	Avante - 2017 - АІ 8064 MO 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1021	2022-03-17 22:36:07.900135+00	8	Accent - 2016 - AE 7193 KO 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1022	2022-03-17 22:36:14.883681+00	7	Accent - 2011 - КA 2219 ВВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1023	2022-03-17 22:36:21.97531+00	6	Accent - 2008 - КА 0729 АР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1024	2022-03-17 22:36:28.457727+00	5	Accent - 2010 - АI 9756 IH 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1025	2022-03-17 22:36:35.425034+00	4	Accent - 2008 - АI 8250 OB 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1026	2022-03-17 22:36:42.485499+00	3	Accent - 2019 - АI 4463 MC 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1027	2022-03-17 22:37:07.864832+00	2	Accent - 2017 - АІ 0312 MX 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1028	2022-03-17 22:37:17.206383+00	21	Corolla - 2015 - КА 2026 ВВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1029	2022-03-17 22:37:27.474504+00	22	Elantra - 2016 - АІ 3605 ОС 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1030	2022-03-17 22:43:59.356248+00	23	Elantra - 2017 - АІ 5508 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1031	2022-03-17 22:44:09.677085+00	24	Elantra - 2016 - ВМ 0588 СМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1032	2022-03-17 22:44:18.359606+00	25	Elantra - 2018 - ВМ 0610 СМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1033	2022-03-17 22:44:26.662777+00	26	Elantra - 2017 - KA 0514 ВН 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1034	2022-03-17 22:44:33.441167+00	27	Elantra - 2016 - KA 0574 ВМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1035	2022-03-17 22:44:41.674464+00	28	Elantra - 2017 - KA 2816 ВВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1036	2022-03-17 22:44:50.119543+00	29	Elantra - 2017 - KA 2853 ВВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1037	2022-03-17 22:44:59.832948+00	30	Elantra - 2017 - KA 3564 ВI 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1038	2022-03-17 22:45:07.087676+00	31	Elantra - 2017 - KA 5754 ВI 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1327	2022-03-19 20:42:29.887369+00	46	Савченко Юрий - 1	2	[]	51	1
1039	2022-03-17 22:45:15.266178+00	33	Elantra - 2018 - KA 9236 ВЕ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1040	2022-03-17 22:45:23.804765+00	34	Elantra - 2015 - KA 9864 AO 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1041	2022-03-17 22:45:31.717068+00	35	Elantra - 2016 - KA 9865 AO 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1042	2022-03-17 22:45:39.807669+00	36	Elantra - 2011 - АІ 0368 MX 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1043	2022-03-17 22:45:47.298116+00	37	Elantra - 2017 - АІ 3159 MМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1044	2022-03-17 22:45:54.774443+00	38	Elantra - 2016 - АІ 3167 MМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1045	2022-03-17 22:46:01.83817+00	39	Elantra - 2016 - АІ 3168 MМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1046	2022-03-17 22:46:09.303536+00	40	Elantra - 2016 - АІ 3351 MA 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1047	2022-03-17 22:46:15.788929+00	41	Elantra - 2018 - АІ 3422 MР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1048	2022-03-17 22:46:23.609597+00	42	Elantra - 2018 - АІ 6304 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1049	2022-03-17 22:46:30.510143+00	43	Elantra - 2017 - АІ 6703 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1050	2022-03-17 22:46:37.232062+00	44	Elantra - 2017 - АІ 6896 ОВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1051	2022-03-17 22:46:43.831272+00	45	Elantra - 2017 - AІ 7365 МI 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1052	2022-03-17 22:46:50.779881+00	46	Elantra - 2016 - AІ 7453 ОВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1053	2022-03-17 22:46:59.109437+00	47	Elantra - 2018 - AІ 7645 МТ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1054	2022-03-17 22:47:07.475694+00	48	Elantra - 2016 - AІ 8132 МТ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1055	2022-03-17 22:47:14.986873+00	49	Elantra - 2016 - AІ 8829 ММ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1056	2022-03-17 22:47:22.673372+00	50	Elantra - 2017 - AМ 6023 ЕТ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1057	2022-03-17 22:47:29.338934+00	52	Fabia - 2008 - АА 8037 РР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1058	2022-03-17 22:47:37.031933+00	53	Fabia - 2015 - АА 8986 СС 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1059	2022-03-17 22:47:44.456476+00	54	Fabia - 2013 - АІ 2803 СС 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1060	2022-03-17 22:47:51.341523+00	55	Fabia - 2008 - АІ 7166 ІМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1061	2022-03-17 22:47:57.780011+00	56	I30 - 2011 - АА 3967 ТТ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1062	2022-03-17 22:48:04.253383+00	57	I30 - 2011 - АА 4421 ЕІ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1063	2022-03-17 22:48:11.91502+00	58	I30 - 2010 - АА 4934 ВР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1064	2022-03-17 22:48:31.786358+00	59	I30 - 2012 - АА 6079 СС 	2	[{"changed": {"fields": ["Car class", "Color", "Fuel"]}}]	15	1
1065	2022-03-17 22:48:41.372363+00	61	Jetta - 2008 - АІ 2258 MМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1066	2022-03-17 22:48:51.037118+00	63	Jetta - 2010 - КА 0345 ВМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1067	2022-03-17 22:48:59.33485+00	64	K5 - 2012 - AI 5266 OO 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1068	2022-03-17 22:49:08.93513+00	65	Carens - 2012 - AI 0712 OВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1069	2022-03-17 22:49:16.562329+00	66	Forte - 2011 - AI 3724 OС 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1070	2022-03-17 22:49:25.376049+00	67	Forte - 2011 - AI 9766 OA 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1071	2022-03-17 22:49:31.663333+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"changed": {"fields": ["Car class", "Fuel"]}}]	15	1
1072	2022-03-17 22:49:38.70031+00	70	K5 - 2016 - АІ 3620 MP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1073	2022-03-17 22:49:45.663677+00	71	K5 - 2016 - АІ 5563 MP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1074	2022-03-17 22:49:53.071814+00	72	K5 - 2017 - АІ 5564 MP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1075	2022-03-17 22:50:00.797661+00	73	K5 - 2017 - АІ 7045 MP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1076	2022-03-17 22:50:16.642263+00	74	K5 - 2016 - АІ 7328 MР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1077	2022-03-17 22:50:23.240821+00	75	K5 - 2015 - АІ 7652 MТ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1078	2022-03-17 22:50:30.285592+00	76	K5 - 2015 - АІ 8089 MI 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1079	2022-03-17 22:50:35.57597+00	77	Sorento - 2016 - AI 2111 MB 	2	[{"changed": {"fields": ["Car class", "Fuel"]}}]	15	1
1080	2022-03-17 22:50:44.85002+00	78	Sportage - 2011 - KА 6164 EC 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1081	2022-03-17 22:50:52.932808+00	80	Mazda - 2013 - АІ 8006 МА 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1082	2022-03-17 22:51:00.910775+00	81	Insignia - 2006 - AI 1495 MO 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1083	2022-03-17 22:51:08.645333+00	82	Passat - 2004 - AА 1556 ТT 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1084	2022-03-17 22:51:19.805947+00	83	Polo - 2015 - KA 0728 AP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1085	2022-03-17 22:51:27.765877+00	84	Polo - 2013 - АЕ 9200 IM 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1086	2022-03-17 22:51:35.079497+00	85	Rio - 2012 - АА 6038 РР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1087	2022-03-17 22:51:43.661624+00	86	Rio - 2012 - АІ 1893 ІІ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1088	2022-03-17 22:51:50.155779+00	87	Rio - 2012 - КА 3306 ВМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1089	2022-03-17 22:51:58.174587+00	88	Rio - 2011 - СА 3701 СК 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1090	2022-03-17 22:52:05.613807+00	89	Sonata - 2012 - АI 6782 ОА 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1091	2022-03-17 22:52:13.715278+00	90	Sonata - 2012 - АI 6786 ОА 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1092	2022-03-17 22:52:20.31286+00	91	Sonata - 2012 - АI 6805 ОА 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1093	2022-03-17 22:52:27.430501+00	92	Sonata - 2016 - АI 6809 ОА 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1094	2022-03-17 22:52:34.753505+00	93	Sonata - 2015 - АМ 3560 ЕО 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1095	2022-03-17 22:52:41.817703+00	94	Sonata - 2015 - АI 0253 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1096	2022-03-17 22:52:48.674074+00	96	Sonata - 2015 - АI 0962 МB 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1097	2022-03-17 22:52:56.274179+00	97	Sonata - 2016 - АI 1256 ММ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1098	2022-03-17 22:53:05.258245+00	98	Sonata - 2016 - АI 2518 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1099	2022-03-17 22:53:11.598919+00	99	Sonata - 2016 - АI 2519 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1100	2022-03-17 22:53:18.210655+00	100	Sonata - 2015 - АI 2846 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1101	2022-03-17 22:53:25.396852+00	101	Sonata - 2015 - АI 2875 ММ 	2	[{"changed": {"fields": ["Case", "Car class", "Fuel"]}}]	15	1
1102	2022-03-17 22:53:34.15607+00	101	Sonata - 2015 - АI 2875 ММ 	2	[{"changed": {"fields": ["Color"]}}]	15	1
1103	2022-03-17 22:53:41.525298+00	102	Sonata - 2017 - АI 2953 ММ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1104	2022-03-17 22:53:48.07427+00	103	Sonata - 2016 - АI 3702 MP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1105	2022-03-17 22:53:56.145329+00	104	Sonata - 2016 - АI 3922 MТ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1106	2022-03-17 22:54:02.619385+00	105	Sonata - 2016 - АI 4668 МЕ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1107	2022-03-17 22:54:11.510779+00	107	Sonata - 2013 - АI 5691 MP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1108	2022-03-17 22:54:19.294725+00	108	Sonata - 2015 - АI 6429 MP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1109	2022-03-17 22:54:28.597787+00	109	Sonata - 2008 - АI 7923 MТ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1110	2022-03-17 22:54:36.933127+00	110	Sonata - 2012 - АI 8861 MТ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1111	2022-03-17 22:54:44.398314+00	111	Sonata - 2017 - АI 9104 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1112	2022-03-17 22:54:51.115587+00	112	Sonata - 2017 - АI 9361 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1113	2022-03-17 22:54:57.713168+00	113	Sonata - 2014 - АI 9364 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1114	2022-03-17 22:55:05.846007+00	114	Sonata - 2016 - АI 9681 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1115	2022-03-17 22:55:12.902926+00	115	Sonata - 2016 - АI 9702 МР 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1116	2022-03-17 22:55:19.574297+00	116	Sonata - 2016 - АI 9814 МВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1117	2022-03-17 22:55:27.98763+00	117	Sonata - 2016 - ВА 7546 ЕМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1118	2022-03-17 22:55:36.735985+00	118	Sonata - 2017 - ВМ 0584 СМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1119	2022-03-17 22:55:44.047307+00	119	Sonata - 2016 - КА 0583 ВM 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1120	2022-03-17 22:55:52.760566+00	120	Sonata - 2015 - КА 0941 ВМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1121	2022-03-17 22:55:59.734415+00	121	Sonata - 2016 - КА 2479 ВВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1122	2022-03-17 22:56:06.278384+00	122	Sonata - 2015 - КА 2641 ВВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1123	2022-03-17 22:56:13.617955+00	123	Sonata - 2015 - КА 3426 ВМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1124	2022-03-17 22:56:19.935384+00	124	Sonata - 2017 - КА 6210 ET 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1125	2022-03-17 22:56:26.999589+00	125	Sonata - 2016 - КА 8962 ВМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1126	2022-03-17 22:56:33.7687+00	126	Sonata - 2016 - КА 9531 ВК 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1127	2022-03-17 22:56:40.005299+00	127	Sonata - 2016 - СА 5407 ІВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1128	2022-03-17 22:56:47.57498+00	128	Sonata - 2014 - СА 5408 ІВ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1129	2022-03-17 22:56:55.455853+00	129	Sonata - 2015 - АI 8894 ММ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1130	2022-03-17 22:57:03.111271+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1131	2022-03-17 22:57:10.365155+00	131	Sonata - 2015 - АА 1058 ЕМ 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1132	2022-03-17 22:57:18.317058+00	132	Sonata - 2016 - АI 0886 KP 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1133	2022-03-17 22:57:28.340495+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Case", "Car class", "Color", "Fuel"]}}]	15	1
1134	2022-03-18 15:52:45.232709+00	142	Accent - 2000 - qqq 	3		15	1
1135	2022-03-18 15:52:49.822827+00	141	Accent - 2000 - eeee 	3		15	1
1136	2022-03-18 22:38:48.733699+00	18	Corolla - 2011 - АІ 6453 ОО 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1137	2022-03-18 22:38:54.031144+00	54	Кулиев Азат - Corolla - 2011 - АІ 6453 ОО  - Credit	2	[]	9	1
1138	2022-03-18 22:44:30.941406+00	59	I30 - 2012 - АА 6079 СС 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1139	2022-03-18 22:45:37.367362+00	78	Sportage - 2011 - KА 6164 EC 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1140	2022-03-18 22:46:09.206978+00	83	Polo - 2015 - KA 0728 AP 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1141	2022-03-18 22:47:04.985755+00	53	Fabia - 2015 - АА 8986 СС 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1143	2022-03-18 22:48:00.895446+00	126	Sonata - 2016 - КА 9531 ВК 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1144	2022-03-18 22:48:24.57112+00	88	Rio - 2011 - СА 3701 СК 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1145	2022-03-18 22:48:42.62921+00	5	Accent - 2010 - АI 9756 IH 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1146	2022-03-18 22:49:00.702126+00	13	Avante - 2012 - AІ 9149 МН 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1147	2022-03-18 22:49:26.525584+00	14	735i - 2002 - AI 3839 ОС 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1148	2022-03-18 22:49:40.632227+00	23	Elantra - 2017 - АІ 5508 МР 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1149	2022-03-18 22:50:46.337326+00	43	Elantra - 2017 - АІ 6703 МР 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1150	2022-03-18 22:51:04.005617+00	65	Carens - 2012 - AI 0712 OВ 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1151	2022-03-18 22:51:18.636551+00	54	Fabia - 2013 - АІ 2803 СС 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1152	2022-03-18 22:51:29.537039+00	91	Sonata - 2012 - АI 6805 ОА 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1153	2022-03-18 22:51:43.248057+00	80	Mazda - 2013 - АІ 8006 МА 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1154	2022-03-18 22:51:56.237032+00	58	I30 - 2010 - АА 4934 ВР 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1155	2022-03-18 22:52:09.752751+00	3	Accent - 2019 - АI 4463 MC 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1156	2022-03-18 22:52:24.466335+00	45	Elantra - 2017 - AІ 7365 МI 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1157	2022-03-18 22:52:26.216851+00	30	Абдыресулов Рахман - Elantra - 2017 - AІ 7365 МI  - Credit	2	[]	9	1
1158	2022-03-18 22:52:36.898722+00	55	Fabia - 2008 - АІ 7166 ІМ 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1159	2022-03-18 22:52:50.53585+00	56	I30 - 2011 - АА 3967 ТТ 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1160	2022-03-18 22:53:04.814409+00	59	I30 - 2012 - АА 6079 СС 	2	[]	15	1
1161	2022-03-18 22:53:16.090685+00	95	Sonata - 2015 - АI 0954 МB 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1162	2022-03-18 22:53:36.55191+00	61	Jetta - 2008 - АІ 2258 MМ 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1163	2022-03-18 22:53:49.562964+00	81	Insignia - 2006 - AI 1495 MO 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1164	2022-03-18 22:54:01.723554+00	64	K5 - 2012 - AI 5266 OO 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1165	2022-03-18 22:54:17.839334+00	52	Fabia - 2008 - АА 8037 РР 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1166	2022-03-18 22:54:22.128225+00	38	Яхяев Шамиль - Fabia - 2008 - АА 8037 РР  - Credit	2	[]	9	1
1167	2022-03-18 22:54:38.581113+00	87	Rio - 2012 - КА 3306 ВМ 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1168	2022-03-18 22:54:53.583893+00	6	Accent - 2008 - КА 0729 АР 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1169	2022-03-18 22:55:08.389147+00	17	Corolla - 2018 - АІ 7462 ОВ 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1170	2022-03-18 22:55:23.49529+00	60	I30 - 2008 - АІ 6723 ІН 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1171	2022-03-18 22:55:37.355624+00	67	Forte - 2011 - AI 9766 OA 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1172	2022-03-18 22:55:39.249036+00	43	Халилов Сердар - Forte - 2011 - AI 9766 OA  - Credit	2	[]	9	1
1173	2022-03-18 22:56:14.994769+00	82	Passat - 2004 - AА 1556 ТT 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1174	2022-03-18 22:56:39.270327+00	84	Polo - 2013 - АЕ 9200 IM 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1175	2022-03-18 22:56:54.959601+00	86	Rio - 2012 - АІ 1893 ІІ 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1176	2022-03-18 22:57:09.123736+00	131	Sonata - 2015 - АА 1058 ЕМ 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1177	2022-03-18 22:57:41.205631+00	93	Sonata - 2015 - АМ 3560 ЕО 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1178	2022-03-18 22:57:56.893222+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1179	2022-03-18 22:58:16.365506+00	90	Sonata - 2012 - АI 6786 ОА 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1180	2022-03-18 22:58:28.140481+00	76	K5 - 2015 - АІ 8089 MI 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1181	2022-03-18 22:58:45.489074+00	68	K5 - 2015 - АІ 0589 MІ 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1182	2022-03-18 22:59:00.639495+00	77	Sorento - 2016 - AI 2111 MB 	2	[{"changed": {"fields": ["Driver"]}}]	15	1
1183	2022-03-18 23:57:15.425732+00	1	Alfa	1	[{"added": {}}]	56	1
1184	2022-03-18 23:57:39.883338+00	1	Forte - AI 3724 OС	1	[{"added": {}}]	55	1
1185	2022-03-18 23:57:51.378082+00	1	Forte - AI 3724 OС	2	[{"added": {"name": "casco payment", "object": "Forte - 2011 - 2022-01-18 - 1"}}]	55	1
1186	2022-03-19 00:11:22.546234+00	1	Forte - AI 3724 OС	2	[]	55	1
1187	2022-03-19 00:11:38.692005+00	2	K5 - АІ 3620 MP	1	[{"added": {}}, {"added": {"name": "casco payment", "object": "K5 - 2016 - 2022-03-19 - 1"}}]	55	1
1188	2022-03-19 00:12:14.76792+00	3	K5 - АІ 5563 MP	1	[{"added": {}}, {"added": {"name": "casco payment", "object": "K5 - 2016 - 2021-12-19 - 1"}}]	55	1
1189	2022-03-19 00:12:29.874114+00	3	K5 - АІ 5563 MP	2	[{"added": {"name": "casco payment", "object": "K5 - 2016 - 2022-03-19 - 2"}}]	55	1
1190	2022-03-19 00:13:37.249447+00	1	Forte - AI 3724 OС	1	[{"added": {}}, {"added": {"name": "osago payment", "object": "Forte - 2011 - 2022-03-19"}}]	57	1
1191	2022-03-19 00:13:49.603914+00	2	K5 - АІ 3620 MP	1	[{"added": {}}, {"added": {"name": "osago payment", "object": "K5 - 2016 - 2022-03-19"}}]	57	1
1192	2022-03-19 00:14:14.128599+00	3	K5 - АІ 5563 MP	1	[{"added": {}}, {"added": {"name": "osago payment", "object": "K5 - 2016 - 2022-03-19"}}]	57	1
1193	2022-03-19 00:14:24.810732+00	4	K5 - АІ 5564 MP	1	[{"added": {}}, {"added": {"name": "osago payment", "object": "K5 - 2017 - 2022-03-19"}}]	57	1
1194	2022-03-19 00:19:40.460723+00	132	Sonata - 2016 - АI 0886 KP 	2	[{"changed": {"fields": ["Health status", "Current tyre"]}}]	15	1
1195	2022-03-19 00:19:50.136537+00	134	Forester - 2007 - AI 8500 MT 	2	[{"changed": {"fields": ["Health status", "Current tyre"]}}]	15	1
1196	2022-03-19 00:19:58.905698+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"changed": {"fields": ["Health status", "Current tyre"]}}]	15	1
1197	2022-03-19 00:21:36.108391+00	66	Forte - 2011 - AI 3724 OС 	2	[{"changed": {"fields": ["Health status", "Current tyre"]}}]	15	1
1198	2022-03-19 00:22:09.136894+00	71	K5 - 2016 - АІ 5563 MP 	2	[{"changed": {"fields": ["Health status", "Current tyre"]}}]	15	1
1199	2022-03-19 00:24:44.599721+00	3	K5 - АІ 3620 MP	2	[{"changed": {"fields": ["Car"]}}]	55	1
1200	2022-03-19 00:24:49.621632+00	2	K5 - АІ 5564 MP	2	[{"changed": {"fields": ["Car"]}}]	55	1
1201	2022-03-19 00:24:54.823637+00	1	K5 - АІ 7328 MР	2	[{"changed": {"fields": ["Car"]}}]	55	1
1202	2022-03-19 00:26:37.882302+00	1	Forester - AI 8500 MT	2	[{"changed": {"fields": ["Car"]}}]	55	1
1203	2022-03-19 00:26:56.416357+00	2	Subaru - AI 7103 OB	2	[{"changed": {"fields": ["Car"]}}]	55	1
1204	2022-03-19 00:27:15.040368+00	3	Sonata - АI 0886 KP	2	[{"changed": {"fields": ["Car"]}}]	55	1
1205	2022-03-19 00:27:26.067947+00	1	Forester - AI 8500 MT	2	[{"changed": {"fields": ["Car"]}}]	57	1
1206	2022-03-19 00:27:31.76134+00	2	Subaru - AI 7103 OB	2	[{"changed": {"fields": ["Car"]}}]	57	1
1207	2022-03-19 00:27:37.853652+00	3	Sonata - АI 0886 KP	2	[{"changed": {"fields": ["Car"]}}]	57	1
1208	2022-03-19 00:28:15.049511+00	4	Sonata - АА 1058 ЕМ	2	[{"changed": {"fields": ["Car"]}}]	57	1
1209	2022-03-19 00:31:59.214472+00	125	Sonata - 2016 - КА 8962 ВМ 	2	[{"changed": {"fields": ["Reserve key"]}}]	15	1
1210	2022-03-19 00:32:04.481248+00	134	Forester - 2007 - AI 8500 MT 	2	[{"changed": {"fields": ["Reserve key"]}}]	15	1
1211	2022-03-19 00:32:09.639075+00	133	Subaru - 2011 - AI 7103 OB 	2	[{"changed": {"fields": ["Reserve key"]}}]	15	1
1212	2022-03-19 00:32:14.612211+00	132	Sonata - 2016 - АI 0886 KP 	2	[{"changed": {"fields": ["Reserve key"]}}]	15	1
1213	2022-03-19 00:32:20.675183+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"changed": {"fields": ["Reserve key"]}}]	15	1
1214	2022-03-19 00:32:26.626455+00	129	Sonata - 2015 - АI 8894 ММ 	2	[{"changed": {"fields": ["Reserve key"]}}]	15	1
1215	2022-03-19 00:32:31.522005+00	128	Sonata - 2014 - СА 5408 ІВ 	2	[{"changed": {"fields": ["Reserve key"]}}]	15	1
1216	2022-03-19 00:32:36.861715+00	127	Sonata - 2016 - СА 5407 ІВ 	2	[{"changed": {"fields": ["Reserve key"]}}]	15	1
1217	2022-03-19 00:32:42.808883+00	126	Sonata - 2016 - КА 9531 ВК 	2	[{"changed": {"fields": ["Reserve key"]}}]	15	1
1218	2022-03-19 13:38:51.047472+00	13	Дмитрий Каюн - Sportage - 2011 - KА 6164 EC  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1219	2022-03-19 13:39:00.224518+00	14	Язгулиев Шохрат - Polo - 2015 - KA 0728 AP  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1220	2022-03-19 13:39:03.76166+00	15	Чолиев Юнус - Fabia - 2015 - АА 8986 СС  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1221	2022-03-19 13:39:07.089437+00	16	Акрамов Мукхриддин - Rio - 2012 - АА 6038 РР  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1222	2022-03-19 13:39:10.762049+00	17	Авдиев Бабагелди - Sonata - 2016 - КА 9531 ВК  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1223	2022-03-19 13:39:14.089981+00	18	Айрапетов Артём - Rio - 2011 - СА 3701 СК  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1224	2022-03-19 13:39:17.594135+00	19	Батыров Гурбан - Accent - 2010 - АI 9756 IH  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1225	2022-03-19 13:39:21.670302+00	20	(без договора) ВЫКУП - Avante - 2012 - AІ 9149 МН  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1226	2022-03-19 13:39:24.874367+00	21	Романчук Леонид - 735i - 2002 - AI 3839 ОС  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1227	2022-03-19 13:39:28.924947+00	22	Бережной Сергей - Elantra - 2017 - АІ 5508 МР  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1228	2022-03-19 13:39:32.672588+00	23	Исмайлов Ойвен - Elantra - 2017 - АІ 6703 МР  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1229	2022-03-19 13:39:36.208165+00	24	выкуп Заур - Carens - 2012 - AI 0712 OВ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1230	2022-03-19 13:39:39.862752+00	25	Лаптев Олег - Fabia - 2013 - АІ 2803 СС  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1231	2022-03-19 13:39:43.761709+00	25	Лаптев Олег - Fabia - 2013 - АІ 2803 СС  - Free	2	[]	9	1
1232	2022-03-19 13:39:46.970378+00	26	Кара Эрхан - Sonata - 2012 - АI 6805 ОА  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1233	2022-03-19 13:39:51.080327+00	27	Кулиев Азат - Mazda - 2013 - АІ 8006 МА  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1234	2022-03-19 13:39:54.276118+00	29	Чолиев Рустем - Accent - 2019 - АI 4463 MC  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1235	2022-03-19 13:39:57.697546+00	30	Абдыресулов Рахман - Elantra - 2017 - AІ 7365 МI  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1236	2022-03-19 13:40:01.311143+00	31	Абдиев Довлетгельди - Fabia - 2008 - АІ 7166 ІМ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1237	2022-03-19 13:40:04.390383+00	32	Москалик Юрий - I30 - 2011 - АА 3967 ТТ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1238	2022-03-19 13:40:07.75732+00	33	Гончаров Сергей - I30 - 2012 - АА 6079 СС  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1239	2022-03-19 13:40:11.282193+00	34	Фархатов Фарух - Sonata - 2015 - АI 0954 МB  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1240	2022-03-19 13:40:14.759724+00	35	Атаджанов Бегенч - Jetta - 2008 - АІ 2258 MМ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1241	2022-03-19 13:40:18.008979+00	36	Валиев Эльвин - Insignia - 2006 - AI 1495 MO  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1242	2022-03-19 13:40:21.285728+00	37	Валиев Эльвин - K5 - 2012 - AI 5266 OO  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1243	2022-03-19 13:40:24.342899+00	38	Яхяев Шамиль - Fabia - 2008 - АА 8037 РР  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1244	2022-03-19 13:40:27.445256+00	39	Оммадов Мухамметсердар - Rio - 2012 - КА 3306 ВМ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1245	2022-03-19 13:40:31.091254+00	40	Аманов Довлет - Accent - 2008 - КА 0729 АР  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1246	2022-03-19 13:40:34.719743+00	41	Бахтияров Умитджан - Corolla - 2018 - АІ 7462 ОВ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1247	2022-03-19 13:40:38.113759+00	42	Бердиев Сердар - I30 - 2008 - АІ 6723 ІН  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1248	2022-03-19 13:40:41.385642+00	43	Халилов Сердар - Forte - 2011 - AI 9766 OA  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1249	2022-03-19 13:40:45.107198+00	44	Бегишов Нургелди - Passat - 2004 - AА 1556 ТT  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1250	2022-03-19 13:40:48.770589+00	45	Аманов Байрам - Polo - 2013 - АЕ 9200 IM  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1251	2022-03-19 13:40:52.296768+00	46	Голяченко Богдан - Rio - 2012 - АІ 1893 ІІ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1252	2022-03-19 13:40:56.185582+00	47	Сладкевич Василий - Sonata - 2015 - АА 1058 ЕМ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1253	2022-03-19 13:41:00.208169+00	48	Говкиев Бегенч - Sonata - 2015 - АМ 3560 ЕО  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1325	2022-03-19 18:51:37.237429+00	46	Савченко Юрий - 1	1	[{"added": {}}]	51	1
1254	2022-03-19 13:41:03.987878+00	49	Волошин Александр - Sonata - 2015 - АI 3427 MP  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1255	2022-03-19 13:41:07.249282+00	50	Ященко Ярослав - Sonata - 2012 - АI 6786 ОА  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1256	2022-03-19 13:41:11.138758+00	51	 МОЙСИК - K5 - 2015 - АІ 8089 MI  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1257	2022-03-19 13:41:14.573692+00	52	Пенджиев Шохрат - K5 - 2015 - АІ 0589 MІ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1258	2022-03-19 13:41:18.213139+00	53	Юрий Савченко - Sorento - 2016 - AI 2111 MB  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1259	2022-03-19 13:41:21.867251+00	54	Кулиев Азат - Corolla - 2011 - АІ 6453 ОО  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1260	2022-03-19 13:41:27.390475+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Free	2	[{"changed": {"fields": ["Type"]}}]	9	1
1261	2022-03-19 14:51:26.344329+00	11	Романчук Леонид	2	[]	8	1
1262	2022-03-19 14:51:30.857679+00	1	Seyidov Elvin	2	[]	8	1
1263	2022-03-19 14:51:33.63397+00	2	Mammadxanli Farid	2	[]	8	1
1264	2022-03-19 14:51:36.290608+00	3	Дмитрий Каюн	2	[]	8	1
1265	2022-03-19 14:51:39.330857+00	12	Бережной Сергей	2	[]	8	1
1266	2022-03-19 14:51:42.505476+00	13	Исмайлов Ойвен	2	[]	8	1
1267	2022-03-19 14:51:45.375188+00	14	выкуп Заур	2	[]	8	1
1268	2022-03-19 14:51:48.091709+00	15	Лаптев Олег	2	[]	8	1
1269	2022-03-19 14:51:52.215858+00	16	Кара Эрхан	2	[]	8	1
1270	2022-03-19 14:51:55.300471+00	16	Кара Эрхан	2	[]	8	1
1271	2022-03-19 14:51:58.067148+00	17	Кулиев Азат	2	[]	8	1
1272	2022-03-19 14:52:00.254546+00	18	выкуп Али	2	[]	8	1
1273	2022-03-19 14:52:02.933537+00	19	Чолиев Рустем	2	[]	8	1
1274	2022-03-19 14:52:05.902556+00	20	Абдыресулов Рахман	2	[]	8	1
1275	2022-03-19 14:52:08.908964+00	21	Абдиев Довлетгельди	2	[]	8	1
1276	2022-03-19 14:52:11.579014+00	4	Язгулиев Шохрат	2	[]	8	1
1277	2022-03-19 14:52:14.026741+00	22	Москалик Юрий	2	[]	8	1
1278	2022-03-19 14:52:16.305628+00	23	Гончаров Сергей	2	[]	8	1
1279	2022-03-19 14:52:19.012552+00	24	Фархатов Фарух	2	[]	8	1
1280	2022-03-19 14:52:21.462717+00	25	Атаджанов Бегенч	2	[]	8	1
1281	2022-03-19 14:52:24.33567+00	26	Валиев Эльвин	2	[]	8	1
1282	2022-03-19 14:52:26.989065+00	27	Яхяев Шамиль	2	[]	8	1
1283	2022-03-19 14:52:29.189985+00	28	Оммадов Мухамметсердар	2	[]	8	1
1284	2022-03-19 14:52:31.496945+00	29	Аманов Довлет	2	[]	8	1
1285	2022-03-19 14:52:33.933066+00	30	Бахтияров Умитджан	2	[]	8	1
1286	2022-03-19 14:52:36.701043+00	31	Бердиев Сердар	2	[]	8	1
1287	2022-03-19 14:52:38.907684+00	5	Чолиев Юнус	2	[]	8	1
1288	2022-03-19 14:52:41.09794+00	32	Халилов Сердар	2	[]	8	1
1289	2022-03-19 14:52:43.136891+00	33	Бегишов Нургелди	2	[]	8	1
1290	2022-03-19 14:52:46.176057+00	34	Аманов Байрам	2	[]	8	1
1291	2022-03-19 14:52:48.668211+00	35	Голяченко Богдан	2	[]	8	1
1292	2022-03-19 14:52:51.311612+00	36	Сладкевич Василий	2	[]	8	1
1293	2022-03-19 14:52:53.580407+00	37	Говкиев Бегенч	2	[]	8	1
1294	2022-03-19 14:52:56.139105+00	38	Волошин Александр	2	[]	8	1
1295	2022-03-19 14:52:58.680232+00	39	Ященко Ярослав	2	[]	8	1
1296	2022-03-19 14:53:01.111585+00	40	 МОЙСИК	2	[]	8	1
1297	2022-03-19 14:53:03.271895+00	41	Пенджиев Шохрат	2	[]	8	1
1298	2022-03-19 14:53:05.710888+00	6	Акрамов Мукхриддин	2	[]	8	1
1299	2022-03-19 14:53:07.859156+00	42	Юрий Савченко	2	[]	8	1
1300	2022-03-19 14:53:10.50126+00	7	Авдиев Бабагелди	2	[]	8	1
1301	2022-03-19 14:53:12.936752+00	8	Айрапетов Артём	2	[]	8	1
1302	2022-03-19 14:53:15.421227+00	9	Батыров Гурбан	2	[]	8	1
1303	2022-03-19 14:53:18.214459+00	10	(без договора) ВЫКУП	2	[]	8	1
1304	2022-03-19 14:53:40.305742+00	9	Эрхан Кара - 10	3		51	1
1305	2022-03-19 14:55:15.517838+00	1	Seyidov Elvin	2	[]	8	1
1306	2022-03-19 17:20:50.140304+00	134	Forester - 2007 - AI 8500 MT 	2	[{"deleted": {"name": "car image", "object": "Forester - 2007 - AI 8500 MT "}}]	15	1
1307	2022-03-19 17:23:04.024266+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"added": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 3427 MP "}}]	15	1
1308	2022-03-19 17:24:22.995564+00	130	Sonata - 2015 - АI 3427 MP 	2	[{"deleted": {"name": "car image", "object": "Sonata - 2015 - \\u0410I 3427 MP "}}]	15	1
1309	2022-03-19 17:31:19.196785+00	21	Абдиев Довлетгельди	2	[{"added": {"name": "user image", "object": ""}}]	8	1
1310	2022-03-19 17:33:30.937489+00	4	Язгулиев Шохрат	2	[{"added": {"name": "user image", "object": ""}}]	8	1
1311	2022-03-19 17:47:06.688999+00	1	Язгулиев Шохрат	1	[{"added": {}}]	41	1
1312	2022-03-19 17:47:15.745453+00	1	Язгулиев Шохрат	2	[{"changed": {"fields": ["Amount", "Status"]}}]	41	1
1313	2022-03-19 17:48:49.050991+00	1	Язгулиев Шохрат	2	[]	41	1
1314	2022-03-19 17:49:30.209417+00	2	Язгулиев Шохрат	1	[{"added": {}}]	41	1
1315	2022-03-19 18:01:49.632516+00	2	Язгулиев Шохрат	2	[{"changed": {"fields": ["Note"]}}]	41	1
1316	2022-03-19 18:08:19.703506+00	4	Язгулиев Шохрат	2	[{"changed": {"fields": ["Note"]}}]	8	1
1317	2022-03-19 18:08:56.963322+00	4	Язгулиев Шохрат	2	[{"changed": {"fields": ["Note"]}}]	8	1
1318	2022-03-19 18:11:54.515175+00	42	Юрий Савченко	2	[{"changed": {"fields": ["Note"]}}, {"added": {"name": "user image", "object": ""}}]	8	1
1319	2022-03-19 18:12:37.340237+00	41	Пенджиев Шохрат	2	[{"changed": {"fields": ["Note"]}}, {"added": {"name": "user image", "object": ""}}]	8	1
1320	2022-03-19 18:12:52.608384+00	2	Юрий Савченко	2	[{"changed": {"fields": ["Customer"]}}]	41	1
1321	2022-03-19 18:13:12.370462+00	1	Пенджиев Шохрат	2	[{"changed": {"fields": ["Customer"]}}]	41	1
1322	2022-03-19 18:45:50.469915+00	44	Савченко Юрий - 4	1	[{"added": {}}]	51	1
1323	2022-03-19 18:47:46.020091+00	44	Савченко Юрий - 1	2	[{"changed": {"fields": ["Score"]}}]	51	1
1324	2022-03-19 18:47:58.486456+00	45	Шохрат Пенджиев - 4	1	[{"added": {}}]	51	1
1328	2022-03-19 22:41:44.050274+00	58	Seyidov Elvin - Sonata - 2016 - АI 0886 KP  - Rent	1	[{"added": {}}]	9	1
1329	2022-03-19 22:42:02.118933+00	1	Sonata - 2016 - АI 0886 KP	1	[{"added": {}}, {"added": {"name": "revenue", "object": "Elvin - \\u0410I 0886 KP  2022-03-10"}}]	59	1
1330	2022-03-19 22:42:16.647601+00	1	Sonata - 2016 - АI 0886 KP	2	[{"changed": {"name": "revenue", "object": "Elvin - \\u0410I 0886 KP  2022-03-10", "fields": ["Rent amount"]}}]	59	1
1331	2022-03-20 19:16:20.936199+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Rent Taxi Percent	2	[{"changed": {"fields": ["Type"]}}]	9	1
1332	2022-03-20 19:19:03.108283+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Rent Taxi Percent	2	[{"changed": {"fields": ["End date"]}}]	9	1
1333	2022-03-20 19:19:15.132112+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Rent Taxi Percent	2	[{"changed": {"fields": ["End date"]}}]	9	1
1334	2022-03-20 19:20:35.949376+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Rent Taxi Percent	2	[{"changed": {"fields": ["End date"]}}]	9	1
1335	2022-03-20 19:21:41.094006+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Rent Taxi Percent	2	[]	9	1
1336	2022-03-20 19:22:43.42938+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Rent Taxi Percent	2	[]	9	1
1337	2022-03-20 19:23:07.057336+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-11"}}, {"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-12"}}, {"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-12"}}, {"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-12"}}, {"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-12"}}, {"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-12"}}, {"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-12"}}, {"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-13"}}]	34	1
1338	2022-03-20 19:30:54.779676+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"added": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-19"}}]	34	1
1339	2022-03-20 19:31:31.063747+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-19"}}]	34	1
1340	2022-03-20 19:38:38.374959+00	204	Avante - 2016 - АІ 8243 OВ - 2022-03-19 - Bolt	2	[{"changed": {"fields": ["Order count"]}}]	35	1
1341	2022-03-20 19:43:33.995965+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-19"}}]	34	1
1342	2022-03-20 19:45:17.045406+00	79	alvinseyidov - АІ 8243 OВ - 2022-03-19	2	[{"changed": {"name": "revenue item", "object": "Avante - 2016 - \\u0410\\u0406 8243 O\\u0412 - 2022-03-19 - Uber", "fields": ["Order count"]}}]	33	1
1343	2022-03-20 22:15:09.468581+00	10	Avante - 2016 - АІ 8243 OВ	2	[{"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-10"}}, {"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-18"}}, {"deleted": {"name": "revenue", "object": "alvinseyidov - \\u0410\\u0406 8243 O\\u0412 - 2022-03-19"}}]	34	1
1344	2022-03-20 23:34:19.128377+00	1	Settings for Rent Taxi	1	[{"added": {}}]	45	1
1345	2022-03-20 23:34:26.748646+00	1	Elvin - КА 3676 ВО  2022-03-07 - 2022-03-13	2	[]	37	1
1346	2022-03-20 23:40:55.010023+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Rent Taxi Week	2	[{"changed": {"fields": ["Type"]}}]	9	1
1347	2022-03-20 23:40:56.397699+00	1	Sportage - 2011 - KА 6164 EC	2	[{"changed": {"fields": ["Car"]}}, {"changed": {"name": "revenue", "object": "Elvin - K\\u0410 6164 EC  2022-03-07 - 2022-03-13", "fields": ["Contract"]}}]	36	1
1348	2022-03-21 10:15:03.782619+00	57	Seyidov Elvin - Avante - 2016 - АІ 8243 OВ  - Rent Taxi Week	2	[{"changed": {"fields": ["End date"]}}]	9	1
1349	2022-03-21 10:16:29.57752+00	57	Seyidov Elvin - Sportage - 2011 - KА 6164 EC  - Rent Taxi Week	2	[{"changed": {"fields": ["Car"]}}]	9	1
1350	2022-03-21 18:54:00.531791+00	1	K5 - 2016 - АІ 3620 MP	1	[{"added": {}}]	50	1
1351	2022-03-21 18:54:15.875047+00	58	Seyidov Elvin - Sonata - 2016 - АI 0886 KP - F  - Rent Taxi Daily	2	[{"changed": {"fields": ["Type"]}}]	9	1
1352	2022-03-21 18:54:38.334283+00	1	Elvin - АІ 3620 MP  2022-03-20	1	[{"added": {}}]	47	1
1353	2022-03-21 18:55:34.947292+00	1	Settings for Rent Taxi	1	[{"added": {}}]	46	1
1354	2022-03-21 18:56:19.620713+00	1	Elvin - АІ 3620 MP  2022-03-20	2	[{"added": {"name": "revenue item", "object": "K5 - 2016 - \\u0410\\u0406 3620 MP - 2022-03-20 - Bolt"}}]	47	1
1355	2022-03-21 21:53:26.029509+00	1	Forester - AI 8500 MT	2	[{"added": {"name": "casco image", "object": "Forester - AI 8500 MT"}}]	55	1
1356	2022-03-21 21:53:32.851279+00	2	Subaru - AI 7103 OB	2	[{"added": {"name": "casco image", "object": "Subaru - AI 7103 OB"}}]	55	1
1357	2022-03-21 21:53:38.407512+00	3	Sonata - АI 0886 KP	2	[{"added": {"name": "casco image", "object": "Sonata - \\u0410I 0886 KP"}}]	55	1
1358	2022-03-21 22:19:33.624711+00	4	Sonata - АА 1058 ЕМ	2	[{"added": {"name": "osago image", "object": "Sonata - 2015"}}]	57	1
1359	2022-03-21 22:19:40.080244+00	1	Forester - AI 8500 MT	2	[{"added": {"name": "osago image", "object": "Forester - 2007"}}]	57	1
1360	2022-03-21 22:19:46.605889+00	2	Subaru - AI 7103 OB	2	[{"added": {"name": "osago image", "object": "Subaru - 2011"}}]	57	1
1361	2022-03-21 22:19:51.827363+00	3	Sonata - АI 0886 KP	2	[{"added": {"name": "osago image", "object": "Sonata - 2016"}}]	57	1
1362	2022-03-22 00:56:19.427138+00	1	K5 - 2017 - АІ 5564 MP - F 	1	[{"added": {}}]	62	1
1363	2022-03-22 01:04:54.141285+00	1	K5 - 2017 - АІ 5564 MP - F 	2	[{"changed": {"fields": ["Driver"]}}]	62	1
1364	2022-03-22 01:05:03.09164+00	1	Accent - 2017 - АІ 0312 MX - F 	2	[{"changed": {"fields": ["Car"]}}]	62	1
1365	2022-03-22 01:20:57.398491+00	1	Accent - 2017 - АІ 0312 MX - F 	2	[{"changed": {"fields": ["Insurance company"]}}]	62	1
1366	2022-03-22 01:26:30.723835+00	1	Accent - 2017 - АІ 0312 MX - F 	2	[{"changed": {"fields": ["Insurance back staff"]}}]	62	1
1367	2022-03-22 01:27:27.818634+00	1	Accent - 2017 - АІ 0312 MX - F 	2	[{"changed": {"fields": ["Insurance back date"]}}]	62	1
1368	2022-03-22 01:27:52.420701+00	1	Accent - 2017 - АІ 0312 MX - F 	2	[{"changed": {"fields": ["Note"]}}]	62	1
1369	2022-03-22 01:31:50.24425+00	1	АЛЬЯНС	2	[{"changed": {"fields": ["Name"]}}]	56	1
1370	2022-03-24 16:58:46.870098+00	3	Юрий Савченко	1	[{"added": {}}]	41	1
1371	2022-03-24 16:59:04.256033+00	4	Юрий Савченко	1	[{"added": {}}]	41	1
1372	2022-03-24 17:29:32.207878+00	3	Юрий Савченко	2	[{"changed": {"fields": ["Paid date", "Status"]}}]	41	1
1373	2022-03-24 18:02:17.992318+00	3	Юрий Савченко	2	[{"changed": {"fields": ["Car"]}}]	41	1
1374	2022-03-24 18:07:11.825097+00	1	Пенджиев Шохрат	2	[{"changed": {"fields": ["Car"]}}]	41	1
1375	2022-03-24 18:07:15.360394+00	4	Юрий Савченко	2	[{"changed": {"fields": ["Car"]}}]	41	1
1376	2022-03-24 18:07:19.635513+00	3	Юрий Савченко	2	[{"changed": {"fields": ["Car"]}}]	41	1
1377	2022-03-24 18:07:23.991089+00	2	Юрий Савченко	2	[{"changed": {"fields": ["Car"]}}]	41	1
1378	2022-03-24 18:33:11.550187+00	1	Пенджиев Шохрат	2	[{"changed": {"fields": ["Note"]}}]	41	1
1379	2022-03-24 18:33:17.153686+00	4	Юрий Савченко	2	[{"changed": {"fields": ["Note"]}}]	41	1
1380	2022-03-24 18:33:19.675215+00	3	Юрий Савченко	2	[{"changed": {"fields": ["Note"]}}]	41	1
1381	2022-03-24 18:34:13.444255+00	2	Юрий Савченко	2	[{"changed": {"fields": ["Note"]}}]	41	1
1382	2022-03-24 18:35:02.610662+00	5	Кара Эрхан	1	[{"added": {}}]	41	1
1383	2022-03-24 18:37:03.155622+00	5	Кара Эрхан	2	[]	41	1
1384	2022-03-24 18:37:44.815461+00	6	Чолиев Рустем	1	[{"added": {}}]	41	1
1385	2022-03-24 18:38:39.616228+00	7	Кара Эрхан	1	[{"added": {}}]	41	1
1386	2022-03-24 18:39:09.601181+00	8	Чолиев Рустем	1	[{"added": {}}]	41	1
1387	2022-03-24 22:00:49.573133+00	1	K5	1	[{"added": {}}]	39	1
1388	2022-03-24 22:10:06.692425+00	1	K5	2	[{"changed": {"fields": ["No", "Note"]}}]	39	1
1389	2022-03-24 22:13:54.382558+00	1	K5	2	[]	39	1
1390	2022-03-24 22:14:18.515957+00	2	K5	1	[{"added": {}}]	39	1
1391	2022-03-24 22:14:39.55026+00	2	K5	2	[{"changed": {"fields": ["Note"]}}]	39	1
1392	2022-03-24 22:21:21.293192+00	2	K5	2	[{"changed": {"fields": ["Debt status"]}}]	39	1
1393	2022-03-24 22:23:02.331954+00	1	K5	2	[{"changed": {"fields": ["Debt status"]}}]	39	1
1394	2022-03-24 22:23:10.860091+00	1	K5	2	[]	39	1
1395	2022-03-24 22:23:41.347547+00	3	Avante	1	[{"added": {}}]	39	1
1396	2022-03-24 22:23:45.791171+00	3	Avante	2	[{"changed": {"fields": ["Debt status"]}}]	39	1
1397	2022-03-26 16:19:54.433293+00	1	Sonata - 2016 - АI 0886 KP	2	[{"changed": {"name": "revenue", "object": "Elvin - \\u0410I 0886 KP  2022-03-10", "fields": ["End date"]}}]	59	1
1398	2022-03-26 16:23:54.192444+00	132	Sonata - 2016 - АI 0886 KP - R 	2	[{"changed": {"fields": ["Status"]}}]	15	1
1399	2022-03-26 19:56:12.174564+00	1	Седан	2	[{"changed": {"fields": ["Name"]}}]	18	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	authtoken	token
7	authtoken	tokenproxy
8	account	customuser
9	account	contract
10	account	userphone
11	account	userimage
12	account	passportimage
13	account	licenseimage
14	car	brand
15	car	car
16	car	carclass
17	car	carimage
18	car	case
19	car	color
20	car	fuel
21	car	office
22	car	model
23	car	carpaper
24	car	carimagetask
25	core	administrativedeposit
26	core	administrativewithdraw
27	core	currency
28	core	balance
29	credit	credit
30	credit	creditpayment
31	credit	creditimage
32	taxi	company
33	taxi	revenue
34	taxi	taxi
35	taxi	revenueitem
36	rent_taxi	renttaxi
37	rent_taxi	revenue
38	rent_taxi	revenueitem
39	shtraf	shtraf
40	shtraf	shtrafimage
41	debt	debt
42	debt	debtimage
43	taxi	tariff
44	rent_taxi	tariff
45	rent_taxi	settings
46	rent_taxi_daily	settings
47	rent_taxi_daily	revenue
48	rent_taxi_daily	revenueitem
49	rent_taxi_daily	renttaxi
50	rent_taxi_daily	renttaxidaily
51	account	score
52	insurance	cascoimage
53	insurance	osagopayment
54	insurance	cascopayment
55	insurance	casco
56	insurance	company
57	insurance	osago
58	insurance	osagoimage
59	rent	rent
60	rent	settings
61	rent	revenue
62	dtp	dtp
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	account	0001_initial	2022-02-23 13:23:55.37886+00
2	car	0001_initial	2022-02-23 13:23:55.438789+00
3	taxi	0001_initial	2022-02-23 13:23:55.500064+00
4	car	0002_initial	2022-02-23 13:23:55.637199+00
5	contenttypes	0001_initial	2022-02-23 13:23:55.644567+00
6	contenttypes	0002_remove_content_type_name	2022-02-23 13:23:55.664023+00
7	auth	0001_initial	2022-02-23 13:23:55.690406+00
8	auth	0002_alter_permission_name_max_length	2022-02-23 13:23:55.696179+00
9	auth	0003_alter_user_email_max_length	2022-02-23 13:23:55.70207+00
10	auth	0004_alter_user_username_opts	2022-02-23 13:23:55.707886+00
11	auth	0005_alter_user_last_login_null	2022-02-23 13:23:55.713343+00
12	auth	0006_require_contenttypes_0002	2022-02-23 13:23:55.71507+00
13	auth	0007_alter_validators_add_error_messages	2022-02-23 13:23:55.721655+00
14	auth	0008_alter_user_username_max_length	2022-02-23 13:23:55.726651+00
15	auth	0009_alter_user_last_name_max_length	2022-02-23 13:23:55.731625+00
16	auth	0010_alter_group_name_max_length	2022-02-23 13:23:55.737515+00
17	auth	0011_update_proxy_permissions	2022-02-23 13:23:55.753864+00
18	auth	0012_alter_user_first_name_max_length	2022-02-23 13:23:55.766857+00
19	account	0002_initial	2022-02-23 13:23:55.850255+00
20	admin	0001_initial	2022-02-23 13:23:55.879293+00
21	admin	0002_logentry_remove_auto_add	2022-02-23 13:23:55.895084+00
22	admin	0003_logentry_add_action_flag_choices	2022-02-23 13:23:55.910891+00
23	authtoken	0001_initial	2022-02-23 13:23:55.938869+00
24	authtoken	0002_auto_20160226_1747	2022-02-23 13:23:55.995449+00
25	authtoken	0003_tokenproxy	2022-02-23 13:23:55.998625+00
26	core	0001_initial	2022-02-23 13:23:56.01464+00
27	credit	0001_initial	2022-02-23 13:23:56.095891+00
28	debt	0001_initial	2022-02-23 13:23:56.158637+00
29	debt	0002_alter_debtimage_debt	2022-02-23 13:23:56.18483+00
30	taxi	0002_alter_revenue_taxi_alter_revenueitem_revenue	2022-02-23 13:23:56.293472+00
31	rent_taxi	0001_initial	2022-02-23 13:23:56.357087+00
32	rent_taxi	0002_rename_end_date_renttaxi_date_remove_renttaxi_driver_and_more	2022-02-23 13:23:56.46387+00
33	rent_taxi	0003_revenue	2022-02-23 13:23:56.498417+00
34	rent_taxi	0004_revenueitem	2022-02-23 13:23:56.52948+00
35	rent_taxi	0005_revenueitem_bolt_will_pay_revenueitem_debt_and_more	2022-02-23 13:23:56.558949+00
36	rent_taxi	0006_remove_revenueitem_debt_and_more	2022-02-23 13:23:56.650625+00
37	sessions	0001_initial	2022-02-23 13:23:56.658822+00
38	shtraf	0001_initial	2022-02-23 13:23:56.72552+00
39	shtraf	0002_shtraf_is_active	2022-02-23 13:23:56.750523+00
40	taxi	0003_revenueitem_debt_all_revenueitem_debt_today	2022-02-23 13:23:56.840355+00
41	taxi	0004_revenueitem_taxi	2022-02-23 13:23:56.868398+00
42	taxi	0005_alter_revenueitem_taxi	2022-02-23 13:23:56.893742+00
43	car	0003_remove_car_status_brand_logo_car_health_status_and_more	2022-03-02 21:04:11.223666+00
44	car	0004_car_status	2022-03-02 21:04:11.242483+00
45	car	0005_car_driver	2022-03-02 23:06:57.500461+00
46	account	0003_customuser_is_black_listed_alter_customuser_type	2022-03-03 08:45:45.600708+00
47	credit	0002_remove_credit_end_date_remove_credit_start_date	2022-03-03 18:43:05.867916+00
48	credit	0003_rename_total_credit_car_value_total_credit_expenses_and_more	2022-03-03 19:59:24.820108+00
49	credit	0004_alter_credit_payment_year	2022-03-03 21:55:00.945866+00
50	credit	0005_remove_credit_debt	2022-03-12 14:19:07.912641+00
51	taxi	0006_remove_taxi_date	2022-03-12 14:19:07.931812+00
52	taxi	0007_tariff	2022-03-12 14:19:07.948567+00
53	taxi	0008_tariff_company	2022-03-12 15:34:19.540796+00
54	rent_taxi	0007_tariff_remove_renttaxi_date	2022-03-12 15:34:19.568118+00
55	rent_taxi	0008_rename_bolt_will_pay_revenueitem_company_will_pay_brand_and_more	2022-03-12 15:34:19.673064+00
56	taxi	0009_remove_company_tarif1_count_and_more	2022-03-12 15:39:43.985118+00
57	account	0004_contract_deposit	2022-03-12 17:00:43.748319+00
58	rent_taxi	0009_rename_bolt_will_pay_total_revenueitem_company_will_pay_total	2022-03-12 18:27:19.444456+00
59	rent_taxi	0010_settings_revenueitem_we_must_pay	2022-03-12 18:27:19.465267+00
60	taxi	0010_revenueitem_driver_must_pay_revenueitem_we_must_pay	2022-03-12 18:27:19.490399+00
61	taxi	0011_rename_paid_revenueitem_driver_paid_and_more	2022-03-12 18:40:07.973523+00
62	rent_taxi	0011_rename_paid_revenueitem_driver_paid_and_more	2022-03-12 21:40:10.277069+00
63	rent_taxi	0012_rename_company_comission_revenueitem_company_commission	2022-03-12 21:40:10.290603+00
64	rent_taxi	0013_rename_commission_settings_commission_from_driver	2022-03-12 21:40:10.29505+00
65	credit	0006_alter_creditpayment_options	2022-03-13 12:03:42.831618+00
66	rent_taxi	0014_alter_revenue_options	2022-03-13 12:03:42.853762+00
67	taxi	0012_alter_revenue_options	2022-03-13 12:03:42.874453+00
68	rent_taxi	0015_alter_revenueitem_options_and_more	2022-03-13 21:38:57.311591+00
69	taxi	0013_alter_revenueitem_options	2022-03-13 21:38:57.324998+00
70	rent_taxi_daily	0001_initial	2022-03-14 10:45:43.106332+00
71	rent_taxi	0016_alter_renttaxi_car_alter_revenue_contract_and_more	2022-03-14 12:02:17.028093+00
72	rent_taxi_daily	0002_remove_revenue_rent_taxi_alter_revenue_contract_and_more	2022-03-14 12:02:17.316511+00
73	taxi	0014_alter_revenue_contract_alter_revenue_driver_and_more	2022-03-14 12:02:17.524562+00
74	account	0005_score	2022-03-14 15:46:09.997275+00
75	account	0006_rename_deposit_contract_deposit_amount	2022-03-14 15:46:10.023701+00
76	insurance	0001_initial	2022-03-14 19:07:51.518951+00
77	debt	0003_remove_debt_date_remove_debt_paid_debt_status	2022-03-15 09:15:21.854518+00
78	shtraf	0003_remove_shtraf_paid_shtraf_debt_status_shtraf_no_and_more	2022-03-15 09:15:21.995632+00
79	debt	0004_remove_debtimage_debt_delete_debt_delete_debtimage	2022-03-15 09:26:48.689104+00
80	shtraf	0004_remove_shtrafimage_shtraf_delete_shtraf_and_more	2022-03-15 09:26:48.810443+00
81	debt	0005_initial	2022-03-15 09:30:30.630443+00
82	shtraf	0005_initial	2022-03-15 09:30:30.71387+00
83	car	0006_alter_car_model_alter_car_office	2022-03-18 22:28:18.399583+00
84	insurance	0002_cascopayment_step	2022-03-18 22:28:18.410121+00
85	insurance	0003_alter_cascopayment_options	2022-03-18 22:28:18.417819+00
86	account	0007_alter_contract_type	2022-03-19 14:48:06.508227+00
87	account	0008_score_customer_score_note_score_score	2022-03-19 14:48:06.633579+00
88	account	0009_alter_customuser_is_active	2022-03-19 17:12:54.537925+00
89	car	0007_alter_car_driver	2022-03-19 17:12:54.616959+00
90	debt	0006_rename_driver_debt_customer	2022-03-19 17:12:54.651299+00
91	debt	0007_debt_note	2022-03-19 17:54:13.315431+00
92	account	0010_customuser_note	2022-03-19 18:00:26.642437+00
93	account	0011_alter_customuser_options_customuser_rating	2022-03-19 20:38:46.975988+00
94	rent	0001_initial	2022-03-19 22:39:41.205069+00
95	rent	0002_revenue_deposit_amount_revenue_is_deposit_in_and_more	2022-03-19 22:39:41.277337+00
96	rent	0003_alter_revenue_options_rename_date_revenue_start_date	2022-03-19 22:39:41.329578+00
97	rent	0004_revenue_end_date	2022-03-19 22:39:41.352684+00
98	rent	0005_remove_revenue_amount_card	2022-03-19 22:39:41.37668+00
99	rent	0006_remove_revenue_amount_cash	2022-03-19 22:39:41.398941+00
100	taxi	0015_rename_driver_revenue_customer	2022-03-20 19:13:32.304187+00
101	rent_taxi_daily	0003_rename_rent_taxi_revenueitem_rent_taxi_daily	2022-03-21 18:45:49.719363+00
102	dtp	0001_initial	2022-03-22 00:55:07.591892+00
103	dtp	0002_dtp_driver	2022-03-22 00:55:07.638195+00
104	dtp	0003_alter_dtp_driver	2022-03-22 00:55:07.683449+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
0znbpal655dl89vczbvm6vnqlitylvtq	.eJxVjEEOwiAQRe_C2hBhoIBL9z0DmWFAqoYmpV0Z765NutDtf-_9l4i4rTVuPS9xYnERSpx-N8L0yG0HfMd2m2Wa27pMJHdFHrTLceb8vB7u30HFXr81AnHySSNBVolDAS5nbQmV5-zVANoYa5ESIfgSOAzF2BzQOEXOgRPvDxPuOJI:1nMrdK:uXIYNhwrrrAE4oeFe-vL99UmTJCawjtgP_nwe8TDVRA	2022-03-09 13:24:42.89516+00
mcrd94ogk32siiw45fl80wf7zqgg87yn	.eJxVjEEOwiAQRe_C2hBhoIBL9z0DmWFAqoYmpV0Z765NutDtf-_9l4i4rTVuPS9xYnERSpx-N8L0yG0HfMd2m2Wa27pMJHdFHrTLceb8vB7u30HFXr81AnHySSNBVolDAS5nbQmV5-zVANoYa5ESIfgSOAzF2BzQOEXOgRPvDxPuOJI:1nNJna:TiPxhhNhiFBK0-ur3c4F4Ny4q_vtaXOgby1fB7nO9QQ	2022-03-10 19:29:10.499281+00
gyddxhten2alz26j3nb02yntt4duo833	.eJxVjEEOwiAQRe_C2hBhoIBL9z0DmWFAqoYmpV0Z765NutDtf-_9l4i4rTVuPS9xYnERSpx-N8L0yG0HfMd2m2Wa27pMJHdFHrTLceb8vB7u30HFXr81AnHySSNBVolDAS5nbQmV5-zVANoYa5ESIfgSOAzF2BzQOEXOgRPvDxPuOJI:1nR73b:ew8yyZSM-S6bCLXfINm_dljEpQUPa7OUhj9ogWWULRs	2022-03-21 06:41:23.175466+00
povnef8h43ovcoskujcgfz19abij7et2	.eJxVjEEOwiAQRe_C2hBhoIBL9z0DmWFAqoYmpV0Z765NutDtf-_9l4i4rTVuPS9xYnERSpx-N8L0yG0HfMd2m2Wa27pMJHdFHrTLceb8vB7u30HFXr81AnHySSNBVolDAS5nbQmV5-zVANoYa5ESIfgSOAzF2BzQOEXOgRPvDxPuOJI:1nWF3j:STFJ2Tu8NBdJzOX58rJY1LSeMKk9fcZd41NXQDAXBrM	2022-04-04 10:14:43.939665+00
4bp6b8xhjb2r29bib4ebz0jqmy17pzsf	.eJxVjEEOwiAQRe_C2hBhoIBL9z0DmWFAqoYmpV0Z765NutDtf-_9l4i4rTVuPS9xYnERSpx-N8L0yG0HfMd2m2Wa27pMJHdFHrTLceb8vB7u30HFXr81AnHySSNBVolDAS5nbQmV5-zVANoYa5ESIfgSOAzF2BzQOEXOgRPvDxPuOJI:1nbSma:mu74e2B9xK7LZBW7BdDFpLBqv8H_1Da65t-e5BDxU5A	2022-04-18 19:54:36.403447+00
\.


--
-- Data for Name: dtp_dtp; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.dtp_dtp (id, created_at, updated_at, date, is_driver_fault, insurance, expert_observation, expert_inspection, insurance_back_status, insurance_back_amount, insurance_back_date, insurance_back_staff, note, is_verified, is_active, car_id, insurance_company_id, driver_id) FROM stdin;
1	2022-03-22 00:56:19.424093+00	2022-03-22 01:27:52.418263+00	2022-03-22	f	C	f	f	f	\N	2022-03-22	Farid Mammadxanli	Lorem Ipsum is simply dummy text of the printing and typesetting industry.	f	t	2	1	1
\.


--
-- Data for Name: insurance_casco; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.insurance_casco (id, amount_year, compensation, step, start_date, end_date, is_verified, is_active, car_id, company_id) FROM stdin;
1	10000	1000	1	2022-01-18	2023-01-18	f	t	134	1
2	0	0	1	2022-03-19	2023-03-19	f	t	133	1
3	0	0	2	2021-12-19	2022-12-19	f	t	132	1
\.


--
-- Data for Name: insurance_cascoimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.insurance_cascoimage (id, file, casco_id) FROM stdin;
1	casco/1_6yTh97G.jpg	1
2	casco/3_mipCCVw.png	2
3	casco/1_iv8eUkU.jpg	3
\.


--
-- Data for Name: insurance_cascopayment; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.insurance_cascopayment (id, created_at, updated_at, date, amount, is_verified, casco_id, step) FROM stdin;
1	2022-03-18 23:57:51.374799+00	2022-03-18 23:57:51.374819+00	2022-01-18	250	f	1	1
2	2022-03-19 00:11:38.688817+00	2022-03-19 00:11:38.688846+00	2022-03-19	100	f	2	1
3	2022-03-19 00:12:14.764502+00	2022-03-19 00:12:14.764525+00	2021-12-19	100	f	3	1
4	2022-03-19 00:12:29.871093+00	2022-03-19 00:12:29.871116+00	2022-03-19	100	f	3	2
\.


--
-- Data for Name: insurance_company; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.insurance_company (id, name) FROM stdin;
1	АЛЬЯНС
\.


--
-- Data for Name: insurance_osago; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.insurance_osago (id, amount_year, compensation, start_date, end_date, is_verified, is_active, car_id, company_id) FROM stdin;
4	0	0	2022-03-19	2023-03-19	f	t	131	1
1	0	0	2022-03-19	2023-03-19	f	t	134	1
2	0	0	2022-03-19	2023-03-19	f	t	133	1
3	0	0	2022-03-19	2023-03-19	f	t	132	1
\.


--
-- Data for Name: insurance_osagoimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.insurance_osagoimage (id, file, osago_id) FROM stdin;
1	osago/3.png	4
2	osago/3.webp	1
3	osago/02-2012-kia-rio-5-front-left-view.webp	2
4	osago/3_yiIMkoE.png	3
\.


--
-- Data for Name: insurance_osagopayment; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.insurance_osagopayment (id, created_at, updated_at, date, amount, is_verified, osago_id) FROM stdin;
1	2022-03-19 00:13:37.246629+00	2022-03-19 00:13:37.246654+00	2022-03-19	0	f	1
2	2022-03-19 00:13:49.601833+00	2022-03-19 00:13:49.601857+00	2022-03-19	0	f	2
3	2022-03-19 00:14:14.126466+00	2022-03-19 00:14:14.12649+00	2022-03-19	0	f	3
4	2022-03-19 00:14:24.808764+00	2022-03-19 00:14:24.808788+00	2022-03-19	0	f	4
\.


--
-- Data for Name: rent_rent; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_rent (id, is_active, car_id) FROM stdin;
1	t	132
\.


--
-- Data for Name: rent_revenue; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_revenue (id, created_at, updated_at, start_date, rented_days, rent_amount, is_paid, contract_id, driver_id, rent_id, deposit_amount, is_deposit_in, is_deposit_out, end_date) FROM stdin;
1	2022-03-19 22:42:02.114116+00	2022-03-26 16:19:54.431184+00	2022-03-10	17	50000	t	58	1	1	1500	t	f	2022-03-27
\.


--
-- Data for Name: rent_settings; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_settings (id, rent_per_day, deposit_amount) FROM stdin;
\.


--
-- Data for Name: rent_taxi_daily_renttaxidaily; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_daily_renttaxidaily (id, created_at, updated_at, is_active, car_id) FROM stdin;
1	2022-03-21 18:54:00.527201+00	2022-03-21 18:54:00.527228+00	t	70
\.


--
-- Data for Name: rent_taxi_daily_revenue; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_daily_revenue (id, created_at, updated_at, date, contract_id, driver_id, rent_taxi_daily_id) FROM stdin;
1	2022-03-21 18:54:38.32131+00	2022-03-21 18:56:19.608301+00	2022-03-20	58	1	1
\.


--
-- Data for Name: rent_taxi_daily_revenueitem; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_daily_revenueitem (id, order_count, company_will_pay_brand, amount_cash, amount_card, company_commission, rent_amount, commission_from_driver, debt_to_bolt, company_will_pay_total, driver_must_pay, we_must_pay, driver_paid, we_paid, debt_today, debt_all, company_id, rent_taxi_daily_id, revenue_id) FROM stdin;
1	55	0	1000	800	450	4500	0	0	350	3700	0	0	0	-3700	-3700	1	1	1
\.


--
-- Data for Name: rent_taxi_daily_settings; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_daily_settings (id, commission_from_driver, daily_rent) FROM stdin;
1	0	4500
\.


--
-- Data for Name: rent_taxi_renttaxi; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_renttaxi (id, created_at, updated_at, car_id, is_active) FROM stdin;
1	2022-03-12 15:14:55.56558+00	2022-03-20 23:40:56.382222+00	78	t
\.


--
-- Data for Name: rent_taxi_revenue; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_revenue (id, created_at, updated_at, start_date, end_date, contract_id, driver_id, rent_taxi_id) FROM stdin;
1	2022-03-12 15:14:55.567824+00	2022-03-20 23:40:56.383218+00	2022-03-07	2022-03-13	57	1	1
\.


--
-- Data for Name: rent_taxi_revenueitem; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_revenueitem (id, amount_cash, amount_card, order_count, driver_paid, company_id, revenue_id, company_will_pay_brand, rent_amount, company_will_pay_total, commission_from_driver, debt_all, debt_to_bolt, debt_today, driver_must_pay, rent_taxi_id, we_must_pay, company_commission, we_paid) FROM stdin;
4	5000	1000	90	0	1	1	1080	4500	580	225	-4225	500	-4225	4225	1	0	1500	0
\.


--
-- Data for Name: rent_taxi_settings; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_settings (id, commission_from_driver, weekly_rent) FROM stdin;
1	5	4500
\.


--
-- Data for Name: rent_taxi_tariff; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.rent_taxi_tariff (id, limit1, limit2, limit3, limit1_tariff, limit2_tariff, limit3_tariff, company_id) FROM stdin;
1	90	120	150	12	13	14	1
2	90	120	150	12	13	14	2
3	90	120	150	12	13	14	3
\.


--
-- Data for Name: shtraf_shtraf; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.shtraf_shtraf (id, created_at, updated_at, no, datetime, created_date, paid_date, amount, note, status, debt_status, is_active, car_id, driver_id) FROM stdin;
2	2022-03-24 22:14:18.513639+00	2022-03-24 22:21:21.288943+00	4354	2022-03-24 22:13:58+00	2022-03-24	2022-03-24	6000	Штраф из-за превышения скорости.	P	P	t	74	2
1	2022-03-24 22:00:49.569058+00	2022-03-24 22:23:10.857456+00	4236	2022-03-24 22:00:33+00	2022-03-24	\N	5000	Штраф из-за превышения скорости.	N	N	t	74	1
3	2022-03-24 22:23:41.345038+00	2022-03-24 22:23:45.787417+00	4323	2022-03-24 22:23:14+00	2022-03-24	2022-03-24	3000	Штраф из-за превышения скорости.	N	N	t	13	3
\.


--
-- Data for Name: shtraf_shtrafimage; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.shtraf_shtrafimage (id, file, shtraf_id) FROM stdin;
\.


--
-- Data for Name: taxi_company; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.taxi_company (id, name, commission) FROM stdin;
1	Bolt	25
2	Uber	25
3	Uklon	25
\.


--
-- Data for Name: taxi_revenue; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.taxi_revenue (id, created_at, updated_at, date, contract_id, customer_id, taxi_id) FROM stdin;
80	2022-03-20 22:15:57.958693+00	2022-03-20 22:15:57.958736+00	2022-03-18	57	1	10
81	2022-03-29 12:52:11.02035+00	2022-03-29 12:52:11.020387+00	2022-03-29	57	1	10
82	2022-03-29 12:53:06.273802+00	2022-03-29 12:53:06.273831+00	2022-03-29	57	1	10
\.


--
-- Data for Name: taxi_revenueitem; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.taxi_revenueitem (id, amount_cash, amount_card, order_count, driver_paid, company_id, revenue_id, debt_all, debt_today, taxi_id, driver_must_pay, we_must_pay, we_paid) FROM stdin;
216	1000	1000	100	0	1	80	200	200	10	0	200	0
217	0	0	0	0	2	80	0	0	10	0	0	0
218	1000	1000	25	0	3	80	200	200	10	0	200	0
219	1000	500	0	0	1	81	-50	-250	10	250	0	0
220	0	0	0	0	2	81	0	0	10	0	0	0
221	0	0	0	0	3	81	200	0	10	0	0	0
222	2000	1000	0	0	1	82	300	100	10	0	100	0
223	0	0	0	0	2	82	0	0	10	0	0	0
224	0	0	0	0	3	82	200	0	10	0	0	0
\.


--
-- Data for Name: taxi_tariff; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.taxi_tariff (id, limit1, limit2, limit3, limit1_driver_percent, limit2_driver_percent, limit3_driver_percent, company_id) FROM stdin;
1	0	2000	2500	50	60	70	1
2	0	2000	2500	50	60	70	2
3	0	2000	2500	50	60	70	3
\.


--
-- Data for Name: taxi_taxi; Type: TABLE DATA; Schema: public; Owner: alvinseyidov
--

COPY public.taxi_taxi (id, created_at, updated_at, is_active, car_id) FROM stdin;
10	2022-02-23 13:36:29.116239+00	2022-03-20 22:15:09.388779+00	t	10
\.


--
-- Name: account_contract_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_contract_id_seq', 58, true);


--
-- Name: account_customuser_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_customuser_groups_id_seq', 1, false);


--
-- Name: account_customuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_customuser_id_seq', 42, true);


--
-- Name: account_customuser_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_customuser_user_permissions_id_seq', 1, false);


--
-- Name: account_licenseimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_licenseimage_id_seq', 1, false);


--
-- Name: account_passportimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_passportimage_id_seq', 1, false);


--
-- Name: account_score_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_score_id_seq', 46, true);


--
-- Name: account_userimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_userimage_id_seq', 4, true);


--
-- Name: account_userphone_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.account_userphone_id_seq', 36, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 248, true);


--
-- Name: car_brand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_brand_id_seq', 9, true);


--
-- Name: car_car_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_car_id_seq', 142, true);


--
-- Name: car_carclass_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carclass_id_seq', 1, true);


--
-- Name: car_carimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carimage_id_seq', 137, true);


--
-- Name: car_carimagetask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carimagetask_id_seq', 1, false);


--
-- Name: car_carimagetask_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carimagetask_images_id_seq', 1, false);


--
-- Name: car_carpaper_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_carpaper_id_seq', 1, true);


--
-- Name: car_case_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_case_id_seq', 1, true);


--
-- Name: car_color_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_color_id_seq', 1, true);


--
-- Name: car_fuel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_fuel_id_seq', 1, true);


--
-- Name: car_model_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_model_id_seq', 22, true);


--
-- Name: car_office_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.car_office_id_seq', 1, true);


--
-- Name: core_administrativedeposit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.core_administrativedeposit_id_seq', 1, false);


--
-- Name: core_administrativewithdraw_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.core_administrativewithdraw_id_seq', 1, false);


--
-- Name: core_balance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.core_balance_id_seq', 1, false);


--
-- Name: core_currency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.core_currency_id_seq', 1, true);


--
-- Name: credit_credit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.credit_credit_id_seq', 48, true);


--
-- Name: credit_creditimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.credit_creditimage_id_seq', 48, true);


--
-- Name: credit_creditpayment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.credit_creditpayment_id_seq', 67, true);


--
-- Name: debt_debt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.debt_debt_id_seq', 8, true);


--
-- Name: debt_debtimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.debt_debtimage_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1399, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 62, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 104, true);


--
-- Name: dtp_dtp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.dtp_dtp_id_seq', 1, true);


--
-- Name: insurance_casco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.insurance_casco_id_seq', 3, true);


--
-- Name: insurance_cascoimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.insurance_cascoimage_id_seq', 3, true);


--
-- Name: insurance_cascopayment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.insurance_cascopayment_id_seq', 4, true);


--
-- Name: insurance_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.insurance_company_id_seq', 1, true);


--
-- Name: insurance_osago_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.insurance_osago_id_seq', 4, true);


--
-- Name: insurance_osagoimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.insurance_osagoimage_id_seq', 4, true);


--
-- Name: insurance_osagopayment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.insurance_osagopayment_id_seq', 4, true);


--
-- Name: rent_rent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_rent_id_seq', 1, true);


--
-- Name: rent_revenue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_revenue_id_seq', 1, true);


--
-- Name: rent_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_settings_id_seq', 1, false);


--
-- Name: rent_taxi_daily_renttaxidaily_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_daily_renttaxidaily_id_seq', 1, true);


--
-- Name: rent_taxi_daily_revenue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_daily_revenue_id_seq', 1, true);


--
-- Name: rent_taxi_daily_revenueitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_daily_revenueitem_id_seq', 1, true);


--
-- Name: rent_taxi_daily_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_daily_settings_id_seq', 1, true);


--
-- Name: rent_taxi_renttaxi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_renttaxi_id_seq', 1, true);


--
-- Name: rent_taxi_revenue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_revenue_id_seq', 1, true);


--
-- Name: rent_taxi_revenueitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_revenueitem_id_seq', 4, true);


--
-- Name: rent_taxi_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_settings_id_seq', 1, true);


--
-- Name: rent_taxi_tariff_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.rent_taxi_tariff_id_seq', 3, true);


--
-- Name: shtraf_shtraf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.shtraf_shtraf_id_seq', 3, true);


--
-- Name: shtraf_shtrafimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.shtraf_shtrafimage_id_seq', 1, false);


--
-- Name: taxi_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.taxi_company_id_seq', 3, true);


--
-- Name: taxi_revenue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.taxi_revenue_id_seq', 82, true);


--
-- Name: taxi_revenueitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.taxi_revenueitem_id_seq', 224, true);


--
-- Name: taxi_tariff_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.taxi_tariff_id_seq', 3, true);


--
-- Name: taxi_taxi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alvinseyidov
--

SELECT pg_catalog.setval('public.taxi_taxi_id_seq', 10, true);


--
-- Name: account_contract account_contract_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_contract
    ADD CONSTRAINT account_contract_pkey PRIMARY KEY (id);


--
-- Name: account_customuser_groups account_customuser_groups_customuser_id_group_id_7e51db7b_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups
    ADD CONSTRAINT account_customuser_groups_customuser_id_group_id_7e51db7b_uniq UNIQUE (customuser_id, group_id);


--
-- Name: account_customuser_groups account_customuser_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups
    ADD CONSTRAINT account_customuser_groups_pkey PRIMARY KEY (id);


--
-- Name: account_customuser account_customuser_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser
    ADD CONSTRAINT account_customuser_pkey PRIMARY KEY (id);


--
-- Name: account_customuser_user_permissions account_customuser_user__customuser_id_permission_650e378f_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions
    ADD CONSTRAINT account_customuser_user__customuser_id_permission_650e378f_uniq UNIQUE (customuser_id, permission_id);


--
-- Name: account_customuser_user_permissions account_customuser_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions
    ADD CONSTRAINT account_customuser_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: account_customuser account_customuser_username_key; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser
    ADD CONSTRAINT account_customuser_username_key UNIQUE (username);


--
-- Name: account_licenseimage account_licenseimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_licenseimage
    ADD CONSTRAINT account_licenseimage_pkey PRIMARY KEY (id);


--
-- Name: account_passportimage account_passportimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_passportimage
    ADD CONSTRAINT account_passportimage_pkey PRIMARY KEY (id);


--
-- Name: account_score account_score_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_score
    ADD CONSTRAINT account_score_pkey PRIMARY KEY (id);


--
-- Name: account_userimage account_userimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userimage
    ADD CONSTRAINT account_userimage_pkey PRIMARY KEY (id);


--
-- Name: account_userphone account_userphone_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userphone
    ADD CONSTRAINT account_userphone_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: car_brand car_brand_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_brand
    ADD CONSTRAINT car_brand_pkey PRIMARY KEY (id);


--
-- Name: car_car car_car_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_pkey PRIMARY KEY (id);


--
-- Name: car_carclass car_carclass_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carclass
    ADD CONSTRAINT car_carclass_pkey PRIMARY KEY (id);


--
-- Name: car_carimage car_carimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimage
    ADD CONSTRAINT car_carimage_pkey PRIMARY KEY (id);


--
-- Name: car_carimagetask_images car_carimagetask_images_carimagetask_id_carimage_79281200_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images
    ADD CONSTRAINT car_carimagetask_images_carimagetask_id_carimage_79281200_uniq UNIQUE (carimagetask_id, carimage_id);


--
-- Name: car_carimagetask_images car_carimagetask_images_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images
    ADD CONSTRAINT car_carimagetask_images_pkey PRIMARY KEY (id);


--
-- Name: car_carimagetask car_carimagetask_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask
    ADD CONSTRAINT car_carimagetask_pkey PRIMARY KEY (id);


--
-- Name: car_carpaper car_carpaper_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carpaper
    ADD CONSTRAINT car_carpaper_pkey PRIMARY KEY (id);


--
-- Name: car_case car_case_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_case
    ADD CONSTRAINT car_case_pkey PRIMARY KEY (id);


--
-- Name: car_color car_color_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_color
    ADD CONSTRAINT car_color_pkey PRIMARY KEY (id);


--
-- Name: car_fuel car_fuel_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_fuel
    ADD CONSTRAINT car_fuel_pkey PRIMARY KEY (id);


--
-- Name: car_model car_model_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_model
    ADD CONSTRAINT car_model_pkey PRIMARY KEY (id);


--
-- Name: car_office car_office_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_office
    ADD CONSTRAINT car_office_pkey PRIMARY KEY (id);


--
-- Name: core_administrativedeposit core_administrativedeposit_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_administrativedeposit
    ADD CONSTRAINT core_administrativedeposit_pkey PRIMARY KEY (id);


--
-- Name: core_administrativewithdraw core_administrativewithdraw_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_administrativewithdraw
    ADD CONSTRAINT core_administrativewithdraw_pkey PRIMARY KEY (id);


--
-- Name: core_balance core_balance_currency_type_5be3e167_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_balance
    ADD CONSTRAINT core_balance_currency_type_5be3e167_uniq UNIQUE (currency, type);


--
-- Name: core_balance core_balance_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_balance
    ADD CONSTRAINT core_balance_pkey PRIMARY KEY (id);


--
-- Name: core_currency core_currency_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.core_currency
    ADD CONSTRAINT core_currency_pkey PRIMARY KEY (id);


--
-- Name: credit_credit credit_credit_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_credit
    ADD CONSTRAINT credit_credit_pkey PRIMARY KEY (id);


--
-- Name: credit_creditimage credit_creditimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditimage
    ADD CONSTRAINT credit_creditimage_pkey PRIMARY KEY (id);


--
-- Name: credit_creditpayment credit_creditpayment_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditpayment
    ADD CONSTRAINT credit_creditpayment_pkey PRIMARY KEY (id);


--
-- Name: debt_debt debt_debt_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debt
    ADD CONSTRAINT debt_debt_pkey PRIMARY KEY (id);


--
-- Name: debt_debtimage debt_debtimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debtimage
    ADD CONSTRAINT debt_debtimage_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: dtp_dtp dtp_dtp_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.dtp_dtp
    ADD CONSTRAINT dtp_dtp_pkey PRIMARY KEY (id);


--
-- Name: insurance_casco insurance_casco_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_casco
    ADD CONSTRAINT insurance_casco_pkey PRIMARY KEY (id);


--
-- Name: insurance_cascoimage insurance_cascoimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_cascoimage
    ADD CONSTRAINT insurance_cascoimage_pkey PRIMARY KEY (id);


--
-- Name: insurance_cascopayment insurance_cascopayment_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_cascopayment
    ADD CONSTRAINT insurance_cascopayment_pkey PRIMARY KEY (id);


--
-- Name: insurance_company insurance_company_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_company
    ADD CONSTRAINT insurance_company_pkey PRIMARY KEY (id);


--
-- Name: insurance_osago insurance_osago_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osago
    ADD CONSTRAINT insurance_osago_pkey PRIMARY KEY (id);


--
-- Name: insurance_osagoimage insurance_osagoimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osagoimage
    ADD CONSTRAINT insurance_osagoimage_pkey PRIMARY KEY (id);


--
-- Name: insurance_osagopayment insurance_osagopayment_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osagopayment
    ADD CONSTRAINT insurance_osagopayment_pkey PRIMARY KEY (id);


--
-- Name: rent_rent rent_rent_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_rent
    ADD CONSTRAINT rent_rent_pkey PRIMARY KEY (id);


--
-- Name: rent_revenue rent_revenue_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_revenue
    ADD CONSTRAINT rent_revenue_pkey PRIMARY KEY (id);


--
-- Name: rent_settings rent_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_settings
    ADD CONSTRAINT rent_settings_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_daily_renttaxidaily rent_taxi_daily_renttaxidaily_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_renttaxidaily
    ADD CONSTRAINT rent_taxi_daily_renttaxidaily_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_daily_revenue rent_taxi_daily_revenue_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenue
    ADD CONSTRAINT rent_taxi_daily_revenue_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_daily_revenueitem rent_taxi_daily_revenueitem_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenueitem
    ADD CONSTRAINT rent_taxi_daily_revenueitem_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_daily_settings rent_taxi_daily_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_settings
    ADD CONSTRAINT rent_taxi_daily_settings_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_renttaxi rent_taxi_renttaxi_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_renttaxi
    ADD CONSTRAINT rent_taxi_renttaxi_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_revenue rent_taxi_revenue_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue
    ADD CONSTRAINT rent_taxi_revenue_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_revenueitem rent_taxi_revenueitem_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem
    ADD CONSTRAINT rent_taxi_revenueitem_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_settings rent_taxi_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_settings
    ADD CONSTRAINT rent_taxi_settings_pkey PRIMARY KEY (id);


--
-- Name: rent_taxi_tariff rent_taxi_tariff_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_tariff
    ADD CONSTRAINT rent_taxi_tariff_pkey PRIMARY KEY (id);


--
-- Name: shtraf_shtraf shtraf_shtraf_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtraf
    ADD CONSTRAINT shtraf_shtraf_pkey PRIMARY KEY (id);


--
-- Name: shtraf_shtrafimage shtraf_shtrafimage_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtrafimage
    ADD CONSTRAINT shtraf_shtrafimage_pkey PRIMARY KEY (id);


--
-- Name: taxi_company taxi_company_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_company
    ADD CONSTRAINT taxi_company_pkey PRIMARY KEY (id);


--
-- Name: taxi_revenue taxi_revenue_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue
    ADD CONSTRAINT taxi_revenue_pkey PRIMARY KEY (id);


--
-- Name: taxi_revenueitem taxi_revenueitem_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem
    ADD CONSTRAINT taxi_revenueitem_pkey PRIMARY KEY (id);


--
-- Name: taxi_tariff taxi_tariff_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_tariff
    ADD CONSTRAINT taxi_tariff_pkey PRIMARY KEY (id);


--
-- Name: taxi_taxi taxi_taxi_pkey; Type: CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_taxi
    ADD CONSTRAINT taxi_taxi_pkey PRIMARY KEY (id);


--
-- Name: account_contract_car_id_5a2aec70; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_contract_car_id_5a2aec70 ON public.account_contract USING btree (car_id);


--
-- Name: account_contract_customer_id_034347f8; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_contract_customer_id_034347f8 ON public.account_contract USING btree (customer_id);


--
-- Name: account_customuser_groups_customuser_id_b6c60904; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_groups_customuser_id_b6c60904 ON public.account_customuser_groups USING btree (customuser_id);


--
-- Name: account_customuser_groups_group_id_2be9f6d7; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_groups_group_id_2be9f6d7 ON public.account_customuser_groups USING btree (group_id);


--
-- Name: account_customuser_user_permissions_customuser_id_03bcc114; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_user_permissions_customuser_id_03bcc114 ON public.account_customuser_user_permissions USING btree (customuser_id);


--
-- Name: account_customuser_user_permissions_permission_id_f4aec423; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_user_permissions_permission_id_f4aec423 ON public.account_customuser_user_permissions USING btree (permission_id);


--
-- Name: account_customuser_username_724ae020_like; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_customuser_username_724ae020_like ON public.account_customuser USING btree (username varchar_pattern_ops);


--
-- Name: account_licenseimage_user_id_2fa01b01; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_licenseimage_user_id_2fa01b01 ON public.account_licenseimage USING btree (user_id);


--
-- Name: account_passportimage_user_id_43c1cf55; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_passportimage_user_id_43c1cf55 ON public.account_passportimage USING btree (user_id);


--
-- Name: account_score_customer_id_e16f9226; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_score_customer_id_e16f9226 ON public.account_score USING btree (customer_id);


--
-- Name: account_userimage_user_id_60023a82; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_userimage_user_id_60023a82 ON public.account_userimage USING btree (user_id);


--
-- Name: account_userphone_user_id_bf883813; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX account_userphone_user_id_bf883813 ON public.account_userphone USING btree (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: car_car_advertise_id_a27f1939; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_advertise_id_a27f1939 ON public.car_car USING btree (advertise_id);


--
-- Name: car_car_car_class_id_b7eb688e; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_car_class_id_b7eb688e ON public.car_car USING btree (car_class_id);


--
-- Name: car_car_case_id_b68dd611; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_case_id_b68dd611 ON public.car_car USING btree (case_id);


--
-- Name: car_car_color_id_f863d3f6; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_color_id_f863d3f6 ON public.car_car USING btree (color_id);


--
-- Name: car_car_driver_id_e2236767; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_driver_id_e2236767 ON public.car_car USING btree (driver_id);


--
-- Name: car_car_fuel_id_84981bd3; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_fuel_id_84981bd3 ON public.car_car USING btree (fuel_id);


--
-- Name: car_car_model_id_c6a3329e; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_model_id_c6a3329e ON public.car_car USING btree (model_id);


--
-- Name: car_car_office_id_93687ce9; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_car_office_id_93687ce9 ON public.car_car USING btree (office_id);


--
-- Name: car_carimage_car_id_da6e853f; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimage_car_id_da6e853f ON public.car_carimage USING btree (car_id);


--
-- Name: car_carimagetask_car_id_80477bac; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimagetask_car_id_80477bac ON public.car_carimagetask USING btree (car_id);


--
-- Name: car_carimagetask_images_carimage_id_b6bab1b6; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimagetask_images_carimage_id_b6bab1b6 ON public.car_carimagetask_images USING btree (carimage_id);


--
-- Name: car_carimagetask_images_carimagetask_id_ea5311ff; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimagetask_images_carimagetask_id_ea5311ff ON public.car_carimagetask_images USING btree (carimagetask_id);


--
-- Name: car_carimagetask_user_id_c86f4ff7; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carimagetask_user_id_c86f4ff7 ON public.car_carimagetask USING btree (user_id);


--
-- Name: car_carpaper_car_id_1e838de9; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_carpaper_car_id_1e838de9 ON public.car_carpaper USING btree (car_id);


--
-- Name: car_model_brand_id_a19b3370; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX car_model_brand_id_a19b3370 ON public.car_model USING btree (brand_id);


--
-- Name: credit_credit_contract_id_36edfa16; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX credit_credit_contract_id_36edfa16 ON public.credit_credit USING btree (contract_id);


--
-- Name: credit_creditimage_credit_id_6eb7d057; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX credit_creditimage_credit_id_6eb7d057 ON public.credit_creditimage USING btree (credit_id);


--
-- Name: credit_creditpayment_credit_id_0481595c; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX credit_creditpayment_credit_id_0481595c ON public.credit_creditpayment USING btree (credit_id);


--
-- Name: debt_debt_car_id_fe8a8c5b; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX debt_debt_car_id_fe8a8c5b ON public.debt_debt USING btree (car_id);


--
-- Name: debt_debt_driver_id_3af7225e; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX debt_debt_driver_id_3af7225e ON public.debt_debt USING btree (customer_id);


--
-- Name: debt_debtimage_debt_id_0e8cca69; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX debt_debtimage_debt_id_0e8cca69 ON public.debt_debtimage USING btree (debt_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: dtp_dtp_car_id_d7422ff7; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX dtp_dtp_car_id_d7422ff7 ON public.dtp_dtp USING btree (car_id);


--
-- Name: dtp_dtp_driver_id_8c5d7a5d; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX dtp_dtp_driver_id_8c5d7a5d ON public.dtp_dtp USING btree (driver_id);


--
-- Name: dtp_dtp_insurance_company_id_b9865705; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX dtp_dtp_insurance_company_id_b9865705 ON public.dtp_dtp USING btree (insurance_company_id);


--
-- Name: insurance_casco_car_id_7c95810a; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX insurance_casco_car_id_7c95810a ON public.insurance_casco USING btree (car_id);


--
-- Name: insurance_casco_company_id_0ca7a876; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX insurance_casco_company_id_0ca7a876 ON public.insurance_casco USING btree (company_id);


--
-- Name: insurance_cascoimage_casco_id_996ed88d; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX insurance_cascoimage_casco_id_996ed88d ON public.insurance_cascoimage USING btree (casco_id);


--
-- Name: insurance_cascopayment_casco_id_e8b54858; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX insurance_cascopayment_casco_id_e8b54858 ON public.insurance_cascopayment USING btree (casco_id);


--
-- Name: insurance_osago_car_id_2e09d756; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX insurance_osago_car_id_2e09d756 ON public.insurance_osago USING btree (car_id);


--
-- Name: insurance_osago_company_id_826578e3; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX insurance_osago_company_id_826578e3 ON public.insurance_osago USING btree (company_id);


--
-- Name: insurance_osagoimage_osago_id_87ca2394; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX insurance_osagoimage_osago_id_87ca2394 ON public.insurance_osagoimage USING btree (osago_id);


--
-- Name: insurance_osagopayment_osago_id_dd59941f; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX insurance_osagopayment_osago_id_dd59941f ON public.insurance_osagopayment USING btree (osago_id);


--
-- Name: rent_rent_car_id_6f294939; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_rent_car_id_6f294939 ON public.rent_rent USING btree (car_id);


--
-- Name: rent_revenue_contract_id_228bca99; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_revenue_contract_id_228bca99 ON public.rent_revenue USING btree (contract_id);


--
-- Name: rent_revenue_driver_id_4e058bb3; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_revenue_driver_id_4e058bb3 ON public.rent_revenue USING btree (driver_id);


--
-- Name: rent_revenue_rent_id_9f4226d7; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_revenue_rent_id_9f4226d7 ON public.rent_revenue USING btree (rent_id);


--
-- Name: rent_taxi_daily_renttaxidaily_car_id_7c1f9ff8; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_daily_renttaxidaily_car_id_7c1f9ff8 ON public.rent_taxi_daily_renttaxidaily USING btree (car_id);


--
-- Name: rent_taxi_daily_revenue_contract_id_7801d46e; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_daily_revenue_contract_id_7801d46e ON public.rent_taxi_daily_revenue USING btree (contract_id);


--
-- Name: rent_taxi_daily_revenue_driver_id_71a10f1d; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_daily_revenue_driver_id_71a10f1d ON public.rent_taxi_daily_revenue USING btree (driver_id);


--
-- Name: rent_taxi_daily_revenue_rent_taxi_daily_id_afceffb9; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_daily_revenue_rent_taxi_daily_id_afceffb9 ON public.rent_taxi_daily_revenue USING btree (rent_taxi_daily_id);


--
-- Name: rent_taxi_daily_revenueitem_company_id_1213da59; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_daily_revenueitem_company_id_1213da59 ON public.rent_taxi_daily_revenueitem USING btree (company_id);


--
-- Name: rent_taxi_daily_revenueitem_rent_taxi_id_fcca4773; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_daily_revenueitem_rent_taxi_id_fcca4773 ON public.rent_taxi_daily_revenueitem USING btree (rent_taxi_daily_id);


--
-- Name: rent_taxi_daily_revenueitem_revenue_id_bb99823d; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_daily_revenueitem_revenue_id_bb99823d ON public.rent_taxi_daily_revenueitem USING btree (revenue_id);


--
-- Name: rent_taxi_renttaxi_car_id_c9a98fae; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_renttaxi_car_id_c9a98fae ON public.rent_taxi_renttaxi USING btree (car_id);


--
-- Name: rent_taxi_revenue_contract_id_ac7af62c; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenue_contract_id_ac7af62c ON public.rent_taxi_revenue USING btree (contract_id);


--
-- Name: rent_taxi_revenue_driver_id_975ee1ce; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenue_driver_id_975ee1ce ON public.rent_taxi_revenue USING btree (driver_id);


--
-- Name: rent_taxi_revenue_renttaxi_id_7f9b0691; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenue_renttaxi_id_7f9b0691 ON public.rent_taxi_revenue USING btree (rent_taxi_id);


--
-- Name: rent_taxi_revenueitem_company_id_c6b8085b; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenueitem_company_id_c6b8085b ON public.rent_taxi_revenueitem USING btree (company_id);


--
-- Name: rent_taxi_revenueitem_rent_taxi_id_dd2412d5; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenueitem_rent_taxi_id_dd2412d5 ON public.rent_taxi_revenueitem USING btree (rent_taxi_id);


--
-- Name: rent_taxi_revenueitem_revenue_id_b8126f01; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_revenueitem_revenue_id_b8126f01 ON public.rent_taxi_revenueitem USING btree (revenue_id);


--
-- Name: rent_taxi_tariff_company_id_88407c69; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX rent_taxi_tariff_company_id_88407c69 ON public.rent_taxi_tariff USING btree (company_id);


--
-- Name: shtraf_shtraf_car_id_fd95b8fd; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX shtraf_shtraf_car_id_fd95b8fd ON public.shtraf_shtraf USING btree (car_id);


--
-- Name: shtraf_shtraf_driver_id_8a231c53; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX shtraf_shtraf_driver_id_8a231c53 ON public.shtraf_shtraf USING btree (driver_id);


--
-- Name: shtraf_shtrafimage_shtraf_id_4de9eba2; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX shtraf_shtrafimage_shtraf_id_4de9eba2 ON public.shtraf_shtrafimage USING btree (shtraf_id);


--
-- Name: taxi_revenue_contract_id_6335489b; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenue_contract_id_6335489b ON public.taxi_revenue USING btree (contract_id);


--
-- Name: taxi_revenue_driver_id_8d1abf69; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenue_driver_id_8d1abf69 ON public.taxi_revenue USING btree (customer_id);


--
-- Name: taxi_revenue_taxi_id_b848c7a1; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenue_taxi_id_b848c7a1 ON public.taxi_revenue USING btree (taxi_id);


--
-- Name: taxi_revenueitem_company_id_185a75e6; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenueitem_company_id_185a75e6 ON public.taxi_revenueitem USING btree (company_id);


--
-- Name: taxi_revenueitem_revenue_id_cb85c3d8; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenueitem_revenue_id_cb85c3d8 ON public.taxi_revenueitem USING btree (revenue_id);


--
-- Name: taxi_revenueitem_taxi_id_5bff8b44; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_revenueitem_taxi_id_5bff8b44 ON public.taxi_revenueitem USING btree (taxi_id);


--
-- Name: taxi_tariff_company_id_8ad3aa96; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_tariff_company_id_8ad3aa96 ON public.taxi_tariff USING btree (company_id);


--
-- Name: taxi_taxi_car_id_3cbef2c9; Type: INDEX; Schema: public; Owner: alvinseyidov
--

CREATE INDEX taxi_taxi_car_id_3cbef2c9 ON public.taxi_taxi USING btree (car_id);


--
-- Name: account_contract account_contract_car_id_5a2aec70_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_contract
    ADD CONSTRAINT account_contract_car_id_5a2aec70_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_contract account_contract_customer_id_034347f8_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_contract
    ADD CONSTRAINT account_contract_customer_id_034347f8_fk_account_customuser_id FOREIGN KEY (customer_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_customuser_groups account_customuser_g_customuser_id_b6c60904_fk_account_c; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups
    ADD CONSTRAINT account_customuser_g_customuser_id_b6c60904_fk_account_c FOREIGN KEY (customuser_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_customuser_groups account_customuser_groups_group_id_2be9f6d7_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_groups
    ADD CONSTRAINT account_customuser_groups_group_id_2be9f6d7_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_customuser_user_permissions account_customuser_u_customuser_id_03bcc114_fk_account_c; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions
    ADD CONSTRAINT account_customuser_u_customuser_id_03bcc114_fk_account_c FOREIGN KEY (customuser_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_customuser_user_permissions account_customuser_u_permission_id_f4aec423_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_customuser_user_permissions
    ADD CONSTRAINT account_customuser_u_permission_id_f4aec423_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_licenseimage account_licenseimage_user_id_2fa01b01_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_licenseimage
    ADD CONSTRAINT account_licenseimage_user_id_2fa01b01_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_passportimage account_passportimage_user_id_43c1cf55_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_passportimage
    ADD CONSTRAINT account_passportimage_user_id_43c1cf55_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_score account_score_customer_id_e16f9226_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_score
    ADD CONSTRAINT account_score_customer_id_e16f9226_fk_account_customuser_id FOREIGN KEY (customer_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_userimage account_userimage_user_id_60023a82_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userimage
    ADD CONSTRAINT account_userimage_user_id_60023a82_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_userphone account_userphone_user_id_bf883813_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.account_userphone
    ADD CONSTRAINT account_userphone_user_id_bf883813_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_advertise_id_a27f1939_fk_taxi_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_advertise_id_a27f1939_fk_taxi_company_id FOREIGN KEY (advertise_id) REFERENCES public.taxi_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_car_class_id_b7eb688e_fk_car_carclass_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_car_class_id_b7eb688e_fk_car_carclass_id FOREIGN KEY (car_class_id) REFERENCES public.car_carclass(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_case_id_b68dd611_fk_car_case_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_case_id_b68dd611_fk_car_case_id FOREIGN KEY (case_id) REFERENCES public.car_case(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_color_id_f863d3f6_fk_car_color_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_color_id_f863d3f6_fk_car_color_id FOREIGN KEY (color_id) REFERENCES public.car_color(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_driver_id_e2236767_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_driver_id_e2236767_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_fuel_id_84981bd3_fk_car_fuel_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_fuel_id_84981bd3_fk_car_fuel_id FOREIGN KEY (fuel_id) REFERENCES public.car_fuel(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_model_id_c6a3329e_fk_car_model_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_model_id_c6a3329e_fk_car_model_id FOREIGN KEY (model_id) REFERENCES public.car_model(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_car car_car_office_id_93687ce9_fk_car_office_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_car
    ADD CONSTRAINT car_car_office_id_93687ce9_fk_car_office_id FOREIGN KEY (office_id) REFERENCES public.car_office(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimage car_carimage_car_id_da6e853f_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimage
    ADD CONSTRAINT car_carimage_car_id_da6e853f_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimagetask car_carimagetask_car_id_80477bac_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask
    ADD CONSTRAINT car_carimagetask_car_id_80477bac_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimagetask_images car_carimagetask_ima_carimagetask_id_ea5311ff_fk_car_carim; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images
    ADD CONSTRAINT car_carimagetask_ima_carimagetask_id_ea5311ff_fk_car_carim FOREIGN KEY (carimagetask_id) REFERENCES public.car_carimagetask(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimagetask_images car_carimagetask_images_carimage_id_b6bab1b6_fk_car_carimage_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask_images
    ADD CONSTRAINT car_carimagetask_images_carimage_id_b6bab1b6_fk_car_carimage_id FOREIGN KEY (carimage_id) REFERENCES public.car_carimage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carimagetask car_carimagetask_user_id_c86f4ff7_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carimagetask
    ADD CONSTRAINT car_carimagetask_user_id_c86f4ff7_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_carpaper car_carpaper_car_id_1e838de9_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_carpaper
    ADD CONSTRAINT car_carpaper_car_id_1e838de9_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_model car_model_brand_id_a19b3370_fk_car_brand_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.car_model
    ADD CONSTRAINT car_model_brand_id_a19b3370_fk_car_brand_id FOREIGN KEY (brand_id) REFERENCES public.car_brand(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: credit_credit credit_credit_contract_id_36edfa16_fk_account_contract_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_credit
    ADD CONSTRAINT credit_credit_contract_id_36edfa16_fk_account_contract_id FOREIGN KEY (contract_id) REFERENCES public.account_contract(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: credit_creditimage credit_creditimage_credit_id_6eb7d057_fk_credit_credit_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditimage
    ADD CONSTRAINT credit_creditimage_credit_id_6eb7d057_fk_credit_credit_id FOREIGN KEY (credit_id) REFERENCES public.credit_credit(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: credit_creditpayment credit_creditpayment_credit_id_0481595c_fk_credit_credit_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.credit_creditpayment
    ADD CONSTRAINT credit_creditpayment_credit_id_0481595c_fk_credit_credit_id FOREIGN KEY (credit_id) REFERENCES public.credit_credit(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: debt_debt debt_debt_car_id_fe8a8c5b_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debt
    ADD CONSTRAINT debt_debt_car_id_fe8a8c5b_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: debt_debt debt_debt_customer_id_b92b3d26_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debt
    ADD CONSTRAINT debt_debt_customer_id_b92b3d26_fk_account_customuser_id FOREIGN KEY (customer_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: debt_debtimage debt_debtimage_debt_id_0e8cca69_fk_debt_debt_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.debt_debtimage
    ADD CONSTRAINT debt_debtimage_debt_id_0e8cca69_fk_debt_debt_id FOREIGN KEY (debt_id) REFERENCES public.debt_debt(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_account_customuser_id FOREIGN KEY (user_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dtp_dtp dtp_dtp_car_id_d7422ff7_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.dtp_dtp
    ADD CONSTRAINT dtp_dtp_car_id_d7422ff7_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dtp_dtp dtp_dtp_driver_id_8c5d7a5d_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.dtp_dtp
    ADD CONSTRAINT dtp_dtp_driver_id_8c5d7a5d_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dtp_dtp dtp_dtp_insurance_company_id_b9865705_fk_insurance_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.dtp_dtp
    ADD CONSTRAINT dtp_dtp_insurance_company_id_b9865705_fk_insurance_company_id FOREIGN KEY (insurance_company_id) REFERENCES public.insurance_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: insurance_casco insurance_casco_car_id_7c95810a_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_casco
    ADD CONSTRAINT insurance_casco_car_id_7c95810a_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: insurance_casco insurance_casco_company_id_0ca7a876_fk_insurance_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_casco
    ADD CONSTRAINT insurance_casco_company_id_0ca7a876_fk_insurance_company_id FOREIGN KEY (company_id) REFERENCES public.insurance_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: insurance_cascoimage insurance_cascoimage_casco_id_996ed88d_fk_insurance_casco_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_cascoimage
    ADD CONSTRAINT insurance_cascoimage_casco_id_996ed88d_fk_insurance_casco_id FOREIGN KEY (casco_id) REFERENCES public.insurance_casco(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: insurance_cascopayment insurance_cascopayment_casco_id_e8b54858_fk_insurance_casco_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_cascopayment
    ADD CONSTRAINT insurance_cascopayment_casco_id_e8b54858_fk_insurance_casco_id FOREIGN KEY (casco_id) REFERENCES public.insurance_casco(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: insurance_osago insurance_osago_car_id_2e09d756_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osago
    ADD CONSTRAINT insurance_osago_car_id_2e09d756_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: insurance_osago insurance_osago_company_id_826578e3_fk_insurance_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osago
    ADD CONSTRAINT insurance_osago_company_id_826578e3_fk_insurance_company_id FOREIGN KEY (company_id) REFERENCES public.insurance_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: insurance_osagoimage insurance_osagoimage_osago_id_87ca2394_fk_insurance_osago_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osagoimage
    ADD CONSTRAINT insurance_osagoimage_osago_id_87ca2394_fk_insurance_osago_id FOREIGN KEY (osago_id) REFERENCES public.insurance_osago(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: insurance_osagopayment insurance_osagopayment_osago_id_dd59941f_fk_insurance_osago_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.insurance_osagopayment
    ADD CONSTRAINT insurance_osagopayment_osago_id_dd59941f_fk_insurance_osago_id FOREIGN KEY (osago_id) REFERENCES public.insurance_osago(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_rent rent_rent_car_id_6f294939_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_rent
    ADD CONSTRAINT rent_rent_car_id_6f294939_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_revenue rent_revenue_contract_id_228bca99_fk_account_contract_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_revenue
    ADD CONSTRAINT rent_revenue_contract_id_228bca99_fk_account_contract_id FOREIGN KEY (contract_id) REFERENCES public.account_contract(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_revenue rent_revenue_driver_id_4e058bb3_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_revenue
    ADD CONSTRAINT rent_revenue_driver_id_4e058bb3_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_revenue rent_revenue_rent_id_9f4226d7_fk_rent_rent_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_revenue
    ADD CONSTRAINT rent_revenue_rent_id_9f4226d7_fk_rent_rent_id FOREIGN KEY (rent_id) REFERENCES public.rent_rent(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_daily_renttaxidaily rent_taxi_daily_renttaxidaily_car_id_7c1f9ff8_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_renttaxidaily
    ADD CONSTRAINT rent_taxi_daily_renttaxidaily_car_id_7c1f9ff8_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_daily_revenueitem rent_taxi_daily_reve_company_id_1213da59_fk_taxi_comp; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenueitem
    ADD CONSTRAINT rent_taxi_daily_reve_company_id_1213da59_fk_taxi_comp FOREIGN KEY (company_id) REFERENCES public.taxi_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_daily_revenue rent_taxi_daily_reve_contract_id_7801d46e_fk_account_c; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenue
    ADD CONSTRAINT rent_taxi_daily_reve_contract_id_7801d46e_fk_account_c FOREIGN KEY (contract_id) REFERENCES public.account_contract(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_daily_revenue rent_taxi_daily_reve_driver_id_71a10f1d_fk_account_c; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenue
    ADD CONSTRAINT rent_taxi_daily_reve_driver_id_71a10f1d_fk_account_c FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_daily_revenueitem rent_taxi_daily_reve_rent_taxi_daily_id_32d50d60_fk_rent_taxi; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenueitem
    ADD CONSTRAINT rent_taxi_daily_reve_rent_taxi_daily_id_32d50d60_fk_rent_taxi FOREIGN KEY (rent_taxi_daily_id) REFERENCES public.rent_taxi_daily_renttaxidaily(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_daily_revenue rent_taxi_daily_reve_rent_taxi_daily_id_afceffb9_fk_rent_taxi; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenue
    ADD CONSTRAINT rent_taxi_daily_reve_rent_taxi_daily_id_afceffb9_fk_rent_taxi FOREIGN KEY (rent_taxi_daily_id) REFERENCES public.rent_taxi_daily_renttaxidaily(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_daily_revenueitem rent_taxi_daily_reve_revenue_id_bb99823d_fk_rent_taxi; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_daily_revenueitem
    ADD CONSTRAINT rent_taxi_daily_reve_revenue_id_bb99823d_fk_rent_taxi FOREIGN KEY (revenue_id) REFERENCES public.rent_taxi_daily_revenue(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_renttaxi rent_taxi_renttaxi_car_id_c9a98fae_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_renttaxi
    ADD CONSTRAINT rent_taxi_renttaxi_car_id_c9a98fae_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenue rent_taxi_revenue_contract_id_ac7af62c_fk_account_contract_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue
    ADD CONSTRAINT rent_taxi_revenue_contract_id_ac7af62c_fk_account_contract_id FOREIGN KEY (contract_id) REFERENCES public.account_contract(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenue rent_taxi_revenue_driver_id_975ee1ce_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue
    ADD CONSTRAINT rent_taxi_revenue_driver_id_975ee1ce_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenue rent_taxi_revenue_rent_taxi_id_72a122c1_fk_rent_taxi; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenue
    ADD CONSTRAINT rent_taxi_revenue_rent_taxi_id_72a122c1_fk_rent_taxi FOREIGN KEY (rent_taxi_id) REFERENCES public.rent_taxi_renttaxi(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenueitem rent_taxi_revenueite_rent_taxi_id_dd2412d5_fk_rent_taxi; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem
    ADD CONSTRAINT rent_taxi_revenueite_rent_taxi_id_dd2412d5_fk_rent_taxi FOREIGN KEY (rent_taxi_id) REFERENCES public.rent_taxi_renttaxi(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenueitem rent_taxi_revenueite_revenue_id_b8126f01_fk_rent_taxi; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem
    ADD CONSTRAINT rent_taxi_revenueite_revenue_id_b8126f01_fk_rent_taxi FOREIGN KEY (revenue_id) REFERENCES public.rent_taxi_revenue(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_revenueitem rent_taxi_revenueitem_company_id_c6b8085b_fk_taxi_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_revenueitem
    ADD CONSTRAINT rent_taxi_revenueitem_company_id_c6b8085b_fk_taxi_company_id FOREIGN KEY (company_id) REFERENCES public.taxi_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rent_taxi_tariff rent_taxi_tariff_company_id_88407c69_fk_taxi_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.rent_taxi_tariff
    ADD CONSTRAINT rent_taxi_tariff_company_id_88407c69_fk_taxi_company_id FOREIGN KEY (company_id) REFERENCES public.taxi_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shtraf_shtraf shtraf_shtraf_car_id_fd95b8fd_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtraf
    ADD CONSTRAINT shtraf_shtraf_car_id_fd95b8fd_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shtraf_shtraf shtraf_shtraf_driver_id_8a231c53_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtraf
    ADD CONSTRAINT shtraf_shtraf_driver_id_8a231c53_fk_account_customuser_id FOREIGN KEY (driver_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shtraf_shtrafimage shtraf_shtrafimage_shtraf_id_4de9eba2_fk_shtraf_shtraf_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.shtraf_shtrafimage
    ADD CONSTRAINT shtraf_shtrafimage_shtraf_id_4de9eba2_fk_shtraf_shtraf_id FOREIGN KEY (shtraf_id) REFERENCES public.shtraf_shtraf(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenue taxi_revenue_contract_id_6335489b_fk_account_contract_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue
    ADD CONSTRAINT taxi_revenue_contract_id_6335489b_fk_account_contract_id FOREIGN KEY (contract_id) REFERENCES public.account_contract(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenue taxi_revenue_customer_id_f9f4fd9f_fk_account_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue
    ADD CONSTRAINT taxi_revenue_customer_id_f9f4fd9f_fk_account_customuser_id FOREIGN KEY (customer_id) REFERENCES public.account_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenue taxi_revenue_taxi_id_b848c7a1_fk_taxi_taxi_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenue
    ADD CONSTRAINT taxi_revenue_taxi_id_b848c7a1_fk_taxi_taxi_id FOREIGN KEY (taxi_id) REFERENCES public.taxi_taxi(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenueitem taxi_revenueitem_company_id_185a75e6_fk_taxi_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem
    ADD CONSTRAINT taxi_revenueitem_company_id_185a75e6_fk_taxi_company_id FOREIGN KEY (company_id) REFERENCES public.taxi_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenueitem taxi_revenueitem_revenue_id_cb85c3d8_fk_taxi_revenue_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem
    ADD CONSTRAINT taxi_revenueitem_revenue_id_cb85c3d8_fk_taxi_revenue_id FOREIGN KEY (revenue_id) REFERENCES public.taxi_revenue(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_revenueitem taxi_revenueitem_taxi_id_5bff8b44_fk_taxi_taxi_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_revenueitem
    ADD CONSTRAINT taxi_revenueitem_taxi_id_5bff8b44_fk_taxi_taxi_id FOREIGN KEY (taxi_id) REFERENCES public.taxi_taxi(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_tariff taxi_tariff_company_id_8ad3aa96_fk_taxi_company_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_tariff
    ADD CONSTRAINT taxi_tariff_company_id_8ad3aa96_fk_taxi_company_id FOREIGN KEY (company_id) REFERENCES public.taxi_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: taxi_taxi taxi_taxi_car_id_3cbef2c9_fk_car_car_id; Type: FK CONSTRAINT; Schema: public; Owner: alvinseyidov
--

ALTER TABLE ONLY public.taxi_taxi
    ADD CONSTRAINT taxi_taxi_car_id_3cbef2c9_fk_car_car_id FOREIGN KEY (car_id) REFERENCES public.car_car(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

