import imp
from django.db import models
from account.models import CustomMixin
from car.models import Car
from django.contrib.auth import get_user_model

User = get_user_model()

class Debt(CustomMixin):
    STATUS = (
        ('P','Paid'),
        ('R','Refunded'),
        ('N','Not paid'),
    )
    car = models.ForeignKey(Car, blank=True, null=True, on_delete=models.CASCADE, related_name="debts")
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="debts")
    note = models.TextField(null=True, blank=True)
    created_date = models.DateField()
    paid_date = models.DateField(blank=True, null=True)
    
    amount = models.FloatField()
    status = models.CharField(max_length=1, choices=STATUS, default='C')
    is_active = models.BooleanField(default=True)
    
    def __str__(self):
        return self.customer.get_full_name()

class DebtImage(models.Model):
    debt = models.ForeignKey(Debt, on_delete=models.CASCADE, related_name="files")
    file = models.FileField()

     