from django.contrib import admin
from .models import *


class DebtItemInline(admin.TabularInline):
    model = DebtImage
    extra = 1

@admin.register(Debt)
class DebtAdmin(admin.ModelAdmin):
    inlines = [DebtItemInline,]

