from django.contrib.auth import get_user_model
from django.db.models import Count, Q
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from account.api.permissions import CustomDjangoModelPermissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import SAFE_METHODS, IsAuthenticated
from debt.api.serializers import *
from debt.models import *
from credit.filters import CreditFilter, CreditPaymentFilter
from rest_framework.viewsets import ModelViewSet
from datetime import date, timedelta
from django.utils import timezone

User = get_user_model()

class DebtListAPIView(generics.ListAPIView):
    queryset = Debt.objects.all().order_by('-id')
    serializer_class = DebtListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    
class UserListWithDebtAPIView(generics.ListAPIView):
    serializer_class = UserListWithDebtSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        users = User.objects.filter(Q(debts__status='N')|Q(debts__paid_date__isnull=False,debts__paid_date__gte=monday_of_this_week)).distinct()
        return users

class DebtCreateAPIView(generics.CreateAPIView):
    queryset = Debt.objects.all().order_by('-id')
    serializer_class = DebtCreateSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]