from django.db import models
from django.db.models import fields
from django.contrib.auth import get_user_model
from django.db.models import fields, Avg, Sum, Q
from debt.models import *
from account.models import Score
from rest_framework import serializers
from account.api.serializers import CustomerSerializer, UserImageSerializer, PassportImageSerializer, UserPhoneSerializer
from car.api.serializers import BasicCarSerializer
from datetime import date, timedelta
from django.utils import timezone

User = get_user_model()
class DebtImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = DebtImage
        fields = '__all__'
 
class DebtListSerializer(serializers.ModelSerializer):
    files = DebtImageSerializer(many=True)
    customer = CustomerSerializer()
    car = BasicCarSerializer()
    class Meta:
        model = Debt
        fields = ['id','amount','is_active','status','car','customer','files']


class DebtCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Debt
        fields = '__all__'


class UserListWithDebtSerializer(serializers.ModelSerializer):
    images = UserImageSerializer(many=True)
    passports = PassportImageSerializer(many=True)
    phones = UserPhoneSerializer(many=True)
    cars = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    debt = serializers.SerializerMethodField()
    debts = serializers.SerializerMethodField()

    def get_debt(self, obj):
        debt = Debt.objects.filter(customer=obj,status='N').aggregate(Sum('amount'))
        return debt['amount__sum']

    def get_rating(self, obj):
        rating = Score.objects.filter(customer=obj).aggregate(Avg('score'))
        return int(round(rating['score__avg']))

    def get_cars(self, obj):
        cars = obj.cars.filter(is_active=True)
        data = []
        for car in cars:
            data.append({
                'id': car.id,
                'model':car.model.name,
                'number':car.number,
                'year':car.year,
                'status': car.status
            })
        return data

   
    def get_debts(self,obj,*args,**kwargs):
        some_day_last_week = timezone.now().date() - timedelta(days=7)
        monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
        monday_of_this_week = monday_of_last_week + timedelta(days=7)
        data = obj.debts.filter(Q(status='N')|Q(paid_date__isnull=False,paid_date__gte=monday_of_this_week)).distinct()
        map = []
        for d in data:
            j = {}
            j['id'] = d.id
            j['status'] = d.status
            j['amount'] = d.amount
            j['created_date'] = d.created_date
            if d.paid_date:
                j['paid_date'] = d.paid_date
            if d.car:
                j['carid'] = d.car.id
                j['carname'] = d.car.model.name
                j['carnumber'] = d.car.number
                j['caryear'] = d.car.year
            j['note'] = d.note
            map.append(j)
        return map
       
    class Meta:
        model = User
        fields = [
            'id','first_name','last_name','passport','license_number','debts',
            'username',
            'father_name',
            'birth_date',
            'get_full_name',
            'images',
            'passports',
            'phones',
            'cars',
            'rating',
            'debt',
            'note']