from django.urls import path
from debt.api import views as api_views
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers


app_name = 'debt'

urlpatterns = [
    path('list/', api_views.DebtListAPIView.as_view(), name='debt-list'),
    path('list/user/', api_views.UserListWithDebtAPIView.as_view(), name='debt-list-user'),
    path('create/', api_views.DebtCreateAPIView.as_view(), name='debt-create'),

]
